<?php
	/**
	 * TAKA+ v1.0
	 * @package takaplus
	 * @date 12.1.2016
	 * @version 0.0.1
	 * @website taka.com.vn
	 *
	 * THÊM CÁC SHAPE CỦA BẠN BÊN DƯỚI
	 *
	*/

    // Homepage
    $shape -> add( 'public/home', array(
        'head',
        'header',
        'article',
        'footer',
        'foot'
    ) );

    // Autho
    $shape -> add( 'public/autho', array(
        'head',
        'article'
    ) );

	/**
	 * Kết thúc phần custom shape
	 *
	 * CODING OR DOING AN EXCEPTION ...
	*/
?>
