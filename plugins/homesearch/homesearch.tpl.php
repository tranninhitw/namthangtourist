{@style}
<div class="homesearch">
    <div class="search-blocks">
        <div class="block js_homesearch-block" data-popup="popup-1">
            <img src="assets/images/icons/ticket.svg" alt="" />
            <span>Tour Riêng</span>
        </div>
        <div class="block js_homesearch-block" data-popup="popup-2">
            <img src="assets/images/icons/tickets.svg" alt="" />
            <span>Tour Ghép</span>
        </div>
        <div class="block js_homesearch-block" data-popup="popup-3">
            <img src="assets/images/icons/sale.svg" alt="" />
            <span>Tour sốc</span>
        </div>
        <div class="block js_homesearch-block" data-popup="popup-4">
            <img src="assets/images/icons/train.svg" alt="" />
            <span>Tàu lửa</span>
        </div>
        <div class="block js_homesearch-block" data-popup="popup-5">
            <img src="assets/images/icons/bus.svg" alt="" />
            <span>Xe buýt</span>
        </div>
        <div class="block js_homesearch-block" data-popup="popup-6">
            <img src="assets/images/icons/car.svg" alt="" />
            <span>Xe riêng</span>
        </div>
        <div class="block js_homesearch-block" data-popup="popup-7">
            <img src="assets/images/icons/plane.svg" alt="" />
            <span>Máy bay</span>
        </div>
        <div class="block js_homesearch-block" data-popup="popup-8">
            <img src="assets/images/icons/hotel.svg" alt="" />
            <span>Khách sạn</span>
        </div>
        <div class="block js_homesearch-block" data-popup="popup-9">
            <img src="assets/images/icons/combo.svg" alt="" />
            <span>Combo</span>
        </div>
    </div>
    <div class="popup-homesearch-overlay js_popup-homesearch-overlay">
        <button type="button" class="popup-homesearch-close js_popup-homesearch-close"> <i class="fas fa-times"></i> </button>

        <!-- Tour riêng -->
        <div class="popup-homesearch popup-1">
            <h3>Tìm tour riêng giá tốt</h3>
            <form class="">
                <ul class="selectbox">
                    <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Chọn nơi đến" />
                    <div class="selectbox-options js_selectbox-options">
                        <li class="selectbox-option" data-value="1">Sapa</li>
                        <li class="selectbox-option" data-value="2">Hà Khẩu</li>
                        <li class="selectbox-option" data-value="3">Hà Giang</li>
                        <li class="selectbox-option" data-value="4">Ninh Bình</li>
                        <li class="selectbox-option" data-value="5">Hà Nội</li>
                        <li class="selectbox-option" data-value="6">Phú Quốc</li>
                        <li class="selectbox-option" data-value="7">Côn Đảo</li>
                    </div>
                </ul>
                <ul class="selectbox">
                    <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Chọn nơi đi" />
                    <div class="selectbox-options js_selectbox-options">
                        <li class="selectbox-option" data-value="1">Sapa</li>
                        <li class="selectbox-option" data-value="2">Hà Khẩu</li>
                        <li class="selectbox-option" data-value="3">Hà Giang</li>
                        <li class="selectbox-option" data-value="4">Ninh Bình</li>
                        <li class="selectbox-option" data-value="5">Hà Nội</li>
                        <li class="selectbox-option" data-value="6">Phú Quốc</li>
                        <li class="selectbox-option" data-value="7">Côn Đảo</li>
                    </div>
                </ul>
                <input type="text" class="popup-homesearch-textbox js_datetimepicker" value="Chọn ngày" />
                <input type="submit" class="popup-homesearch-button" value="Tìm tour ngay" />
            </form>
        </div>

        <!-- Tour ghép -->
        <div class="popup-homesearch popup-2">
            <h3>Tìm tour ghép giá tốt</h3>
            <form class="">
                <ul class="selectbox">
                    <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Chọn nơi đến" />
                    <div class="selectbox-options js_selectbox-options">
                        <li class="selectbox-option" data-value="1">Sapa</li>
                        <li class="selectbox-option" data-value="2">Hà Khẩu</li>
                        <li class="selectbox-option" data-value="3">Hà Giang</li>
                        <li class="selectbox-option" data-value="4">Ninh Bình</li>
                        <li class="selectbox-option" data-value="5">Hà Nội</li>
                        <li class="selectbox-option" data-value="6">Phú Quốc</li>
                        <li class="selectbox-option" data-value="7">Côn Đảo</li>
                    </div>
                </ul>
                <ul class="selectbox">
                    <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Chọn nơi đi" />
                    <div class="selectbox-options js_selectbox-options">
                        <li class="selectbox-option" data-value="1">Sapa</li>
                        <li class="selectbox-option" data-value="2">Hà Khẩu</li>
                        <li class="selectbox-option" data-value="3">Hà Giang</li>
                        <li class="selectbox-option" data-value="4">Ninh Bình</li>
                        <li class="selectbox-option" data-value="5">Hà Nội</li>
                        <li class="selectbox-option" data-value="6">Phú Quốc</li>
                        <li class="selectbox-option" data-value="7">Côn Đảo</li>
                    </div>
                </ul>
                <input type="text" class="popup-homesearch-textbox js_datetimepicker" value="Chọn ngày" />
                <input type="submit" class="popup-homesearch-button" value="Tìm tour ngay" />
            </form>
        </div>

        <!-- Tour sốc -->
        <div class="popup-homesearch popup-3">
            <h3>Tour siêu rẻ cực sốc</h3>
            <form class="">
                <ul class="selectbox">
                    <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Chọn nơi đến" />
                    <div class="selectbox-options js_selectbox-options">
                        <li class="selectbox-option" data-value="1">Sapa</li>
                        <li class="selectbox-option" data-value="2">Hà Khẩu</li>
                        <li class="selectbox-option" data-value="3">Hà Giang</li>
                        <li class="selectbox-option" data-value="4">Ninh Bình</li>
                        <li class="selectbox-option" data-value="5">Hà Nội</li>
                        <li class="selectbox-option" data-value="6">Phú Quốc</li>
                        <li class="selectbox-option" data-value="7">Côn Đảo</li>
                    </div>
                </ul>
                <ul class="selectbox">
                    <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Chọn nơi đi" />
                    <div class="selectbox-options js_selectbox-options">
                        <li class="selectbox-option" data-value="1">Sapa</li>
                        <li class="selectbox-option" data-value="2">Hà Khẩu</li>
                        <li class="selectbox-option" data-value="3">Hà Giang</li>
                        <li class="selectbox-option" data-value="4">Ninh Bình</li>
                        <li class="selectbox-option" data-value="5">Hà Nội</li>
                        <li class="selectbox-option" data-value="6">Phú Quốc</li>
                        <li class="selectbox-option" data-value="7">Côn Đảo</li>
                    </div>
                </ul>
                <input type="text" class="popup-homesearch-textbox js_datetimepicker" value="Chọn ngày" />
                <input type="submit" class="popup-homesearch-button" value="Xem ngay" />
            </form>
        </div>

        <!-- Tàu Lửa -->
        <div class="popup-homesearch popup-4">
            <h3>Đặt vé tàu lửa</h3>
            <form class="">
                <ul class="selectbox">
                    <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Chọn nơi đến" />
                    <div class="selectbox-options js_selectbox-options">
                        <li class="selectbox-option" data-value="1">Sapa</li>
                        <li class="selectbox-option" data-value="2">Hà Khẩu</li>
                        <li class="selectbox-option" data-value="3">Hà Giang</li>
                        <li class="selectbox-option" data-value="4">Ninh Bình</li>
                        <li class="selectbox-option" data-value="5">Hà Nội</li>
                        <li class="selectbox-option" data-value="6">Phú Quốc</li>
                        <li class="selectbox-option" data-value="7">Côn Đảo</li>
                    </div>
                </ul>
                <ul class="selectbox">
                    <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Chọn nơi đi" />
                    <div class="selectbox-options js_selectbox-options">
                        <li class="selectbox-option" data-value="1">Sapa</li>
                        <li class="selectbox-option" data-value="2">Hà Khẩu</li>
                        <li class="selectbox-option" data-value="3">Hà Giang</li>
                        <li class="selectbox-option" data-value="4">Ninh Bình</li>
                        <li class="selectbox-option" data-value="5">Hà Nội</li>
                        <li class="selectbox-option" data-value="6">Phú Quốc</li>
                        <li class="selectbox-option" data-value="7">Côn Đảo</li>
                    </div>
                </ul>
                <input type="text" class="popup-homesearch-textbox js_datetimepicker" value="Chọn ngày" />
                <input type="submit" class="popup-homesearch-button" value="Xem chuyến tàu" />
            </form>
        </div>

        <!-- Xe Buýt -->
        <div class="popup-homesearch popup-5">
            <h3>Xe buýt đi chung</h3>
            <form class="">
                <ul class="selectbox">
                    <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Chọn tỉnh thành" />
                    <div class="selectbox-options js_selectbox-options">
                        <li class="selectbox-option" data-value="1">Sapa</li>
                        <li class="selectbox-option" data-value="4">Ninh Bình</li>
                        <li class="selectbox-option" data-value="5">Hạ Long</li>
                        <li class="selectbox-option" data-value="6">Kiên Giang</li>
                    </div>
                </ul>
                <input type="text" class="popup-homesearch-textbox js_datetimepicker" value="Chọn ngày" />
                <input type="submit" class="popup-homesearch-button" value="Xem chuyến xe" />
            </form>
        </div>

        <!-- Xe Riêng -->
        <div class="popup-homesearch popup-6">
            <h3>Thuê xe tài xế riêng</h3>
            <form class="">
                <ul class="selectbox">
                    <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Chọn tỉnh thành" />
                    <div class="selectbox-options js_selectbox-options">
                        <li class="selectbox-option" data-value="1">Sapa</li>
                        <li class="selectbox-option" data-value="4">Ninh Bình</li>
                        <li class="selectbox-option" data-value="5">Hạ Long</li>
                        <li class="selectbox-option" data-value="6">Kiên Giang</li>
                    </div>
                </ul>
                <input type="text" class="popup-homesearch-textbox js_datetimepicker" value="Chọn ngày" />
                <input type="submit" class="popup-homesearch-button" value="Xem xe đang có" />
            </form>
        </div>

        <!-- Máy Bay -->
        <div class="popup-homesearch popup-7">
            <h3>Đặt vé máy bay</h3>
            <form class="">
                <ul class="selectbox">
                    <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Chọn nơi đến" />
                    <div class="selectbox-options js_selectbox-options">
                        <li class="selectbox-option" data-value="1">Sapa</li>
                        <li class="selectbox-option" data-value="2">Hà Khẩu</li>
                        <li class="selectbox-option" data-value="3">Hà Giang</li>
                        <li class="selectbox-option" data-value="4">Ninh Bình</li>
                        <li class="selectbox-option" data-value="5">Hà Nội</li>
                        <li class="selectbox-option" data-value="6">Phú Quốc</li>
                        <li class="selectbox-option" data-value="7">Côn Đảo</li>
                    </div>
                </ul>
                <ul class="selectbox">
                    <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Chọn nơi đi" />
                    <div class="selectbox-options js_selectbox-options">
                        <li class="selectbox-option" data-value="1">Sapa</li>
                        <li class="selectbox-option" data-value="2">Hà Khẩu</li>
                        <li class="selectbox-option" data-value="3">Hà Giang</li>
                        <li class="selectbox-option" data-value="4">Ninh Bình</li>
                        <li class="selectbox-option" data-value="5">Hà Nội</li>
                        <li class="selectbox-option" data-value="6">Phú Quốc</li>
                        <li class="selectbox-option" data-value="7">Côn Đảo</li>
                    </div>
                </ul>
                <input type="text" class="popup-homesearch-textbox js_datetimepicker" value="Chọn ngày" />
                <input type="submit" class="popup-homesearch-button" value="Xem chuyến bay" />
            </form>
        </div>

        <!-- Khách sạn -->
        <div class="popup-homesearch popup-8">
            <h3>Đặt phòng khách sạn</h3>
            <form class="">
                <ul class="selectbox">
                    <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Bạn muốn đến đâu?" />
                    <div class="selectbox-options js_selectbox-options">
                        <li class="selectbox-option" data-value="1">Sapa</li>
                        <li class="selectbox-option" data-value="2">Hà Khẩu</li>
                        <li class="selectbox-option" data-value="3">Hà Giang</li>
                        <li class="selectbox-option" data-value="4">Ninh Bình</li>
                        <li class="selectbox-option" data-value="5">Hà Nội</li>
                        <li class="selectbox-option" data-value="6">Phú Quốc</li>
                        <li class="selectbox-option" data-value="7">Côn Đảo</li>
                    </div>
                </ul>
                <input type="text" class="popup-homesearch-textbox js_datetimepicker" value="Ngày nhận phòng" />
                <input type="text" class="popup-homesearch-textbox js_datetimepicker" value="Ngày trả phòng" />
                <input type="submit" class="popup-homesearch-button" value="Tìm phòng ngay" />
            </form>
        </div>

        <!-- Combo -->
        <div class="popup-homesearch popup-9">
            <h3>Combo ngon</h3>
            <form class="">
                <ul class="selectbox">
                    <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Bạn muốn đến đâu?" />
                    <div class="selectbox-options js_selectbox-options">
                        <li class="selectbox-option" data-value="1">Sapa</li>
                        <li class="selectbox-option" data-value="2">Hà Khẩu</li>
                        <li class="selectbox-option" data-value="3">Hà Giang</li>
                        <li class="selectbox-option" data-value="4">Ninh Bình</li>
                        <li class="selectbox-option" data-value="5">Hà Nội</li>
                        <li class="selectbox-option" data-value="6">Phú Quốc</li>
                        <li class="selectbox-option" data-value="7">Côn Đảo</li>
                    </div>
                </ul>
                <input type="submit" class="popup-homesearch-button" value="Tìm combo ngon" />
            </form>
        </div>
    </div>
</div>
{@script}
