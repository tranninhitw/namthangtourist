$(document).ready(function() {

    /**
     * Show / hide popup
    */
    $('.js_homesearch-block').click(function() {
        var popup = $(this).data('popup');
        $('.js_popup-homesearch-overlay').addClass('opened');
        $('.' + popup).addClass('opened');
    });

    $('.js_popup-homesearch-close').click(function() {
        $('.js_popup-homesearch-overlay').removeClass('opened');
        $('.popup-homesearch').removeClass('opened');
    });
});
