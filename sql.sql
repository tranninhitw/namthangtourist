# CATEGORY TABLE
create table if not exists tp_cates(
	cate_id int primary key,
    cate_name varchar(255),
    cate_parent int,
    cate_page_title varchar(255),
    cate_seo_url varchar(255) unique,
    cate_meta_description text,
    cate_meta_keywords text,
    cate_image text, #Use only an image
    constraint c_unique unique(cate_name, cate_parent)
) engine innodb character set utf8 collate utf8_unicode_ci;

# QUESTION TABLE
create table if not exists tp_questions (
	question_id int primary key auto_increment,
    question_title text,
    question_description text
) engine innodb character set utf8 collate utf8_unicode_ci;

# USER TABLE
# Basic user information (not admin)
create table if not exists tp_users(
	user_id int primary key auto_increment,
    group_id varchar(255), #JSON all group
	user_username varchar(50) unique,
    user_password varchar(255), #Password key/salt
    user_fullname varchar(100),
    user_email varchar(100) unique,
    user_mobile varchar(100) unique,
    user_points varchar(255), #Collected points
    user_started int,
    user_ips text, #JSON IPs
    user_resetpass_string varchar(255) unique,
    user_address_book text,
    user_block tinyint(1) default 0
) engine innodb character set utf8 collate utf8_unicode_ci;

# GROUP TABLE
create table if not exists tp_groups(
	group_id int primary key auto_increment,
    group_name varchar(100),
    group_privilege text,
    group_type tinyint(1) default 0 # 0: customer | 1: admin
) engine innodb character set utf8 collate utf8_unicode_ci;

# ADMIN TABLE
# This table stores important infomation
# Dont edit or delete in stuck
create table if not exists tp_admins(
	admin_id int primary key auto_increment,
    group_id varchar(255), #JSON all group
    admin_username varchar(50) unique,
    admin_password varchar(255),
    admin_fullname varchar(100),
    admin_email varchar(100) unique ,
    admin_mobile varchar(100) unique,
    admin_ips text, #JSON IPs
    admin_last_session int, #Last online time
    admin_blocked tinyint(1) default 0,
    admin_resetpass_string varchar(255) unique
) engine innodb character set utf8 collate utf8_unicode_ci;

# STATE TABLE
create table if not exists tp_state(
	state_id int primary key auto_increment,
    state_name varchar(100) unique,
    state_description text
) engine innodb character set utf8 collate utf8_unicode_ci;

# PAYMENT METHOD TABLE
create table if not exists tp_payment(
	payment_id int primary key auto_increment,
    payment_name varchar(100) unique,
    payment_description text
) engine innodb character set utf8 collate utf8_unicode_ci;

create table if not exists tp_transaction(
	transaction_id int primary key auto_increment,
    transaction_amount float,
    transaction_bank varchar(255),
    transaction_no varchar(255),
    transaction_card_type varchar(255),
    transaction_order_info text,
    transaction_pay_date varchar(255), #YYYYMMDD-HH:II:SS
    transaction_response_code varchar(5),
    transaction_secure_hash varchar(255),
    order_code varchar(255)
);

# LOGS TABLE
create table if not exists tp_logs(
	log_id int primary key auto_increment,
    log_time int,
    log_ip varchar(100),
    log_session_id varchar(255),
    log_device varchar(255),
    log_browser varchar(255),
    log_die_time int
) engine innodb character set utf8 collate utf8_unicode_ci;

# SETTING TABLE
create table if not exists tp_settings(
	setting_id int primary key auto_increment,
    setting_name varchar(255) unique,
    setting_value text
)engine innodb character set utf8 collate utf8_unicode_ci;

# ADVERTISE TABLE
create table if not exists tp_advertises(
	advertise_id int primary key auto_increment,
    advertise_name varchar(255) unique,
    advertise_link text,
    advertise_target varchar(20) default '_self',
    advertise_title varchar(255),
    advertise_click_amount int default 0,
    advertise_start_time int unsigned,
    advertise_image text
) engine innodb character set utf8 collate utf8_unicode_ci;

# EMAIL MARKETING TABLE
create table if not exists tp_marketing_email(
	marketing_email_id int primary key auto_increment,
    marketing_email_time int unsigned,
    marketing_email_amount int unsigned,
    marketing_email_receivers text, #JSON all receivers, include status of each one
    marketing_email_contents text,
    marketing_email_result float #Percent of successfully sent per all
) engine innodb character set utf8 collate utf8_unicode_ci;

# BLOG CATE TABLE
create table if not exists tp_blog_cates(
	blog_cate_id int primary key auto_increment,
    blog_cate_name varchar(255) unique,
    blog_cate_parent int,
    blog_cate_page_title varchar(255),
    blog_cate_seo_url varchar(255) unique,
    blog_cate_meta_description text,
    blog_cate_meta_keywords text,
    constraint c_b_c unique( blog_cate_name, blog_cate_parent )
) engine innodb character set utf8 collate utf8_unicode_ci;

# POST TABLE
create table if not exists tp_posts(
	post_id int primary key auto_increment,
    admin_id int,
    blog_cate_id varchar(255),
    post_title varchar(255),
    post_page_title varchar(255),
    post_contents text,
    post_compact text,
    post_meta_description text,
    post_meta_keywords text,
    post_seo_url varchar(255) unique,
    post_allow_comment tinyint(1) default 1, #1: Allowed | 0: Disallowed
    post_as_draff tinyint(1) default 1, #1: Publish | 0: Save as draff
    post_thumbnail text, #Image use as thumbnail
    post_time int unsigned,
    post_view_count int unsigned default 0
)  engine innodb character set utf8 collate utf8_unicode_ci;

# COMMENT TABLE
create table if not exists tp_comments(
	comment_id int primary key auto_increment,
    comment_url text,
    user_id int,
    comment_parent int, # ID of comment if this is a reply
    comment_contents text,
    comment_time int unsigned,
    comment_author_name varchar(255),
    comment_author_email varchar(255),
    comment_like_amount int unsigned default 0,
    comment_publish tinyint(1) default 1 # 1: publish | 0: hidden
) engine innodb character set utf8 collate utf8_unicode_ci;

# PAGE TABLE
create table if not exists tp_pages(
	page_id int primary key auto_increment,
    page_name varchar(255) unique,
    page_page_title varchar(255),
    page_seo_url varchar(255) unique,
    page_meta_description text,
    page_meta_keywords text,
    page_contents text,
    page_publish tinyint(1) default 1, #1: Publish | 0: hidden
    page_view_amount int unsigned # Amount of all views until current
)  engine innodb character set utf8 collate utf8_unicode_ci;

#Mailbox
create table if not exists tp_mailbox(
    mailbox_id int primary key auto_increment,
    mailbox_time int unsigned,
    mailbox_contents text,
    mailbox_title text,
    mailbox_sender_email varchar(255),
    mailbox_seen tinyint(1) default 0,
    mailbox_important tinyint(1) default 0
) engine innodb character set utf8 collate utf8_unicode_ci;

#Table notification
CREATE TABLE IF NOT EXISTS tp_notifications (
	notification_id int primary key auto_increment,
    notification_number int,
    notification_type varchar(255) unique,
    notification_read tinyint(1) default 1
) engine innodb character set utf8 collate utf8_unicode_ci;
