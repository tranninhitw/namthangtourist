{@style}
<section class="module-wrapper">
	<section class="module-toolbar">
		<h3 class="module-heading">Sản phẩm / Danh mục <small>Cpanel</small></h3>
		<div class="module-button-area">
			<span class="buttons normal-buttons cate-addnew-btn"><i class="fa fa-plus" aria-hidden="true"></i> Thêm</span>
			<span class="buttons disabled-buttons cate-cancel-btn"><i class="fa fa-undo" aria-hidden="true"></i> Hủy</span>
		</div>
	</section>
	<section class="module-content">
		<div class="module-addnew cate-addnew">
			<h3>Nhập tên để thêm danh mục, sau đó sắp xếp và lưu:</h3>
			<span class="buttons disabled-buttons cate-name-save-btn"><i class="fa fa-floppy-o"></i> Lưu</span>
			<table class="form-tables">
				<tr>
					<td>Tên danh mục (*)</td>
					<td>
						<input class="inputs normal-inputs required-inputs cate-textbox cate-name-textbox" type="text" placeholder="Nhập tên danh mục" />
					</td>
				</tr>
				<tr>
					<td>
						Title (*)
					</td>
					<td>
						<input type="text" class="inputs required-inputs cate-textbox" placeholder="Page title" />
					</td>
				</tr>
				<tr>
					<td>
						URL (*)
					</td>
					<td>
						<input type="text" class="inputs required-inputs cate-textbox" placeholder="Url" />
					</td>
				</tr>
				<tr>
					<td>
						Meta description (*)
					</td>
					<td>
						<textarea class="inputs required-inputs cate-textbox" placeholder="Meta description"></textarea>
					</td>
				</tr>
				<tr>
					<td>
						Meta keywords (*)
					</td>
					<td>
						<textarea class="inputs required-inputs cate-textbox" placeholder="Meta keywords"></textarea>
					</td>
				</tr>
				<tr>
					<td>
						Hình ảnh
					</td>
					<td>
						<input type="file" class="file-inputs" id="cate-image" />
						<label for="cate-image">
							<span>Chọn hình ảnh</span>
							<span id="file-input-caption">Chưa chọn file...</span>
						</label>
					</td>
				</tr>
			</table>
		</div>
		<div class="cate-list-wrapper">
			<div class="module-button-area">
				<span class="buttons disabled-buttons cate-save-order-btn save-btn"><i class="fa fa-floppy-o"></i> Lưu</span>
				<span class="buttons disabled-buttons cate-undo-order-btn undo-btn"><i class="fa fa-undo" aria-hidden="true"></i> Hủy</span>
			</div>
			<h3><b>Kéo thả</b> để sắp xếp danh mục, <b>nhấp đúp</b> để sửa:</h3>
			{@list}
		</div>
	</section>
</section>
{@script}