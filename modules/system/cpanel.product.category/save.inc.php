<?php 
	if( isset( $_POST['data'] ) ) {
		
		//Embed class
		$sql = new Sql();
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		
		//Save data
		foreach( $_POST['data'] as $key => $arr ) {
			
			//Get data and remove all special characters
			$cateName = $arr['cate_name'];
			$cateParent = $arr['cate_parent'];
			$dataCateId = $arr['data_cate_id'];
			$cateId = $arr['cate_id'];
			$catePageTitle = $arr['cate_seo_page_title'];
			$cateUrl = $arr['cate_seo_url'];
			$cateMetaDes = $arr['cate_seo_meta_description'];
			$cateMetaKey = $arr['cate_seo_meta_keywords'];
			$cateImage = $arr['cate_image'];
			
			$cateName = strip_tags( $cateName );
			$catePageTitle = strip_tags( $catePageTitle );
			$cateUrl = strip_tags( $cateUrl );
			$cateMetaDes = strip_tags( $cateMetaDes );
			$cateMetaKey = strip_tags( $cateMetaKey );
			$cateImage = strip_tags( $cateImage );
			$cateParent = (int)$cateParent;
			$dataCateId = (int)$dataCateId;
			$cateId = (int)$cateId;
			
			//Insert when $cateId === 0
			$data = array(
				':cate_id' => $cateId,
				':cate_name' => $cateName,
				':cate_parent' => $cateParent,
				':cate_page_title' => $catePageTitle,
				':cate_seo_url' => $cateUrl,
				':cate_meta_description' => $cateMetaDes,
				':cate_meta_keywords' => $cateMetaKey,
				':cate_image' => $cateImage
			);
			
			if( $dataCateId == 0 ) {
				$r = $exec -> add( $sql -> get( 6 ), $data );
			}
			//Update when $cateId <> 0
			else {
				$r = $exec -> exec( $sql -> get( 7 ), $data );
			}
		}
		if( !$r )
			echo '0|Trùng tên danh mục';
		else
			echo '1|Lưu thành công';
	}
?>