$( document ).ready( function() {
	"use strict";
	
	/*Sortable handle*/
	PRODUCT_CATEGORY.initSortable();

	/*When click addnew*/
	$( document ).on( 'click', '.cate-addnew-btn', function() {
		MODULE.cmd( 'addnew', $( '.cate-cancel-btn' ) );
	} );
	
	/*When click cancel*/
	$( document ).on( 'click', '.cate-cancel-btn', function() {
		MODULE.cmd( 'cancel', $( '.cate-cancel-btn' ) );
	} );
	
	/*When click slide down cate*/ 
	$( document ).off( 'click', '.cate-caret' ).on( 'click', '.cate-caret', function( e ) {
		e.stopPropagation();
		$( this ).next().next().next().slideToggle( 'fast' );
		$( this ).find( '.fa' ).toggleClass( 'fa-caret-right' ).toggleClass( 'fa-caret-down' );
	} );
	
	/*When input textbox of cate name*/
	$( document ).on( 'input', '.cate-addnew .cate-textbox', function( e ) {
		MODULE.cmd( 'input', $( '.cate-name-save-btn' ) );
		
		//Insert new cate when enter
		if( e.which == 13 ) {
			var value = $( this ).val();
			value = value.latinise();
			value = $.trim( value );
			if( value.length !== 0 && !REGEX.test( value ) ) {
				$( '.cate-name-save-btn' ).click();
			}
		}
	} );
	
	/*When input on cate name textbox*/
	$( document ).on( 'input', '.cate-name-textbox', function() {
		/*Sync catename when edit*/
		var text = $( this ).val();
		var parent = $( this ).closest( 'div' );
		if( !parent.hasClass( 'cate-addnew' ) ) {
			parent.parent().find( '.cate-name' ).text( text );
		}
		
		/*Autofill url*/
		var url = text.latinise();
		url = url.replace( / /g, '-' );
		url = url.toLowerCase();
		$( this ).parent().parent().next().next().find( '.cate-textbox' ).val( url );
		
	} );
	
	/*When input textbox of cate list*/
	$( document ).on( 'input', '.sortable .cate-textbox', function() {
		var condition = ULTI.validate( $( '.sortable .required-inputs' ) );
		if( condition ) {
			DOM.enableSaveBtn();
		}
		else {
			DOM.disableSaveBtn();
		}
	} );
	
	/*When click on save new cate*/
	//Notice: Image of cate is saved with name like: 'danhmuc_url_id'
	$( document ).off( 'click', '.cate-name-save-btn' ).on( 'click', '.cate-name-save-btn', function(){
		var textboxes = $( '.module-addnew' ).find( '.cate-textbox' ),
			cateName = textboxes[0].value,
			cateId = new Date().getTime() / 1000 | 0,
			pageTitle = textboxes[1].value,
			url = textboxes[2].value,
			metaDes = textboxes[3].value,
			metaKey = textboxes[4].value,
			fileInput = document.getElementById( 'cate-image' );
		
		var callback = function( imgSrc ) {
			if( imgSrc ) {
				imgSrc = JSON.parse( imgSrc )[0];
				var img = '<img src="' + _WWW + '/uploads/media/' + imgSrc + '" />';
			}
			else {
				var img = 'Không có hình ảnh';
			}
			var li = '<li id="cate_' + cateId + '" data-cate-id="0"><div><span class="cate-caret"> <i class="fa fa-caret-right" aria-hidden="true"></i> </span> <span class="cate-name">' + cateName + '</span> <span class="cate-delete" data-cate-id="' + cateId + '"><i class="fa fa-times" aria-hidden="true"></i></span><div><table class="form-tables"><tr><td>Tên danh mục (*)</td><td> <input class="inputs normal-inputs required-inputs cate-textbox cate-name-textbox" type="text" placeholder="Nhập tên danh mục" value="' + cateName + '" /><input type="text" class="hidden cate-textbox" value="' + imgSrc + '" /></td></tr><tr><td> Title (*)</td><td> <input type="text" class="inputs required-inputs cate-textbox" placeholder="Page title" value="' + pageTitle + '" /></td></tr><tr><td> URL (*)</td><td> <input type="text" class="inputs required-inputs cate-textbox" placeholder="Url" value="' + url + '" /></td></tr><tr><td> Meta description (*)</td><td><textarea class="inputs required-inputs cate-textbox" placeholder="Meta description">' + metaDes + '</textarea></td></tr><tr><td> Meta keywords (*)</td><td><textarea class="inputs required-inputs cate-textbox" placeholder="Meta keywords">' + metaKey + '</textarea></td></tr><tr><td> Hình ảnh</td><td> <input type="file" class="file-inputs" id="cate-image" accept="image/*" /> <label for="cate-image"> <span>Chọn hình ảnh</span> <span>Chưa chọn file...</span> </label></td></tr><tr><td colspan="2">' + img + '</td></tr></table></div></div></li>';
			$( '.sortable' ).prepend( li );
		};
		
		ULTI.upload( fileInput.files, 'media', 'danhmuc_' + url + '_' + cateId, callback );
		
		/* Empty textbox */
		$( '.module-addnew .cate-textbox' ).val( '' );
		
		/*Disable button*/
		$( this ).addClass( 'disabled-buttons' ).removeClass( 'save-buttons' );
		
		/*Get order of list*/
		PRODUCT_CATEGORY.updateOrder();
		
		/*Slide up addnew panel*/
		$( '.cate-cancel-btn' ).click();
		
		/*Enable save order button*/
		DOM.enableSaveBtn();
	} );
	
	/*When click delete cate button*/
	$( document ).on( 'click', '.cate-delete', function() {
		var deleteFn = function( target ) {
			var id = target.data( 'cate-id' );
			target.parent().parent().remove();
			
			var url = 'include/2';
			var data = {
				'cate_id' : id
			};
			
			var callback = function( html ) {
				var arr = html.split( '|' );
				var state = arr[0];
				var string = arr[1];
				if( html ) DOM.showStatus( string, state );
			};
			
			AJAX.post( url, data, callback );
			
			/*Get order of list*/
			PRODUCT_CATEGORY.updateOrder();
		};
		var text = 'Bạn có chắc chắn xóa?';
		if( $( this ).parent().next().is( 'ol' ) ) {
			text = 'Xóa danh mục này sẽ xóa tất cả danh mục con, bạn có chắc?';
		}
		DOM.showPopup( text, deleteFn, $( this ) );
	} );
	
	/*When change image of each cate*/
	$( document ).off( 'change', '.file-inputs.cate-textbox' ).on( 'change', '.file-inputs.cate-textbox', function() {
		var file = this.files;
		if( file.length > 0 ) {
			var cateURL = $( this ).data( 'cate-url' );
			var cateId = $( this ).data( 'cate-id' );
			var inputStore = $( this ).data( 'input-store' );
			var callback = function( html ) {
				html = JSON.parse( html )[0];
				var input = document.getElementById( inputStore );
				input.value = html;
				input.defaultValue = html;
				DOM.enableSaveBtn();
			};
			ULTI.upload( file, 'media', 'danhmuc_' + cateURL + '_' + cateId, callback );
		}
	} );
	
	/*When click save order of list*/
	$( document ).off( 'click', '.cate-save-order-btn' ).on( 'click', '.cate-save-order-btn', function() {
		$( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i> Đang lưu' );
		var order = $( 'ol.sortable' ).nestedSortable( 'toArray' );
		order.splice( 0, 1 );
		var len = order.length;
		var data = new Array();
		for( var i = 0; i < len; i++ ) {
			var obj = order[i],
				el = $( '#cate_' + obj['id'] ),
				cateParent = obj['parent_id'] === null ? 0 : obj['parent_id'],
				inputs = el.find( '.cate-textbox' ),
				dataCateId = el.data( 'cate-id' ),
				cateId = obj['id'],
				cateName = inputs[0].value,
				image = inputs[1].value,
				pageTitle = inputs[2].value,
				url = inputs[3].value,
				metaDes = inputs[4].value,
				metaKey = inputs[5].value,				
				arr = {
					'cate_name': cateName,
					'cate_parent': cateParent,
					'cate_id': cateId,
					'data_cate_id': dataCateId,
					'cate_seo_page_title': pageTitle,
					'cate_seo_url': url,
					'cate_seo_meta_description': metaDes,
					'cate_seo_meta_keywords': metaKey,
					'cate_image': image
				};
			data.push( arr );
		}
		
		/*Ajax*/
		var url = 'include/1';
		var data = {
			'data': data
		};
		var callback = function( html ) {
			var htmlArr = html.split( '|' );
			if( htmlArr[0] == '1' ) {
				ULTI.done( htmlArr[1] );
			}
			else {
				DOM.showStatus( htmlArr[1], parseInt( htmlArr[0] ) );
				$( '.cate-save-order-btn' ).removeClass( 'saving' ).html( '<i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu' );
			}
			
			//Update oldOrder
			PRODUCT_CATEGORY.updateOrder();
		};
		AJAX.post( url, data, callback );
	} );
	
	/*When click undo change order*/
	$( document ).on( 'click', '.cate-undo-order-btn', function() {
		AJAX.load( $( 'article' ), _WWW + '/admin/sanpham/danhmuc article' );
	} );
} );