<?php 
	$exec = new Exec( HOST, USER, PASS, DBNAME );
	$sql = new Sql();
	
	$cates = $exec -> get( $sql -> get( 8 ) );
	
	
	/**
	 * Fn to build cate tree
	*/
	
	$cates = find_children( $cates );
	$html = '<ol class="sortable ui-sortable cate-list">';
	$html .= build_tree( $cates );
	$html .= '</ol>';
	echo $html;
	
	
	function find_children( $cates, $parentId = 0 ) {
		$tree = array();
		foreach( $cates as $key => $cate ) {
			if( $cate['cate_parent'] == $parentId ) {
				array_push( $tree, array( 
					'cate_info' => $cate,
					'cate_children' => find_children( $cates, $cate['cate_id'] )
				) );
			}
		}
		return $tree;
	}
	
	function build_tree( $tree ) {
		$html = '';
		foreach( $tree as $key => $branch ) {
			$childTree = '<ol>';
			$childTree .= build_tree( $branch['cate_children'] );
			$childTree .= '</ol>';
			$cate = $branch['cate_info'];
			$cateName = trim( $cate['cate_name'] );
			$html .= '
				<li id="cate_' . $cate['cate_id'] . '" data-cate-id="' . $cate['cate_id'] . '">
					<div>
						<span class="cate-caret">
							<i class="fa fa-caret-right" aria-hidden="true"></i>
						</span>
						<span class="cate-name">' . $cateName . '</span>
						<span class="cate-delete" data-cate-id="' . $cate['cate_id'] . '"><i class="fa fa-times" aria-hidden="true"></i></span>
						<div>
							<table class="form-tables">
								<tr>
									<td>Tên danh mục (*)</td>
									<td>
										<input class="inputs normal-inputs required-inputs cate-textbox cate-name-textbox" type="text" placeholder="Tên danh mục" value="' . $cateName . '" />
										<input id="input-image-' . $cate['cate_id'] . '" class="hidden cate-textbox" type="text" value="' . $cate['cate_image'] . '" />
									</td>
								</tr>
								<tr>
									<td>
										Title (*)
									</td>
									<td>
										<input type="text" class="inputs required-inputs cate-textbox" placeholder="Page title" value="' . $cate['cate_page_title'] . '" />
									</td>
								</tr>
								<tr>
									<td>
										URL (*)
									</td>
									<td>
										<input type="text" class="inputs required-inputs cate-textbox" placeholder="Url" value="' . $cate['cate_seo_url'] . '" />
									</td>
								</tr>
								<tr>
									<td>
										Meta description (*)
									</td>
									<td>
										<textarea class="inputs required-inputs cate-textbox" placeholder="Meta description">' . $cate['cate_meta_description'] . '</textarea>
									</td>
								</tr>
								<tr>
									<td>
										Meta keywords (*)
									</td>
									<td>
										<textarea class="inputs required-inputs cate-textbox" placeholder="Meta keywords">' . $cate['cate_meta_keywords'] . '</textarea>
									</td>
								</tr>
								<tr>
									<td>
										Hình ảnh
									</td>
									<td>
										<input type="file" class="file-inputs cate-textbox" data-input-store="input-image-' . $cate['cate_id'] . '" data-cate-url="' . $cate['cate_seo_url'] . '" data-cate-id="' . $cate['cate_id'] . '" id="cate-image-' . $cate['cate_id'] . '" accept="image/*" />
										<label for="cate-image-' . $cate['cate_id'] . '">
											<span>Đổi hình ảnh</span>
											<span>Chưa chọn file...</span>
										</label>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<img src="' . _WWW . '/uploads/media/' . $cate['cate_image'] . '" />
									</td>
								</tr>
							</table>
						</div>
					</div>
					' . $childTree . '
				</li>
			';
		}
		$html = str_replace( '<ol></ol>', '', $html );
		return $html;
	}
	
?>