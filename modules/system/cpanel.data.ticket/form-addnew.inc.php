<?php
$exec = new Exec( HOST, USER, PASS, DBNAME );
$sql = new Sql();

$html = '
		<div class="tabs module-addnew">
			<table class="form-tables tbl">
				<tr>
					<td>Tên tour (*)</td>
					<td>
						<select name="tour_id" id="tour_id" class="inputs ticket-textbox required-inputs">
                            <option value="">Vũng tàu- đà lạt</option>
                            <option value="">Đại nam - phú quốc</option>
                        </select>
					</td>
				</tr>
				<tr>
					<td>Nơi khởi hành (*)</td>
					<td>
						<input type="text" name="ticket_departure" class="inputs ticket-textbox required-inputs ticket_departure" placeholder="Nơi khởi hành" />
					</td>
				</tr>
				<tr>
					<td>Nơi đến (*)</td>
					<td>
						<input type="text" name="ticket_destination" class="inputs ticket-textbox required-inputs ticket_destination" placeholder="Nơi đến" />
					</td>
				</tr>
				<tr>
					<td>Ngày giờ (*)</td>
					<td>
						<input type="text" name="ticket_datetime" class="inputs ticket-textbox required-inputs ticket_datetime" placeholder="Ngày giờ" />
					</td>
				</tr>
				<tr>
					<td>Giá vé (*)</td>
					<td>
						<input type="text" name="ticket_price" class="inputs ticket-textbox required-inputs ticket_price" placeholder="Giá vé" />
					</td>
				</tr>
				<tr>
					<td>Số lượng vé (*)</td>
					<td>    
						<input type="text" name="ticket_quantity" class="inputs ticket-textbox required-inputs ticket_quantity" placeholder="Số lượng vé" />
					</td>
				</tr>
			</table>
		</div>
	';

if( isset( $_GET['action'], $_GET['id'] ) && $_GET['action'] == 'edit' ) {
    //Get ticket by id
    $data = array(
        ':ticket_id' => (int)$_GET['id'],
    );
    $ticket = $exec -> get( $sql -> get( 15 ), $data );
    $ticket = $ticket[0];
    $ticket_name = $ticket['ticket_name'];
    $html = '
		<div class="tabs module-addnew">
			<span class="tab-title">Thêm vé </span>
			<div class="module-addnew-btn">
				<span class="buttons disabled-buttons ticket-save-new save-btn"><i class="fa fa-floppy-o"></i> Lưu</span>
			</div>
			<table class="form-tables tbl">
				<tr>
					<td>Tên tour (*)</td>
					<td>
						<select name="tour_id" id="tour_id" class="inputs ticket-textbox required-inputs">
                            <option value="">Vũng tàu- đà lạt</option>
                            <option value="">Đại nam - phú quốc</option>
                        </select>
					</td>
				</tr>
				<tr>
					<td>Nơi khởi hành (*)</td>
					<td>
						<input value="' . $ticket['ticket_departure'] . '" type="text" name="ticket_departure" class="inputs ticket-textbox required-inputs ticket_departure" placeholder="Nơi khởi hành" />
					</td>
				</tr>
				<tr>
					<td>Nơi đến (*)</td>
					<td>
						<input value="' . $ticket['ticket_destination'] . '" type="text" name="ticket_destination" class="inputs ticket-textbox required-inputs ticket_destination" placeholder="Nơi đến" />
					</td>
				</tr>
				<tr>
					<td>Ngày giờ (*)</td>
					<td>
						<input value="' . $ticket['ticket_datetime'] . '" type="text" name="ticket_datetime" class="inputs ticket-textbox required-inputs ticket_datetime" placeholder="Ngày giờ" />
					</td>
				</tr>
				<tr>
					<td>Giá vé (*)</td>
					<td>
						<input value="' . $ticket['ticket_price'] . '" type="text" name="ticket_price" class="inputs ticket-textbox required-inputs ticket_price" placeholder="Giá vé" />
					</td>
				</tr>
				<tr>
					<td>Số lượng vé (*)</td>
					<td>    
						<input value="' . $ticket['	ticket_quantity'] . '" type="text" name="	ticket_quantity" class="inputs ticket-textbox required-inputs 	ticket_quantity" placeholder="Số lượng vé" />
					</td>
				</tr>
			</table>
		</div>
	';
}
echo $html;
?>
