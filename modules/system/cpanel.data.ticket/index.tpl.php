{@style}
<section class="module-wrapper">
    <section class="module-toolbar">
        <h3 class="module-heading">Quản lý / Vé <small>Cpanel</small></h3>
        <div class="module-button-area">
            <span class="buttons normal-buttons ticket-addnew-btn"><i class="fa fa-plus"></i> Thêm</span>
            <span class="buttons disabled-buttons ticket-cancel-btn"><i class="fa fa-undo"></i> Hủy</span>
        </div>
    </section>
    <section class="module-content">
        <div class="module-addnew tabs">
            <span class="tab-title">Thêm dịch vụ đi kèm</span>
            <span class="buttons disabled-buttons save-btn save-new-ticket"><i class="fa fa-floppy-o"></i> Lưu</span>
            {@form-addnew}
        </div>
        <div class="area-list-wrapper">
            <table class="tables">
                <tr class="title-rows">
                    <td>Số thứ tự</td>
                    <td>Tên tour</td>
                    <td>Nơi đi</td>
                    <td>Nơi đến</td>
                    <td>Giá vé</td>
                    <td>Số lượng</td>
                    <td>Ngày / Giờ</td>
                </tr>
                {@list}
            </table>
        </div>
    </section>
</section>
{@script}