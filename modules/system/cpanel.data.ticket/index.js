$( document ).ready( function() {

    var PARAMS = ULTI.queryString();

    /*Show addnew form*/
    $( document ).off( 'click', '.ticket-addnew-btn' ).on( 'click', '.ticket-addnew-btn', function() {
        MODULE.cmd( 'addnew', $( '.ticket-cancel-btn' ) );
    } );

    /*Hide addnew form*/
    $( document ).off( 'click', '.ticket-cancel-btn' ).on( 'click', '.ticket-cancel-btn', function() {
        MODULE.cmd( 'cancel', $( '.ticket-cancel-btn' ) );
    } );

    /*Validate form*/
    $( document ).on( 'input change', '.area-textbox', function() {
        var allDone = ULTI.validate( $( '.module-addnew .required-inputs' ), 'empty' );
        if( allDone ) {
            DOM.enableSaveBtn();
        }
        else {
            DOM.disableSaveBtn();
        }
    } );


    /*Save area*/
    $( document ).off( 'click', '.save-new-ticket' ).on( 'click', '.save-new-ticket', function() {
        $( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw"></i> Đang lưu' );

        var url = 'include/23';
        var data = $( '.ticket-form .area-textbox' ).serialize();
        if( PARAMS.action == 'edit' ) {
            data += '&action=edit&id=' + PARAMS.id;
        }
        var callback = function( html ) {
            htmlArr = html.split( '|' );
            if( htmlArr[0] == '1' ) {
                ULTI.done( htmlArr[1] );
            }
            else {
                DOM.showStatus( htmlArr[1], parseInt( htmlArr[0] ) );
                $( '.save-new-ticket' ).removeClass( 'saving' ).html( '<i class="fa fa-floppy-o"></i> Lưu' );
            }
        };
        AJAX.post( url, data, callback );
    } );

    /*Edit ticket*/
    $( document ).off( 'click', '.edit-ticket' ).on( 'click', '.edit-ticket', function() {
        DOM.loadTplThisEl( '/admin/caidat/diadanh?action=edit&id=' + $( this ).data( 'id' ) );
    } );

    /*When edit*/
    $( document ).ready( function() {
        if( PARAMS.action == 'edit' ) {
            $( '.ticket-addnew-btn' ).click();
            DOM.enableSaveBtn();
        }
    } );

    /*Delete ticket*/
    $( document ).off( 'click', '.delete-ticket' ).on( 'click', '.delete-ticket', function() {
        var deleteFn = function( target ) {
            var url = 'include/24';
            var data = {
                'id': target.data( 'id' )
            };
            var callback = function( html ) {
                var htmlArr = html.split( '|' );
                DOM.showStatus( htmlArr[1], parseInt(htmlArr[0]) );
                DOM.loadTplThisEl( '/admin/caidat/ticket' );
            };
            AJAX.post( url, data, callback );
        };
        var text = "Bạn có chắc chắn xóa vé này?";
        DOM.showPopup( text, deleteFn, $( this ) );
    } );
} );