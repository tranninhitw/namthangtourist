<header>
	<div class="header-logo">
		<span class="logo-mini">
			<b>TK</b>C
		</span>
		<span class="logo-normal">
			<img src="images/logo_taka.png" title="Taka cpanel" alt="Taka cpanel" />
			<p>Taka Cpanel</p>
		</span>
	</div>
	<div class="header-profile">
		{@profile}
	</div>
</header>