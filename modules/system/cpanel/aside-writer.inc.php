<?php
	$db = unserialize( TP_DB_PARAMS );
	$exec = new Exec( $db['DB_HOST'], $db['DB_USERNAME'], $db['DB_PASSWORD'], $db['DB_SCHEMA_NAME'] );
	
	$prefix = $db['DB_TABLE_PREFIX'];
	$sql = "SELECT * FROM " . $prefix . "notifications;";
	$notification = $exec -> get( $sql );
	$mail = '<span class="notification-wrapper"></span>';
	$order = '<span class="notification-wrapper"></span>';
	$reorder = '<span class="notification-wrapper"></span>';
	$comment = '<span class="notification-wrapper"></span>';
	$total = '<span class="notification-wrapper"></span>';
	$customer = '<span class="notification-wrapper"></span>';
	$number = array();
	foreach( $notification as $key => $value ) {
		if( $value['notification_read'] == 0 ) {
			$number[$value['notification_type']] = $value['notification_number'];
			$$value['notification_type'] = '<span class="notification-wrapper"><span class="realtime-notification">' . $value['notification_number'] . '</span></span>';
		}
	}
	
	$number['order'] = isset( $number['order'] ) ? $number['order'] : 0;
	$number['reorder'] = isset( $number['reorder'] ) ? $number['reorder'] : 0;
	$number['customer'] = isset( $number['customer'] ) ? $number['customer'] : 0;
	$sum = $number['order'] + $number['reorder'] + $number['customer'];
	if( $sum > 0 ) {
		$total = '<span class="notification-wrapper"><span class="realtime-notification">' . $sum . '</span></span>';
	}	
	
	$html = '
		<ul class="aside-list">
			<li class="aside-item aside-homepage">
				<i class="fa fa-home" aria-hidden="true"></i>
				<span>Trang chủ</span>
				<a href="{@www}/" target="_blank">Xem trang chủ</a>
			</li>
			<li class="aside-item aside-overview realtime-item" data-type="mail">
				<i class="fa fa-tachometer" aria-hidden="true"></i>
				<span>Tổng quan</span>
				' . $mail . '
				<a href="' . TP_REL_ROOT . 'admin">Tổng quan</a>
			</li>
			<li class="aside-item">
				<i class="fa fa-pencil" aria-hidden="true"></i>
				<span >Nội dung</span>
				' . $comment . '
				<a href="">Nội dung</a>
				<span class="aside-child-button">
					<i class="fa fa-caret-down" aria-hidden="true"></i>
				</span>
			</li>
			<li class="aside-child-row">
				<ul class="aside-child-list">
					<li class="aside-child-item main">
						<i class="fa fa-files-o" aria-hidden="true"></i>
						<span>Bài viết</span>
						<a href="' . TP_REL_ROOT . 'admin/noidung/baiviet">Bài viết</a>
					</li>
					<li class="aside-child-item realtime-item" data-type="comment">
						<i class="fa fa-comments-o" aria-hidden="true"></i>
						<span>Bình luận</span>
						' . $comment . '
						<a href="' . TP_REL_ROOT . 'admin/noidung/binhluan">Bình luận</a>
					</li>
					<li class="aside-child-item">
						<i class="fa fa-file-code-o" aria-hidden="true"></i>
						<span>Trang tĩnh</span>
						<a href="' . TP_REL_ROOT . 'admin/noidung/trang">Trang tĩnh</a>
					</li>
					<li class="aside-child-item">
						<i class="fa fa-question " aria-hidden="true"></i>
						<span>Câu hỏi</span>
						<a href="' . TP_REL_ROOT . 'admin/noidung/cauhoi">Câu hỏi</a>
					</li>
				</ul>
			</li>
		</ul>
	';
	echo $html;
?>