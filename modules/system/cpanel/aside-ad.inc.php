<?php
	$db = unserialize( TP_DB_PARAMS );
	$exec = new Exec( $db['DB_HOST'], $db['DB_USERNAME'], $db['DB_PASSWORD'], $db['DB_SCHEMA_NAME'] );
	
	$prefix = $db['DB_TABLE_PREFIX'];
	$sql = "SELECT * FROM " . $prefix . "notifications;";
	$notification = $exec -> get( $sql );
	$mail = '<span class="notification-wrapper"></span>';
	$order = '<span class="notification-wrapper"></span>';
	$reorder = '<span class="notification-wrapper"></span>';
	$comment = '<span class="notification-wrapper"></span>';
	$total = '<span class="notification-wrapper"></span>';
	$customer = '<span class="notification-wrapper"></span>';
	$number = array();
	foreach( $notification as $key => $value ) {
		if( $value['notification_read'] == 0 ) {
			$number[$value['notification_type']] = $value['notification_number'];
			$$value['notification_type'] = '<span class="notification-wrapper"><span class="realtime-notification">' . $value['notification_number'] . '</span></span>';
		}
	}
	
	$number['order'] = isset( $number['order'] ) ? $number['order'] : 0;
	$number['reorder'] = isset( $number['reorder'] ) ? $number['reorder'] : 0;
	$number['customer'] = isset( $number['customer'] ) ? $number['customer'] : 0;
	$sum = $number['order'] + $number['reorder'] + $number['customer'];
	if( $sum > 0 ) {
		$total = '<span class="notification-wrapper"><span class="realtime-notification">' . $sum . '</span></span>';
	}	
	$html = '
		<ul class="aside-list">
			<li class="aside-item aside-homepage">
				<i class="fa fa-home" aria-hidden="true"></i>
				<span>Trang chủ</span>
				<a href="' . TP_REL_ROOT . '" target="_blank">Xem trang chủ</a>
			</li>
			<li class="aside-item aside-overview realtime-item" data-type="mail">
				<i class="fa fa-tachometer" aria-hidden="true"></i>
				<span>Tổng quan</span>
				' . $mail . '
				<a href="' . TP_REL_ROOT . 'admin">Tổng quan</a>
			</li>
			<li class="aside-item">
				<i class="fa fa-laptop" aria-hidden="true"></i>
				<span>Sản phẩm</span>
				<a href="">Sản phẩm</a>
				<span class="aside-child-button">
					<i class="fa fa-caret-down" aria-hidden="true"></i>
				</span>
			</li>
			<li class="aside-child-row">
				<ul class="aside-child-list">
					<li class="aside-child-item main">
						<i class="fa fa-table" aria-hidden="true"></i>
						<span>Sản phẩm</span>
						<a href="' . TP_REL_ROOT . 'admin/sanpham/sanpham">Sản phẩm</a>
					</li>
					<li class="aside-child-item">
						<i class="fa fa-folder-o" aria-hidden="true"></i>
						<span>Danh mục</span>
						<a href="' . TP_REL_ROOT . 'admin/sanpham/danhmuc">Danh mục</a>
					</li>
				</ul>
			</li>
			<li class="aside-item">
				<i class="fa fa-shopping-basket" aria-hidden="true"></i>
				<span>Bán hàng</span>
				<a href="">Bán hàng</a>
				' . $total . '
				<span class="aside-child-button">
					<i class="fa fa-caret-down" aria-hidden="true"></i>
				</span>
			</li>
			<li class="aside-child-row">
				<ul class="aside-child-list">
					<li class="aside-child-item main realtime-item" data-type="order">
						<i class="fa fa-file-text-o" aria-hidden="true"></i>
						<span>Đơn đặt hàng</span>
						' . $order . '
						<a href="' . TP_REL_ROOT . 'admin/banhang/dondathang">Đơn đặt hàng</a>
					</li>
					<li class="aside-child-item realtime-item" data-type="reorder">
						<i class="fa fa-file-text" aria-hidden="true"></i>
						<span>Đơn hàng hủy</span>
						' . $reorder . '
						<a href="' . TP_REL_ROOT . 'admin/banhang/dontrahang">Đơn hàng hủy</a>
					</li>
					<li class="aside-child-item">
						<i class="fa fa-gift" aria-hidden="true"></i>
						<span>Voucher</span>
						<a href="' . TP_REL_ROOT . 'admin/banhang/voucher">Voucher</a>
					</li>
					<li class="aside-child-item">
						<i class="fa fa-usd" aria-hidden="true"></i>
						<span>Chiết khấu</span>
						<a href="' . TP_REL_ROOT . 'admin/banhang/chietkhau">Chiết khấu</a>
					</li>
					<li class="aside-child-item realtime-item" data-type="customer">
						<i class="fa fa-users" aria-hidden="true"></i>
						<span>Khách hàng</span>
						' . $customer . '
						<a href="' . TP_REL_ROOT . 'admin/khachhang/khachhang">Khách hàng</a>
					</li>
				</ul>
			</li>
			<li class="aside-item">
				<i class="fa fa-pencil" aria-hidden="true"></i>
				<span>Nội dung</span>
				' . $comment . '
				<a href="">Nội dung</a>
				<span class="aside-child-button">
					<i class="fa fa-caret-down" aria-hidden="true"></i>
				</span>
			</li>
			<li class="aside-child-row">
				<ul class="aside-child-list">
					<li class="aside-child-item main">
						<i class="fa fa-files-o" aria-hidden="true"></i>
						<span>Bài viết</span>
						<a href="' . TP_REL_ROOT . 'admin/noidung/baiviet">Bài viết</a>
					</li>
					<li class="aside-child-item realtime-item" data-type="comment">
						<i class="fa fa-comments-o" aria-hidden="true"></i>
						<span>Bình luận</span>
						' . $comment . '
						<a href="' . TP_REL_ROOT . 'admin/noidung/binhluan">Bình luận</a>
					</li>
					<li class="aside-child-item">
						<i class="fa fa-file-code-o" aria-hidden="true"></i>
						<span>Trang tĩnh</span>
						<a href="' . TP_REL_ROOT . 'admin/noidung/trang">Trang tĩnh</a>
					</li>
					<li class="aside-child-item">
						<i class="fa fa-question " aria-hidden="true"></i>
						<span>Câu hỏi</span>
						<a href="' . TP_REL_ROOT . 'admin/noidung/cauhoi">Câu hỏi</a>
					</li>
				</ul>
			</li>
			<li class="aside-item">
				<i class="fa fa-pie-chart" aria-hidden="true"></i>
				<span>Thống kê</span>
				<a href="">Thống kê</a>
				<span class="aside-child-button">
					<i class="fa fa-caret-down" aria-hidden="true"></i>
				</span>
			</li>
			<li class="aside-child-row">
				<ul class="aside-child-list">
					<li class="aside-child-item main">
						<i class="fa fa-shopping-cart" aria-hidden="true"></i>
						<span>Mua hàng</span>
						<a href="' . TP_REL_ROOT . 'admin/thongke/muahang">Mua hàng</a>
					</li>
					<li class="aside-child-item">
						<i class="fa fa-cubes" aria-hidden="true"></i>
						<span>Tồn kho</span>
						<a href="' . TP_REL_ROOT . 'admin/thongke/tonkho">Tồn kho</a>
					</li>
					<li class="aside-child-item">
						<i class="fa fa-cc-visa" aria-hidden="true"></i>
						<span>Thanh toán</span>
						<a href="' . TP_REL_ROOT . 'admin/thongke/thanhtoan">Thanh toán</a>
					</li>
					<li class="aside-child-item">
						<i class="fa fa-line-chart" aria-hidden="true"></i>
						<span>Doanh số</span>
						<a href="' . TP_REL_ROOT . 'admin/thongke/doanhso">Doanh số</a>
					</li>
					<li class="aside-child-item">
						<i class="fa fa-file-o" aria-hidden="true"></i>
						<span>Đơn hàng</span>
						<a href="' . TP_REL_ROOT . 'admin/thongke/donhang">Đơn hàng</a>
					</li>
					<li class="aside-child-item" id="tc">
						<i class="fa fa-street-view" aria-hidden="true"></i>
						<span>Truy cập</span>
						<a href="' . TP_REL_ROOT . 'admin/thongke/truycap">Truy cập</a>
					</li>
				</ul>
			</li>
			<li class="aside-item">
				<i class="fa fa-cogs" aria-hidden="true"></i>
				<span >Hệ thống</span>
				<a href="">Hệ thống</a>
				<span class="aside-child-button">
					<i class="fa fa-caret-down" aria-hidden="true"></i>
				</span>
			</li>
			<li class="aside-child-row">
				<ul class="aside-child-list">
					<li class="aside-child-item main">
						<i class="fa fa-cog" aria-hidden="true"></i>
						<span>Cài đặt chung</span>
						<a href="' . TP_REL_ROOT . 'admin/caidat/caidatchung">Cài đặt chung</a>
					</li>
					<li class="aside-child-item">
						<i class="fa fa-map-marker" aria-hidden="true"></i>
						<span>Địa danh</span>
						<a href="' . TP_REL_ROOT . 'admin/caidat/diadanh">Địa danh</a>
					</li>
					<li class="aside-child-item">
						<i class="fa fa-lock" aria-hidden="true"></i>
						<span>Tài khoản</span>
						<a href="' . TP_REL_ROOT . 'admin/caidat/taikhoan">Tài khoản</a>
					</li>
					<li class="aside-child-item">
						<i class="fa fa-sliders" aria-hidden="true"></i>
						<span>Slider</span>
						<a href="' . TP_REL_ROOT . 'admin/giaodien/slider">Slider</a>
					</li>
				</ul>
			</li>
		</ul>
	';
	echo $html;
?>