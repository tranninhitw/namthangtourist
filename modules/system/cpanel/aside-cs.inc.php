<?php
	$db = unserialize( TP_DB_PARAMS );
	$exec = new Exec( $db['DB_HOST'], $db['DB_USERNAME'], $db['DB_PASSWORD'], $db['DB_SCHEMA_NAME'] );
	
	$prefix = $db['DB_TABLE_PREFIX'];
	$sql = "SELECT * FROM " . $prefix . "notifications;";
	$notification = $exec -> get( $sql );
	$mail = '<span class="notification-wrapper"></span>';
	$order = '<span class="notification-wrapper"></span>';
	$reorder = '<span class="notification-wrapper"></span>';
	$comment = '<span class="notification-wrapper"></span>';
	$total = '<span class="notification-wrapper"></span>';
	$customer = '<span class="notification-wrapper"></span>';
	$number = array();
	foreach( $notification as $key => $value ) {
		if( $value['notification_read'] == 0 ) {
			$number[$value['notification_type']] = $value['notification_number'];
			$$value['notification_type'] = '<span class="notification-wrapper"><span class="realtime-notification">' . $value['notification_number'] . '</span></span>';
		}
	}
	
	$number['order'] = isset( $number['order'] ) ? $number['order'] : 0;
	$number['reorder'] = isset( $number['reorder'] ) ? $number['reorder'] : 0;
	$number['customer'] = isset( $number['customer'] ) ? $number['customer'] : 0;
	$sum = $number['order'] + $number['reorder'] + $number['customer'];
	if( $sum > 0 ) {
		$total = '<span class="notification-wrapper"><span class="realtime-notification">' . $sum . '</span></span>';
	}	
	
	$html = '
		<ul class="aside-list">
			<li class="aside-item aside-homepage">
				<i class="fa fa-home" aria-hidden="true"></i>
				<span>Trang chủ</span>
				<a href="' . TP_REL_ROOT . '" target="_blank">Xem trang chủ</a>
			</li>
			<li class="aside-item aside-overview realtime-item" data-type="mail">
				<i class="fa fa-tachometer" aria-hidden="true"></i>
				<span>Tổng quan</span>
				' . $mail . '
				<a href="' . TP_REL_ROOT . 'admin">Tổng quan</a>
			</li>
			<li class="aside-item">
				<i class="fa fa-laptop" aria-hidden="true"></i>
				<span>Sản phẩm</span>
				<a href="">Sản phẩm</a>
				<span class="aside-child-button">
					<i class="fa fa-caret-down" aria-hidden="true"></i>
				</span>
			</li>
			<li class="aside-child-row">
				<ul class="aside-child-list">
					<li class="aside-child-item main">
						<i class="fa fa-table" aria-hidden="true"></i>
						<span>Sản phẩm</span>
						<a href="' . TP_REL_ROOT . 'admin/sanpham/sanpham">Sản phẩm</a>
					</li>
					<li class="aside-child-item">
						<i class="fa fa-folder-o" aria-hidden="true"></i>
						<span>Danh mục</span>
						<a href="' . TP_REL_ROOT . 'admin/sanpham/danhmuc">Danh mục</a>
					</li>
				</ul>
			</li>
			<li class="aside-item">
				<i class="fa fa-shopping-basket" aria-hidden="true"></i>
				<span>Bán hàng</span>
				<a href="">Bán hàng</a>
				' . $total . '
				<span class="aside-child-button">
					<i class="fa fa-caret-down" aria-hidden="true"></i>
				</span>
			</li>
			<li class="aside-child-row">
				<ul class="aside-child-list">
					<li class="aside-child-item main realtime-item" data-type="order">
						<i class="fa fa-file-text-o" aria-hidden="true"></i>
						<span>Đơn đặt hàng</span>
						' . $order . '
						<a href="' . TP_REL_ROOT . 'admin/banhang/dondathang">Đơn đặt hàng</a>
					</li>
					<li class="aside-child-item realtime-item" data-type="reorder">
						<i class="fa fa-file-text" aria-hidden="true"></i>
						<span>Đơn hàng hủy</span>
						' . $reorder . '
						<a href="' . TP_REL_ROOT . 'admin/banhang/dontrahang">Đơn hàng hủy</a>
					</li>
					<li class="aside-child-item">
						<i class="fa fa-gift" aria-hidden="true"></i>
						<span>Voucher</span>
						<a href="' . TP_REL_ROOT . 'admin/banhang/voucher">Voucher</a>
					</li>
					<li class="aside-child-item">
						<i class="fa fa-usd" aria-hidden="true"></i>
						<span>Chiết khấu</span>
						<a href="' . TP_REL_ROOT . 'admin/banhang/chietkhau">Chiết khấu</a>
					</li>
					<li class="aside-child-item realtime-item" data-type="customer">
						<i class="fa fa-users" aria-hidden="true"></i>
						<span>Khách hàng</span>
						' . $customer . '
						<a href="' . TP_REL_ROOT . 'admin/khachhang/khachhang">Khách hàng</a>
					</li>
				</ul>
			</li>
		</ul>
	';
	echo $html;
?>