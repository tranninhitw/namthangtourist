<?php 
	if( isset( $_POST['type'] ) ) {
		$db = unserialize( TP_DB_PARAMS );
		$exec = new Exec( $db['DB_HOST'], $db['DB_USERNAME'], $db['DB_PASSWORD'], $db['DB_SCHEMA_NAME'] );
		
		$prefix = $db['DB_TABLE_PREFIX'];
		$sql = "UPDATE " . $prefix . "notifications SET notification_number = 0, notification_read = 1 WHERE notification_type = :notification_type;";
		
		$exec -> exec( $sql, array(
			':notification_type' => $_POST['type']
		) );
	}
?>