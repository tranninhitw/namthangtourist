$( document ).ready( function() {
	//Check all
	$( '.mailbox-toolbar-item input' ).change( function() {
		if( $( this ).is( ':checked' ) ) {
			$( '.mailbox-list input' ).prop( 'checked', true );
		} else {
			$( '.mailbox-list input' ).prop( 'checked', false );
		}
	} );
	
	//Read mail
	$( document ).off( 'click', '#read' ).on( 'click', '#read', function() {
		var a = [];
		$.each( $( '.mailbox-list input' ), function() {
			if( $( this ).is( ':checked' ) ) {
				var b = $( this ).parent().data( 'id' );
				a.push(b);
			}
		} );
		var url = 'include/20';
		var data = {
			'mailbox_id': a
		};
		var callback = function( html ) {
			$( '.mailbox-list' ).find( 'input:checked' ).parent().removeClass( 'new-mail' ).addClass( 'read-mail' );
		};
		AJAX.post( url, data, callback );
		
	} );
	
	//Important mail	
	$( document ).off( 'click', '#important' ).on( 'click', '#important', function() {
		var a = [];
		$.each( $( '.mailbox-list input' ), function() {
			if( $( this ).is( ':checked' ) ) {
				var b = $( this ).parent().data( 'id' );
				a.push(b);
			}
		} );
		var url = 'include/21';
		var data = {
			'mailbox_id': a
		};
		var callback = function( html ) {
			if( $( '.mailbox-list' ).find( 'input:checked' ).next().hasClass( 'mail-important' ) ) {
				$( '.mailbox-list' ).find( 'input:checked' ).next().removeClass( 'mail-important' ).addClass( 'mail-normal' );
			}else {
				$( '.mailbox-list' ).find( 'input:checked' ).next().removeClass( 'mail-normal' ).addClass( 'mail-important' );
			}
		};
		AJAX.post( url, data, callback );	
	} );
	
	$( document ).off( 'click', '.icon' ).on( 'click', '.icon', function() {
		var _this = $( this );
		var a = [];
		var b = $( this ).parent().data( 'id' );
		a.push(b);
		var url = 'include/21';
		var data = {
			'mailbox_id': a
		};
		var callback = function( html ) {
			if( _this.hasClass( 'mail-important' ) ) {
				_this.removeClass( 'mail-important' ).addClass( 'mail-normal' );
			}else {
				_this.removeClass( 'mail-normal' ).addClass( 'mail-important' );
			}
		};
		AJAX.post( url, data, callback );	
	} );
	
	//Del mail	
	$( document ).off( 'click', '#del' ).on( 'click', '#del', function() {
		var deleteFn = function( target ) {
			var a = [];
			$.each( $( '.mailbox-list input' ), function() {
				if( $( this ).is( ':checked' ) ) {
					var b = $( this ).parent().data( 'id' );
					a.push(b);
				}
			} );
			var url = 'include/22';
			var data = {
				'mailbox_id': a
			};
			var callback = function( html ) {
				DOM.loadTplThisEl( '/admin' );
			};
			AJAX.post( url, data, callback );
		};			
		var text = 'Bạn có chắc chắn muốn xóa?';
		DOM.showPopup( text, deleteFn, $( this ) );
	} );
	
	//Show message
	$( document ).off( 'click', '.mail-author, .mail-title, .mail-compact').on( 'click', '.mail-author, .mail-title, .mail-compact', function() {
		var _this = $( this );
		var a = [];
		a.push( _this.parent().data( 'id' ) );
		var url = 'include/20';
		var data = {
			'mailbox_id': a
		};
		var callback = function( html ) {
			_this.parent().removeClass( 'new-mail' ).addClass( 'read-mail' );
		};
		AJAX.post( url, data, callback );
		_this.parent().find( '.content' ).toggleClass( 'show' );
	} );
	
	/*Load more*/
	$( document ).off( 'click', '.loadmore-btn' ).on( 'click', '.loadmore-btn', function() {
		$( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i> Đang tải' );
		var offset =  $( this ).data( 'offset' );
		offset = parseInt( offset ) + 5;
		var url = 'include/25';
		var data = {
			'offset': offset	
		};
		var _this = $( this );
		var li = $( '.mailbox-list' );
		var callback = function( html ) {
			$( '.loadmore-btn' ).removeClass( 'saving' ).html( 'Tải thêm' );
			if( html.search( 'empty' ) === -1 ) {
				li.append( html );
				_this.data( 'offset', offset );
				var row = li.find( 'li' ).length;
				row = parseInt( row );
				$( '.viewing' ).text( row );
			}
			else {
				var emptyRow = '<li><span>Đã tải hết tất cả</span></li>';
				li.find( '> li:last-child' ).after( emptyRow );
				_this.remove();
			}
		};
		AJAX.post( url, data, callback );
	} )
} );