<?php
	if( isset( $_POST['mailbox_id'] ) ) {
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		$sql = new Sql();
		
		foreach( $_POST['mailbox_id'] as $id ) {
			$data = array(
				'mailbox_id' => $id
			);
			$r = $exec -> get( $sql -> get( 153 ), $data );
			$stt = $r[0]['mailbox_important'];
			$mailbox_important = $stt ? 0 : 1;
			$save = array(
				'mailbox_id' => $id,
				'mailbox_important' => $mailbox_important
			);
			$exec -> exec( $sql -> get( 152 ), $save );
		}
	}
?>