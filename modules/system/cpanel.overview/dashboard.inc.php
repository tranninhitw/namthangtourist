<?php
	$exec = new Exec( HOST, USER, PASS, DBNAME );
	$sql = new Sql();
	
	//Đơn hàng
	$orders = $exec -> get( $sql -> get( 194 ), array(
		':order_time' => strtotime( date( 'd-m-Y' ) )
	) );
	$orderNew = $orderWait = $orderLoad = $orderSuccess = 0;
	foreach( $orders as $key => $o ) {
		switch( $o['state_id'] ) {
			case 1:
				$orderNew++;
				break;
			case 2:
				$orderWait++;
				break;
			case 3:
				$orderLoad++;
				break;
			case 4:
				$orderSuccess++;
				break;
		}
	}
	
	//Doanh thu
	$revenues = $exec -> get( $sql -> get( 199 ), array(
		':order_time' => strtotime( date( 'd-m-Y' ) ),
	) );
	
	$revenue = $revenues[0]['revenue'] == null ? 0 : $revenues[0]['revenue'];
	
	//Tài khoản
	$accounts = $exec -> get( $sql -> get( 200 ), array(
		':user_started' => strtotime( date( 'd-m-Y' ) ),
	) );
	
	$account = $accounts[0]['account'] == null ? 0 : $accounts[0]['account'];
	
	$html = '
		<div class="overview-order-new-status status-panel">
			<span class="status-number">' . $orderNew . '</span>
			<p class="status-title">Đơn hàng mới</p>
			<p class="status-link">
				<a href="/admin/banhang/dondathang?sort_by_money=asc&filter_by_state=1&filter_by_time_from=&filter_by_time_to=&filter_by_shipping_time_from=&filter_by_shipping_time_to=&filter=true">Xử lý đơn hàng</a>
			</p>
		</div>
		<div class="overview-order-waitting-status status-panel">
			<span class="status-number">' . $orderWait . '</span>
			<p class="status-title">Đơn hàng chờ giao</p>
			<p class="status-link">
				<a href="/admin/banhang/dondathang?sort_by_money=asc&filter_by_state=2&filter_by_time_from=&filter_by_time_to=&filter_by_shipping_time_from=&filter_by_shipping_time_to=&filter=true">Xử lý đơn hàng</a>
			</p>
		</div>
		<div class="overview-order-loading-status status-panel">
			<span class="status-number">' . $orderLoad . '</span>
			<p class="status-title">Đơn hàng đang giao</p>
			<p class="status-link">
				<a href="/admin/banhang/dondathang?sort_by_money=asc&filter_by_state=3&filter_by_time_from=&filter_by_time_to=&filter_by_shipping_time_from=&filter_by_shipping_time_to=&filter=true">Xử lý đơn hàng</a>
			</p>
		</div>
		<div class="overview-order-success-status status-panel">
			<span class="status-number">' . $orderSuccess . '</span>
			<p class="status-title">Đơn hàng đã giao</p>
			<p class="status-link">
				<a href="/admin/banhang/dondathang?sort_by_money=asc&filter_by_state=4&filter_by_time_from=&filter_by_time_to=&filter_by_shipping_time_from=&filter_by_shipping_time_to=&filter=true">Xem đơn hàng</a>
			</p>
		</div>
		<div class="overview-sale-status status-panel">
			<span class="status-number">' . number_format( $revenue, 0, ',', '.' ) . '</span>
			<p class="status-title">Doanh thu hôm nay</p>
			<p class="status-link">
				<a href="/admin/thongke/doanhso">Xem chi tiết</a>
			</p>
		</div>
		<div class="overview-member-status status-panel">
			<span class="status-number">' . $account . '</span>
			<p class="status-title">Tài khoản mới trong ngày</p>
			<p class="status-link">
				<a href="/admin/khachhang/khachhang?sort_by_money=asc&filter_by_type=all&filter_by_city=0&filter_by_time_from=' . date( 'd/m/Y', time() ) . '&filter_by_time_to=' . date( 'd/m/Y', time() + 86400 ) . '&filter=true">Xem danh sách</a>
			</p>
		</div>
	';
	echo $html;
?>