<?php
	$exec = new Exec( HOST, USER, PASS, DBNAME );
	$sql = new Sql();
	
	$html = '';
	if( isset( $_POST['offset'] ) ) {
		$offset = (int)$_POST['offset'];
	}
	else $offset = 0;
	$r = $exec -> get( $sql -> get( 150 ), array(
		':offset' => $offset
	), true );
	if( count( $r ) !== 0 ) {
		foreach( $r as $key => $m ) {
			$mailbox_seen = $m['mailbox_seen'] ? 'new-mail' : 'read-mail';
			$important = $m['mailbox_important'] ? 'important' : 'normal';
			$html .= '
				<li class="mailbox-mail ' . $mailbox_seen . '" data-id="' . $m['mailbox_id'] . '">
					<input type="checkbox" class="mail-selector" />
					<span class="mail-' . $important . ' icon"></span>
					<span class="mail-author">' . $m['mailbox_sender_email'] . '</span>
					<span class="mail-title">' . $m['mailbox_title'] . '</span>
					<span class="mail-compact">' . strip_tags( $m['mailbox_contents'] ) . '</span>
					<span class="content">
						<span class="time">Gửi lúc: ' . date( 'd/m/Y H:s:i', $m['mailbox_time'] ) . '</span>
						' . $m['mailbox_contents'] . '						
					</span>
					
				</li>
			';
		}		
	} else {
		$html .= '
			<li class="mailbox-mail empty">
				<span>Hộp thư trống</span>
			</li>
		';
	}
	echo $html;
?>