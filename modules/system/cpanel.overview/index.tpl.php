{@style}
<section class="module-wrapper">
	<section class="module-toolbar">
		<h3 class="module-heading">Trang tổng quan <small>Cpanel</small></h3>
	</section>
	<section class="module-content">
		<div class="overview-panel">
			{@dashboard}
		</div>
		<div class="tabs overview-mailbox">
			<h3 class="mailbox-heading">Hộp thư đến:</h3>
			<div class="mailbox-toolbar">
				<ul class="mailbox-toolbar-list">
					<li class="mailbox-toolbar-item">
						<input type="checkbox" title="Chọn tất cả" />
					</li>
					<li class="mailbox-toolbar-item">
						<button id="del">Xóa</button>
					</li>
					<li class="mailbox-toolbar-item">
						<button id="read">Đã đọc</button>
					</li>
					<li class="mailbox-toolbar-item">
						<button id="important">Quan trọng</button>
					</li>
				</ul>
			</div>
			<ul class="mailbox-list">
				{@list}
			</ul>
			{@pager}
		</div>
	</section>
</section>
{@script}