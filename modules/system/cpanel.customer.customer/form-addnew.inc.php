<?php
	$exec = new Exec( HOST, USER, PASS, DBNAME );
	$sql = new Sql();
	
	$data = array(
		':setting_name' => 'city'
	);
	$r = $exec -> get( $sql -> get( 10 ), $data );
	$cities = $r[0]['setting_value'];
	$cities = json_decode( $cities, true );
	
	if( isset( $_GET['action'], $_GET['id'] ) && $_GET['action'] == 'edit' ) {
		$city_b = '';
		//Get customer		
		$data = array(
			':user_id' => (int)$_GET['id']
		);
		$customer = $exec -> get( $sql -> get( 111 ), $data );
		$customer = $customer[0];
		$address = json_decode( $customer['user_address_book'], true );
		$point = json_decode( $customer['user_points'], true );
		$point = $point['balance'] == '' ? 0 : $point['balance'];
		$check = $customer['group_id'] ? 'checked' : '';
		
		foreach( $cities as $key => $arr ) {
			$selected = '';
			if( $address['payment']['city'] == $arr['id'] ) {
				$selected = 'selected';
			}
			$city_b .= '<option value="' . $arr['id'] . '" ' . $selected . '>' . $arr['name'] . '</option>';
		};
		
		$html = '
			<div class="tabs module-addnew">
				<span class="tab-title">Sửa khách hàng</span>
				<div class="module-addnew-btn">
					<span class="buttons disabled-buttons customer-save-new save-btn"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</span>
					<span class="buttons disabled-buttons customer-cancel-new"><i class="fa fa-undo" aria-hidden="true"></i> Hủy</span>
				</div>			
				<table class="form-tables tbl">
					<tr>
						<td>Là nhân viên Manulife?</td>
						<td>
							<input id="get-group" name="get_group" type="checkbox" class="inputs checkboxes customer-textbox" value="1" ' . $check . '/>
							<label for="get-group"></label>
						</td>
					</tr>
					<tr>
						<td>Tên khách hàng</td>
						<td>
							<input name="customer_fullname" type="text" class="inputs customer-textbox" placeholder="Tên khách hàng" value="' . $customer['user_fullname'] . '"/>
						</td>
					</tr>
					<tr>
						<td>Tên đăng nhập (*)</td>
						<td>
							<input name="customer_username" type="text" class="inputs required-inputs customer-textbox" placeholder="Tên đăng nhập" value="' . $customer['user_username'] . '"/>
						</td>
					</tr>
					<tr>
						<td>Email</td>
						<td>
							<input name="customer_email" type="email" class="inputs customer-textbox" placeholder="Email" value="' . $customer['user_email'] . '"/>
						</td>
					</tr>
					<tr>
						<td>Số điện thoại (*)</td>
						<td>
							<input name="customer_mobile" type="tel" class="inputs required-inputs customer-textbox" value="' . $customer['user_mobile'] . '">
						</td>
					</tr>
					<tr>
						<td>Tài khoản thưởng</td>
						<td>
							<span class="currency-input-wrapper"><input name="customer_points" type="text" class="inputs customer-textbox currency-inputs" value="' . number_format( $point, 0, ',', '.' ) . '"></span>
						</td>
					</tr>
					<tr>
						<td>Tỉnh / Thành phố (*)</td>
						<td>
							<select class="city inputs selects customer-textbox required-inputs" name="customer_city">
								<option value="">Tỉnh / Thành phố</option>
								' . $city_b . '
							</select>
						</td>
					</tr>
				</table>			
			</div>
		';
	} else {
		$city_a = '';
		foreach( $cities as $key => $arr ) {
			$city_a .= '<option value="' . $arr['id'] . '">' . $arr['name'] . '</option>';
		};
		$html = '
			<div class="tabs module-addnew">
				<span class="tab-title">Thêm khách hàng</span>
				<div class="module-addnew-btn">
					<span class="buttons disabled-buttons customer-save-new save-btn"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</span>
					<span class="buttons disabled-buttons customer-cancel-new"><i class="fa fa-undo" aria-hidden="true"></i> Hủy</span>
				</div>			
				<table class="form-tables tbl">
					<tr>
						<td>Là nhân viên?</td>
						<td>
							<input id="get-group" name="get_group" type="checkbox" class="inputs checkboxes customer-textbox" value="1"/>
							<label for="get-group"></label>
						</td>
					</tr>
					<tr>
						<td>Tên khách hàng</td>
						<td>
							<input name="customer_fullname" type="text" class="inputs customer-textbox" placeholder="Tên khách hàng"/>
						</td>
					</tr>
					<tr>
						<td>Tên đăng nhập (*)</td>
						<td>
							<input name="customer_username" type="text" class="inputs required-inputs customer-textbox" placeholder="Tên đăng nhập"/>
						</td>
					</tr>
					<tr>
						<td>Email</td>
						<td>
							<input name="customer_email" type="email" class="inputs customer-textbox" placeholder="Email"/>
						</td>
					</tr>
					<tr>
						<td>Số điện thoại (*)</td>
						<td>
							<input name="customer_mobile" type="tel" class="inputs required-inputs customer-textbox" placeholder="Số điện thoại"/>
						</td>
					</tr>
					<tr>
						<td>Tài khoản thưởng</td>
						<td>
							<span class="currency-input-wrapper"><input name="customer_points" type="text" class="inputs customer-textbox currency-inputs"></span>
						</td>
					</tr>
					<tr>
						<td>Tỉnh / Thành phố (*)</td>
						<td>
							<select class="city inputs selects customer-textbox required-inputs" name="customer_city">
								<option value="">Tỉnh / Thành phố</option>
								' . $city_a . '
							</select>
						</td>
					</tr>
				</table>			
			</div>
		';
	}
	echo $html;
?>