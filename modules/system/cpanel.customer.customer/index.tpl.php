{@style}
<section class="module-wrapper">
	<section class="module-toolbar">
		<h3 class="module-heading">Khách hàng / Khách hàng <small>Cpanel</small></h3>
		<div class="module-button-area">
			<input class="inputs customer-search-textbox" placeholder="Họ tên, điện thoại, email (enter)" />
			<span class="buttons normal-buttons customer-addnew-btn"><i class="fa fa-plus" aria-hidden="true"></i> Thêm</span>
			<span class="buttons normal-buttons customer-filter-btn"><i class="fa fa-filter" aria-hidden="true"></i> Lọc</span>
			<span class="buttons disabled-buttons customer-cancel-btn"><i class="fa fa-undo" aria-hidden="true"></i> Hủy</span>
		</div>
	</section>
	<section class="module-content">
		<div class="tabs module-filter">
			<span class="tab-title">Lọc và sắp xếp</span>
			<div class="module-addnew-btn">
				<span class="buttons disabled-buttons filter-apply-btn save-btn"><i class="fa fa-floppy-o" aria-hidden="true"></i> Áp dụng</span>
				<a href="include/50?export=xls" target="_blank" class="buttons normal-buttons export-xls"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Xuất excel</a>
			</div>
			{@filter-form}
		</div>
		{@form-addnew}
		<div class="customer-list-wrapper">
			<table class="tables module customer-table">
				<tr class="title-rows">
					<td>Tên đăng nhập</td>
					<td>Họ tên</td>
					<td>Loại</td>
					<td>Tỉnh</td>
					<td>SĐT</td>
					<td>Tài khoản thưởng</td>
					<td>Biên tập</td>
				</tr>
				{@list}
			</table>
		</div>
		{@pager}
	</section>
</section>
{@script}