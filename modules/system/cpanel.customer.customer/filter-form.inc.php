<?php 
	$exec = new Exec( HOST, USER, PASS, DBNAME );
	$sql = new Sql();
	
	$city = $exec -> get( $sql -> get( 157 ), array(
		':setting_name' => 'city'
	) );
	$city = $city[0]['setting_value'];
	$city = json_decode( $city, true );
	$cityHtml = '';
	foreach( $city as $key => $arr ) {
		$cityHtml .= '
			<option value="' . $arr['id'] . '">' . $arr['name'] . '</option>
		';
	}
	if( !isset( $_GET['filter'] ) ) {		
		$html = '
			<table class="form-tables">
				<tr>
					<td>Tài khoản thưởng</td>
					<td>
						<input name="sort_by_money" value="asc" id="sort-by-money-asc" type="radio" class="radios filter-textbox" checked />
						<label for="sort-by-money-asc"></label> <span class="label">Tăng dần</span>
						<input name="sort_by_money" value="desc" id="sort-by-money-desc" type="radio" class="radios filter-textbox" />
						<label for="sort-by-money-desc"></label> <span class="label">Giảm dần</span>
					</td>
				</tr>
				<tr>
					<td>Loại tài khoản</td>
					<td>
						<select name="filter_by_type" class="selects filter-textbox">
							<option value="all">Tất cả</option>
							<option value="0">Khách hàng</option>
							<option value="1">Nhân viên Manulife</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Tỉnh</td>
					<td>
						<select name="filter_by_city" class="selects filter-textbox">
							<option value="0">Tất cả</option>
							' . $cityHtml . '
						</select>
					</td>
				</tr>
				<tr>
					<td>Ngày đăng ký</td>
					<td>
						<span class="label">Từ: </span><input type="text" class="inputs datetime-picker filter-textbox" name="filter_by_time_from" placeholder="dd/mm/yyyy" />
						<span class="label">đến: </span><input type="text" class="inputs datetime-picker filter-textbox" name="filter_by_time_to" placeholder="dd/mm/yyyy" />
					</td>
				</tr>
			</table>
		';
	}
	else {
		//Get $_GET
		$sortByMoney = strip_tags( $_GET['sort_by_money'] );
		$filterByType = strip_tags( $_GET['filter_by_type'] );
		$filterByCity = strip_tags( $_GET['filter_by_city'] );
		$filterByTimeFrom = strip_tags( $_GET['filter_by_time_from'] );
		$filterByTimeTo = strip_tags( $_GET['filter_by_time_to'] );
		
		//Get data for each textbox
		$check1 = $sortByMoney == 'asc' ? 'checked' : '';
		$check2 = $sortByMoney == 'desc' ? 'checked' : '';
		
		$select1 = array( 'all' => '', '0' => '', '1' => '' );
		$select1[$filterByType] = 'selected';
		
		$select2 = $filterByCity == '0' ? 'selected' : '';
		$cityHtml = '<option value="0" ' . $select2 . '>Tất cả</option>';
		foreach( $city as $key => $arr ) {
			if( $filterByCity == $arr['id'] ) {
				$cityHtml .= '
					<option value="' . $arr['id'] . '" selected>' . $arr['name'] . '</option>
				';
			}
			else {
				$cityHtml .= '
					<option value="' . $arr['id'] . '">' . $arr['name'] . '</option>
				';
			}
		}
		
		$html = '
			<table class="form-tables">
				<tr>
					<td>Tài khoản thưởng</td>
					<td>
						<input name="sort_by_money" value="asc" id="sort-by-money-asc" type="radio" class="radios filter-textbox" ' . $check1 . ' />
						<label for="sort-by-money-asc"></label> <span class="label">Tăng dần</span>
						<input name="sort_by_money" value="desc" id="sort-by-money-desc" type="radio" class="radios filter-textbox" ' . $check2 . ' />
						<label for="sort-by-money-desc"></label> <span class="label">Giảm dần</span>
					</td>
				</tr>
				<tr>
					<td>Loại tài khoản</td>
					<td>
						<select name="filter_by_type" class="selects filter-textbox">
							<option value="all" ' . $select1['all'] . '>Tất cả</option>
							<option value="0" ' . $select1[0] . '>Khách hàng</option>
							<option value="1" ' . $select1[1] . '>Nhân viên Manulife</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Tỉnh</td>
					<td>
						<select name="filter_by_city" class="selects filter-textbox">
							' . $cityHtml . '
						</select>
					</td>
				</tr>
				<tr>
					<td>Ngày đăng ký</td>
					<td>
						<span class="label">Từ: </span><input type="text" value="' . $filterByTimeFrom . '" class="inputs datetime-picker filter-textbox" name="filter_by_time_from" placeholder="dd/mm/yyyy" />
						<span class="label">đến: </span><input type="text" value="' . $filterByTimeTo . '" class="inputs datetime-picker filter-textbox" name="filter_by_time_to" placeholder="dd/mm/yyyy" />
					</td>
				</tr>
			</table>
		';
	}
	
	echo $html;
?>