<?php
	function filter( $exec, $sql ) {
		//Update table dp_users (user_city / user_balance)
		$allUsers = $exec -> get( $sql -> get( 211 ) );
		foreach( $allUsers as $key => $user ) {
			$userCity = json_decode( $user['user_address_book'], true );
			$userCity = $userCity['payment']['city'];
			
			$userBalance = json_decode( $user['user_points'], true );
			$userBalance = $userBalance['balance'];
			
			$exec -> exec( $sql -> get( 210 ), array(
				':user_balance' => $userBalance,
				':user_city' => $userCity,
				':user_id' => $user['user_id']
			) );
		}
		
		
		//Get $_GET
		$sortByMoney = strip_tags( $_GET['sort_by_money'] );
		$filterByType = strip_tags( $_GET['filter_by_type'] );
		$filterByCity = strip_tags( $_GET['filter_by_city'] );
		$filterByTimeFrom = strip_tags( $_GET['filter_by_time_from'] );
		$filterByTimeTo = strip_tags( $_GET['filter_by_time_to'] );
		
		//Convert to time
		$filterByTimeFrom = strtotime( str_replace( '/', '-', $filterByTimeFrom ) );
		$filterByTimeTo = strtotime( str_replace( '/', '-', $filterByTimeTo ) );
		
		//Get SQL query
		$sqlText1 = $filterByType != 'all' ? ' group_id = ' . (int)$filterByType : ' 1=1';
		$sqlText2 = $filterByCity != 0 ? ' AND user_city IS NOT NULL AND user_city = ' . (int)$filterByCity : '';
		$sqlText3 = $filterByTimeFrom > 0 ? ' AND user_started >= ' . $filterByTimeFrom : '';
		$sqlText4 = $filterByTimeTo > 0 ? ' AND user_started <= ' . $filterByTimeTo : '';
		$sqlText5 = $sortByMoney == 'asc' ? ' ORDER BY user_balance ASC' : ' ORDER BY user_balance DESC';
		$sqlTextS = isset( $_GET['s'] ) && !empty( $_GET['s'] ) ? ' AND ( user_fullname REGEXP "[[:<:]]' . $_GET['s'] . '[[:>:]]" OR user_mobile = "' . $_GET['s'] . '" OR user_email REGEXP "[[:<:]]' . $_GET['s'] . '[[:>:]]" )' : '';
		
		//Page seperatoring
		$offset = isset( $_GET['offset'] ) ? $_GET['offset'] : 0;
		$totalRows = $exec -> get( "SELECT COUNT(*) AS rows FROM dp_users WHERE" . $sqlText1 . $sqlText2 . $sqlText3 . $sqlText4 . $sqlTextS );
		$totalRows = $totalRows[0]['rows'];
		$_SESSION['total_rows'] = $totalRows;
		$sqlText = "SELECT * FROM dp_users WHERE" . $sqlText1 . $sqlText2 . $sqlText3 . $sqlText4 . $sqlText5 . $sqlTextS . " LIMIT 5 OFFSET :offset";
		$customers = $exec -> get( $sqlText, array(
			':offset' => (int)$offset
		), true );
		
		return $customers;
	}
	
	$exec = new Exec( HOST, USER, PASS, DBNAME );
	$sql = new Sql();
	
	if( isset( $_GET['offset'] ) ) {
		$offset = (int)$_GET['offset'];
	}
	else $offset = 0;
	
	//Get city
	$data = array(
		'setting_name' => 'city'
	);
	$cities = $exec -> get( $sql -> get( 157 ), $data );
	$cities = $cities[0]['setting_value'];
	$cities = json_decode( $cities, true );
	
	$html = '';
	if( isset( $_GET['s'] ) ) {
		//Apply filter
		if( isset( $_GET['filter'] ) && $_GET['filter'] == 'true' ) {
			$customers = filter( $exec, $sql );
		}
		
		//Only search
		else {
			$offset = isset( $_GET['offset'] ) ? $_GET['offset'] : 0;
			$totalRows = $exec -> get( $sql -> get( 213 ), array(
				':keyword' => "[[:<:]]" . strip_tags( $_GET['s'] ) . "[[:>:]]",
				':keyword3' => "[[:<:]]" . strip_tags( $_GET['s'] ) . "[[:>:]]",
				':keyword2' => strip_tags( $_GET['s'] ),
			) );
			$totalRows = $totalRows[0]['rows'];
			$_SESSION['total_rows'] = $totalRows;
			$customers = $exec -> get( $sql -> get( 212 ), array(
				':keyword' => "[[:<:]]" . strip_tags( $_GET['s'] ) . "[[:>:]]",
				':keyword3' => "[[:<:]]" . strip_tags( $_GET['s'] ) . "[[:>:]]",
				':keyword2' => strip_tags( $_GET['s'] ),
				':offset' => (int)$offset
			), true );
		}
		if( count( $customers ) == 0 ) {
			$html = '
				<tr>
					<td colspan="7">Không tìm thấy khách hàng có tên, điện thoại hoặc email là <i>' . $_GET['s'] . '</i></td>
				</tr>
			';
		}
	}
	
	//No search
	else {
		//Apply filter
		if( isset( $_GET['filter'] ) && $_GET['filter'] == 'true' ) {
			$customers = filter( $exec, $sql );
		}
		
		//No apply filter
		else {
			//Get customer
			$customers = $exec -> get( $sql -> get( 110 ), array( 
				':offset' => $offset
			), true );
	
		}
		
		if( count( $customers ) == 0 ) {
			$html = '
				<tr>
					<td colspan="7">Chưa có khách hàng nào</td>
				</tr>
			';
		}
	}
	
	if( count( $customers ) !== 0 ) {
		foreach( $customers as $key => $customer ) {
			$type = $customer['group_id'] == 1 ? 'Nhân viên' : 'Khách hàng';
			$address = json_decode( $customer['user_address_book'], true );
			$point = json_decode( $customer['user_points'], true );
			$point = $point['balance'] == '' ? 0 : $point['balance'];
			$city_name = '';
			foreach( $cities as $key => $city ) {
				if( $address['payment']['city'] == $city['id'] ) {
					$city_name = $city['name'];
					break;
				}
			}
			$block = $customer['user_block'] ? 'class="block"' : '';
			$button = $customer['user_block'] ? 'fa-unlock' : '';
			$html .= '
				<tr ' . $block . '>
					<td>' . $customer['user_username'] . '</td>
					<td>' . $customer['user_fullname'] . '</td>
					<td>' . $type . '</td>
					<td>' . $city_name . '</td>
					<td>' . $customer['user_mobile'] . '</td>
					<td>' . number_format( $point, 0, ',', '.' ) . ' VNĐ</td>
					<td>
						<button data-href="' . _WWW . '/admin/khachhang/khachhang?action=edit&id=' . $customer['user_id'] . '"class="mini-buttons normal-buttons edit-customer-btn" title="Sửa"><i class="fa fa-pencil" aria-hidden="true"></i></button>
						<button class="mini-buttons normal-buttons block-customer-btn" title="Khóa" data-id="' . $customer['user_id'] . '"><i class="fa fa-lock ' . $button . '" aria-hidden="true"></i></button>
					</td>
				</tr>
			';
		}
	}
	
	echo $html;
?>