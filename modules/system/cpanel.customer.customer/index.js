$( document ).ready( function() {
	/*PARAMS*/
	PARAMS = ULTI.queryString();
	
	/*When click addnew*/
	$( document ).on( 'click', '.customer-addnew-btn', function() {
		MODULE.cmd( 'addnew', $( '.customer-cancel-btn' ) );
	} );
	
	/*When click cancel*/
	$( document ).on( 'click', '.customer-cancel-btn', function() {
		$( '.selects option' ).prop( 'selected', false );
		MODULE.cmd( 'cancel', $( '.customer-cancel-btn' ) );		
		$( '.module-filter' ).hide();
		//Disable save buttons
		DOM.disableSaveBtn();
	} );
	
	/*On input customer-textbox*/
	$( document ).on( 'input change', '.customer-textbox', function() {
		var allDone = ULTI.validate( $( '.module-addnew .required-inputs' ), 'empty' );
		if( allDone ) {
			DOM.enableSaveBtn();
		}
		else {
			DOM.disableSaveBtn();
		}
	} );
	
	/*When click edit*/
	$( document ).on( 'click', '.edit-customer-btn', function() {
		//Load tpl
		DOM.loadTplThisEl( $( this ) );
	} );
	
	/*When edit*/
	$( document ).ready( function() {
		if( PARAMS.action == 'edit' ) {		
			//Fade In addnew form
			$( '.customer-addnew-btn' ).click();
		}
	} );
	
	/*Save new customer*/
	$( document ).off( 'click', '.customer-save-new' ).on( 'click', '.customer-save-new', function() {
		$( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i> Đang lưu' );
		//Save data
		var data = $( '.module-addnew .customer-textbox' ).serialize();
		if( PARAMS.action == 'edit' ) {
			data += '&action=edit&id=' + PARAMS.id;
		}
		
		var url = 'include/18';
		var callback = function( html ) {
			htmlArr = html.split( '|' );
			if( htmlArr[0] == '1' ) {
				ULTI.done( htmlArr[1] );
			}
			else {
				DOM.showStatus( htmlArr[1], parseInt( htmlArr[0] ) );
				$( '.question-save-new' ).removeClass( 'saving' ).html( '<i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu' );
			}
		};
		AJAX.post( url, data, callback );
	} );
	
	/*When click block*/
	$( document ).off( 'click', '.block-customer-btn' ).on( 'click', '.block-customer-btn', function() {
		var _this = $( this );
		var data = {
			'user_id': $( this ).data( 'id' )
		};
		var url = 'include/27';
		var callback = function( html ) {
			ULTI.done( html );
		};
		AJAX.post( url, data, callback );
	} );
	
	/*Click filter button*/
	$( document ).off( 'click', '.customer-filter-btn' ).on( 'click', '.customer-filter-btn', function(){
		$( '.module-filter' ).show();
		$( '.customer-cancel-btn' ).removeClass( 'disabled-buttons' ).addClass( 'cancel-buttons' );
	} );
	
	/*Load more*/
	$( document ).off( 'click', '.loadmore-btn' ).on( 'click', '.loadmore-btn', function() {
		$( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i> Đang tải' );
		var offset =  $( this ).data( 'offset' );
		offset = parseInt( offset ) + 5;
		var url = 'include/28?' + location.href.split( '?' )[1];
		var data = {
			'offset': offset	
		};
		var _this = $( this );
		var tbl = $( '.customer-table' );
		var callback = function( html ) {
			$( '.loadmore-btn' ).removeClass( 'saving' ).html( 'Tải thêm' );
			if( html.search( 'colspan="7"' ) === -1 ) {
				tbl.find( 'tbody' ).append( html );
				_this.data( 'offset', offset );
				var row = tbl.find( 'tr' ).length;
				row = parseInt( row ) - 1;
				$( '.viewing' ).text( row );
			}
			else {
				var emptyRow = '<tr><td colspan="7">Đã tải hết tất cả</td></tr>';
				tbl.find( 'tr:last-of-type' ).after( emptyRow );
				_this.remove();
			}
		};
		AJAX.get( url, data, callback );
	} );
	
	/*When change filter-textbox*/
	$( document ).off( 'change input', '.filter-textbox' ).on( 'change input', '.filter-textbox', function() {
		$( '.filter-apply-btn' ).removeClass( 'disabled-buttons' ).addClass( 'save-buttons' );
	} );
	
	/*Click filter-apply button*/
	$( document ).off( 'click', '.filter-apply-btn' ).on( 'click', '.filter-apply-btn', function() {
		var data = $( '.filter-textbox' ).serialize() + '&filter=true';
		if( PARAMS.s ) {
			data += '&s=' + PARAMS.s;
		}
		DOM.loadTplThisEl( '/admin/khachhang/khachhang?' + data );
	} );
	
	/*Search order*/
	$( document ).off( 'keyup', '.customer-search-textbox' ).on( 'keyup', '.customer-search-textbox', function( e ) {
		if( e.which == 13 || e.keyCode == 13 ) {
			var value = $( this ).val();
			if( value ) {
				DOM.loadTplThisEl( '/admin/khachhang/khachhang?s=' + encodeURIComponent( value ) );
			}
		}
	} );
	
	/*Datetimepciker*/
	$( document ).ready( function() {
		$.datetimepicker.setLocale( 'vi' );
		jQuery( '.datetime-picker' ).datetimepicker({
		  timepicker:false,
		  format:'d/m/Y'
		});
	} );
	
	/*Change href for export xls*/
	$( document ).ready( function() {
		if( PARAMS.filter == 'true' ) {
			$( '.export-xls' ).prop( 'href', 'include/50?' + location.href.split( '?' )[1] + '&export=xls' );
		}
	} );
} );