<?php
	//Turn off autoload
	$autoloadFuncs = spl_autoload_functions();
	foreach($autoloadFuncs as $unregisterFunc)
	{
		spl_autoload_unregister($unregisterFunc);
	}
	require_once _CORE . '/phpexcel.cls.php';
	function filter( $exec, $sql ) {
		//Update table dp_users (user_city / user_balance)
		$allUsers = $exec -> get( $sql -> get( 211 ) );
		foreach( $allUsers as $key => $user ) {
			$userCity = json_decode( $user['user_address_book'], true );
			$userCity = $userCity['payment']['city'];
			
			$userBalance = json_decode( $user['user_points'], true );
			$userBalance = $userBalance['balance'];
			
			$exec -> exec( $sql -> get( 210 ), array(
				':user_balance' => $userBalance,
				':user_city' => $userCity,
				':user_id' => $user['user_id']
			) );
		}
		
		
		//Get $_GET
		$sortByMoney = strip_tags( $_GET['sort_by_money'] );
		$filterByType = strip_tags( $_GET['filter_by_type'] );
		$filterByCity = strip_tags( $_GET['filter_by_city'] );
		$filterByTimeFrom = strip_tags( $_GET['filter_by_time_from'] );
		$filterByTimeTo = strip_tags( $_GET['filter_by_time_to'] );
		
		//Convert to time
		$filterByTimeFrom = strtotime( str_replace( '/', '-', $filterByTimeFrom ) );
		$filterByTimeTo = strtotime( str_replace( '/', '-', $filterByTimeTo ) );
		
		//Get SQL query
		$sqlText1 = $filterByType != 'all' ? ' group_id = ' . (int)$filterByType : ' 1=1';
		$sqlText2 = $filterByCity != 0 ? ' AND user_city IS NOT NULL AND user_city = ' . (int)$filterByCity : '';
		$sqlText3 = $filterByTimeFrom > 0 ? ' AND user_started >= ' . $filterByTimeFrom : '';
		$sqlText4 = $filterByTimeTo > 0 ? ' AND user_started <= ' . $filterByTimeTo : '';
		$sqlText5 = $sortByMoney == 'asc' ? ' ORDER BY user_balance ASC' : ' ORDER BY user_balance DESC';
		$sqlTextS = isset( $_GET['s'] ) && !empty( $_GET['s'] ) ? ' AND ( user_fullname REGEXP "[[:<:]]' . $_GET['s'] . '[[:>:]]" OR user_mobile = "' . $_GET['s'] . '" OR user_email REGEXP "[[:<:]]' . $_GET['s'] . '[[:>:]]" )' : '';
		
		//Page seperatoring
		$sqlText = "SELECT * FROM dp_users WHERE" . $sqlText1 . $sqlText2 . $sqlText3 . $sqlText4 . $sqlText5 . $sqlTextS;
		$customers = $exec -> get( $sqlText );
		
		return $customers;
	}
	
	//Get order from db
	if( isset( $_GET['export'] ) && $_GET['export'] == 'xls' ) {
		
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		$sql = new Sql();
		
		//If not apply filter
		if( !isset( $_GET['filter'] ) ) {
			$customers = $exec -> get( $sql -> get( 215 ) );
		}
		else {
			$customers = filter( $exec, $sql );
		}
	}
	
	//Build xls data
	$objPHPExcel = new PHPExcel();
	
	//Set title of document
	$subHeading = 'Từ trước đến nay';
	if( isset( $_GET['filter_by_time_from'] ) && !empty( $_GET['filter_by_time_from'] ) ) {
		$toText = isset( $_GET['filter_by_time_to'] ) && !empty( $_GET['filter_by_time_to'] ) ? ' đến ' . $_GET['filter_by_time_to'] : '';
		$subHeading = 'Từ ngày ' . $_GET['filter_by_time_from'] . $toText;
	}
	$objPHPExcel -> getActiveSheet() 
		-> setCellValue( 'A1', 'BÁO CÁO KHÁCH HÀNG' )
		-> setCellValue( 'A2', $subHeading );
		
	$objPHPExcel -> getActiveSheet() -> mergeCells( 'A1:H1' );
	$objPHPExcel -> getActiveSheet() -> mergeCells( 'A2:H2' );
	$objPHPExcel -> getActiveSheet() 
		-> setCellValue( 'A3', 'STT' )
		-> setCellValue( 'B3', 'Họ tên' )
		-> setCellValue( 'C3', 'Loại' )
		-> setCellValue( 'D3', 'Tỉnh' )
		-> setCellValue( 'E3', 'Số điện thoại' )
		-> setCellValue( 'F3', 'Email' )
		-> setCellValue( 'G3', 'Tài khoản' )
		-> setCellValue( 'H3', 'Điểm thưởng' )
		-> setCellValue( 'I3', 'Ngày đăng ký' );
	$columns = range( 'A', 'I' );
	$titleRows = 4;
	
	//Get city
	$data = array(
		'setting_name' => 'city'
	);
	$cities = $exec -> get( $sql -> get( 157 ), $data );
	$cities = $cities[0]['setting_value'];
	$cities = json_decode( $cities, true );
	
	foreach( $customers as $key => $customer ) {
		//Get type
		$type = $customer['group_id'] == 0 ? 'Khách hàng' : 'Nhân viên Manulife';
		
		//Get city name
		$cityName = '';
		$address = json_decode( $customer['user_address_book'], true );
		foreach( $cities as $key4 => $city ) {
			if( $address['payment']['city'] == $city['id'] ) {
				$cityName = $city['name'];
				break;
			}
		}
		
		//Get balance and point
		$points = json_decode( $customer['user_points'], true );
		$balance = 0;
		$point = 0;
		$balance = isset( $points['balance'] ) ? $points['balance'] : 0;
		$point = isset( $points['points'] ) ? $points['points'] : 0;
		
		//Rows for sheet
		$rows = array(
			$key + 1,
			$customer['user_fullname'],
			$type,
			$cityName,
			$customer['user_mobile'],
			$customer['user_email'],
			$balance,
			$point,
			date( 'd-m-Y', $customer['user_started'] )
		);
		foreach( $columns as $key1 => $column ) {
			$objPHPExcel -> getActiveSheet() -> setCellValue( $column . ( $key + $titleRows ), $rows[$key1] );
		}
	}
	
	//Styling sheet
	$styleArray = array(
		'font'  => array(
			'bold'  => false,
			'color' => array('rgb' => '515151'),
			'size'  => 12,
			'name'  => 'Arial',
		),
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);
	
	$objPHPExcel -> getActiveSheet() -> getStyle( 'A3:' . $objPHPExcel -> getActiveSheet() -> getHighestColumn() . $objPHPExcel -> getActiveSheet() -> getHighestRow() ) -> applyFromArray( $styleArray );
	
	//Styling heading
	$styleArray = array(
		'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => 'FF0000'),
			'size'  => 14,
			'name'  => 'Arial',
		),
		'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
	);
	$objPHPExcel -> getActiveSheet() -> getStyle( 'A1:I2' ) -> applyFromArray( $styleArray );
	
	//Styling heading of table
	$styleArray = array(
		'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => '515151'),
			'size'  => 12,
			'name'  => 'Arial',
		),
		'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        ),
		'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'BDF2FC')
        )
	);
	$objPHPExcel -> getActiveSheet() -> getStyle( 'A3:I3' ) -> applyFromArray( $styleArray );
	
	//Autosize column width
	PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
	foreach( range( 'A','I' ) as $columnID ) {
		$objPHPExcel -> getActiveSheet() -> getColumnDimension( $columnID ) -> setAutoSize( true );
	}
	
	// Rename worksheet
	$objPHPExcel->getActiveSheet()->setTitle('Khách hàng');

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Quatangtraotay-report.xls"');
	header('Cache-Control: max-age=0');
	
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;
?>