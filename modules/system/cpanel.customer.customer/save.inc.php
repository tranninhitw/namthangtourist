<?php 
	if( isset( $_POST['customer_username'] ) ) {
		$smtp = new Smtp( SMTP_HOST, SMTP_USER, SMTP_PASS, SMTP_PORT, MAIL_FROM, MAIL_REPLY_TO );
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		$sql = new Sql();
		$hasher = new Password( 8, false );
		
		//Get data
		foreach( $_POST as $key => $value ) {
			$$key = $value;
		}		
		
		$get_group = isset( $get_group ) ? $get_group : 0;
		
		$data = array(
			':group_id' => $get_group,
			':user_fullname' => $customer_fullname,
			':user_username' => $customer_username,
			':user_email' => empty( $customer_email ) ? null : $customer_email,
			':user_mobile' => $customer_mobile
		);
		
		if( isset( $action ) && $action == 'edit' ) {
			$data[':user_id'] = (int)$id;
			$customer = $exec -> get( $sql -> get( 111 ), array(
				':user_id' => (int)$id
			) );
			$customer = $customer[0];
			$address = json_decode( $customer['user_address_book'], true );
			$address['payment']['city'] = $customer_city;
			$data[':user_address_book'] = json_encode( $address );
			$points = json_decode( $customer['user_points'], true );
			$points['balance'] = str_replace( '.', '', $customer_points );
			$data[':user_points'] = json_encode( $points );
			$r = $exec -> exec( $sql -> get( 155 ), $data );
			if( $r ) {
				echo '1|Lưu thành công';
			} else {
				echo '0|Trùng tên đăng nhập, điện thoại hoặc email';
			}
		}
		else {			
			$pass = '';
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$pass = '';
			for ($i = 0; $i < 6; $i++) {
				$pass .= $characters[rand( 0, $charactersLength - 1 )];
			}
			$data[':user_password'] = $hasher -> HashPassword( $pass );
			$data[':user_started'] = time();
			//Generate string
			$string = str_shuffle( md5( microtime() ) );
			$data[':user_resetpass_string'] = $string;
			//Address book
			$user_address_book = array(
				'shipping' => array(
					'fullname' => '',					
					'mobile' => '',					
					'address' => '',					
					'city' => '',					
					'district' => '',					
				),
				'bill' => array(
					'company' => '',					
					'tax' => '',					
					'address' => ''				
				),
				'payment' => array(
					'fullname' => $customer_fullname,					
					'mobile' => $customer_mobile,					
					'address' => '',					
					'city' => $customer_city,					
					'district' => '',					
				)
			);
			$data[':user_address_book'] = json_encode( $user_address_book );
			//Point
			$user_points = array(
				'points' => '',
				'balance' => str_replace( '.', '', $customer_points )
			);
			$data[':user_points'] = json_encode( $user_points );
			$r = $exec -> add( $sql -> get( 156 ), $data );
			
			if( $r ) {
				//Send mail
				$sub = 'Bạn đã được cấp tài khoản tại Quatangtraotay.com.vn';
				$contents = '
					<tr>
						<td style="padding: 10px 40px; padding-bottom: 150px;">
							<p>Chúc mừng bạn đã được cấp tài khoản tại Quatangtraotay.com.vn</p>
							<p>Tên đăng nhập: <b>' . $customer_username . '</b>
							<p>Mật khẩu: <b>' . $pass . '</b></p>
							<p>Đăng nhập tại <a href="http://quatangtraotay.com.vn/taikhoan">đây</a></p>
							<p>Trân trọng cám ơn!</p>
						</td>
					</tr>
				';
				$emailTpl = file_get_contents( _MODULES . '/home/email.tpl.php' );
				$contents = str_replace( '{@contents}', $contents, $emailTpl );
				$r = $smtp -> send( $customer_email, $sub, $contents );
				if( $r ) {
					echo '1|Đăng ký thành công, vui lòng kiểm tra email';
				}
			}
			else {
				echo '0|Trùng email, tên đăng nhập hoặc số điện thoại';
			}
		}
	}
?>