$( document ).ready( function() {

    var PARAMS = ULTI.queryString();

    /*Show addnew form*/
    $( document ).off( 'click', '.place-addnew-btn' ).on( 'click', '.place-addnew-btn', function() {
        MODULE.cmd( 'addnew', $( '.place-cancel-btn' ) );
    } );

    /*Hide addnew form*/
    $( document ).off( 'click', '.place-cancel-btn' ).on( 'click', '.place-cancel-btn', function() {
        MODULE.cmd( 'cancel', $( '.place-cancel-btn' ) );
    } );

    /*Validate form*/
    $( document ).on( 'input change', '.place-textbox', function() {
        var allDone = ULTI.validate( $( '.module-addnew .required-inputs' ), 'empty' );
        if( allDone ) {
            DOM.enableSaveBtn();
        }
        else {
            DOM.disableSaveBtn();
        }
    } );


    /*Save area*/
    $( document ).off( 'click', '.save-new-place' ).on( 'click', '.save-new-place', function() {
        $( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i> Đang lưu' );

        var url = 'include/23';
        var data = $( '.place-form .area-textbox' ).serialize();
        if( PARAMS.action == 'edit' ) {
            data += '&action=edit&id=' + PARAMS.id;
        }
        var callback = function( html ) {
            htmlArr = html.split( '|' );
            if( htmlArr[0] == '1' ) {
                ULTI.done( htmlArr[1] );
            }
            else {
                DOM.showStatus( htmlArr[1], parseInt( htmlArr[0] ) );
                $( '.save-new-place' ).removeClass( 'saving' ).html( '<i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu' );
            }
        };
        AJAX.post( url, data, callback );
    } );

    /*Edit place*/
    $( document ).off( 'click', '.edit-place' ).on( 'click', '.edit-place', function() {
        DOM.loadTplThisEl( '/admin/caidat/diadanh?action=edit&id=' + $( this ).data( 'id' ) );
    } );

    /*When edit*/
    $( document ).ready( function() {
        if( PARAMS.action == 'edit' ) {
            $( '.place-addnew-btn' ).click();
            DOM.enableSaveBtn();
        }
    } );

    /*Delete place*/
    $( document ).off( 'click', '.delete-place' ).on( 'click', '.delete-place', function() {
        var deleteFn = function( target ) {
            var url = 'include/24';
            var data = {
                'id': target.data( 'id' )
            };
            var callback = function( html ) {
                var htmlArr = html.split( '|' );
                DOM.showStatus( htmlArr[1], parseInt(htmlArr[0]) );
                DOM.loadTplThisEl( '/admin/caidat/diadanh' );
            };
            AJAX.post( url, data, callback );
        };
        var text = "Bạn có chắc chắn xóa địa điểm này?";
        DOM.showPopup( text, deleteFn, $( this ) );
    } );
} );