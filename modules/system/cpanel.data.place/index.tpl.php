{@style}
<section class="module-wrapper">
    <section class="module-toolbar">
        <h3 class="module-heading">Quản lý / Địa điểm tour <small>Cpanel</small></h3>
        <div class="module-button-area">
            <span class="buttons normal-buttons place-addnew-btn"><i class="fa fa-plus"></i> Thêm</span>
            <span class="buttons disabled-buttons place-cancel-btn"><i class="fa fa-undo"></i> Hủy</span>
        </div>
    </section>
    <section class="module-content">
        <div class="module-addnew tabs">
            <span class="tab-title">Thêm địa điểm tour</span>
            <span class="buttons disabled-buttons save-btn save-new-place"><i class="fa fa-floppy-o"></i> Lưu</span>
            {@form-addnew}
        </div>
        <div class="area-list-wrapper">
            <table class="tables">
                <tr class="title-rows">
                    <td>Số thứ tự</td>
                    <td>Tên địa điểm tour</td>
                </tr>
                {@list}
            </table>
        </div>
    </section>
</section>
{@script}