<?php
$exec = new Exec( HOST, USER, PASS, DBNAME );
$sql = new Sql();

$html = '
		<div class="tabs module-addnew">
			<table class="form-tables tbl">
				<tr>
					<td>Tên địa điểm </td>
					<td>
						<input name="place_name" type="text" class="inputs place-textbox required-inputs" placeholder="Tên địa điểm " />
					</td>
				</tr>
			</table>
		</div>
	';

if( isset( $_GET['action'], $_GET['id'] ) && $_GET['action'] == 'edit' ) {
    //Get place by id
    $data = array(
        ':place_id' => (int)$_GET['id'],
    );
    $place = $exec -> get( $sql -> get( 15 ), $data );
    $place = $place[0];
    $place_name = $place['place_name'];
    $html = '
		<div class="tabs module-addnew">
			<span class="tab-title">Thêm địa điểm tour </span>
			<div class="module-addnew-btn">
				<span class="buttons disabled-buttons place-save-new save-btn"><i class="fa fa-floppy-o"></i> Lưu</span>
			</div>
			<table class="form-tables tbl">
				<tr>
					<td>Tên địa điểm tour </td>
					<td>
						<input name="place_name" type="text" class="inputs place-textbox required-inputs" placeholder="Tên địa điểm tour " value="'. $place_name .'"/>
					</td>
				</tr>
			</table>
		</div>
	';
}
echo $html;
?>
