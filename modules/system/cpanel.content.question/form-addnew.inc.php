<?php
	if( isset( $_GET['action'], $_GET['id'] ) && $_GET['action'] == 'edit' ) {		
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		$sql = new Sql();
		$id = (int)$_GET['id'];
		
		$question = $exec -> get( $sql -> get( 193 ), array(
			':question_id' => $id
		) );
		$question = $question[0];
		
		$html = '
			<div class="tabs module-addnew">
				<span class="tab-title">Sửa câu hỏi</span>
				<div class="module-addnew-btn">
					<span class="buttons disabled-buttons question-save-new save-btn"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</span>
				</div>			
				<table class="form-tables tab1-tbl">
					<tr>
						<td>Tiêu đề (*)</td>
						<td>
							<input value="' . $question['question_title'] . '" name="question_title" type="text" class="inputs required-inputs question-textbox question-title" placeholder="Tiêu đề" />
						</td>
					</tr>
					<tr>
						<td>Nội dung (*)</td>
						<td>
							<textarea name="question_description" class="inputs wysiwyg required-inputs question-textbox" placeholder="Nội dung">' . $question['question_description'] . '</textarea>
						</td>
					</tr>
				</table>
			</div>
		';
	}
	else {		
		$html = '
			<div class="tabs module-addnew">
				<span class="tab-title">Thêm câu hỏi</span>
				<div class="module-addnew-btn">
					<span class="buttons disabled-buttons question-save-new save-btn"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</span>
				</div>
				<table class="form-tables tab1-tbl">
					<tr>
						<td>Tiêu đề (*)</td>
						<td>
							<input name="question_title" type="text" class="inputs required-inputs question-textbox question-name" placeholder="Tiêu đề" />
						</td>
					</tr>
					<tr>
						<td>Nội dung (*)</td>
						<td>
							<textarea name="question_description" class="inputs wysiwyg required-inputs question-textbox" placeholder="Nội dung"></textarea>
						</td>
					</tr>
				</table>
			</div>
		';
	}
	echo $html;
?>