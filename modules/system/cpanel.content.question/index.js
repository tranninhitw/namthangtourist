$( document ).ready( function() {
	/*Init wysiwyg*/
	DOM.initWysiwyg();
	
	/*Global fileList*/
	var fileList = [];
	
	/*PARAMS*/
	PARAMS = ULTI.queryString();
	
	/*When click addnew*/
	$( document ).on( 'click', '.question-addnew-btn', function() {
		MODULE.cmd( 'addnew', $( '.question-cancel-btn' ) );
	} );
	
	/*When click cancel*/
	$( document ).on( 'click', '.question-cancel-btn', function() {
		MODULE.cmd( 'cancel', $( '.question-cancel-btn' ) );
		//Disable save buttons
		DOM.disableSaveBtn();
	} );
	
	/*On input question-textbox*/
	$( document ).on( 'input change', '.question-textbox', function() {
		var allDone = ULTI.validate( $( '.module-addnew .required-inputs' ), 'empty' );
		if( allDone ) {
			DOM.enableSaveBtn();
		}
		else {
			DOM.disableSaveBtn();
		}
	} );
	
	/*Save new question*/
	$( document ).off( 'click', '.question-save-new' ).on( 'click', '.question-save-new', function() {		
		$( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i> Đang lưu' );
		
		//Save data
		var data = $( '.module-addnew .question-textbox' ).serialize();
		
		if( PARAMS.action == 'edit' ) {
			data += '&action=edit&id=' + PARAMS.id;
		}
		
		var url = 'include/42';
		var callback = function( html ) {
			htmlArr = html.split( '|' );
			if( htmlArr[0] == '1' ) {
				ULTI.done( htmlArr[1] );
			}
			else {
				DOM.showStatus( htmlArr[1], parseInt( htmlArr[0] ) );
				$( '.question-save-new' ).removeClass( 'saving' ).html( '<i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu' );
			}
		};
		AJAX.post( url, data, callback );
	} );
	
	/*Edit product*/
	$( document ).off( 'click', '.edit-question-btn' ).on( 'click', '.edit-question-btn', function() {
		//Load tpl
		DOM.loadTplThisEl( $( this ) );
	} );
	
	/*When edit*/
	$( document ).ready( function() {
		if( PARAMS.action == 'edit' ) {			
			//Fade In addnew form
			$( '.question-addnew-btn' ).click();
		}
	} );
	
	/*Delete delete*/
	$( document ).on( 'click', '.delete-question-btn', function() {
		var deleteFn = function( target ) {
			var id = target.data( 'id' );
			
			var url = 'include/43';
			var data = {
				'question_id': id
			};
			var callback = function( html ) {
				ULTI.done( 'Xóa thành công' );
			};
			AJAX.post( url, data, callback );
		};
		var text = 'Bạn có chắc chắn muốn xóa câu hỏi này?';
		DOM.showPopup( text, deleteFn, $( this ) );
	} );
	
	/*Load more*/
	$( document ).off( 'click', '.loadmore-btn' ).on( 'click', '.loadmore-btn', function() {
		$( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i> Đang tải' );
		var offset =  $( this ).data( 'offset' );
		offset = parseInt( offset ) + 5;
		var url = 'include/48';
		var data = {
			'offset': offset	
		};
		var _this = $( this );
		var tbl = $( '.question-table' );
		var callback = function( html ) {
			$( '.loadmore-btn' ).removeClass( 'saving' ).html( 'Tải thêm' );
			if( html.search( 'colspan="2"' ) === -1 ) {
				tbl.find( 'tbody' ).append( html );
				_this.data( 'offset', offset );
				var row = tbl.find( 'tr' ).length;
				row = parseInt( row ) - 1;
				$( '.viewing' ).text( row );
			}
			else {
				var emptyRow = '<tr><td colspan="2">Đã tải hết tất cả</td></tr>';
				tbl.find( 'tr:last-of-type' ).after( emptyRow );
				_this.remove();
			}
		};
		AJAX.post( url, data, callback );
	} );
	
} ); 