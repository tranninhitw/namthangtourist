<?php 
	$exec = new Exec( HOST, USER, PASS, DBNAME );
	$sql = new Sql();
	
	$offset = isset( $_POST['offset'] ) ? (int)$_POST['offset'] : 0;
	$questions = $exec -> get( $sql -> get( 189 ), array(
		':offset' => $offset
	), true );
	
	$html = '';
	
	if( count( $questions ) === 0 ) {
		echo '
			<tr>
				<td colspan="2">Chưa có câu hỏi nào</td>
			</tr>
		';
	}
	
	foreach( $questions as $key => $question ) {		
		$html .= '
			<tr>
				<td>' . $question['question_title'] . '</td>
				<td>
					<button data-href="' . _WWW . '/admin/noidung/cauhoi?action=edit&id=' . $question['question_id'] . '" class="mini-buttons normal-buttons edit-question-btn" title="Sửa" data-id="' . $question['question_id'] . '"><i class="fa fa-pencil" arial-hidden="true"></i></button>
					<button data-id="' . $question['question_id'] . '" class="mini-buttons cancel-buttons delete-question-btn" title="Xóa"><i class="fa fa-trash" aria-hidden="true"></i></button>
				</td>
			</tr>
		';
	}
	echo $html;
?>