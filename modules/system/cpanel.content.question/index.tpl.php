{@style}
<div class="module-wrapper">
	<section class="module-toolbar">
		<h3 class="module-heading">Nội dung / Câu hỏi <small>Cpanel</small></h3>
		<div class="module-button-area">
				<span class="buttons normal-buttons question-addnew-btn"><i class="fa fa-plus" aria-hidden="true"></i> Thêm</span>
				<span class="buttons disabled-buttons question-cancel-btn"><i class="fa fa-undo" aria-hidden="true"></i> Hủy</span>
		</div>
	</section>
	<section class="module-content">
		{@form-addnew}
		<div class="question-list-wrapper">
			<table class="tables question-table">
				<tr class="title-rows">
					<td>Tiêu đề</td>
					<td>Biên tập</td>
				</tr>
				{@list}
			</table>
		</div>
		{@pager}
	</section>
</div>
{@script}