$( document ).ready( function() {
	
	/*WYSIWYG init*/
	DOM.initWysiwyg();
	
	var PARAMS = ULTI.queryString();
	
	/*Click addnew button*/
	$( document ).off( 'click', '.tour-addnew-btn' ).on( 'click', '.tour-addnew-btn', function() {
		MODULE.cmd( 'addnew', $( '.tour-cancel-btn' ) );
	} );

	/*Validate form*/
	$( document ).on( 'change input', '.tour-textbox', function() {
		var allDone = ULTI.validate( $( '.module-addnew .required-inputs' ), 'empty' );
		if( allDone ) {
			DOM.enableSaveBtn();
		}
		else {
			DOM.disableSaveBtn();
		}
	} );

	/*Click cancel*/
	$( document ).off( 'click', '.tour-cancel-btn' ).on( 'click', '.tour-cancel-btn', function() {
		MODULE.cmd( 'cancel', $( '.tour-cancel-btn' ) );
		DOM.loadTplThisEl( '/admin/noidung/tour' );
	} );


	/*Auto-fill url*/
	$( document ).on( 'input', '.tour-title', function() {
		var value = $( this ).val();
		value = value.latinise();
		value = value.replace( / /g, '-' );
		value = value.toLowerCase();
		$( '.tour-url' ).val( value ).change();
	} );

	/*Upload image when click upload*/
	$( document ).off( 'click', '.upload-btn' ).on( 'click', '.upload-btn', function() {
		var file = $( '#tour-image' )[0].files;
		if( file.length > 0 ) {
			var input = $( '.tour-image' );
			var destination = 'blog';
			var imageName = $( '.tour-url' ).val();
			var callback = function( html ) {
				input.val( html ).change();
			};
			ULTI.upload( file, destination, imageName, callback );
		}
	} );

	/*Save new tour*/
	$( document ).off( 'click', '.tour-save-new' ).on( 'click', '.tour-save-new', function() {
		$( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw"></i> Đang lưu' );

		//Upload image
		$( '.upload-btn' ).click();

		//Save data
		var url = 'include/26';
		var data = $( '.module-addnew .tour-textbox' ).serialize();

		if( PARAMS.action == 'edit' ) {
			data += '&action=edit&id=' + PARAMS.id;
		}

		var callback = function( html ) {
			var htmlArr = html.split( '|' );
			if( htmlArr[0] == '1' ) {
				ULTI.done( htmlArr[1] );
			}
			else {
				DOM.showStatus( htmlArr[1], parseInt( htmlArr[0] ) );
				$( '.tour-save-new' ).removeClass( 'saving' ).html( '<i class="fa fa-floppy-o"></i> Lưu' );
			}
		};
		AJAX.post( url, data, callback );
	} );

	/*When click edit*/
	$( document ).off( 'click', '.edit-tour' ).on( 'click', '.edit-tour', function() {
		var id = $( this ).data( 'id' );
		DOM.loadTplThisEl( '/admin/noidung/tour?action=edit&id=' + id );
	} );

	/*When edit*/
	$( document ).ready( function() {
		if( PARAMS.action == 'edit' ) {
			$( '.tour-addnew-btn' ).click();
		}
	} );

	/*Delete tour*/
	$( document ).off( 'click', '.delete-tour' ).on( 'click', '.delete-tour', function() {
		var deleteFn = function( target ) {
			var id = target.data( 'id' );
			var url = 'include/29';
			var data = {
				'tour_id' : id
			};
			var callback = function( html ) {
				console.log( html );
				var htmlArr = html.split( '|' );
				DOM.showStatus( htmlArr[1], htmlArr[0] );
				DOM.loadTplThisEl( '/admin/noidung/tour' );
			};
			AJAX.post( url, data, callback );
		};
		var text = 'Bạn có chắc chắn xóa tour này?';
		DOM.showPopup( text, deleteFn, $( this ) );
	} );


	/*Load more*/
	$( document ).off( 'click', '.loadmore-btn' ).on( 'click', '.loadmore-btn', function() {
		$( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw"></i> Đang tải' );
		var offset =  $( this ).data( 'offset' );
		offset = parseInt( offset ) + 5;
		var url = 'include/30';
		var data = {
			'offset': offset
		};
		var _this = $( this );
		var tbl = $( '.tour-table' );
		var callback = function( html ) {
			$( '.loadmore-btn' ).removeClass( 'saving' ).html( 'Tải thêm' );
			if( html.search( 'colspan="5"' ) === -1 ) {
				tbl.find( 'tbody' ).append( html );
				_this.data( 'offset', offset );
				var row = tbl.find( 'tr' ).length;
				row = parseInt( row ) - 1;
				$( '.viewing' ).text( row );
			}
			else {
				var emptyRow = '<tr><td colspan="5">Đã tải hết tất cả</td></tr>';
				tbl.find( 'tr:last-of-type' ).after( emptyRow );
				_this.remove();
			}
		};
		AJAX.post( url, data, callback );
	} );
	
} );