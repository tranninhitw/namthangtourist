{@style}
<section class="module-wrapper">
	<section class="module-toolbar">
		<h3 class="module-heading">Nội dung / Tour <small>Cpanel</small></h3>
		<div class="module-button-area">
			<span class="buttons normal-buttons tour-addnew-btn"><i class="fa fa-plus"></i> Thêm</span>
			<span class="buttons disabled-buttons tour-cancel-btn"><i class="fa fa-undo"></i> Hủy</span>
		</div>
	</section>
	<section class="module-content">
		<div class="tabs module-addnew">
			<span class="tab-title">Thêm tour</span>
			<div class="module-addnew-btn">
				<span class="buttons disabled-buttons save-btn tour-save-new"><i class="fa fa-floppy-o"></i> Lưu</span>
			</div>
			<input type="radio" name="tab" id="tab1" class="tab-radio" checked />
			<label for="tab1" class="tab-label">Tổng quát</label>
			<div class="tab-content">
				{@addnew-form}
			</div>
		</div>
		<div class="tour-list-wrapper">
			<table class="tables tour-table">
				<tr class="title-rows">
					<td>Tên tour</td>
					<td>Ngày đăng</td>
					<td>Loại</td>
					<td>Số bình luận</td>
					<td>Biên tập</td>
				</tr>
				{@list}
			</table>
		</div>
		{@pager}
	</section>
</section>
{@script}