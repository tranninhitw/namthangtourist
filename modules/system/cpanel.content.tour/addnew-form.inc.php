<?php 
	if( isset( $_GET['action'] ) && $_GET['action'] == 'edit' ) {
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		$sql = new Sql();
		$id = (int)$_GET['id'];
		
		$tour = $exec -> get( $sql -> get( 167 ), array(
			':id' => $id
		) );
		$tour = $tour[0];
		
		$images = json_decode( $tour['tour_thumbnail'], true );
		$images = $images[0];
		
		$checked = $tour['tour_allow_comment'] == '1' ? 'checked' : '';
		$checked2 = $tour['tour_as_draff'] == '1' ? 'checked' : '';
		
		$cates = '';
		$cateIds = array_filter( explode( ',', $tour['tour_cate_id'] ), 'strlen' );
		foreach( $cateIds as $id ) {
			$text = $id == 'tour-gia-re-nho-the' ? 'Tour giờ chót' : 'Tour giá rẻ nhờ thẻ';
			$cates .= '
				<span data-id="' . $id . '">' . $text . ' <i class="fa fa-times"></i></span>
			';
		}
		
		$html = '
			<table class="form-tables tab1-tbl">
				<tr>
					<td>Tiêu đề tour (*)</td>
					<td>
						<input value="' . $tour['tour_title'] . '" type="text" name="tour_title" class="inputs tour-textbox required-inputs tour-title" placeholder="Tiêu đề tour" />
					</td>
				</tr>
				<tr>
					<td>Mô tả ngắn (*)</td>
					<td>
						<textarea name="tour_compact" class="inputs tour-textbox required-inputs" placeholder="Mô tả ngắn">' . $tour['tour_compact'] . '</textarea>
					</td>
				</tr>
				<tr>
					<td>Danh mục (*)</td>
					<td>
						<div class="suggest-lists">
							<input type="text" class="inputs suggest-list-textbox" placeholder="Chọn danh mục" />
							<div class="suggest-list-panel">
								<span data-id="tour-gia-re-nho-the">Tour giờ chót</span>
								<span data-id="tour-gio-chot">Tour giá rẻ nhờ thẻ</span>
							</div>
						</div>
						<div class="tour-selected suggest-list-result">
							<input value="' . $tour['tour_cate_id'] . '" name="tour_cate" type="text" class="inputs tour-textbox required-inputs hidden" readonly />
							' . $cates . '
						</div>
					</td>
				</tr>
				<tr>
					<td style="float: left">Nội dung tour (*)</td>
					<td>
                        <label for="tour_date">Ngày</label>
                        <input type="text" name="tour_date" class="inputs suggest-list-textbox">
						<textarea name="tour_content" class="inputs wysiwyg tour-textbox required-inputs" placeholder="Nội dung">' . $tour['tour_contents'] . '</textarea>
					</td>
				</tr>
			</table>
		</div>
		<input type="radio" name="tab" id="tab2" class="tab-radio" />
		<label for="tab2" class="tab-label">SEO</label>
		<div class="tab-content">
			<table class="form-tables">
				<tr>
					<td>SEO url (*)</td>
					<td>
						<input value="' . $tour['tour_seo_url'] . '" name="tour_url" type="text" class="inputs tour-textbox required-inputs tour-url" placeholder="Seo url" />
					</td>
				</tr>
				<tr>
					<td>Page title (*)</td>
					<td>
						<input value="' . $tour['tour_page_title'] . '" name="tour_page_title" type="text" class="inputs tour-textbox required-inputs" placeholder="Meta title" />
					</td>
				</tr>
				<tr>
					<td>Meta description (*)</td>
					<td>
						<textarea name="tour_meta_description" class="inputs tour-textbox required-inputs" placeholder="Meta description">' . $tour['tour_meta_description'] . '</textarea>
					</td>
				</tr>
				<tr>
					<td>Meta keywords (*)</td>
					<td>
						<textarea name="tour_meta_keywords" class="inputs tour-textbox required-inputs" placeholder="Meta keywords">' . $tour['tour_meta_keywords'] . '</textarea>
					</td>
				</tr>
			</table>
		</div>
		<input type="radio" name="tab" id="tab3" class="tab-radio" />
		<label for="tab3" class="tab-label">Dữ liệu</label>
		<div class="tab-content">
			<table class="form-tables tab2-tbl">
				<tr>
					<td>Hình đại diện (*)</td>
					<td>
						<button class="upload-btn buttons hidden">Upload</button>
						<input value="' . htmlspecialchars( $tour['tour_thumbnail'] , ENT_QUOTES ) . '" name="tour_avatar" type="text" class="inputs tour-textbox hidden tour-image" readonly />
						<input type="file" class="file-inputs tour-textbox" id="tour-image" />
						<label for="tour-image">
							<span>Đổi hình ảnh</span>
							<span>Chưa chọn file...</span>
						</label>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<img class="old-img" src="/uploads/blog/' . $images . '" />
					</td>
				</tr>
				<tr>
					<td>Cho phép bình luận</td>
					<td>
						<input name="tour_allow_comment" id="allow-cmt" type="checkbox" class="checkboxes tour-textbox" ' . $checked . ' />
						<label for="allow-cmt"></label>
					</td>
				</tr>
				<tr>
					<td>Lưu dạng nháp</td>
					<td>
						<input name="tour_as_draff" id="enable" type="checkbox" class="checkboxes tour-textbox"  ' . $checked2 . ' />
						<label for="enable"></label>
					</td>
				</tr>
			</table>
		';
	}
	else {
		$html = '
			<table class="form-tables tab1-tbl">
				<tr>
					<td>Tiêu đề tour (*)</td>
					<td>
						<input type="text" name="tour_title" class="inputs tour-textbox required-inputs tour-title" placeholder="Tiêu đề tour" />
					</td>
				</tr>
				<tr>
					<td>Mô tả ngắn (*)</td>
					<td>
						<textarea name="tour_compact" class="inputs tour-textbox required-inputs" placeholder="Mô tả ngắn"></textarea>
					</td>
				</tr>
				<tr>
					<td>Danh mục (*)</td>
					<td>
						<div class="suggest-lists">
							<input type="text" class="inputs suggest-list-textbox" placeholder="Chọn danh mục" />
							<div class="suggest-list-panel">
								<span data-id="tour-gia-re-nho-the">Tour giờ chót</span>
								<span data-id="tour-gio-chot">Tour giá rẻ nhờ thẻ</span>
							</div>
						</div>
						<div class="tour-selected suggest-list-result">
							<input name="tour_cate" type="text" class="inputs tour-textbox required-inputs hidden" readonly />
						</div>
					</td>
				</tr>
				<tr>
					<td>Nội dung tour (*)</td>
                    <td>
                        <label for="tour_date">Ngày</label>
                        <input type="text" name="tour_date" class="inputs tour-textbox required-inputs tour-date">
						<textarea name="tour_content" class="inputs wysiwyg tour-textbox required-inputs" placeholder="Nội dung">' . $tour['tour_contents'] . '</textarea>
					</td>
				</tr>
			
			</table>
		</div>
		<input type="radio" name="tab" id="tab2" class="tab-radio" />
		<label for="tab2" class="tab-label">SEO</label>
		<div class="tab-content">
			<table class="form-tables">
				<tr>
					<td>SEO url (*)</td>
					<td>
						<input name="tour_url" type="text" class="inputs tour-textbox required-inputs tour-url" placeholder="Seo url" />
					</td>
				</tr>
				<tr>
					<td>Page title (*)</td>
					<td>
						<input name="tour_page_title" type="text" class="inputs tour-textbox required-inputs" placeholder="Meta title" />
					</td>
				</tr>
				<tr>
					<td>Meta description (*)</td>
					<td>
						<textarea name="tour_meta_description" class="inputs tour-textbox required-inputs" placeholder="Meta description"></textarea>
					</td>
				</tr>
				<tr>
					<td>Meta keywords (*)</td>
					<td>
						<textarea name="tour_meta_keywords" class="inputs tour-textbox required-inputs" placeholder="Meta keywords"></textarea>
					</td>
				</tr>
			</table>
		</div>
		<input type="radio" name="tab" id="tab3" class="tab-radio" />
		<label for="tab3" class="tab-label">Dữ liệu</label>
		<div class="tab-content">
			<table class="form-tables tab2-tbl">
				<tr>
					<td>Hình đại diện (*)</td>
					<td>
						<button class="upload-btn buttons hidden">Upload</button>
						<input name="tour_avatar" type="text" class="inputs tour-textbox hidden tour-image" readonly />
						<input type="file" class="file-inputs tour-textbox required-inputs" id="tour-image" />
						<label for="tour-image">
							<span>Chọn hình ảnh</span>
							<span>Chưa chọn file...</span>
						</label>
					</td>
				</tr>
				<tr>
					<td>Cho phép bình luận</td>
					<td>
						<input name="tour_allow_comment" id="allow-cmt" type="checkbox" class="checkboxes tour-textbox" checked />
						<label for="allow-cmt"></label>
					</td>
				</tr>
				<tr>
					<td>Lưu dạng nháp</td>
					<td>
						<input name="tour_as_draff" id="enable" type="checkbox" class="checkboxes tour-textbox" />
						<label for="enable"></label>
					</td>
				</tr>
			</table>
		';
	}
	echo $html;
?>