<?php 
	if( isset( $_POST['post_title'] ) ) {
		//Init OOP
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		$sql = new Sql();
		
		//Get data
		foreach( $_POST as $key => $value ) {
			$$key = $value;
		}
		
		//Get admin id
		$userData = unserialize( $_SESSION['tk_user_data'] );
		
		$data = array(
			':admin_id' => $userData['admin_id'],
			':tour_cate_id' => $tour_cate,
			':tour_title' => $tour_title,
			':tour_page_title' => $tour_page_title,
			':tour_contents' => $tour_content,
			':tour_compact' => $tour_compact,
			':tour_meta_description' => $tour_meta_description,
			':tour_meta_keywords' => $tour_meta_keywords,
			':tour_seo_url' => preg_replace( '/[^0-9a-zA-Z- ]/', '', $tour_url ),
			':tour_allow_comment' => isset( $tour_allow_comment ) ? 1 : 0,
			':tour_as_draff' => isset( $tour_as_draff ) ? 1 : 0,
			':tour_thumbnail' => $tour_avatar,
			':tour_time' => time()
		);
		
		if( isset( $action ) && $action == 'edit' ) {
			$data[':tour_id'] = (int)$id;
			$r = $exec -> exec( $sql -> get( 168 ), $data );
		}
		else {
			$r = $exec -> add( $sql -> get( 160 ), $data );
		}
		$r ? print( '1|Lưu tour thành công' ) : print( '0|Trùng URL' );
	}
?>