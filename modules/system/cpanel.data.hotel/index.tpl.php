{@style}
<section class="module-wrapper">
    <section class="module-toolbar">
        <h3 class="module-heading">Quản lý / Khách sạn <small>Cpanel</small></h3>
        <div class="module-button-area">
            <span class="buttons normal-buttons hotel-addnew-btn"><i class="fa fa-plus"></i> Thêm</span>
            <span class="buttons disabled-buttons hotel-cancel-btn"><i class="fa fa-undo"></i> Hủy</span>
        </div>
    </section>
    <section class="module-content">
        <div class="module-addnew tabs">
            <span class="tab-title">Thêm khách sạn</span>
            <span class="buttons disabled-buttons save-btn save-new-hotel"><i class="fa fa-floppy-o"></i> Lưu</span>
            {@form-addnew}
        </div>
        <div class="area-list-wrapper">
            <table class="tables">
                <tr class="title-rows">
                    <td>Tên khách sạn</td>
                    <td>Địa chỉ</td>
                    <td>Số điện thoại</td>
                    <td>Website</td>
                </tr>
                {@list}
            </table>
        </div>
    </section>
</section>
{@script}