<?php
$exec = new Exec( HOST, USER, PASS, DBNAME );
$sql = new Sql();

$html = '
		<div class="tabs module-addnew">
		
			<table class="form-tables tbl">
				<tr>
					<td>Tên khách sạn</td>
					<td>
						<input name="hotel_name" type="text" class="inputs hotel-textbox required-inputs" placeholder="Tên khách sạn" />
					</td>
				</tr>
				<tr>
					<td>Địa chỉ</td>
					<td>
						<input name="hotel_address" type="text" class="inputs hotel-textbox required-inputs" placeholder="Địa chỉ"/>
					</td>
				</tr>
				<tr>
					<td>Điện thoại</td>
					<td>
						<input name="hotel_phone" type="text" class="inputs hotel-textbox required-inputs" placeholder="Số điện thoại" />
					</td>
				</tr>
				<tr>
					<td>Website (nếu có)</td>
					<td>
						<input name="hotel_website" type="text" class="inputs hotel-textbox" placeholder="Website của khách sạn" />
					</td>
				</tr>
		
			</table>
		</div>
	';

if( isset( $_GET['action'], $_GET['id'] ) && $_GET['action'] == 'edit' ) {
    //Get hotel by id
    $data = array(
        ':hotel_id' => (int)$_GET['id'],
    );
    $hotel = $exec -> get( $sql -> get( 15 ), $data );
    $hotel = $hotel[0];

    $hotel_name = $hotel['hotel_name'];
    $hotel_address = $hotel['hotel_address'];
    $hotel_phone = $hotel['hotel_phone'];
    $hotel_website = $hotel['hotel_website'];

    $html = '
		<div class="tabs module-addnew">
			<span class="tab-title">Thêm khách sạn</span>
			<div class="module-addnew-btn">
				<span class="buttons disabled-buttons hotel-save-new save-btn"><i class="fa fa-floppy-o"></i> Lưu</span>
			</div>
			<table class="form-tables tbl">
				<tr>
					<td>Tên khách sạn</td>
					<td>
						<input name="hotel_name" type="text" class="inputs hotel-textbox required-inputs" placeholder="Tên khách sạn" value="'. $hotel_name .'"/>
					</td>
				</tr>
				<tr>
					<td>Tên seo url</td>
					<td>
						<input name="hotel_address" type="text" class="inputs hotel-textbox required-inputs" placeholder="Địa chỉ" value="'. $hotel_address .'"/>
					</td>
				</tr>
				<tr>
					<td>Số điện thoại</td>
					<td>
						<input name="hotel_phone" type="text" class="inputs hotel-textbox required-inputs" placeholder="Số điện thoại" value="'. $hotel_phone .'"/>
					</td>
				</tr>
				<tr>
					<td>Website (nếu có)</td>
					<td>
						<input name="hotel_website" type="text" class="inputs hotel-textbox" placeholder="Website" value="'. $hotel_website .'"/>
					</td>
				</tr>
			
			</table>
		</div>
	';
}
echo $html;
?>
