$( document ).ready( function() {
    /*PARAMS*/
    PARAMS = ULTI.queryString();



    /*When click delete*/
    $( document ).off( 'click', '.delete-contact' ).on( 'click', '.delete-contact', function() {
        var deleteFn = function( target ) {
            var id = target.data( 'id' );
            var url = 'include/53';
            var data = {
                'contact' : id
            };
            var callback = function( html ) {
          
                var htmlArr = html.split( '|' );
                DOM.showStatus( htmlArr[1], htmlArr[0] );
                DOM.loadTplThisEl( _WWW + 'admin/noidung/lienhe' );
            };
            AJAX.post( url, data, callback );
        };
        var text = 'Bạn có chắc chắn muốn xóa liên hệ này?';
        DOM.showPopup( text, deleteFn, $( this ) );
    } );
} )
