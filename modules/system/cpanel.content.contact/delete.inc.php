<?php
if( isset( $_POST['contact'] ) ) {
    $exec = new Exec( HOST, USER, PASS, DBNAME );
    $sql = new Sql();

    $r = $exec -> exec( $sql -> get( 28 ), array(
        ':contact_id' => (int)$_POST['contact']
    ) );

    $r ? print( '1|Xóa liên hệ thành công' ) : print( '0|Có lỗi khi cố xóa liên hệ' );
}
?>
