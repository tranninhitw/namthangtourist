<?php
$exec = new Exec( HOST, USER, PASS, DBNAME );
$sql = new Sql();

$html = '
		<div class="tabs module-addnew">
			<table class="form-tables tbl">
				<tr>
					<td>Tên phương tiện vận chuyển (*)</td>
					<td>
						<input type="text" name="transportation_name" class="inputs transportation-textbox required-inputs transportation_name" placeholder="Tên phương tiện vận chuyển" />
					</td>
				</tr>
				<tr>
					<td>Hình phương tiện vận chuyển (*)</td>
					<td>
						<input type="file" name="transportation_image" class="file-inputs transportation-textbox" id="transportation-image"  placeholder="Hình phương tiện vận chuyển" />
					    <label for="transportation-image">
							<span>Đổi hình ảnh</span>
							<span>Chưa chọn file...</span>
						</label>
					</td>
				</tr>
			</table>
		</div>
	';

if( isset( $_GET['action'], $_GET['id'] ) && $_GET['action'] == 'edit' ) {
    //Get transportation by id
    $data = array(
        ':transportation_id' => (int)$_GET['id'],
    );
    $transportation = $exec -> get( $sql -> get( 15 ), $data );
    $transportation = $transportation[0];
    $transportation_name = $transportation['transportation_name'];
    $html = '
		<div class="tabs module-addnew">
			<span class="tab-title">Thêm vé </span>
			<div class="module-addnew-btn">
				<span class="buttons disabled-buttons transportation-save-new save-btn"><i class="fa fa-floppy-o"></i> Lưu</span>
			</div>
			<table class="form-tables tbl">
				<tr>
					<td>Tên phương tiện vận chuyển (*)</td>
					<td>
						<input type="text" name="transportation_name" class="inputs transportation-textbox required-inputs transportation_name" placeholder="Tên phương tiện vận chuyển" />
					</td>
				</tr>
				<tr>
					<td>Hình phương tiện vận chuyển (*)</td>
					<td>
						<input type="file" name="transportation_image" class="file-inputs transportation-textbox" id="transportation-image"  placeholder="Hình phương tiện vận chuyển" />
					    <label for="transportation-image">
							<span>Đổi hình ảnh</span>
							<span>Chưa chọn file...</span>
						</label>
					</td>
				</tr>
			</table>
		</div>
	';
}
echo $html;
?>
