$( document ).ready( function() {

    /*WYSIWYG init*/
    DOM.initWysiwyg();

    var PARAMS = ULTI.queryString();

    /*Click addnew button*/
    $( document ).off( 'click', '.transportation-addnew-btn' ).on( 'click', '.transportation-addnew-btn', function() {
        MODULE.cmd( 'addnew', $( '.transportation-cancel-btn' ) );
    } );

    /*Validate form*/
    $( document ).on( 'change input', '.transportation-textbox', function() {
        var allDone = ULTI.validate( $( '.module-addnew .required-inputs' ), 'empty' );
        if( allDone ) {
            DOM.enableSaveBtn();
        }
        else {
            DOM.disableSaveBtn();
        }
    } );

    /*Click cancel*/
    $( document ).off( 'click', '.transportation-cancel-btn' ).on( 'click', '.transportation-cancel-btn', function() {
        MODULE.cmd( 'cancel', $( '.transportation-cancel-btn' ) );
        DOM.loadTplThisEl( '/admin/noidung/baiviet' );
    } );


    /*Auto-fill url*/
    $( document ).on( 'input', '.transportation-title', function() {
        var value = $( this ).val();
        value = value.latinise();
        value = value.replace( / /g, '-' );
        value = value.toLowerCase();
        $( '.transportation-url' ).val( value ).change();
    } );

    /*Upload image when click upload*/
    $( document ).off( 'click', '.upload-btn' ).on( 'click', '.upload-btn', function() {
        var file = $( '#transportation-image' )[0].files;
        if( file.length > 0 ) {
            var input = $( '.transportation-image' );
            var destination = 'blog';
            var imageName = $( '.transportation-url' ).val();
            var callback = function( html ) {
                input.val( html ).change();
            };
            ULTI.upload( file, destination, imageName, callback );
        }
    } );

    /*Save new transportation*/
    $( document ).off( 'click', '.transportation-save-new' ).on( 'click', '.transportation-save-new', function() {
        $( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i> Đang lưu' );

        //Upload image
        $( '.upload-btn' ).click();

        //Save data
        var url = 'include/26';
        var data = $( '.module-addnew .transportation-textbox' ).serialize();

        if( PARAMS.action == 'edit' ) {
            data += '&action=edit&id=' + PARAMS.id;
        }

        var callback = function( html ) {
            var htmlArr = html.split( '|' );
            if( htmlArr[0] == '1' ) {
                ULTI.done( htmlArr[1] );
            }
            else {
                DOM.showStatus( htmlArr[1], parseInt( htmlArr[0] ) );
                $( '.transportation-save-new' ).removeClass( 'saving' ).html( '<i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu' );
            }
        };
        AJAX.post( url, data, callback );
    } );

    /*When click edit*/
    $( document ).off( 'click', '.edit-transportation' ).on( 'click', '.edit-transportation', function() {
        var id = $( this ).data( 'id' );
        DOM.loadTplThisEl( '/admin/noidung/baiviet?action=edit&id=' + id );
    } );

    /*When edit*/
    $( document ).ready( function() {
        if( PARAMS.action == 'edit' ) {
            $( '.transportation-addnew-btn' ).click();
        }
    } );

    /*Delete transportation*/
    $( document ).off( 'click', '.delete-transportation' ).on( 'click', '.delete-transportation', function() {
        var deleteFn = function( target ) {
            var id = target.data( 'id' );
            var url = 'include/29';
            var data = {
                'transportation_id' : id
            };
            var callback = function( html ) {
                console.log( html );
                var htmlArr = html.split( '|' );
                DOM.showStatus( htmlArr[1], htmlArr[0] );
                DOM.loadTplThisEl( '/admin/noidung/baiviet' );
            };
            AJAX.post( url, data, callback );
        };
        var text = 'Bạn có chắc chắn xóa bài viết này?';
        DOM.showPopup( text, deleteFn, $( this ) );
    } );


    /*Load more*/
    $( document ).off( 'click', '.loadmore-btn' ).on( 'click', '.loadmore-btn', function() {
        $( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i> Đang tải' );
        var offset =  $( this ).data( 'offset' );
        offset = parseInt( offset ) + 5;
        var url = 'include/30';
        var data = {
            'offset': offset
        };
        var _this = $( this );
        var tbl = $( '.transportation-table' );
        var callback = function( html ) {
            $( '.loadmore-btn' ).removeClass( 'saving' ).html( 'Tải thêm' );
            if( html.search( 'colspan="5"' ) === -1 ) {
                tbl.find( 'tbody' ).append( html );
                _this.data( 'offset', offset );
                var row = tbl.find( 'tr' ).length;
                row = parseInt( row ) - 1;
                $( '.viewing' ).text( row );
            }
            else {
                var emptyRow = '<tr><td colspan="5">Đã tải hết tất cả</td></tr>';
                tbl.find( 'tr:last-of-type' ).after( emptyRow );
                _this.remove();
            }
        };
        AJAX.post( url, data, callback );
    } );

} );