{@style}
<section class="module-wrapper">
    <section class="module-toolbar">
        <h3 class="module-heading">Quản lý / Phương tiện vận chuyển <small>Cpanel</small></h3>
        <div class="module-button-area">
            <span class="buttons normal-buttons transportation-addnew-btn"><i class="fa fa-plus"></i> Thêm</span>
            <span class="buttons disabled-buttons transportation-cancel-btn"><i class="fa fa-undo"></i> Hủy</span>
        </div>
    </section>
    <section class="module-content">
        <div class="module-addnew tabs">
            <span class="tab-title">Thêm phương tiện vận chuyển</span>
            <span class="buttons disabled-buttons save-btn save-new-transportation"><i class="fa fa-floppy-o"></i> Lưu</span>
            {@form-addnew}
        </div>
        <div class="area-list-wrapper">
            <table class="tables">
                <tr class="title-rows">
                    <td>Số thứ tự</td>
                    <td>Tên phương tiện</td>
                    <td>Hình phương tiện vận chuyển</td>
                </tr>
                {@list}
            </table>
        </div>
    </section>
</section>
{@script}