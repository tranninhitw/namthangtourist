<?php
$exec = new Exec( HOST, USER, PASS, DBNAME );
$sql = new Sql();

$html = '
		<div class="tabs module-addnew">
			<span class="tab-title">Thêm danh mục</span>
			<div class="module-addnew-btn">
				<span class="buttons disabled-buttons cate-save-new save-btn"><i class="fa fa-floppy-o"></i> Lưu</span>
			</div>
			<table class="form-tables tbl">
				<tr>
					<td>Tên danh mục</td>
					<td>
						<input name="cate_name" type="text" class="inputs cate-textbox required-inputs" placeholder="Tên danh mục" />
					</td>
				</tr>
				<tr>
					<td>Tên seo url</td>
					<td>
						<input name="cate_seo_url" type="text" class="inputs cate-textbox" placeholder="Tên seo url"/>
					</td>
				</tr>
				<tr>
					<td>Meta description</td>
					<td>
						<input name="cate_meta_description" type="text" class="inputs cate-textbox" placeholder="Meta_description" />
					</td>
				</tr>
				<tr>
					<td>Meta keywords</td>
					<td>
						<input name="cate_meta_keywords" type="text" class="inputs cate-textbox" placeholder="Meta_keywords" />
					</td>
				</tr>
		
			</table>
		</div>
	';

if( isset( $_GET['action'], $_GET['id'] ) && $_GET['action'] == 'edit' ) {
    //Get cate by id
    $data = array(
        ':cate_id' => (int)$_GET['id'],
    );
    $cate = $exec -> get( $sql -> get( 15 ), $data );
//        echo "<pre>";
//        print_r($cate);
//        echo "</pre>";
    $cate = $cate[0];

    $cate_name = $cate['cate_name'];
    $cate_seo_url = $cate['cate_seo_url'];
    $cate_meta_description = $cate['cate_meta_description'];
    $cate_meta_keywords = $cate['cate_meta_keywords'];

    $html = '
		<div class="tabs module-addnew">
			<span class="tab-title">Thêm danh mục</span>
			<div class="module-addnew-btn">
				<span class="buttons disabled-buttons cate-save-new save-btn"><i class="fa fa-floppy-o"></i> Lưu</span>
			</div>
			<table class="form-tables tbl">
				<tr>
					<td>Tên danh mục</td>
					<td>
						<input name="cate_name" type="text" class="inputs cate-textbox required-inputs" placeholder="Tên danh mục" value="'. $cate_name .'"/>
					</td>
				</tr>
				<tr>
					<td>Tên seo url</td>
					<td>
						<input name="cate_seo_url" type="text" class="inputs cate-textbox" placeholder="Tên seo url" value="'. $cate_seo_url .'"/>
					</td>
				</tr>
				<tr>
					<td>Meta description</td>
					<td>
						<input name="cate_meta_description" type="text" class="inputs cate-textbox" placeholder="Mô tả" value="'. $cate_meta_description .'"/>
					</td>
				</tr>
				<tr>
					<td>Meta keywords</td>
					<td>
						<input name="cate_meta_keywords" type="text" class="inputs cate-textbox" placeholder="Mô tả" value="'. $cate_meta_keywords .'"/>
					</td>
				</tr>
			
			</table>
		</div>
	';
}
echo $html;
?>
