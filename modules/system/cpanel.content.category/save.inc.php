<?php
    if (isset($_POST['cate_name'])){
        $exec = new Exec(HOST, USER, PASS, DBNAME);
        $sql = new Sql();
        $data = [
            ':cate_name' => $_POST['cate_name'],
            ':cate_seo_url' => preg_replace( '/[^0-9a-zA-Z- ]/', '', $_POST['cate_name'] ),
            ':cate_meta_description' => preg_replace( '/[^0-9a-zA-Z- ]/', '', $_POST['cate_meta_description'] ),
            ':cate_meta_keywords' => preg_replace( '/[^0-9a-zA-Z- ]/', '', $_POST['cate_meta_keywords'] ),
        ];
    
//        echo "<pre>";
//        print_r($action);
//        echo "</pre>";
        if( isset( $_POST['action'] ) && $_POST['action'] == 'edit' ) {
            // edit category
            $data['cate_id'] = $_POST['id'];
            $category = $exec ->exec($sql->get(17), $data);
            if( $category) {
                echo '1|Cập nhập danh mục thành công';
            }
            else {
                echo '0|Cập nhập danh mục thất bại';
            }
        }else{
             // add category
             $category = $exec -> add($sql->get(14), $data);
             if( $category ) {
                 echo '1|Thêm mới danh mục thành công';
             }
             else {
                 echo '0|Trùng danh mục, Vui lòng đặt tên khác';
             }
        }

    }
