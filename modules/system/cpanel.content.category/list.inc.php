<?php
$exec       = new Exec(HOST, USER, PASS, DBNAME);
$sql        = new Sql();
$html       = '';

$categories = $exec->get($sql->get(16));
//    echo "<pre>";
//    print_r($categories);
//    echo "</pre>";
if (count($categories) == 0)
{
    $html = '
        <tr>
            <td colspan="5" style="text-align: center;">Hiện chưa có bài viết</td>
        </tr>
    ';
}
$stt = 1;
foreach ($categories as $key => $value)
{
    $public     = "";
    $value['is_public'] == 1 ? $public = "Hiển thị" : $public = "Không hiển thị";
    $html .= '
        <tr>
            <td>' . $stt . '</td>
            <td>' . $value['cate_name'] . '</td>
            <td>' . $value['cate_seo_url'] . '</td>
            <td>' . $value['cate_meta_description'] . '</td>
            <td>' . $public . '</td>
            <td>
                <button data-id="' . $value['cate_id'] . '" class="mini-buttons normal-buttons edit-cate" title="Sửa"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                <button data-id="' . $value['cate_id'] . '" class="mini-buttons cancel-buttons delete-cate" title="Xóa"><i class="fa fa-trash" aria-hidden="true"></i></button>
            </td>
        </tr>
    ';
    $stt++;
}
echo $html;
?>