{@style}
<section class="module-wrapper">
    <section class="module-toolbar">
        <h3 class="module-heading">Hệ thống / Danh mục <small>Cpanel</small></h3>
        <div class="module-button-area">
            <span class="buttons normal-buttons cate-addnew-btn"><i class="fa fa-plus"></i> Thêm</span>
            <span class="buttons disabled-buttons cate-cancel-btn"><i class="fa fa-undo"></i> Hủy</span>
        </div>
    </section>
    <section class="module-content">
        {@form-addnew}
        <div class="cate-list-wrapper">
            <table class="tables module cate-table">
                <colgroup>
                    <col width="5%"></col>
                    <col width="20%"></col>
                    <col width="25%"></col>
                    <col width="20%"></col>
                    <col width="15%"></col>
                </colgroup>
                <tr class="title-rows">
                    <td>STT</td>
                    <td>Tên danh mục</td>
                    <td>Tên SEO url</td>
                    <td>Mô tả</td>
                    <td>Hiển thị</td>
                    <td>Biên tập</td>
                </tr>
                {@list}
            </table>
        </div>
    </section>
</section>
{@script}
