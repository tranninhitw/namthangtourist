$( document ).ready( function() {
    /*PARAMS*/
    PARAMS = ULTI.queryString();

    /*When click addnew*/
    $( document ).on( 'click', '.cate-addnew-btn', function() {
        MODULE.cmd( 'addnew', $( '.cate-cancel-btn' ) );
    } );

    /*When click cancel*/
    $( document ).on( 'click', '.cate-cancel-btn', function() {
        $( '.selects option' ).prop( 'selected', false );
        MODULE.cmd( 'cancel', $( '.cate-cancel-btn' ) );
        //Disable save buttons
        DOM.disableSaveBtn();
    } );

    /*On input cate-textbox*/
    $( document ).on( 'input change', '.cate-textbox', function() {
        var allDone = ULTI.validate( $( '.module-addnew .required-inputs' ), 'empty' );
        if( allDone ) {
            DOM.enableSaveBtn();
        }
        else {
            DOM.disableSaveBtn();
        }
    } );

    /*When click edit*/
    $( document ).off( 'click', '.edit-cate' ).on( 'click', '.edit-cate', function() {
        //Load tpl
        let id = $(this).data('id');
        DOM.loadTplThisEl( _WWW + 'admin/noidung/danhmuc?action=edit&id=' + id );
    } );

    /*When edit*/
    $( document ).ready( function() {
        if( PARAMS.action == 'edit' ) {
            console.log(PARAMS)
            //Fade In addnew form
            $( '.cate-addnew-btn' ).click();
        }
    } );

    /*Save new cate*/
    $( document ).off( 'click', '.cate-save-new' ).on( 'click', '.cate-save-new', function() {
        $( '.cate-save-new' ).addClass( 'saving' ).html( '<i class="fa fa-spinner" aria-hidden="true"></i> Đang lưu</span>' );
        //Save data
        var data = $( '.module-addnew .cate-textbox' ).serialize();
        if( PARAMS.action == 'edit' ) {
            data += '&action=edit&id=' + PARAMS.id;
        }

        var url = 'include/50';
        var callback = function( html ) {
            $( '.cate-save-new' ).removeClass( 'saving' );
            var htmlArr = html.split( '|' );
            console.log(htmlArr);
            if( parseInt( htmlArr[0] ) == 1 ) {
                ULTI.done( htmlArr[1] );
            }
            else {
                DOM.showStatus( htmlArr[1], parseInt( htmlArr[0] ) );
                $( '.cate-save-new' ).removeClass( 'saving' ).html( 'Lưu' );
            }
        };
        AJAX.post( url, data, callback );
    } );

    /*When click delete*/
    $( document ).off( 'click', '.delete-cate' ).on( 'click', '.delete-cate', function() {
        var deleteFn = function( target ) {
            var id = target.data( 'id' );
            var url = 'include/49';
            var data = {
                'cate_id' : id
            };
            var callback = function( html ) {

                var htmlArr = html.split( '|' );
                DOM.showStatus( htmlArr[1], htmlArr[0] );
                DOM.loadTplThisEl( _WWW + 'admin/noidung/danhmuc' );
            };
            AJAX.post( url, data, callback );
        };
        var text = 'Bạn có chắc chắn muốn xóa danh mục này?';
        DOM.showPopup( text, deleteFn, $( this ) );
    } );
} )
