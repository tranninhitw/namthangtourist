<?php 
	$exec = new Exec( HOST, USER, PASS, DBNAME );
	$sql = new Sql();
	
	$city = $exec -> get( $sql -> get( 157 ), array(
		':setting_name' => 'city'
	) );
	
	$city = json_decode( $city[0]['setting_value'], true );
	
	$html = '';
	$city = count( $city ) > 0 ? $city : array();
	
	if( count( $city ) == 0 ) {
		$html = '
			<tr>
				<td colspan="4" style="text-align: center;">Chưa có tỉnh thành</td>
			</tr>
		';
	}
	
	foreach( $city as $key => $arr ) {
		$district = '';
		$i = 0;
		foreach( $arr['district'] as $key2 => $arr2 ) {
			$i++;
			$district .= $arr2['name'] . ',';
			if( $i == 5 ) {
				break;
			}
		}
		
		$district = trim( $district, ',' ) . '...';
		
		switch( $arr['area'] ) {
			case 1:
				$area = 'Miền trung';
				break;
			case 2:
				$area = 'Miền nam';
				break;
			case 3:
				$area = 'Miền bắc';
				break;
			case 4:
				$area = 'Vùng xa';
				break;
			case 5:
				$area = 'Hồ Chí Minh';
				break;
		}
		
		
		$html .= '
			<tr>
				<td>' . $arr['name'] . '</td>
				<td>' . $area . '</td>
				<td>' . $district . '</td>
				<td>
					<span class="mini-buttons buttons normal-buttons edit-city" title="Sửa" data-id="' . $arr['id'] . '"><i class="fa fa-pencil" aria-hidden="true"></i></span>
					<span class="mini-buttons buttons cancel-buttons delete-city" title="Xóa" data-id="' . $arr['id'] . '"><i class="fa fa-trash" aria-hidden="true"></i></span>
				</td>
			</tr>
		';
	}
	echo $html;
?>