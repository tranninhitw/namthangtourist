{@style}
<section class="module-wrapper">
	<section class="module-toolbar">
		<h3 class="module-heading">Hệ thống / Địa danh <small>Cpanel</small></h3>
		<div class="module-button-area">
			<span class="buttons normal-buttons city-addnew-btn"><i class="fa fa-plus" aria-hidden="true"></i> Thêm</span>
			<span class="buttons disabled-buttons city-cancel-btn"><i class="fa fa-undo" aria-hidden="true"></i> Hủy</span>
		</div>
	</section>
	<section class="module-content">
		<div class="module-addnew tabs">
			<span class="tab-title">Thêm tỉnh</span>
			<span class="buttons disabled-buttons save-btn save-new-city"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</span>
			{@form-addnew}
		</div>
		<div class="area-list-wrapper">
			<table class="tables">
				<tr class="title-rows">
					<td>Tên tỉnh</td>
					<td>Vùng miền</td>
					<td>Các huyện</td>
					<td>Biên tập</td>
				</tr>
				{@list}
			</table>
		</div>
	</section>
</section>
{@script}