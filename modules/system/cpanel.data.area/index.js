$( document ).ready( function() {
	
	var PARAMS = ULTI.queryString();
	
	/*Show addnew form*/
	$( document ).off( 'click', '.city-addnew-btn' ).on( 'click', '.city-addnew-btn', function() {
		MODULE.cmd( 'addnew', $( '.city-cancel-btn' ) );
	} );
	
	/*Hide addnew form*/
	$( document ).off( 'click', '.city-cancel-btn' ).on( 'click', '.city-cancel-btn', function() {
		MODULE.cmd( 'cancel', $( '.city-cancel-btn' ) );
	} );
	
	/*Validate form*/
	$( document ).on( 'input change', '.area-textbox', function() {
		var allDone = ULTI.validate( $( '.module-addnew .required-inputs' ), 'empty' );
		if( allDone ) {
			DOM.enableSaveBtn();
		}
		else {
			DOM.disableSaveBtn();
		}
	} );
	
	
	/*Save area*/
	$( document ).off( 'click', '.save-new-city' ).on( 'click', '.save-new-city', function() {
		$( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i> Đang lưu' );
		
		var url = 'include/23';
		var data = $( '.city-form .area-textbox' ).serialize();
		if( PARAMS.action == 'edit' ) {
			data += '&action=edit&id=' + PARAMS.id;
		}
		var callback = function( html ) {
			htmlArr = html.split( '|' );
			if( htmlArr[0] == '1' ) {
				ULTI.done( htmlArr[1] );
			}
			else {
				DOM.showStatus( htmlArr[1], parseInt( htmlArr[0] ) );
				$( '.save-new-city' ).removeClass( 'saving' ).html( '<i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu' );
			}
		};
		AJAX.post( url, data, callback );
	} );
	
	/*Edit city*/
	$( document ).off( 'click', '.edit-city' ).on( 'click', '.edit-city', function() {
		DOM.loadTplThisEl( '/admin/caidat/diadanh?action=edit&id=' + $( this ).data( 'id' ) );
	} );
	
	/*When edit*/
	$( document ).ready( function() {
		if( PARAMS.action == 'edit' ) {
			$( '.city-addnew-btn' ).click();
			DOM.enableSaveBtn();
		}
	} );
	
	/*Delete city*/
	$( document ).off( 'click', '.delete-city' ).on( 'click', '.delete-city', function() {
		var deleteFn = function( target ) {
			var url = 'include/24';
			var data = {
				'id': target.data( 'id' )
			};
			var callback = function( html ) {
				var htmlArr = html.split( '|' );
				DOM.showStatus( htmlArr[1], parseInt(htmlArr[0]) );
				DOM.loadTplThisEl( '/admin/caidat/diadanh' );
			};
			AJAX.post( url, data, callback );
		};
		var text = "Bạn có chắc chắn xóa tỉnh này?";
		DOM.showPopup( text, deleteFn, $( this ) );
	} );
} );