<?php
	if( isset( $_GET['action'] ) && $_GET['action'] == 'edit' ) {
		
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		$sql = new Sql();
		
		$city = $exec -> get( $sql -> get( 157 ), array(
			':setting_name' => 'city'
		) );
		
		$city = json_decode( $city[0]['setting_value'], true );
		
		$city = count( $city ) > 0 ? $city : array();
		
		//Find city by id
		foreach( $city as $value ) {
			if( $value['id'] == (int)$_GET['id'] ) {
				$city = $value;
			}
		}
		
		//Get district
		$district1 = '';
		$district2 = '';
		foreach( $city['district'] as $key2 => $arr2 ) {
			if( $arr2['special'] == 0 ) {
				$district1 .= $arr2['name'] . ',';
			}
			else {
				$district2 .= $arr2['name'] . ',';
			}
		}
		
		$district1 = trim( $district1, ',' );
		$district2 = trim( $district2, ',' );
		
		//Get area
		$select1 = $city['area'] == 1 ? 'selected' : '';
		$select2 = $city['area'] == 2 ? 'selected' : '';
		$select3 = $city['area'] == 3 ? 'selected' : '';
		$select4 = $city['area'] == 4 ? 'selected' : '';
		
		$html = '
			<table class="form-tables city-form">
				<tr>
					<td>Tên tỉnh</td>
					<td>
						<input value="' . $city['name'] . '" type="text" class="inputs area-textbox required-inputs" name="city_name" />
					</td>
				</tr>
				<tr>
					<td>Các huyện</td>
					<td>
						<textarea class="inputs area-textbox required-inputs" name="city_district" placeholder="Cách nhau dấu phẩy">' . $district1 . '</textarea>
					</td>
				</tr>
				<tr>
					<td>Các huyện vùng xa</td>
					<td>
						<textarea class="inputs area-textbox required-inputs" name="city_district_special" placeholder="Cách nhau dấu phẩy">' . $district2 . '</textarea>
					</td>
				</tr>
				<tr>
					<td>Vùng miền</td>
					<td>
						<select class="inputs area-textbox required-inputs" name="city_area">
							<option value="">Chọn vùng</option>
							<option value="1" ' . $select1 . '>Miền trung</option>
							<option value="2" ' . $select2 . '>Miền nam</option>
							<option value="3" ' . $select3 . '>Miền bắc</option>
							<option value="4" ' . $select4 . '>Hồ Chí Minh</option>
						</select>
					</td>
				</tr>
			</table>
		';
	}
	else {
		$html = '
			<table class="form-tables city-form">
				<tr>
					<td>Tên tỉnh</td>
					<td>
						<input type="text" class="inputs area-textbox required-inputs" name="city_name" />
					</td>
				</tr>
				<tr>
					<td>Các huyện</td>
					<td>
						<textarea class="inputs area-textbox required-inputs" name="city_district" placeholder="Cách nhau dấu phẩy"></textarea>
					</td>
				</tr>
				<tr>
					<td>Các huyện vùng xa</td>
					<td>
						<textarea class="inputs area-textbox required-inputs" name="city_district_special" placeholder="Cách nhau dấu phẩy"></textarea>
					</td>
				</tr>
				<tr>
					<td>Vùng miền</td>
					<td>
						<select class="inputs area-textbox required-inputs" name="city_area">
							<option value="">Chọn vùng</option>
							<option value="1">Miền trung</option>
							<option value="2">Miền nam</option>
							<option value="3">Miền bắc</option>
							<option value="4">Hồ Chí Minh</option>
						</select>
					</td>
				</tr>
			</table>
		';
	}
	echo $html;
?>