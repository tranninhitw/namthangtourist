<?php 
	if( isset( $_POST['id'] ) ) {
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		$sql = new Sql();
		
		$oldCity = $exec -> get( $sql -> get( 157 ), array(
			':setting_name' => 'city'
		) );
		$oldCity = $oldCity[0]['setting_value'];
		
		$oldCity = json_decode( $oldCity, true );
		
		foreach( $oldCity as $key => $city ) {
			if( $city['id'] == (int)$_POST['id'] ) {
				unset( $oldCity[$key] );
				$oldCity = array_values( $oldCity );
			}
		}
		
		$data = array(
			':setting_value' => json_encode( $oldCity ),
			':setting_name' => 'city'
		);
		
		$r = $exec -> exec( $sql -> get( 158 ), $data );
		$r ? print( '1|Xóa thành phố thành công' ) : print( '0|Có lỗi xảy ra khi cố xóa thành phố' );
	}
?>