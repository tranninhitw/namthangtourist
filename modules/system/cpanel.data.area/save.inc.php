<?php 
	if( isset( $_POST['city_name'] ) ) {
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		$sql = new Sql();
		
		$oldCity = $exec -> get( $sql -> get( 157 ), array(
			':setting_name' => 'city'
		) );
		$oldCity = $oldCity[0]['setting_value'];
		
		$condition = preg_match( '/"' . strip_tags( $_POST['city_name'] . '"/i' ), json_encode( json_decode( $oldCity ), JSON_UNESCAPED_UNICODE ) );
		
		$oldCity = json_decode( $oldCity, true );
		
		$oldCity = is_null( $oldCity ) ? array() : $oldCity;
		if( !$condition || ( isset( $_POST['action'] ) && $_POST['action'] == 'edit' ) ) {
			if( isset( $_POST['action'] ) && $_POST['action'] == 'edit' ) {
				$district = array();
				
				//Get normal district
				$districtList = array_filter( explode( ',', strip_tags( $_POST['city_district'] ) ), 'strlen' );
				$i = 0;
				foreach( $districtList as $value ) {
					$i++;
					array_push( $district, array(
						'id' => $i,
						'name' => $value,
						'special' => 0
					) );
				}
				
				//Get special district
				$districtList = array_filter( explode( ',', strip_tags( $_POST['city_district_special'] ) ), 'strlen' );
				foreach( $districtList as $value ) {
					$i++;
					array_push( $district, array(
						'id' => $i,
						'name' => $value,
						'special' => 1
					) );
				}
				$newCity = array(
					'id' => (int)$_POST['id'],
					'name' => strip_tags( $_POST['city_name'] ),
					'area' => strip_tags( $_POST['city_area'] ),
					'district' => $district
				);
				
				//Replace old city with new one
				for( $i = 0; $i < count( $oldCity ); $i++ ) {
					if( $oldCity[$i]['id'] == (int)$_POST['id'] ) {
						$oldCity[$i] = $newCity;
					}
				}
				
				$data = array(
					':setting_value' => json_encode( $oldCity ),
					':setting_name' => 'city'
				);
				
				$r = $exec -> exec( $sql -> get( 158 ), $data );
				$r ? print( '1|Sửa thành phố thành công' ) : print( '0|Có lỗi xảy ra khi cố sửa thành phố' );
			}
			else {
				$district = array();
				//Get normal district
				$districtList = array_filter( explode( ',', strip_tags( $_POST['city_district'] ) ), 'strlen' );
				$i = 0;
				foreach( $districtList as $value ) {
					$i++;
					array_push( $district, array(
						'id' => $i,
						'name' => $value,
						'special' => 0
					) );
				}
				
				//Get special district
				$districtList = array_filter( explode( ',', strip_tags( $_POST['city_district_special'] ) ), 'strlen' );
				foreach( $districtList as $value ) {
					$i++;
					array_push( $district, array(
						'id' => $i,
						'name' => $value,
						'special' => 1
					) );
				}
				
				//Newcity
				$newCity = array(
					'id' => count( $oldCity ) + 1,
					'name' => strip_tags( $_POST['city_name'] ),
					'area' => strip_tags( $_POST['city_area'] ),
					'district' => $district
				);
				
				array_push( $oldCity, $newCity );
				$data = array(
					':setting_value' => json_encode( $oldCity ),
					':setting_name' => 'city'
				);
				
				$r = $exec -> exec( $sql -> get( 158 ), $data );
				$r ? print( '1|Thêm thành phố thành công' ) : print( '0|Có lỗi xảy ra khi cố thêm thành phố' );
			}
		}
		else {
			echo '0|Trùng tên thành phố';
		}
	}
?>