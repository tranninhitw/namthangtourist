<?php 
	if( isset( $_POST['account_username'] ) ) {
		$smtp = new Smtp( SMTP_HOST, SMTP_USER, SMTP_PASS, SMTP_PORT, MAIL_FROM, MAIL_REPLY_TO );
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		$sql = new Sql();
		$hasher = new Password( 8, false );
		
		//Get data
		foreach( $_POST as $key => $value ) {
			$$key = $value;
		}		
		
		$data = array(
			':group_id' => $account_group,
			':admin_fullname' => $account_fullname,
			':admin_username' => $account_username,
			':admin_email' => $account_email,
			':admin_mobile' => $account_mobile
		);
		
		if( isset( $action ) && $action == 'edit' ) {
			$data[':admin_id'] = (int)$id;
			$r = $exec -> exec( $sql -> get( 182 ), $data );
			if( $r ) {
				echo '1|Lưu thành công';
			} else {
				echo '0|Thất bại';
			}
		}
		else {			
			$pass = '';
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$pass = '';
			for ($i = 0; $i < 6; $i++) {
				$pass .= $characters[rand( 0, $charactersLength - 1 )];
			}
			$data[':admin_password'] = $hasher -> HashPassword( $pass );
			//Generate string
			$string = str_shuffle( md5( microtime() ) );
			$data[':admin_resetpass_string'] = $string;
			$r = $exec -> add( $sql -> get( 183 ), $data );
			
			if( $r ) {
				//Send mail
				$sub = 'BẠN ĐÃ TRỞ THÀNH QUẢN TRỊ CỦA TRANG Quatangtraotay.com.vn';
				$contents = '
					<tr>
						<td style="padding: 0 40px; margin: 0 auto;">
							<p>Chúc mừng bạn đã trở thành quản trị của <b>Quatangtraotay.com.vn</b></p>
							<p>Bên dưới là thông tin đăng nhập của bạn:</p>
							<p>Tên đăng nhập: <b>' . $account_username . '</b>
							<p>Mật khẩu: <b>' . $pass . '</b></p>
							<p>Bạn vui lòng đăng nhập tại <a href="http://quatangtraotay.com.vn/admin">đây</a></p>
							<p>Sau khi đăng nhập vui lòng đổi lại mật khẩu.</p>
							<p>Trân trọng cám ơn!</p>
						</td>
					</tr>
				';
				$emailTpl = file_get_contents( _MODULES . '/home/email.tpl.php' );
				$contents = str_replace( '{@contents}', $contents, $emailTpl );
				$r = $smtp -> send( $account_email, $sub, $contents );
				if( $r ) {
					echo '1|Đăng ký thành công, vui lòng kiểm tra email';
				}
			}
			else {
				echo '0|Trùng email, tên đăng nhập hoặc số điện thoại';
			}
		}
	}
?>