{@style}
<section class="module-wrapper">
	<section class="module-toolbar">
		<h3 class="module-heading">Cài đặt / Nhóm <small>Cpanel</small></h3>
		<div class="module-button-area">
			<input class="inputs sgroup-search-textbox" placeholder="Nhập tên nhóm" />
			<span class="buttons normal-buttons sgroup-addnew-btn"><i class="fa fa-plus" aria-hidden="true"></i> Thêm</span>
			<span class="buttons normal-buttons sgroup-filter-btn"><i class="fa fa-filter" aria-hidden="true"></i> Lọc</span>
			<span class="buttons disabled-buttons sgroup-cancel-btn"><i class="fa fa-undo" aria-hidden="true"></i> Hủy</span>
		</div>
	</section>
	<section class="module-content">
		<div class="tabs module">
			<span class="tab-title">Thêm nhóm</span>
			<div class="module-addnew-btn">
				<span class="buttons disabled-buttons sgroup-save-new"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</span>
				<span class="buttons disabled-buttons sgroup-cancel-new"><i class="fa fa-undo" aria-hidden="true"></i> Hủy</span>
			</div>
			<table class="form-tables tbl">
				<tr>
					<td>Tên nhóm(*)</td>
					<td>
						<input type="text" class="inputs" placeholder="Tên nhóm" />
					</td>
				</tr>
				<tr>
					<td colspan="2">Quyền</td>
				</tr>
				<tr>
					<td class="privileges">					
						<input class="radios" type="radio" name="privilege" id="privilege1" />
						<label for="privilege1"></label>
						<span>Truy cập</span>
					</td>
					<td rowspan="2">
						<div class="cate-privilege">
							<table>
								<tr>
									<td>
										<input id="product-privilege" class="checkboxes" type="checkbox"/>
										<label for="product-privilege"></label>
										<span>Mục sản phẩm</span>
									</td>
								</tr>
								<tr>
									<td>
										<input id="sale-privilege" class="checkboxes" type="checkbox" />
										<label for="sale-privilege"></label>
										<span>Mục bán hàng</span>
									</td>
								</tr>
								<tr>
									<td>
										<input id="customer-privilege" class="checkboxes" type="checkbox" />
										<label for="customer-privilege"></label>
										<span>Mục khách hàng</span>
									</td>
								</tr>
								<tr>
									<td>
										<input id="content-privilege" class="checkboxes" type="checkbox" />
										<label for="content-privilege"></label>
										<span>Mục nội dung</span>
									</td>
								</tr>
								<tr>
									<td>
										<input id="statist-privilege" class="checkboxes" type="checkbox" />
										<label for="statist-privilege"></label>
										<span>Mục thống kê</span>
									</td>
								</tr>
								<tr>
									<td>
										<input id="appearance-privilege" class="checkboxes" type="checkbox" />
										<label for="appearance-privilege"></label>
										<span>Mục giao diện</span>
									</td>
								</tr>
								<tr>
									<td>
										<input id="setting-privilege" class="checkboxes" type="checkbox" />
										<label for="setting-privilege"></label>
										<span>Mục hệ thống</span>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td class="privileges">					
						<input class="radios" type="radio" name="privilege" id="privilege2" />
						<label for="privilege2"></label>
						<span>Chỉnh sửa</span>
					</td>
				</tr>
				<tr class="check">
					<td colspan="2">
						<input class="radios" type="radio" name="check" id="chekall" />
						<label for="chekall"></label>
						<span>Chọn tất cả</span>
						<input class="radios" type="radio" name="check" id="uncheckall" />
						<label for="uncheckall"></label>
						<span>Bỏ chọn tất cả</span>
					</td>
				</tr>
			</table>
		</div>
		<div class="group-list-wrapper">
			<table class="tables module">
				<tr class="title-rows">
					<td>Tên nhóm</td>
					<td>Biên tập</td>
				</tr>
				<tr>
					<td>Nhóm 1</td>
					<td>
						<button class="mini-buttons normal-buttons" title="Sửa"><i class="fa fa-pencil" aria-hidden="true"></i></button>
						<button class="mini-buttons cancel-buttons" title="Xóa"><i class="fa fa-trash" aria-hidden="true"></i></button>
					</td>
				</tr>
				<tr>
					<td>Nhóm 1</td>
					<td>
						<button class="mini-buttons normal-buttons" title="Sửa"><i class="fa fa-pencil" aria-hidden="true"></i></button>
						<button class="mini-buttons cancel-buttons" title="Xóa"><i class="fa fa-trash" aria-hidden="true"></i></button>
					</td>
				</tr>
				<tr>
					<td>Nhóm 1</td>
					<td>
						<button class="mini-buttons normal-buttons" title="Sửa"><i class="fa fa-pencil" aria-hidden="true"></i></button>
						<button class="mini-buttons cancel-buttons" title="Xóa"><i class="fa fa-trash" aria-hidden="true"></i></button>
					</td>
				</tr>
				<tr>
					<td>Nhóm 1</td>
					<td>
						<button class="mini-buttons normal-buttons" title="Sửa"><i class="fa fa-pencil" aria-hidden="true"></i></button>
						<button class="mini-buttons cancel-buttons" title="Xóa"><i class="fa fa-trash" aria-hidden="true"></i></button>
					</td>
				</tr>
			</table>
		</div>
		<div class="total-row">
			<span class="loadmore-btn">Tải thêm</span>
			<p class="showing-info">Đang hiển thị <b>2</b> trong số <b>2</b> nhóm khách hàng</p>
		</div>
	</section>
</section>