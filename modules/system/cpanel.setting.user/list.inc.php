<?php
	$exec = new Exec( HOST, USER, PASS, DBNAME );
	$sql = new Sql();

	$html = '';
	$account = $exec -> get( $sql -> get( 22 ) );

	if( count( $account ) !== 0 ) {
		foreach( $account as $key => $account ) {
			$name = $account['user_username'];
			$fullname = $account['user_fullname'];;
			// switch( $account['group_id'] ) {
			// 	case 1:
			// 		$type = 'Admin';
			// 		break;
			// 	case 2:
			// 		$type = 'CS';
			// 		break;
			// 	case 3:
			// 		$type = 'Writer';
			// 		break;
			// }

			$email = $account['user_email'];
			// $mobile = $account['admin_mobile'];

			$block = $account['user_blocked'] ? 'class="block"' : '';
			$button = $account['user_blocked'] ? 'fa-unlock' : '';
			$html .= '
				<tr ' . $block . '>
					<td>' . $name . '</td>
					<td>' . $fullname . '</td>
					<td>' . $email . '</td>
					<td>

						<button class="mini-buttons normal-buttons block-account-btn" title="Khóa" data-id="' . $account['user_id'] . '"><i class="fa fa-lock ' . $button . '" aria-hidden="true"></i></button>
						<button class="mini-buttons cancel-buttons delete-account-btn" title="Xóa" data-id="' . $account['user_id'] . '"><i class="fa fa-trash" aria-hidden="true"></i></button>
					</td>
				</tr>
			';
		}
	} else {
		$html .= '
			<tr>
				<td colspan="5">Chưa có tài khoản nào nào</td>
			</tr>
		';
	}

	echo $html;
?>
