<?php
	if( isset( $_POST['user_id'] ) ) {
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		$sql = new Sql();

		$data = array(
			'user_id' => $_POST['user_id']
		);
		$block = $exec -> get( $sql -> get( 23 ), $data );
		$block = $block[0]['user_blocked'];
		$block = $block ? 0 : 1;
		$save = array(
			'user_id' => $_POST['user_id'],
			'user_blocked' => $block
		);
		$r = $exec -> exec( $sql -> get( 24 ), $save );
		if( $r ) {
			echo 'Thành công';
		}
	}
?>
