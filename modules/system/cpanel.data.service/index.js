$( document ).ready( function() {

    var PARAMS = ULTI.queryString();

    /*Show addnew form*/
    $( document ).off( 'click', '.service-addnew-btn' ).on( 'click', '.service-addnew-btn', function() {
        MODULE.cmd( 'addnew', $( '.service-cancel-btn' ) );
    } );

    /*Hide addnew form*/
    $( document ).off( 'click', '.service-cancel-btn' ).on( 'click', '.service-cancel-btn', function() {
        MODULE.cmd( 'cancel', $( '.service-cancel-btn' ) );
    } );

    /*Validate form*/
    $( document ).on( 'input change', '.service-textbox', function() {
        var allDone = ULTI.validate( $( '.module-addnew .required-inputs' ), 'empty' );
        if( allDone ) {
            DOM.enableSaveBtn();
        }
        else {
            DOM.disableSaveBtn();
        }
    } );


    /*Save area*/
    $( document ).off( 'click', '.save-new-service' ).on( 'click', '.save-new-service', function() {
        $( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i> Đang lưu' );

        var url = 'include/23';
        var data = $( '.service-form .area-textbox' ).serialize();
        if( PARAMS.action == 'edit' ) {
            data += '&action=edit&id=' + PARAMS.id;
        }
        var callback = function( html ) {
            htmlArr = html.split( '|' );
            if( htmlArr[0] == '1' ) {
                ULTI.done( htmlArr[1] );
            }
            else {
                DOM.showStatus( htmlArr[1], parseInt( htmlArr[0] ) );
                $( '.save-new-service' ).removeClass( 'saving' ).html( '<i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu' );
            }
        };
        AJAX.post( url, data, callback );
    } );

    /*Edit service*/
    $( document ).off( 'click', '.edit-service' ).on( 'click', '.edit-service', function() {
        DOM.loadTplThisEl( '/admin/caidat/diadanh?action=edit&id=' + $( this ).data( 'id' ) );
    } );

    /*When edit*/
    $( document ).ready( function() {
        if( PARAMS.action == 'edit' ) {
            $( '.service-addnew-btn' ).click();
            DOM.enableSaveBtn();
        }
    } );

    /*Delete service*/
    $( document ).off( 'click', '.delete-service' ).on( 'click', '.delete-service', function() {
        var deleteFn = function( target ) {
            var url = 'include/24';
            var data = {
                'id': target.data( 'id' )
            };
            var callback = function( html ) {
                var htmlArr = html.split( '|' );
                DOM.showStatus( htmlArr[1], parseInt(htmlArr[0]) );
                DOM.loadTplThisEl( '/admin/caidat/diadanh' );
            };
            AJAX.post( url, data, callback );
        };
        var text = "Bạn có chắc chắn xóa dịch vụ đi kèm này?";
        DOM.showPopup( text, deleteFn, $( this ) );
    } );
} );