<?php
$exec = new Exec( HOST, USER, PASS, DBNAME );
$sql = new Sql();

$html = '
		<div class="tabs module-addnew">
			<table class="form-tables tbl">
				<tr>
					<td>Tên dịch vụ đi kèm </td>
					<td>
						<input name="service_name" type="text" class="inputs service-textbox required-inputs" placeholder="Tên dịch vụ đi kèm " />
					</td>
				</tr>
			</table>
		</div>
	';

if( isset( $_GET['action'], $_GET['id'] ) && $_GET['action'] == 'edit' ) {
    //Get service by id
    $data = array(
        ':service_id' => (int)$_GET['id'],
    );
    $service = $exec -> get( $sql -> get( 15 ), $data );
    $service = $service[0];
    $service_name = $service['service_name'];
    $html = '
		<div class="tabs module-addnew">
			<span class="tab-title">Thêm dịch vụ đi kèm </span>
			<div class="module-addnew-btn">
				<span class="buttons disabled-buttons service-save-new save-btn"><i class="fa fa-floppy-o"></i> Lưu</span>
			</div>
			<table class="form-tables tbl">
				<tr>
					<td>Tên dịch vụ đi kèm </td>
					<td>
						<input name="service_name" type="text" class="inputs service-textbox required-inputs" placeholder="Tên dịch vụ đi kèm " value="'. $service_name .'"/>
					</td>
				</tr>
			</table>
		</div>
	';
}
echo $html;
?>
