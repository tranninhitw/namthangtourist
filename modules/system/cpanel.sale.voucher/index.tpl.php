{@style}
<section class="module-wrapper">
	<section class="module-toolbar">
		<h3 class="module-heading">Bán hàng / Voucher <small>Cpanel</small></h3>
		<div class="module-button-area">
			<span class="buttons normal-buttons voucher-addnew-btn"><i class="fa fa-plus" aria-hidden="true"></i> Thêm</span>
			<span class="buttons disabled-buttons voucher-cancel-btn"><i class="fa fa-undo" aria-hidden="true"></i> Hủy</span>
		</div>
	</section>
	<section class="module-content">
		<div class="module-addnew tabs">
			{@form-addnew}
		</div>
		<div class="voucher-list-wrapper">
			<table class="tables module">
				<tr class="title-rows">
					<td>Tên khuyến mãi</td>
					<td>Giá trị</td>
					<td>Voucher</td>
					<td>Thời gian</td>
					<td>Biên tập</td>
				</tr>
				{@list}
			</table>
		</div>
	</section>
</section>
{@script}