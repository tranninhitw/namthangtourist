<?php 
	$html = '
		<span class="tab-title">Thêm voucher</span>
		<span class="buttons disabled-buttons save-btn save-new-voucher"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</span>
		<table class="form-tables addnew-voucher-tbl">
			<tr>
				<td>Tên khuyến mãi </td>
				<td><input type="text" class="inputs voucher-textbox required-inputs" name="voucher_name" placeholder="Tên khuyến mãi" /></td>
			</tr>
			<tr>
				<td>Giá trị khuyến mãi</td>
				<td class="special-cell">
					<input type="text" name="voucher_discount" class="voucher-textbox inputs currency-inputs" />
					<select class="selects voucher-textbox voucher-unit" name="voucher_unit">
						<option value="1" selected>VNĐ</option>
						<option value="2">%</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Cộng điểm</td>
				<td>
					<input type="number" class="inputs voucher-textbox" name="voucher_point" value="0" />
				</td>
			</tr>
			<tr>
				<td>Bắt đầu</td>
				<td>
					<input type="text" class="inputs datetime-picker voucher-textbox required-inputs" name="voucher_start" placeholder="dd/mm/yyyy hh:mm:ss" />
				</td>
			</tr>
			<tr>
				<td>Kết thúc</td>
				<td>
					<input type="text" class="inputs datetime-picker voucher-textbox required-inputs" placeholder="dd/mm/yyyy hh:mm:ss" name="voucher_end" />
				</td>
			</tr>
			<tr>
				<td>Voucher code</td>
				<td>
					<table class="form-tables child-table">
						<tr>
							<td>Voucher prefix:</td>
							<td>
								<input type="text" class="inputs generate-prefix" placeholder="Prefix" /> 
							</td>
						</tr>
						<tr>
							<td>Số lượng voucher:</td>
							<td>
								<input type="number" class="inputs generate-amount" value="20" /> 
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<span class="buttons normal-buttons generate-voucher">Tạo code</span>
							</td>
						</tr>
					</table>
					<textarea class="inputs generate-result voucher-textbox required-inputs" name="voucher_code" readonly></textarea>
				</td>
			</tr>
		</table>
	';
	if( isset( $_GET['action'] ) && $_GET['action'] == 'edit' ) {
		$id = (int)$_GET['id'];
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		$sql = new Sql();
		$voucher = $exec -> get( $sql -> get( 131 ), array(
			':voucher_id' => $id
		) );
		$voucher = $voucher[0];
		
		//Get voucher value (% OR VND)
		$value = $voucher['voucher_value'] < 1 ? $voucher['voucher_value']*100 : number_format( $voucher['voucher_value'], 0, ',', '.' );
		
		//Get voucher unit
		$selected1 = $voucher['voucher_value'] < 1 ? '' : 'selected';
		$selected2 = $voucher['voucher_value'] < 1 ? 'selected' : '';
		$class = $voucher['voucher_value'] < 1 ? '' : 'currency-inputs';
		
		$html = '
			<span class="tab-title">Sửa voucher</span>
			<span class="buttons disabled-buttons save-btn save-new-voucher"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</span>
			<table class="form-tables addnew-voucher-tbl">
				<tr>
					<td>Tên khuyến mãi </td>
					<td><input value="' . $voucher['voucher_name'] . '" type="text" class="inputs voucher-textbox required-inputs" name="voucher_name" placeholder="Tên khuyến mãi" /></td>
				</tr>
				<tr>
					<td>Giá trị khuyến mãi</td>
					<td class="special-cell">
						<input value="' . $value . '" type="text" name="voucher_discount" class="voucher-textbox inputs ' . $class . '" />
						<select class="selects voucher-textbox voucher-unit" name="voucher_unit">
							<option value="1" ' . $selected1 . '>VNĐ</option>
							<option value="2" ' . $selected2 . '>%</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Cộng điểm</td>
					<td>
						<input type="number" class="inputs voucher-textbox" name="voucher_point" value="' . $voucher['voucher_points'] . '" />
					</td>
				</tr>
				<tr>
					<td>Bắt đầu</td>
					<td>
						<input type="text" class="inputs datetime-picker voucher-textbox required-inputs" name="voucher_start" placeholder="dd/mm/yyyy hh:mm:ss" value="' . date( 'd/m/Y H:i:s', $voucher['voucher_start'] ) . '" />
					</td>
				</tr>
				<tr>
					<td>Kết thúc</td>
					<td>
						<input type="text" class="inputs datetime-picker voucher-textbox required-inputs" placeholder="dd/mm/yyyy hh:mm:ss" value="' . date( 'd/m/Y H:i:s', $voucher['voucher_end'] ) . '" name="voucher_end" />
					</td>
				</tr>
				<tr>
					<td>Voucher code</td>
					<td>
						<textarea class="inputs generate-result voucher-textbox required-inputs" name="voucher_code" readonly>' . $voucher['voucher_code'] . '</textarea>
					</td>
				</tr>
			</table>
		';
	}
	
	echo $html;
?>