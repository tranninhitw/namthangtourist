<?php
	//Turn off autoload
	$autoloadFuncs = spl_autoload_functions();
	foreach($autoloadFuncs as $unregisterFunc)
	{
		spl_autoload_unregister($unregisterFunc);
	}
	require_once _CORE . '/phpexcel.cls.php';
	
	//Get order from db
	if( isset( $_GET['export'] ) && $_GET['export'] == 'xls' ) {
		
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		$sql = new Sql();
		
		//Get all code
		$voucher = $exec -> get( $sql -> get( 262 ), array(
			':voucher_id' => (int)$_GET['voucher_id']
		) );
		$voucher = $voucher[0];
		
		$codes = $voucher['voucher_code_backup'];
		$codes = array_filter( explode( ',', $codes ), 'strlen' );
	
		//Build xls data
		$objPHPExcel = new PHPExcel();
		
		//Set title of document
		$objPHPExcel -> getActiveSheet() 
			-> setCellValue( 'A1', 'BÁO CÁO VOUCHER' )
			-> setCellValue( 'A2', 'Tên chương trình: ', strtoupper( $voucher['voucher_name'] ) );
			
		$objPHPExcel -> getActiveSheet() -> mergeCells( 'A1:E1' );
		$objPHPExcel -> getActiveSheet() -> mergeCells( 'A2:E2' );
		$objPHPExcel -> getActiveSheet() 
			-> setCellValue( 'A3', 'STT' )
			-> setCellValue( 'B3', 'Voucher' )
			-> setCellValue( 'C3', 'Giá trị' )
			-> setCellValue( 'D3', 'Cộng điểm' )
			-> setCellValue( 'E3', 'Thời hạn sử dụng (%)' );
		$columns = range( 'A', 'E' );
		$titleRows = 4;
		
		foreach( $codes as $key => $code ) {
			//Rows for sheet
			$rows = array(
				$key + 1,
				$code,
				$voucher['voucher_value'],
				$voucher['voucher_points'],
				date( 'd/m/Y', $voucher['voucher_start'] ) . '-' . date( 'd/m/Y', $voucher['voucher_end'] )
			);
			foreach( $columns as $key1 => $column ) {
				$objPHPExcel -> getActiveSheet() -> setCellValue( $column . ( $key + $titleRows ), $rows[$key1] );
			}
		}
		
		//Styling sheet
		$styleArray = array(
			'font'  => array(
				'bold'  => false,
				'color' => array('rgb' => '515151'),
				'size'  => 12,
				'name'  => 'Arial',
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		
		$objPHPExcel -> getActiveSheet() -> getStyle( 'A3:' . $objPHPExcel -> getActiveSheet() -> getHighestColumn() . $objPHPExcel -> getActiveSheet() -> getHighestRow() ) -> applyFromArray( $styleArray );
		
		//Styling heading
		$styleArray = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FF0000'),
				'size'  => 14,
				'name'  => 'Arial',
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
		);
		$objPHPExcel -> getActiveSheet() -> getStyle( 'A1:E2' ) -> applyFromArray( $styleArray );
		
		//Styling heading of table
		$styleArray = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => '515151'),
				'size'  => 12,
				'name'  => 'Arial',
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'BDF2FC')
			)
		);
		$objPHPExcel -> getActiveSheet() -> getStyle( 'A3:E3' ) -> applyFromArray( $styleArray );
		
		//Autosize column width
		PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
		foreach( range( 'A','E' ) as $columnID ) {
			$objPHPExcel -> getActiveSheet() -> getColumnDimension( $columnID ) -> setAutoSize( true );
		}
		
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle( $voucher['voucher_name'] );

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Quatangtraotay-report.xls"');
		header('Cache-Control: max-age=0');
		
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}
?>