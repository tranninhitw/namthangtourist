<?php 
	$exec = new Exec( HOST, USER, PASS, DBNAME );
	$sql = new Sql();
	
	if( isset( $_POST['voucher_name'] ) ) {
		if( isset( $_POST['action'] ) && $_POST['action'] == 'edit' ) {
			$start = date_create_from_format( 'd/m/Y H:i:s', strip_tags( $_POST['voucher_start'] ) );
			$end = date_create_from_format( 'd/m/Y H:i:s', strip_tags( $_POST['voucher_end'] ) );
			$value = $_POST['voucher_unit'] == '1' ? str_replace( '.', '', $_POST['voucher_discount'] ) : (float)$_POST['voucher_discount'] / 100;
			
			$data = array(
				':voucher_name' => strip_tags( $_POST['voucher_name'] ),
				':voucher_value' => $value,
				':voucher_points' => strip_tags( $_POST['voucher_point'] ),
				':voucher_start' => $start -> getTimeStamp(),
				':voucher_end' => $end -> getTimeStamp(),
				':voucher_id' => (int)$_POST['id']
			);
			
			$r = $exec -> exec( $sql -> get( 132 ), $data );
			$r ? print( '1|Lưu thay đổi thành công' ) : print( '0|Trùng tên chương trình' );	
		}
		else if ( !isset( $_POST['action'] ) ) {
			$start = date_create_from_format( 'd/m/Y H:i:s', strip_tags( $_POST['voucher_start'] ) );
			$end = date_create_from_format( 'd/m/Y H:i:s', strip_tags( $_POST['voucher_end'] ) );
			$value = $_POST['voucher_unit'] == '1' ? str_replace( '.', '', $_POST['voucher_discount'] ) : (float)$_POST['voucher_discount'] / 100;
			$data = array(
				':voucher_name' => strip_tags( $_POST['voucher_name'] ),
				':voucher_value' => $value,
				':voucher_points' => strip_tags( $_POST['voucher_point'] ),
				':voucher_start' => $start -> getTimeStamp(),
				':voucher_end' => $end -> getTimeStamp(),
				':voucher_code' => strip_tags( $_POST['voucher_code'] ),
				':voucher_code_backup' => strip_tags( $_POST['voucher_code'] )
			);
			
			$r = $exec -> add( $sql -> get( 129 ), $data );
			$r ? print( '1|Thêm voucher thành công' ) : print( '0|Trùng tên chương trình' );	
		}
	}
?>