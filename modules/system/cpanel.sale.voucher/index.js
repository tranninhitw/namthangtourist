$( document ).ready( function() {
	/*Click add*/
	$( document ).on( 'click', '.voucher-addnew-btn', function() {
		MODULE.cmd( 'addnew', $( '.voucher-cancel-btn' ) );
	} );
	
	var PARAMS = ULTI.queryString();
	
	/*Validate addnew*/
	$( document ).on( 'change input', '.voucher-textbox', function() {
		var allDone = ULTI.validate( $( '.addnew-voucher-tbl .required-inputs' ), 'empty' );
		if( allDone ) {
			DOM.enableSaveBtn();
		}
		else {
			DOM.disableSaveBtn();
		}
	} );
	
	/*When chang voucher unit*/
	$( document ).off( 'change', '.voucher-unit' ).on( 'change', '.voucher-unit', function() {
		if( $( this ).val() == '1' ) {
			$( this ).prev().addClass( 'currency-inputs' );
		}
		else {
			$( this ).prev().removeClass( 'currency-inputs' );
		}
	} );
	
	/*Generate voucher code*/
	$( document ).on( 'click', '.generate-voucher', function() {
		var string = '';
		var number = $( '.generate-amount' ).val();
		var prefix = $( '.generate-prefix' ).val();
		if( prefix && number > 0 ) {
			$( '.generate-alert' ).remove();
			for( var i = 0; i < number; i++ ) {
				string = string + prefix + generateUUID() + ',';
			}
		}
		else {
			$( this ).parent().prev().html( '<span class="generate-alert">Chưa điền đủ prefix và số lượng!</span>' );
		}
		$( '.generate-result' ).val( string.substring( 0, string.length - 1 ) ).change();
	} );
	
	/*Save new voucher*/
	$( document ).off( 'click', '.save-new-voucher' ).on( 'click', '.save-new-voucher', function() {
		$( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i> Đang lưu' );
		var url = 'include/15';
		var data = $( '.voucher-textbox' ).serialize();
		if( PARAMS.action == 'edit' ) {
			data += '&action=edit&id=' + PARAMS.id;
		}
		var callback = function( html ) {
			var htmlArr = html.split( '|' );
			if( htmlArr[0] == '1' ) {
				ULTI.done( htmlArr[1] );
			}
			else {
				DOM.showStatus( htmlArr[1], parseInt( htmlArr[0] ) );
				$( '.save-new-voucher' ).removeClass( 'saving' ).html( '<i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu' );
			}
			// var htmlArr = html.split( '|' );
			// DOM.showStatus( htmlArr[1], htmlArr[0] );
			// DOM.disableSaveBtn();
			// $( '.voucher-cancel-btn' ).click();
		};
		AJAX.post( url, data, callback );
	} );
	
	/*Edit voucher*/
	$( document ).on( 'click', '.edit-voucher', function() {
		DOM.loadTplThisEl( '/admin/banhang/voucher?action=edit&id=' + $( this ).data( 'id' ) );
	} );
	
	/*When edit*/
	$( document ).ready( function() {
		if( PARAMS.action == 'edit' ) {
			//Fade In addnew form
			$( '.voucher-addnew-btn' ).click();
		}
	} );
	
	/*Generate Voucher*/
	function generateUUID() {
		var d = new Date().getTime();
		if(window.performance && typeof window.performance.now === "function"){
			d += performance.now();; //use high-precision timer if available
		}
		var uuid = 'xxxxxx'.replace(/[xy]/g, function(c) {
			var r = (d + Math.random()*16)%16 | 0;
			d = Math.floor(d/16);
			return (c=='x' ? r : (r&0x3|0x8)).toString(16);
		});
		return uuid;
	};
	
	/*Click Huy*/
	$( document ).off( 'click', '.voucher-cancel-btn' ).on( 'click', '.voucher-cancel-btn', function() {
		//Clear data
		DOM.loadTplThisEl( '/admin/banhang/voucher' );
	} );
	
	/*Delete voucher 2*/
	$( document ).off( 'click', '.delete-voucher2' ).on( 'click', '.delete-voucher2', function() {
		var deleteFn = function( target ) {
			var id = target.data( 'id' );
			var url = 'include/16';
			var data = {
				'voucher_id': id
			};
			var tr = target.parent().parent();
			var callback = function( html ) {
				var htmlArr = html.split( '|' );
				DOM.showStatus( htmlArr[1], htmlArr[0] );
				tr.remove();
			};
			AJAX.post( url, data, callback );
		};
		var text = 'Bạn có chắc chắn muốn xóa voucher?';
		DOM.showPopup( text, deleteFn, $( this ) );
	} );
	
	/*Datetimepciker*/
	$( document ).ready( function() {
		$.datetimepicker.setLocale( 'vi' );
		jQuery( '.datetime-picker' ).datetimepicker({
		  timepicker: true,
		  format:'d/m/Y H:i:s'
		});
	} );
} );