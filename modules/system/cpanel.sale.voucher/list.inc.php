<?php 
	$exec = new Exec( HOST, USER, PASS, DBNAME );
	$sql = new Sql();
	
	$vouchers = $exec -> get( $sql -> get( 130 ) );
	$html = '';
	
	if( count( $vouchers ) == 0 ) {
		$html = '
			<tr>
				<td colspan="5">Hiện chưa tạo voucher</td>
			</tr>
		';
	}
	
	foreach( $vouchers as $key => $voucher ) {
		$start = new DateTime( date( 'm/d/Y H:i:s', $voucher['voucher_start'] ) );
		$end = new DateTime( date( 'm/d/Y H:i:s', $voucher['voucher_end'] ) );
		$diff = $start -> diff( $end );
		if( $start == $end || $start > $end ) {
			$remain = 'Đã hết hạn';
		}
		else if ( $end > $start ) {
			$remain = 'Còn <b>' . $diff -> y . '</b> năm <b>' . $diff -> m . '</b> tháng <b>' . $diff -> d . '</b> ngày <b>' . $diff -> h . '</b> giờ <b>' . $diff -> i . '</b> phút <b>' . $diff -> s . '</b> giây';
		}
		
		//Get voucher value
		$value = $voucher['voucher_value'] < 1 ? $voucher['voucher_value']*100 . '%' : number_format( $voucher['voucher_value'], 0, ',', '.' ) . ' VNĐ';
		
		$html .= '
			<tr>
				<td>' . $voucher['voucher_name'] . '</td>
				<td>
					<span class="voucher-discount-label">-' . $value . '</span> <span class="voucher-point-label">+' . $voucher['voucher_points'] . ' điểm</span>
				</td>
				<td>Còn ' . count( array_filter( explode( ',', $voucher['voucher_code'] ), 'strlen' ) ) . ' voucher</td>
				<td style="position:relative;">
					<span class="voucher-date-label">' . $remain . '</span>
					<span class="voucher-time-label">' . date( 'H:i:s d/m/Y', $voucher['voucher_start'] ) . ' - ' . date( 'H:i:s d/m/Y', $voucher['voucher_end'] ) . '</span>
				</td>	
				<td>
					<button class="mini-buttons normal-buttons edit-voucher" title="Sửa" data-id="' . $voucher['voucher_id'] . '"><i class="fa fa-pencil" aria-hidden="true"></i></button>
					<button class="mini-buttons cancel-buttons delete-voucher2" title="Xóa" data-id="' . $voucher['voucher_id'] . '"><i class="fa fa-trash" aria-hidden="true"></i></button>
					<a href="include/61?export=xls&voucher_id=' . $voucher['voucher_id'] . '" target="_blank" class="mini-buttons save-buttons export-voucher" title="Xuất excel" data-id="' . $voucher['voucher_id'] . '"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
				</td>
			</tr>
		';
	}
	echo $html;
?>