<?php
$exec = new Exec( HOST, USER, PASS, DBNAME );
$sql = new Sql();

$html = '
		<table class="form-tables tbl">
            <tr>
                <td>Địa chỉ email</td>
                <td>
                    <input name="company_email" type="text" class="inputs guide-textbox required-inputs" placeholder="Địa chỉ email" />
                </td>
            </tr>

            <tr>
                <td>Hotline</td>
                <td>
                    <input name="company_hotline" type="text" class="inputs guide-textbox required-inputs" placeholder="Số điện thoại hotline" />
                </td>
            </tr>
            
            <tr>
                <td>Số điện thoại tàu xe</td>
                <td>
                    <input name="company_phone_train" type="text" class="inputs guide-textbox required-inputs" placeholder="Số điện thoại tàu xe" />
                </td>
            </tr>
            
            <tr>
                <td>Số điện thoại tour mạo hiểm</td>
                <td>
                    <input name="company_phone_risky" type="text" class="inputs guide-textbox required-inputs" placeholder="Số điện thoại tour mạo hiểm" />
                </td>
            </tr>

        </table>
	';
echo $html;
?>