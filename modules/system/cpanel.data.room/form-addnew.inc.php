<?php
$exec = new Exec( HOST, USER, PASS, DBNAME );
$sql = new Sql();

$html = '
		<div class="tabs module-addnew">
			<table class="form-tables tbl">
				<tr>
					<td>Tên khách sạn (*)</td>
					<td>
						<select name="hotel_id" id="hotel_id" class="inputs room-textbox required-inputs">
                            <option value="">Khách sạn hoàng gia</option>
                            <option value="">Khách sạn 5*</option>
                        </select>
					</td>
				</tr>
				<tr>
					<td>Giá phòng (*)</td>
					<td>
						<input type="text" name="room_price" class="inputs room-textbox required-inputs room_price" placeholder="Giá phòng" />
					</td>
				</tr>
				
				<tr>
					<td>Loại phòng (*)</td>
					<td>
						<input type="text" name="room_type" class="inputs room-textbox required-inputs room_type" placeholder="Loại phòng" />
					</td>
				</tr>
				
				<tr>
					<td>Số lượng phòng (*)</td>
					<td>    
						<input type="text" name="room_quantity" class="inputs room-textbox required-inputs room_quantity" placeholder="Số lượng vé" />
					</td>
				</tr>
			</table>
		</div>
	';

if( isset( $_GET['action'], $_GET['id'] ) && $_GET['action'] == 'edit' ) {
    //Get room by id
    $data = array(
        ':room_id' => (int)$_GET['id'],
    );
    $room = $exec -> get( $sql -> get( 15 ), $data );
    $room = $room[0];
    $room_name = $room['room_name'];
    $html = '
		<div class="tabs module-addnew">
			<span class="tab-title">Thêm vé </span>
			<div class="module-addnew-btn">
				<span class="buttons disabled-buttons room-save-new save-btn"><i class="fa fa-floppy-o"></i> Lưu</span>
			</div>
			<table class="form-tables tbl">
				<tr>
					<td>Tên tour (*)</td>
					<td>
						<select name="tour_id" id="tour_id" class="inputs room-textbox required-inputs">
                            <option value="">Vũng tàu- đà lạt</option>
                            <option value="">Đại nam - phú quốc</option>
                        </select>
					</td>
				</tr>
				<tr>
					<td>Nơi khởi hành (*)</td>
					<td>
						<input value="' . $room['room_departure'] . '" type="text" name="room_departure" class="inputs room-textbox required-inputs room_departure" placeholder="Nơi khởi hành" />
					</td>
				</tr>
				<tr>
					<td>Nơi đến (*)</td>
					<td>
						<input value="' . $room['room_type'] . '" type="text" name="room_type" class="inputs room-textbox required-inputs room_type" placeholder="Nơi đến" />
					</td>
				</tr>
				<tr>
					<td>Ngày giờ (*)</td>
					<td>
						<input value="' . $room['room_datetime'] . '" type="text" name="room_datetime" class="inputs room-textbox required-inputs room_datetime" placeholder="Ngày giờ" />
					</td>
				</tr>
				<tr>
					<td>Giá vé (*)</td>
					<td>
						<input value="' . $room['room_price'] . '" type="text" name="room_price" class="inputs room-textbox required-inputs room_price" placeholder="Giá vé" />
					</td>
				</tr>
				<tr>
					<td>Số lượng vé (*)</td>
					<td>    
						<input value="' . $room['	room_quantity'] . '" type="text" name="	room_quantity" class="inputs room-textbox required-inputs 	room_quantity" placeholder="Số lượng vé" />
					</td>
				</tr>
			</table>
		</div>
	';
}
echo $html;
?>
