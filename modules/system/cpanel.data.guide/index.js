$( document ).ready( function() {

    var PARAMS = ULTI.queryString();

    /*Show addnew form*/
    $( document ).off( 'click', '.guide-addnew-btn' ).on( 'click', '.guide-addnew-btn', function() {
        MODULE.cmd( 'addnew', $( '.guide-cancel-btn' ) );
    } );

    /*Hide addnew form*/
    $( document ).off( 'click', '.guide-cancel-btn' ).on( 'click', '.guide-cancel-btn', function() {
        MODULE.cmd( 'cancel', $( '.guide-cancel-btn' ) );
    } );

    /*Validate form*/
    $( document ).on( 'input change', '.area-textbox', function() {
        var allDone = ULTI.validate( $( '.module-addnew .required-inputs' ), 'empty' );
        if( allDone ) {
            DOM.enableSaveBtn();
        }
        else {
            DOM.disableSaveBtn();
        }
    } );


    /*Save area*/
    $( document ).off( 'click', '.save-new-guide' ).on( 'click', '.save-new-guide', function() {
        $( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i> Đang lưu' );

        var url = 'include/23';
        var data = $( '.guide-form .area-textbox' ).serialize();
        if( PARAMS.action == 'edit' ) {
            data += '&action=edit&id=' + PARAMS.id;
        }
        var callback = function( html ) {
            htmlArr = html.split( '|' );
            if( htmlArr[0] == '1' ) {
                ULTI.done( htmlArr[1] );
            }
            else {
                DOM.showStatus( htmlArr[1], parseInt( htmlArr[0] ) );
                $( '.save-new-guide' ).removeClass( 'saving' ).html( '<i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu' );
            }
        };
        AJAX.post( url, data, callback );
    } );

    /*Edit guide*/
    $( document ).off( 'click', '.edit-guide' ).on( 'click', '.edit-guide', function() {
        DOM.loadTplThisEl( '/admin/caidat/diadanh?action=edit&id=' + $( this ).data( 'id' ) );
    } );

    /*When edit*/
    $( document ).ready( function() {
        if( PARAMS.action == 'edit' ) {
            $( '.guide-addnew-btn' ).click();
            DOM.enableSaveBtn();
        }
    } );

    /*Delete guide*/
    $( document ).off( 'click', '.delete-guide' ).on( 'click', '.delete-guide', function() {
        var deleteFn = function( target ) {
            var url = 'include/24';
            var data = {
                'id': target.data( 'id' )
            };
            var callback = function( html ) {
                var htmlArr = html.split( '|' );
                DOM.showStatus( htmlArr[1], parseInt(htmlArr[0]) );
                DOM.loadTplThisEl( '/admin/caidat/diadanh' );
            };
            AJAX.post( url, data, callback );
        };
        var text = "Bạn có chắc chắn xóa hướng dẫn viên này?";
        DOM.showPopup( text, deleteFn, $( this ) );
    } );
} );