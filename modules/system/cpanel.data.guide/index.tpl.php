{@style}
<section class="module-wrapper">
    <section class="module-toolbar">
        <h3 class="module-heading">Quản lý / hướng dẫn viên <small>Cpanel</small></h3>
        <div class="module-button-area">
            <span class="buttons normal-buttons guide-addnew-btn"><i class="fa fa-plus"></i> Thêm</span>
            <span class="buttons disabled-buttons guide-cancel-btn"><i class="fa fa-undo"></i> Hủy</span>
        </div>
    </section>
    <section class="module-content">
        <div class="module-addnew tabs">
            <span class="tab-title">Thêm hướng dẫn viên</span>
            <span class="buttons disabled-buttons save-btn save-new-guide"><i class="fa fa-floppy-o"></i> Lưu</span>
            {@form-addnew}
        </div>
        <div class="area-list-wrapper">
            <table class="tables">
                <tr class="title-rows">
                    <td>Tên hướng dẫn viên</td>
                    <td>Số điện thoại</td>
                    <td>Địa chỉ</td>
                </tr>
                {@list}
            </table>
        </div>
    </section>
</section>
{@script}