<?php
$exec = new Exec( HOST, USER, PASS, DBNAME );
$sql = new Sql();

$html = '
		<div class="tabs module-addnew">
		
			<table class="form-tables tbl">
				<tr>
					<td>Tên hướng dẫn viên</td>
					<td>
						<input name="guide_name" type="text" class="inputs guide-textbox required-inputs" placeholder="Tên hướng dẫn viên" />
					</td>
				</tr>

				<tr>
					<td>Điện thoại</td>
					<td>
						<input name="guide_phone" type="text" class="inputs guide-textbox required-inputs" placeholder="Số điện thoại" />
					</td>
				</tr>
				<tr>
					<td>Địa chỉ (nếu có)</td>
					<td>
						<input name="guide_address" type="text" class="inputs guide-textbox required-inputs" placeholder="Địa chỉ" />
					</td>
				</tr>

		
			</table>
		</div>
	';

if( isset( $_GET['action'], $_GET['id'] ) && $_GET['action'] == 'edit' ) {
    //Get guide by id
    $data = array(
        ':guide_id' => (int)$_GET['id'],
    );
    $guide = $exec -> get( $sql -> get( 15 ), $data );
    $guide = $guide[0];

    $guide_name = $guide['guide_name'];
    $guide_phone = $guide['guide_phone'];
    $guide_address = $guide['guide_address'];

    $html = '
		<div class="tabs module-addnew">
			<span class="tab-title">Thêm hướng dẫn viên</span>
			<div class="module-addnew-btn">
				<span class="buttons disabled-buttons guide-save-new save-btn"><i class="fa fa-floppy-o"></i> Lưu</span>
			</div>
			<table class="form-tables tbl">
				<tr>
					<td>Tên hướng dẫn viên</td>
					<td>
						<input name="guide_name" type="text" class="inputs guide-textbox required-inputs" placeholder="Tên hướng dẫn viên" value="'. $guide_name .'"/>
					</td>
				</tr>
				<tr>
					<td>Số điện thoại</td>
					<td>
						<input name="guide_phone" type="text" class="inputs guide-textbox required-inputs" placeholder="Số điện thoại" value="'. $guide_phone .'"/>
					</td>
				</tr>
				
				<tr>
					<td>Địa chỉ (nếu có)</td>
					<td>
						<input name="guide_address" type="text" class="inputs guide-textbox required-inputs" placeholder="Địa chỉ" value="'. $guide_address .'" />
					</td>
				</tr>
			</table>
		</div>
	';
}
echo $html;
?>
