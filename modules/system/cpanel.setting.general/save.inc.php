<?php
	$exec = new Exec( HOST, USER, PASS, DBNAME );
	$sql = new Sql();
	
	//Get data
	foreach( $_POST as $key => $value ) {
		$$key = $value;
	}
	$settings = $exec -> get( $sql -> get( 187 ) );
	foreach( $settings as $key => $value ) {
		if( $value['setting_name'] == 'default_avatar' ) {
			$default_avatar_tbl = $value['setting_value'];
		}
		if( $value['setting_name'] == 'company_information' ) {
			$company_information_tbl = $value['setting_value'];
		}	
	}
	
	$company_information_arr = json_decode( $company_information_tbl, true );
	$company_information_arr['company_name'] = $company_name;
	$company_information_arr['company_address'] = $company_address;
	$company_information_arr['company_tax_code'] = $company_tax_code;
	$company_information_arr['company_phone'] = $company_phone;
	$company_information_arr['company_fax'] = $company_fax;
	$company_information_arr['company_website'] = $company_website;
	$company_information_arr['company_logo'] = $company_logo == '' ? $company_information_arr['company_logo'] : $company_logo;
	$company_information_arr['company_bill_line_1'] = $company_bill_line_1;
	$company_information_arr['company_bill_line_2'] = $company_bill_line_2;

	$data_minimum_order_value = array(
		'setting_value' => str_replace( '.', '', $minimum_order_value ),
		'setting_name' => 'minimum_order_value'
	);
	
	$data_tax_rate = array(
		'setting_value' => $tax_rate,
		'setting_name' => 'tax_rate'
	);
	
	$data_default_avatar = array(
		'setting_value' => $default_avatar == '' ? $default_avatar_tbl : $default_avatar,
		'setting_name' => 'default_avatar'
	);
	
	$data_how_many_point_equal_to_balance = array(
		'setting_value' => $how_many_point_equal_to_balance,
		'setting_name' => 'how_many_point_equal_to_balance'
	);
	
	$data_minimum_point_to_covert_to_balance = array(
		'setting_value' => $minimum_point_to_covert_to_balance,
		'setting_name' => 'minimum_point_to_covert_to_balance'
	);
	
	$data_minimum_in_stock = array(
		'setting_value' => $minimum_in_stock,
		'setting_name' => 'minimum_in_stock'
	);
	
	$data_company_information = array(
		'setting_value' => json_encode( $company_information_arr ),
		'setting_name' => 'company_information'
	);
	
	$r = $exec -> exec( $sql -> get( 188 ), $data_minimum_order_value );
	$r = $exec -> exec( $sql -> get( 188 ), $data_default_avatar );
	$r = $exec -> exec( $sql -> get( 188 ), $data_how_many_point_equal_to_balance );
	$r = $exec -> exec( $sql -> get( 188 ), $data_minimum_point_to_covert_to_balance );
	$r = $exec -> exec( $sql -> get( 188 ), $data_minimum_in_stock );
	$r = $exec -> exec( $sql -> get( 188 ), $data_company_information );
	$r = $exec -> exec( $sql -> get( 188 ), $data_tax_rate );
	
	$r ? print( '1|Lưu thành công' ) : print( '0|Có lỗi xảy ra' );
?>