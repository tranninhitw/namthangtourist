{@style}
<section class="module-wrapper">
    <section class="module-toolbar">
        <h3 class="module-heading">Cấu hình / Thông tin công ty <small>Cpanel</small></h3>
    </section>
    <section class="module-content">
        <span class="buttons save-buttons disabled-buttons contact-save-new save-btn"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</span>
        {@list}
    </section>
</section>
{@script}