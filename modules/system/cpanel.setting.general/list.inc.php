<?php
$exec = new Exec( HOST, USER, PASS, DBNAME );
$sql = new Sql();

$html = '
		<table class="form-tables tbl">
            <tr>
                <td>Tên công ty</td>
                <td>
                    <input name="company_name" type="text" class="inputs guide-textbox required-inputs" placeholder="Tên công ty" />
                </td>
            </tr>
            
            <tr>
                <td>Địa chỉ trụ sở</td>
                <td>
                    <input name="company_address" type="text" class="inputs guide-textbox required-inputs" placeholder="Địa chỉ trụ sở" />
                </td>
            </tr>
            
            <tr>
                <td>Địa chỉ văn phòng giao dịch</td>
                <td>
                    <input name="transaction_office_address" type="text" class="inputs guide-textbox required-inputs" placeholder="Địa chỉ văn phòng giao dịch" />
                </td>
            </tr>
        </table>
	';
echo $html;
?>