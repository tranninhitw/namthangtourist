$( document ).ready( function() {	
	/*Global fileList*/
	var fileList = [];
	
	/*PARAMS*/
	PARAMS = ULTI.queryString();
	
	/*Preview when upload avatar*/
	$( document ).off( 'change', '.company-logo' ).on( 'change', '.company-logo', function() {
		var len = this.files.length;
		if( len > 0 ) {
			$( '.company-logo-preview' ).empty();
			var reader = new FileReader();
			reader.onload = function( e ) {
				$( '.company-logo-preview' ).append( '<div class="preview-el"><img src="' + e.target.result + '" /></div>' );
				$( '.tab-radio' ).change();
			};
			reader.readAsDataURL( this.files[0] );
		}
		else {
			$( '.company-logo-preview' ).empty();
			$( '.tab-radio' ).change();
		}
	} );
	
	$( document ).off( 'change', '.default-avatar' ).on( 'change', '.default-avatar', function() {
		var len = this.files.length;
		if( len > 0 ) {
			$( '.default-avatar-preview' ).empty();
			var reader = new FileReader();
			reader.onload = function( e ) {
				$( '.default-avatar-preview' ).append( '<div class="preview-el"><img src="' + e.target.result + '" /></div>' );
				$( '.tab-radio' ).change();
			};
			reader.readAsDataURL( this.files[0] );
		}
		else {
			$( '.default-avatar-preview' ).empty();
			$( '.tab-radio' ).change();
		}
	} );
	
	/*On input general-textbox*/
	$( document ).on( 'input change', '.general-textbox', function() {
		DOM.enableSaveBtn();
	} );
	
	/*Upload avatar when click upload*/
	$( document ).off( 'click', '.upload-image2' ).on( 'click', '.upload-image2', function() {
		var file = $( '.company-logo' )[0].files;
		if( file.length > 0 ) {
			var input = $( '.company-logo-input' );
			var destination = 'backgrounds';
			var imageName = 'billlogo';
			var callback = function( html ) {
				input.val( html );
			};
			ULTI.upload( file, destination, imageName, callback );
		}
	} );
	
	$( document ).off( 'click', '.upload-image' ).on( 'click', '.upload-image', function() {
		var file = $( '.default-avatar' )[0].files;
		if( file.length > 0 ) {
			var input = $( '.default-avatar-input' );
			var destination = 'avatars';
			var imageName = 'avatar';
			var callback = function( html ) {
				input.val( html );
			};
			ULTI.upload( file, destination, imageName, callback );
		}
	} );
	
	/*Save setting*/
	$( document ).off( 'click', '.general-save-new' ).on( 'click', '.general-save-new', function() {
		var confirmFn = function( target ) {
			target.addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i> Đang lưu' );
			
			//Upload image
			$( '.upload-image, .upload-image2' ).click();
			
			//Save data
			var data = $( '.module .general-textbox' ).serialize();
			var url = 'include/44';
			var callback = function( html ) {
				htmlArr = html.split( '|' );
				if( htmlArr[0] == '1' ) {
					ULTI.done( htmlArr[1] );
				}
				else {
					DOM.showStatus( htmlArr[1], parseInt( htmlArr[0] ) );
					target.removeClass( 'saving' ).html( '<i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu' );
				}
			};
			AJAX.post( url, data, callback );
		};		
		var text = 'Bạn có muốn lưu các thiết lập?';
		DOM.showPopup( text, confirmFn, $( this ) );
	} );
} ); 