<?php 
	$html = '
		<table class="form-tables">
			<tr>
				<td>Giá trị đơn hàng</td>
				<td>
					<input name="sort_by_money" value="asc" id="sort-by-money-asc" type="radio" class="radios filter-textbox" checked />
					<label for="sort-by-money-asc"></label> <span class="label">Tăng dần</span>
					<input name="sort_by_money" value="desc" id="sort-by-money-desc" type="radio" class="radios filter-textbox" />
					<label for="sort-by-money-desc"></label> <span class="label">Giảm dần</span>
				</td>
			</tr>
			<tr>
				<td>Giá trị đơn hàng</td>
				<td>
					<span class="label">Từ: </span><span class="currency-input-wrapper"><input type="text" class="inputs currency-inputs filter-textbox" name="filter_by_money_from" /></span>
					<span class="label">đến: </span><span class="currency-input-wrapper"><input type="text" class="inputs currency-inputs filter-textbox" name="filter_by_money_to" /></span>
				</td>
			</tr>
			<tr>
				<td>Trạng thái đơn hàng</td>
				<td>
					<select name="filter_by_state" class="selects filter-textbox">
						<option value="0">Tất cả</option>
						<option value="1">Đã tiếp nhận</option>
						<option value="2">Đang đóng gói</option>
						<option value="3">Đang giao hàng</option>
						<option value="4">Đã giao hàng</option>
						<option value="6">Chờ CS xác nhận hủy</option>
						<option value="5">Đã hủy</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Ngày đặt hàng</td>
				<td>
					<span class="label">Từ: </span><input type="text" class="inputs datetime-picker filter-textbox" name="filter_by_time_from" placeholder="dd/mm/yyyy" />
					<span class="label">đến: </span><input type="text" class="inputs datetime-picker filter-textbox" name="filter_by_time_to" placeholder="dd/mm/yyyy" />
				</td>
			</tr>
			<tr>
				<td>Ngày giao hàng</td>
				<td>
					<span class="label">Từ: </span><input type="text" class="inputs datetime-picker filter-textbox" name="filter_by_shipping_time_from" placeholder="dd/mm/yyyy" />
					<span class="label">đến: </span><input type="text" class="inputs datetime-picker filter-textbox" name="filter_by_shipping_time_to" placeholder="dd/mm/yyyy" />
				</td>
			</tr>
		</table>
	';
	
	if( isset( $_GET['filter'] ) && $_GET['filter'] == 'true' ) {
		//Get $_GET
		$sortByMoney = strip_tags( $_GET['sort_by_money'] );
		$filterByState = strip_tags( $_GET['filter_by_state'] );
		$filterByTimeFrom = strip_tags( $_GET['filter_by_time_from'] );
		$filterByTimeTo = strip_tags( $_GET['filter_by_time_to'] );
		$filterByShippingTimeFrom = strip_tags( $_GET['filter_by_shipping_time_from'] );
		$filterByShippingTimeTo = strip_tags( $_GET['filter_by_shipping_time_to'] );
		$filterByMoneyFrom = strip_tags( $_GET['filter_by_money_from'] );
		$filterByMoneyTo = strip_tags( $_GET['filter_by_money_to'] );
		
		//Radio sort by money
		$checked1 = $sortByMoney  == 'asc' ? 'checked' : '';
		$checked2 = $sortByMoney  == 'desc' ? 'checked' : '';
		
		//Select box filter by state
		$select = array( '', '', '', '', '', '', '' );
		$select[$filterByState] = 'selected';

		$html = '
			<table class="form-tables">
				<tr>
					<td>Giá trị đơn hàng</td>
					<td>
						<input name="sort_by_money" value="asc" id="sort-by-money-asc" type="radio" class="radios filter-textbox" ' . $checked1 . ' />
						<label for="sort-by-money-asc"></label> <span class="label">Tăng dần</span>
						<input name="sort_by_money" value="desc" id="sort-by-money-desc" type="radio" class="radios filter-textbox" ' . $checked2 . ' />
						<label for="sort-by-money-desc"></label> <span class="label">Giảm dần</span>
					</td>
				</tr>
				<tr>
					<td>Giá trị đơn hàng</td>
					<td>
						<span class="label">Từ: </span><span class="currency-input-wrapper"><input type="text" class="inputs currency-inputs filter-textbox" name="filter_by_money_from" value="' . $filterByMoneyFrom . '" /></span>
						<span class="label">đến: </span><span class="currency-input-wrapper"><input type="text" class="inputs currency-inputs filter-textbox" name="filter_by_money_to" value="' . $filterByMoneyTo . '" /></span>
					</td>
				</tr>
				<tr>
					<td>Trạng thái đơn hàng</td>
					<td>
						<select name="filter_by_state" class="selects filter-textbox">
							<option value="0" ' . $select[0] . '>Tất cả</option>
							<option value="1" ' . $select[1] . '>Đã tiếp nhận</option>
							<option value="2" ' . $select[2] . '>Đang đóng gói</option>
							<option value="3" ' . $select[3] . '>Đang giao hàng</option>
							<option value="4" ' . $select[4] . '>Đã giao hàng</option>
							<option value="6" ' . $select[6] . '>Chờ CS xác nhận hủy</option>
							<option value="5" ' . $select[5] . '>Đã hủy</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Ngày đặt hàng</td>
					<td>
						<span class="label">Từ: </span><input type="text" class="inputs datetime-picker filter-textbox" name="filter_by_time_from" value="' . $filterByTimeFrom . '" placeholder="dd/mm/yyyy" />
						<span class="label">đến: </span><input type="text" class="inputs datetime-picker filter-textbox" name="filter_by_time_to" value="' . $filterByTimeTo . '" placeholder="dd/mm/yyyy" />
					</td>
				</tr>
				<tr>
					<td>Ngày giao hàng</td>
					<td>
						<span class="label">Từ: </span><input type="text" class="inputs datetime-picker filter-textbox" name="filter_by_shipping_time_from" value="' . $filterByShippingTimeFrom . '" placeholder="dd/mm/yyyy" />
						<span class="label">đến: </span><input type="text" class="inputs datetime-picker filter-textbox" name="filter_by_shipping_time_to" value="' . $filterByShippingTimeTo . '" placeholder="dd/mm/yyyy" />
					</td>
				</tr>
			</table>
		';
	}
	
	echo $html;
?>