<?php
	//Turn off autoload
	$autoloadFuncs = spl_autoload_functions();
	foreach($autoloadFuncs as $unregisterFunc)
	{
		spl_autoload_unregister($unregisterFunc);
	}
	require_once _CORE . '/phpexcel.cls.php';
	
	function uni_convert( $str ) {
		return str_replace( array('["', '"]'), array('',''), json_encode( array( $str ) ) );
	}
	
	function filter( $exec, $sql ) {
		//Update shipping time for temporary table
		$tempOrders = $exec -> get( $sql -> get( 249 ) );
		foreach( $tempOrders as $key => $tempOrder ) {
			$time = json_decode( $tempOrder['order_shipping_infos'], true );
			$time = $time['time'];
			$exec -> exec( $sql -> get( 250 ), array(
				':order_id' => $tempOrder['order_id'],
				':order_shipping_time' => $time
			) );
		}
		
		//Get $_GET
		$sortByMoney = strip_tags( $_GET['sort_by_money'] );
		$filterByState = strip_tags( $_GET['filter_by_state'] );
		$filterByTimeFrom = strip_tags( $_GET['filter_by_time_from'] );
		$filterByTimeTo = strip_tags( $_GET['filter_by_time_to'] );
		$filterByShippingTimeFrom = strip_tags( $_GET['filter_by_shipping_time_from'] );
		$filterByShippingTimeTo = strip_tags( $_GET['filter_by_shipping_time_to'] );
		$filterByMoneyFrom = str_replace( '.', '', strip_tags( $_GET['filter_by_money_from'] ) );
		$filterByMoneyTo = str_replace( '.', '', strip_tags( $_GET['filter_by_money_to'] ) );
		$keyword = isset( $_GET['s'] ) ? $_GET['s'] : '';
		
		//Convert to time
		$timeF = strtotime( str_replace( '/', '-', $filterByTimeFrom ) );
		$timeT = strtotime( str_replace( '/', '-', $filterByTimeTo ) );
		$shipT = strtotime( str_replace( '/', '-', $filterByShippingTimeTo ) );
		$shipF = strtotime( str_replace( '/', '-', $filterByShippingTimeFrom ) );
		
		//Get SQL query
		$inClause = '';
		$sqlTextS = '';
		if( !empty( $keyword ) ) {
			$sqlTextS = ' AND MATCH( t1.order_code, t1.order_recipient_infos, t1.order_who_make_infos) AGAINST( \'' . str_replace( '\\', '\\\\', strip_tags( uni_convert( $keyword ) ) ) . '\' IN BOOLEAN MODE )';
		}
		
		$sqlText1 = $filterByState != '0' ? ' t2.state_id = ' . $filterByState : ' 1=1';
		$sqlText2 = $timeF > 0 ? ' AND t2.order_time >= ' . $timeF : '';
		$sqlText3 = $timeT > 0 ? ' AND ' . $timeT . ' >= t2.order_time' : '';
		$sqlText4 = $shipF > 0 ? ' AND t2.order_shipping_time >= ' . $shipF : '';
		$sqlText5 = $shipT > 0 ? ' AND ' . $shipT . ' >= t2.order_shipping_time' : '';
		$sqlText6 = (float)$filterByMoneyFrom > 0 ? ' AND t2.order_total_money > ' . (float)$filterByMoneyFrom : '';
		$sqlText7 = (float)$filterByMoneyTo > 0 ? ' AND t2.order_total_money < ' . (float)$filterByMoneyTo : '';
		$sqlText8 = $sortByMoney == 'desc' ? ' ORDER BY order_total_money DESC' : ' ORDER BY order_total_money ASC';
		$viewName = 'filter_view_' . str_replace( array( '.', ':' ), array( '', '' ), $_SERVER['REMOTE_ADDR'] );
		$sqlText = "CREATE VIEW " . $viewName . " AS SELECT t1.* FROM dp_orders AS t1 JOIN dp_temp_orders AS t2 ON t1.order_id = t2.order_id WHERE" . $sqlText1 . $sqlText2 . $sqlText3 . $sqlText4 . $sqlText5 . $sqlText6 . $sqlText7 . $sqlTextS . ';';
		
		//Create view
		$exec -> exec( $sqlText );
		$orders = $exec -> get( "SELECT * FROM " . $viewName . $sqlText8 );
		
		$exec -> exec( "DROP VIEW IF EXISTS " . $viewName . ";" );
		
		return $orders;
	}
	
	//Get order from db
	if( isset( $_GET['export'] ) && $_GET['export'] == 'xls' ) {
		
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		$sql = new Sql();
		
		//If not apply filter
		if( !isset( $_GET['filter'] ) ) {
			$orders = $exec -> get( $sql -> get( 214 ) );
		}
		else {
			$orders = filter( $exec, $sql );
		}
	}
	
	//Build xls data
	$objPHPExcel = new PHPExcel();
	
	//Set title of document
	$subHeading = 'Từ trước đến nay';
	if( isset( $_GET['filter_by_time_from'] ) && !empty( $_GET['filter_by_time_from'] ) ) {
		$toText = isset( $_GET['filter_by_time_to'] ) && !empty( $_GET['filter_by_time_to'] ) ? ' đến ' . $_GET['filter_by_time_to'] : '';
		$subHeading = 'Từ ngày ' . $_GET['filter_by_time_from'] . $toText;
	}
	$objPHPExcel -> getActiveSheet() 
		-> setCellValue( 'A1', 'BÁO CÁO ĐƠN HÀNG' )
		-> setCellValue( 'A2', $subHeading );
		
	$objPHPExcel -> getActiveSheet() -> mergeCells( 'A1:J1' );
	$objPHPExcel -> getActiveSheet() -> mergeCells( 'A2:J2' );
	$objPHPExcel -> getActiveSheet() 
		-> setCellValue( 'A3', 'STT' )
		-> setCellValue( 'B3', 'Ngày đặt' )
		-> setCellValue( 'C3', 'Mã đơn hàng' )
		-> setCellValue( 'D3', 'Tên khách hàng' )
		-> setCellValue( 'E3', 'Người đặt hàng' )
		-> setCellValue( 'F3', 'Số điện thoại' )
		-> setCellValue( 'G3', 'Địa chỉ người đặt hàng' )
		-> setCellValue( 'H3', 'Số tiền' )
		-> setCellValue( 'I3', 'Tình trạng' )
		-> setCellValue( 'J3', 'Thông tin xuất hóa đơn' );
	$columns = range( 'A', 'J' );
	$titleRows = 4;
	
	foreach( $orders as $key => $order ) {
		//Get recipient info
		$recipient = json_decode( $order['order_recipient_infos'], true );
		$fullname1 = $recipient['fullname'];
		
		//Get who make fullname
		$whomake = json_decode( $order['order_who_make_infos'], true );
		$fullname2 = $whomake['fullname'];
		
		//Get state label
		$state = $exec -> get( $sql -> get( 106 ), array(
			':state_id' => $order['state_id']
		) );
		$stateLabel = $state[0]['state_name'];
		
		//Mobile number
		$mobile = $recipient['different_address'] == '1' ? $recipient['mobile'] : $whomake['mobile']; 
		
		//Bill infos
		$bill = json_decode( $order['order_bill_infos'], true );
		$billInfos = $bill['need_bill'] == '1' ? 'Công ty: ' . $bill['company_name'] . ', MST: ' . $bill['company_tax_code'] . ', Địa chỉ: ' . $bill['company_address'] : '';
		
		//Rows for sheet
		$rows = array(
			$key + 1,
			date( 'd-m-Y', $order['order_time'] ),
			$order['order_code'],
			$fullname1,
			$fullname2,
			$mobile,
			$whomake['address'],
			$order['order_total_money'],
			$stateLabel,
			$billInfos
		);
		foreach( $columns as $key1 => $column ) {
			$objPHPExcel -> getActiveSheet() -> setCellValue( $column . ( $key + $titleRows ), $rows[$key1] );
		}
	}
	
	//Styling sheet
	$styleArray = array(
		'font'  => array(
			'bold'  => false,
			'color' => array('rgb' => '515151'),
			'size'  => 12,
			'name'  => 'Arial',
		),
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);
	
	$objPHPExcel -> getActiveSheet() -> getStyle( 'A3:' . $objPHPExcel -> getActiveSheet() -> getHighestColumn() . $objPHPExcel -> getActiveSheet() -> getHighestRow() ) -> applyFromArray( $styleArray );
	
	//Styling heading
	$styleArray = array(
		'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => 'FF0000'),
			'size'  => 14,
			'name'  => 'Arial',
		),
		'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
	);
	$objPHPExcel -> getActiveSheet() -> getStyle( 'A1:J2' ) -> applyFromArray( $styleArray );
	
	//Styling heading of table
	$styleArray = array(
		'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => '515151'),
			'size'  => 12,
			'name'  => 'Arial',
		),
		'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        ),
		'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'BDF2FC')
        )
	);
	$objPHPExcel -> getActiveSheet() -> getStyle( 'A3:J3' ) -> applyFromArray( $styleArray );
	
	//Autosize column width
	PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
	foreach( range( 'A','J' ) as $columnID ) {
		$objPHPExcel -> getActiveSheet() -> getColumnDimension( $columnID ) -> setAutoSize( true );
	}
	
	// Rename worksheet
	$objPHPExcel->getActiveSheet()->setTitle('Đơn hàng');

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Quatangtraotay-report.xls"');
	header('Cache-Control: max-age=0');
	
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;
?>