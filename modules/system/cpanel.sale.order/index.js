$( document ).ready( function() {	
	var PARAMS = ULTI.queryString();
	
	/*Click link of order*/
	$( document ).on( 'click', '.order-detail-link', function( e ) {
		e.preventDefault();
		DOM.loadTplThisEl( $( this ) );
	} );
	
	/*Search order*/
	$( document ).off( 'keyup', '.order-search-textbox' ).on( 'keyup', '.order-search-textbox', function( e ) {
		if( e.which == 13 || e.keyCode == 13 ) {
			var value = $( this ).val();
			if( value ) {
				DOM.loadTplThisEl( '/admin/banhang/dondathang?s=' + encodeURIComponent( value ) );
			}
		}
	} );
	
	/*Load more*/
	$( document ).off( 'click', '.loadmore-btn' ).on( 'click', '.loadmore-btn', function() {
		$( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i> Đang tải' );
		var offset =  $( this ).data( 'offset' );
		offset = parseInt( offset ) + 5;
		var url = 'include/10?' + location.href.split( '?' )[1];
		var data = {
			'offset': offset	
		};
		var _this = $( this );
		var tbl = $( '.order-table' );
		var callback = function( html ) {
			$( '.loadmore-btn' ).removeClass( 'saving' ).html( 'Tải thêm' );
			if( html.search( 'colspan="6"' ) === -1 ) {
				tbl.find( 'tbody' ).append( html );
				_this.data( 'offset', offset );
				var row = tbl.find( 'tr' ).length;
				row = parseInt( row ) - 1;
				$( '.viewing' ).text( row );
			}
			else {
				var emptyRow = '<tr><td colspan="6">Đã tải hết tất cả</td></tr>';
				tbl.find( 'tr:last-of-type' ).after( emptyRow );
				_this.remove();
			}
		};
		AJAX.get( url, data, callback );
	} );
	
	/*Click filter button*/
	$( document ).off( 'click', '.filter-btn' ).on( 'click', '.filter-btn', function() {
		MODULE.cmd( 'addnew', $( '.cancel-btn' ) );
	} );
	
	/*Click cancel button*/
	$( document ).off( 'click', '.cancel-btn' ).on( 'click', '.cancel-btn', function() {
		MODULE.cmd( 'cancel', $( '.cancel-btn' ) );
	} );
	
	/*Datetimepciker*/
	$( document ).ready( function() {
		$.datetimepicker.setLocale( 'vi' );
		jQuery( '.datetime-picker' ).datetimepicker({
		  timepicker:false,
		  format:'d/m/Y'
		});
	} );
	
	/*When change filter-textbox*/
	$( document ).off( 'change input', '.filter-textbox' ).on( 'change input', '.filter-textbox', function() {
		$( '.filter-apply-btn' ).removeClass( 'disabled-buttons' ).addClass( 'save-buttons' );
	} );
	
	/*Click filter-apply button*/
	$( document ).off( 'click', '.filter-apply-btn' ).on( 'click', '.filter-apply-btn', function() {
		var data = $( '.filter-textbox' ).serialize() + '&filter=true';
		if( PARAMS.s ) {
			data += '&s=' + PARAMS.s;
		}
		DOM.loadTplThisEl( '/admin/banhang/dondathang?' + data );
	} );
	
	/*Change href for export xls*/
	$( document ).ready( function() {
		if( PARAMS.filter == 'true' ) {
			$( '.export-xls' ).prop( 'href', 'include/49?' + location.href.split( '?' )[1] + '&export=xls' );
		}
	} );
} );