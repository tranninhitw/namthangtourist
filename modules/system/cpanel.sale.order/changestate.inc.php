<?php 
	if( isset( $_POST['order_code'] ) ) {
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		$sql = new Sql();
		$smtp = new Smtp( SMTP_HOST, SMTP_USER, SMTP_PASS, SMTP_PORT, MAIL_FROM, MAIL_REPLY_TO );
		$sms = new Sms( SMS_USER, SMS_PASS, SMS_SENDER );
		
		//Get data
		$orderCode = strip_tags( $_POST['order_code'] );
		$stateId = strip_tags( $_POST['state_id'] );
		$currentState = strip_tags( $_POST['current_state'] );
		
		//Fn 
		function send_mail( $order, $state, $smtp, $exec, $sql ) {
			//Send or not
			$post = json_decode( $order['order_who_make_infos'], true );
			if( $post['email'] == '' ) 
				return;
			else
				$to = $post['email'];
			
			//Get recipient infos
			$recipientInfos = json_decode( $order['order_recipient_infos'], true );
			
			//Get products, discount
			$products = json_decode( $order['order_product'], true );
			$discount = json_decode( $order['order_discount'], true );
			
			//Get product list
			$productList = '';
			$i = 0;
			$totalMoney = 0;
			foreach( $products as $key => $product ) {
				$i++;
				$image = json_decode( $product['product_avatar'], true );
				$image = '<img width="40" height="40" style="width: 100%; height: auto; display: block;" src="http://quatangtraotay.com.vn/uploads/media/' . $image[0] . '" />';
				$money = $product['product_price'] * $product['buy_amount'];
				$totalMoney += $money;	
				$money = number_format( $money, 0, ',', '.' );
				$color = array( 'odd' => '#f2f2f2', 'even' => '#FFFFFF' );
				$type = $i % 2 == 0 ? 'even' : 'odd';
				$productList .= '
					<tr style="background-color: ' . $color[$type] . ';">
						<td style="border-color: #ddd; width: 10%; padding: 5px 10px">' . $image . '</td>
						<td style="border-color: #ddd; padding: 10px;">' . $product['product_name'] . '</td>
						<td style="border-color: #ddd; text-align: right; padding: 10px;">' . number_format( $product['product_price'], 0, ',', '.' ) . '</td>
						<td style="border-color: #ddd; width: 15%; text-align: center; padding: 10px">' . $product['buy_amount'] . '</td>
						<td style="border-color: #ddd; text-align: right; padding: 10px">' . $money . '</td>
					</tr>
				';
			}		

			//Get promote list
			$promoteList = '';
			if( $discount['voucher'] > 0 ) {
				$promoteList .= '
					<tr>
						<td style="width: 70%;"><i>Mã khuyến mãi: </i></td>
						<td><span><b>- ' . $discount['voucher'] . '%</b></span></td>
					</tr>
				';	
			}
			if( $discount['commission'] > 0 ) {
				$promoteList .= '
					<tr>
						<td style="width: 70%;"><i>Chiết khấu theo giá trị đơn hàng: </i></td>
						<td><span><b>- ' . number_format( $discount['commission'], 0, ',', '.' ) . ' VNĐ</b></span></td>
					</tr>
				';
			}
			if( $discount['balance'] > 0 ) {
				$promoteList .= '
					<tr>
						<td style="width: 70%;"><i>Tài khoản thưởng: </i></td>
						<td><span><b>- ' . number_format( $discount['balance'], 0, ',', '.' ) . ' VNĐ</b></span></td>
					</tr>
				';
			}
			
			$address = $recipientInfos['different_address'] == 1 ? $recipientInfos['address'] : $post['address'];
			$finalMoney = $order['order_final_money'];
			$orderTime = date( 'H:i', time() );
			$orderDate = date( 'd/m/Y', time() );
			if( $state == '4' ) {
				$sub = 'ĐƠN HÀNG ĐÃ ĐƯỢC GIAO THÀNH CÔNG';
				$contents = '
					<tr>
						<td style="padding: 0 40px; margin: 0 auto;">
							<table style="border-collapse: collapse; table-layout: fixed; width: 100%; margin-bottom: 30px;">
								<tr>
									<td style="padding: 10px;">Xin chào ' . $post['fullname'] . '</td>
								</tr>
								<tr>
									<td style="padding: 10px;"><b>Quatangtraotay.com.vn</b> xin chúc mừng và thông báo đơn hàng mã số <b>' . $orderCode . '</b> của bạn đã được giao lúc <b>' . $orderTime . '</b>, ngày <b>' . $orderDate . '</b>.</td>
								</tr>
								<tr>
									<td style="padding: 10px; border: 2px solid #ddd; border-bottom: none;">
										<table border="1" style="border-collapse: collapse;table-layout: fixed; width: 100%; border: 1px solid #ddd;">
											<tr style="text-align: center; background-color: #eef7e9; color: #0a6746;">
												<td style="width: 10%; border-color: #ddd;"></td>
												<td style="padding: 10px; border-color: #ddd;"><b>Tên sản phẩm</b></td>
												<td style="padding: 10px; border-color: #ddd;"><b>Đơn giá</b></td>
												<td style="width: 15%; padding: 10px; border-color: #ddd;"><b>Số lượng</b></td>
												<td style="padding: 10px; border-color: #ddd;"><b>Thành tiền</b></td>
											</tr>
											' . $productList . '
										</table>
										<div style="border-bottom: 1px solid #ddd; width: 100%; margin: 20px 0;"></div>
										<table style="border-collapse: collapse;table-layout: fixed; width: 100%; text-align: right;">
											<tr>
												<td style="width: 70%;">Trị giá hàng hóa</td>
												<td><span><b>' . number_format( $totalMoney, 0, ',', '.' ) . '</b><span><span> VNĐ</span></td>
											</tr>
											' . $promoteList . '
											<tr>
												<td style="width: 70%;">
													<p style="margin: 0;"><b>Trị giá đơn hàng</b></p>
													<h5 style="font-weight: normal; margin: 0;">(Đã bao gồm VAT)</h5>
												</td>
												<td><span><b>' . number_format( $finalMoney, 0, ',', '.' ) . '</b><span><span> VNĐ</span></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style="padding: 10px; border: 2px solid #ddd; border-top: none;">
										<table style="border-collapse: collapse;table-layout: fixed; width: 100%; text-align: left;">
											<tr>
												<td colspan="2" style="text-align: left; background-color: #eaf4e4; padding: 5px;">Địa chỉ giao hàng: ' . $address . '</td>
											</tr>
											<tr><td colspan="2" style="padding: 5px;"></td></tr>
											<tr>
												<td style="text-align: left; background-color: #eaf4e4; padding: 5px;">Thông tin <b>Người Đặt Hàng</b></td>
												<td style="text-align: left; background-color: #eaf4e4; padding: 5px;">Thông tin <b>Người Nhận Hàng</b></td>
											</tr>
											<tr>
												<td style="text-align: left; padding: 5px;">
													<p>' . $post['fullname'] . '</p>
													<p>' . $post['mobile'] . '</p>
													<p>' . $post['address'] . '</p>
												</td>
												<td style="text-align: left; padding: 5px;">
													<p>' . $recipientInfos['fullname'] . '</p>
													<p>' . $recipientInfos['mobile'] . '</p>
													<p>' . $recipientInfos['address'] . '</p>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style="padding: 5px 0;"><i>Xin cảm ơn bạn đã đặt hàng tại Quatangtraotay.com.vn</i></td>
								</tr>
								<tr>
									<td style="padding: 5px 0;">Nếu có bất kỳ vấn đề gì, xin vui lòng gọi 1900 7290 để được hỗ trợ</td>
								</tr>
								<tr>
									<td style="padding: 5px 0;"><i>Kính chào trân trọng.</i></td>
								</tr>
							</table>
						</td>
					</tr>
				';
			}
			else if ( $state == '5' ) {
				$sub = 'ĐƠN HÀNG ĐÃ ĐƯỢC HỦY';
				$contents = '
					<tr>
						<td style="padding: 0 40px; margin: 0 auto;">
							<table style="border-collapse: collapse; table-layout: fixed; width: 100%; margin-bottom: 30px;">
								<tr>
									<td style="padding: 10px;">Xin chào ' . $post['fullname'] . '</td>
								</tr>
								<tr>
									<td style="padding: 10px;"><b>Quatangtraotay.com.vn</b> xin thông báo đơn hàng <b>' . $orderCode . '</b> của bạn đã được hủy lúc <b>' . $orderTime . '</b>, ngày <b>' . $orderDate . '</b>.</td>
								</tr>
								<tr>
									<td style="padding: 10px; border: 2px solid #ddd;">
										<table border="1" style="border-collapse: collapse;table-layout: fixed; width: 100%; border: 1px solid #ddd;">
											<tr style="text-align: center; background-color: #eef7e9; color: #0a6746;">
												<td style="width: 10%; border-color: #ddd;"></td>
												<td style="padding: 10px; border-color: #ddd;"><b>Tên sản phẩm</b></td>
												<td style="padding: 10px; border-color: #ddd;"><b>Đơn giá</b></td>
												<td style="width: 15%; padding: 10px; border-color: #ddd;"><b>Số lượng</b></td>
												<td style="padding: 10px; border-color: #ddd;"><b>Thành tiền</b></td>
											</tr>
											' . $productList . '
										</table>
										<div style="border-bottom: 1px solid #ddd; width: 100%; margin: 20px 0;"></div>
										<table style="border-collapse: collapse;table-layout: fixed; width: 100%; text-align: right;">
											<tr>
												<td style="width: 70%;">Trị giá hàng hóa</td>
												<td><span><b>' . number_format( $totalMoney, 0, ',', '.' ) . '</b><span><span> VNĐ</span></td>
											</tr>
											' . $promoteList . '
											<tr>
												<td style="width: 70%;">
													<p style="margin: 0;"><b>Trị giá đơn hàng</b></p>
													<h5 style="font-weight: normal; margin: 0;">(Đã bao gồm VAT)</h5>
												</td>
												<td><span><b>' . number_format( $finalMoney, 0, ',', '.' ) . '</b><span><span> VNĐ</span></td>
											</tr>
											<tr>
												<td colspan="2" style="text-align: left; background-color: #eaf4e4; padding: 5px;">Địa chỉ giao hàng: ' . $address . '</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style="padding: 5px 0;"><i>Xin cảm ơn bạn đã đặt hàng tại Quatangtraotay.com.vn</i></td>
								</tr>
								<tr>
									<td style="padding: 5px 0;">Nếu có bất kỳ vấn đề gì, xin vui lòng gọi 1900 7290 để được hỗ trợ</td>
								</tr>
								<tr>
									<td style="padding: 5px 0;"><i>Kính chào trân trọng.</i></td>
								</tr>
							</table>
						</td>
					</tr>
				';
			}
			
			$emailTpl = file_get_contents( _MODULES . '/home/email.tpl.php' );
			$contents = str_replace( '{@contents}', $contents, $emailTpl );
			return $smtp -> send( $to, $sub, $contents );
		}
		
		//Get order
		$order = $exec -> get( $sql -> get( 112 ), array(
			':order_code' => $orderCode
		) );
		
		//Update timeline
		switch( $stateId ) {
			case 2:
				$event = 'Đơn hàng <b>' . $orderCode . '</b> đã được xác nhận qua điện thoại.';
				break;
			case 3:
				$event = 'Đơn hàng <b>' . $orderCode . '</b> đã được giao cho bên vận chuyển, đang chờ xác nhận giao hàng thành công.';
				break;
			case 4:
				//Get from db
				$order = $exec -> get( $sql -> get( 43 ), array(
					':order_code' => $orderCode
				) );
				$order = $order[0];
				$whomake = json_decode( $order['order_who_make_infos'], true );
				
				//Send mail
				if( $whomake['email'] != '' ) {
					send_mail( $order, $stateId, $smtp, $exec, $sql );
				}
				
				//Send SMS
				if( $whomake['email'] == '' ) {
					$text = 'Xin chao ban, Quatangtraotay xin thong bao don hang ' . $orderCode . ' cua ban da duoc giao thanh cong ngay ' . date( 'd/m/y', time() ) . '. Vui long lien he Hotline: 19007290';
					$number = $whomake['mobile'];
					$sms -> send( $number, $text );
				}
				
				$event = 'Đơn hàng <b>' . $orderCode . '</b> đã giao hàng thành công';
				break;
			case 5: 
				//Get from db
				$order = $exec -> get( $sql -> get( 43 ), array(
					':order_code' => $orderCode
				) );
				$order = $order[0];
				$whomake = json_decode( $order['order_who_make_infos'], true );
				
				//Send mail
				if( $whomake['email'] != '' ) {
					send_mail( $order, $stateId, $smtp, $exec, $sql );
				}
				
				//Send SMS
				if( $whomake['email'] == '' ) {
					$text = 'Xin chao ban, Quatangtraotay xin thong bao don hang ' . $orderCode . ' cua ban da duoc huy luc ' . date( 'H', time() ) . 'h-' . date( 'd/m/y', time() ) . '. Vui long lien he Hotline: 19007290';
					$number = $whomake['mobile'];
					$sms -> send( $number, $text );
				}
				
				//Add into reorder table
				$data_re = array(
					':order_id' => (int)$_POST['id'],
					':state_id' => 5,
					':reorder_note' => 'Hủy bởi CS',
					':reorder_time' => time(),
					':shipped' => $currentState == 4 ? 1 : 0
				);
				$re = $exec -> add( $sql -> get( 47 ), $data_re );
				
				//Update amount of products
				$products = json_decode( $order['order_product'], true );
				foreach( $products as $key => $product ) {
					$oldAmount = $exec -> get( $sql -> get( 261 ), array(
						':id' => $product['product_id']
					) );
					$oldAmount = json_decode( $oldAmount[0]['product_amount_by_branch'], true );
					$oldAmount = (int)$oldAmount[1];
					$exec -> exec( $sql -> get( 260 ), array(
						':amount' => json_encode( array( '1' => $oldAmount + $product['buy_amount'] ) ),
						':id' => $product['product_id']
					) );
				}
				
				$event = 'Đơn hàng <b>' . $orderCode . '</b> đã hủy';
				break;
		}
		
		$newTimeline = array(
			'time' => time(),
			'event' => $event
		);
		$timeline = json_decode( $order['order_timeline'], true );
		array_push( $timeline, $newTimeline );
		
		$r = $exec -> exec( $sql -> get( 113 ), array(
			':order_code' => $orderCode,
			':state_id' => $stateId,
			':order_timeline' => json_encode( $timeline )
		) );
		
		$r ? print( '1|Cập nhật trạng thái thành công' ) : print( '0|Lỗi khi cố cập nhật trạng thái' );
	}
?>