<?php 
	$exec = new Exec( HOST, USER, PASS, DBNAME );
	$sql = new Sql();
	
	if( isset( $_GET['order_code'] ) ) {
		$orderCode = strip_tags( $_GET['order_code'] );
		$order = $exec -> get( $sql -> get( 107 ), array( 
			':order_code' => $orderCode
		) );
		
		if( count( $order ) == 0 ) {
			$html = '
				<section class="module-content">
					<div>Không tìm thấy đơn hàng <b>' . $orderCode . '</b></div>
				</section>
			';
		}
		else {
		
			$order = $order[0];
			
			//Cities
			$cities = $exec -> get( $sql -> get( 180 ), array(
				':setting_name' => 'city'
			) );
			$cities = json_decode( $cities[0]['setting_value'], true );
			
			//Get payment name
			$payment = $exec -> get( $sql -> get( 108 ), array( 
				':payment_id' => $order['payment_id']
			) );
			$paymentName = $payment[0]['payment_name'];
			
			//Get shipping time
			$shipping = json_decode( $order['order_shipping_infos'], true );
			$shippingTime = $shipping['time'];
			
			//Get make order
			$makeOrder = json_decode( $order['order_who_make_infos'], true );
			
			//Get recipient
			$recipient = json_decode( $order['order_recipient_infos'], true );
			if( (int)$recipient['different_address'] == 1 ) {
				
				//Get city and district
				foreach( $cities as $key => $arr ) {
					if( $arr['id'] == $recipient['city'] ) {
						$city = $arr['name'];
						foreach( $arr['district'] as $key => $arr2 ) {
							if( $arr2['id'] == $recipient['district'] ) {
								$district = $arr2['name'];
								break;
							}
						}
						break;
					}
				}
				$recipientHTML = '
					<tr>
						<td>Họ tên</td>
						<td>' . $recipient['fullname'] . '</td>
					</tr>
					<tr>
						<td>Số điện thoại</td>
						<td>' . $recipient['mobile'] . '</td>
					</tr>
					<tr>
						<td>Địa chỉ</td>
						<td>' . $recipient['address'] . '</td>
					</tr>
					<tr>
						<td>Tỉnh/Huyện</td>
						<td>' . $city . '/' . $district . '</td>
					</tr>
				';
			}
			else {
				$recipientHTML = '
					<tr>
						<td colspan="2">Thông tin người nhận giống thông tin người đặt</td>
					</tr>
				';
			}
			
			//Get city and district
			foreach( $cities as $key => $arr ) {
				if( $arr['id'] == $makeOrder['city'] ) {
					$city1 = $arr['name'];
					foreach( $arr['district'] as $key => $arr2 ) {
						if( $arr2['id'] == $makeOrder['district'] ) {
							$district1 = $arr2['name'];
							break;
						}
					}
					break;
				}
			}
			
			//Get bill info
			$billInfo = json_decode( $order['order_bill_infos'], true );
			if( (int)$billInfo['need_bill'] == 1 ) {
				$billHTML = '
					<tr>
						<td>Tên công ty</td>
						<td>' . $billInfo['company_name'] . '</td>
					</tr>
					<tr>
						<td>Địa chỉ</td>
						<td>' . $billInfo['company_address'] . '</td>
					</tr>
					<tr>
						<td>Mã số thuế</td>
						<td>' . $billInfo['company_tax_code'] . '</td>
					</tr>
				';
			}
			else {
				$billHTML = '
					<tr>
						<td colspan="2">Không yêu cầu xuất hóa đơn</td>
					</tr>
				';
			}
			
			//Get product table html
			$productTable = '';
			$products = json_decode( $order['order_product'], true );
			$totalMoney = $totalAmount = 0;
			foreach( $products as $key => $product ) {
				$image = json_decode( $product['product_images'], true );
				$money = $product['buy_amount'] * $product['product_price'];
				$totalMoney += $money;
				$totalAmount += $product['buy_amount'];
				$productTable .= '
					<tr>
						<td>
							<img src="/uploads/media/' . $image[0] . '" />
						</td>
						<td>' . $product['product_name'] . '</td>
						<td>' . number_format( $product['product_price'], 0, ',', '.' ) . ' VNĐ</td>
						<td>' . $product['buy_amount'] . '</td>
						<td>' . number_format( $money, 0, ',', '.' ) . ' VNĐ</td>
					</tr>
				';
			}
			
			//Get promote table html
			$promoteTable = '';
			$discount = json_decode( $order['order_discount'], true );
			
			//Balance
			if( isset( $discount['balance'] ) && $discount['balance'] > 0 ) {
				$promoteTable .= '
					<tr>
						<td colspan="4">Tài khoản thưởng</td>
						<td>-' . number_format( $discount['balance'], 0, ',', '.' ) . ' VNĐ</td>
					</tr>
				';
			}
			
			//Voucher
			if( isset( $discount['voucher'] ) && $discount['voucher'] > 0 ) {
				$value = $discount['voucher'] < 1 ? $discount['voucher']*100 . '%' : number_format( $discount['voucher'], 0, ',', '.' ) . ' VNĐ';
				$promoteTable .= '
					<tr>
						<td colspan="4">Mã khuyến mãi</td>
						<td>-' . $value . '</td>
					</tr>
				';
			}
			
			//Commission
			if( isset( $discount['commission'] ) && $discount['commission'] > 0 ) {
				$promoteTable .= '
					<tr>
						<td colspan="4">Chiết khấu theo giá trị đơn hàng</td>
						<td>-' . number_format( $discount['commission'], 0, ',', '.' ) . ' VNĐ</td>
					</tr>
				';
			}
			
			$promoteTable .= '
				<tr>
					<td colspan="3">
						Tổng cộng
					</td>
					<td>' . $totalAmount . '</td>
					<td>' . number_format( $order['order_final_money'], 0, ',', '.' ) . ' VNĐ</td>
				</tr>
			';
			
			//Get log table html
			$logTable = '';
			$logs = json_decode( $order['order_timeline'], true );
			foreach( $logs as $key => $log ) {
				$logTable .= '
					<tr>
						<td>' . date( 'H:i:s d/m/Y', $log['time'] ) . '</td>
						<td>' . $log['event'] . '</td>
					</tr>
				';
			}
			
			//Get state label
			$state = $exec -> get( $sql -> get( 106 ), array(
				':state_id' => $order['state_id']
			) );
			$stateLabel = $state[0]['state_name'];
			
			//Get next order link
			$nextOrderCode = $exec -> get( $sql -> get( 175 ), array(
				':order_id' => $order['order_id']
			) );
			$nextOrderCode = isset( $nextOrderCode[0] ) ? $nextOrderCode[0]['order_code'] : '';
			if( !empty( $nextOrderCode ) ) {
				$nextOrderLink = '/admin/banhang/dondathang/chitiet?order_code=' . $nextOrderCode;
				$nextOrder = '<a class="buttons normal-buttons" href="' . $nextOrderLink . '"><i class="fa fa-arrow-right" aria-hidden="true"></i> Tiếp theo</a>';
			}
			else {
				if( $order['state_id'] == 4 )
					$nextOrder = '<span class="congrates">Chúc mừng bạn đã duyệt xong tất cả !</span>';
				else
					$nextOrder = '';
			}
			
			//Get order note
			$orderNote = json_decode( $order['order_note'], true );
			$orderNote1 = $orderNote['customer_note'] ? $orderNote['customer_note'] : '';
			$orderNote2 = $orderNote['cs_note'] ? $orderNote['cs_note'] : '';
			$orderNote3 = $orderNote['deliving_note'] ? $orderNote['deliving_note'] : '';
			
			$html = '
				<section class="module-content">
					<div class="tabs module">
						<span class="tab-title">Chi tiết đơn hàng</span>
						<input type="radio" name="tab" id="tab1" class="tab-radio" checked />
						<label for="tab1" class="tab-label">Tổng quan</label>
						<div class="tab-content">
							<div class="order-box box-special">
								<h3 class="section-title">Danh sách sản phẩm</h3>
								<table class="tables product-table">
									<tr class="title-rows">
										<td>Hình ảnh</td>
										<td>Tên sản phẩm</td>
										<td>Đơn giá</td>
										<td>Số lượng</td>
										<td>Thành tiền</td>
									</tr>
									' . $productTable . $promoteTable . '
								</table>
							</div>
							<div class="order-box">
								<h3 class="section-title">Thông tin chung</h3>
								<table class="form-tables overview">
									<tr>
										<td>Mã đơn hàng</td>
										<td><b>' . $order['order_code'] . '</b></td>
									</tr>
									<tr>
										<td>Ngày đặt hàng</td>
										<td>' . date( 'H:i:s d/m/Y', $order['order_time'] ) . '</td>
									</tr>
									<tr>
										<td>Ngày giao hàng</td>
										<td>' . date( 'H:i:s d/m/Y', $shippingTime ) . '</td>
									</tr>
									<tr>
										<td>Thanh toán</td>
										<td>' . $paymentName . '</td>
									</tr>
									<tr>
										<td>Trị giá hàng hóa</td>
										<td><b>' . number_format( $order['order_total_money'], 0, ',', '.' ) . '</b> VNĐ</td>
									</tr>
									<tr>
										<td>Trị giá đơn hàng</td>
										<td><b>' . number_format( $order['order_final_money'], 0, ',', '.' ) . '</b> VNĐ</td>
									</tr>
									<tr>
										<td>In ra</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2">
											<a href="include/58?order_code=' . $orderCode . '" target="_blank" class="print buttons normal-buttons print-vat-btn"><i class="fa fa-print" aria-hidden="true"></i> In hóa đơn</a>
											<a href="include/59?order_code=' . $orderCode . '" target="_blank" class="print buttons normal-buttons print-deliving-btn"><i class="fa fa-print" aria-hidden="true"></i> In vận đơn</a>
											<a href="include/60?export=xls&order_code=' . $orderCode . '" target="_blank" class="buttons normal-buttons print-deliving-btn"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Xuất excel</a>
										</td>
									</tr>
								</table>
							</div>
							<div class="order-box">
								<h3 class="section-title">Thông tin người đặt hàng</h3>
								<table class="form-tables info">
									<tr>
										<td>Họ tên</td>
										<td>' . $makeOrder['fullname'] . '</td>
									</tr>
									<tr>
										<td>Email</td>
										<td>' . $makeOrder['email'] . '</td>
									</tr>
									<tr>
										<td>Số điện thoại</td>
										<td>' . $makeOrder['mobile'] . '</td>
									</tr>
									<tr>
										<td>Địa chỉ</td>
										<td>' . $makeOrder['address'] . '</td>
									</tr>
									<tr>
										<td>Tỉnh/Huyện</td>
										<td>' . $city1 . '/' . $district1 . '</td>
									</tr>
								</table>
							</div>
							<div class="order-box">
								<h3 class="section-title">Thông tin người nhận hàng</h3>
								<table class="form-tables info">
									' . $recipientHTML . '
								</table>
							</div>
							<div class="order-box">
								<h3 class="section-title">Thông tin xuất hóa đơn</h3>
								<table class="form-tables info">
									' . $billHTML . '
								</table>
							</div>
							<div class="order-box" style="float: right;">
								<h3 class="section-title controller">Xử lý đơn hàng</h3>
								<table class="form-tables">
									<tr>
										<td>Ghi chú của khách</td>
										<td>
											<input type="text" value="' . $order['order_id'] . '" class="inputs hidden order-id-storer" />
											<textarea class="inputs customer-note" readonly placeholder="Ghi chú của khách">' . $orderNote1 . '</textarea>
										</td>
									</tr>
									<tr>
										<td>Ghi chú của CS</td>
										<td style="position:relative;">
											<span class="tooltip">Bấm [Ctrl + Enter] để lưu</span>
											<textarea class="inputs cs-note" placeholder="CS viết ghi chú vào đây">' . $orderNote2 . '</textarea>
										</td>
									</tr>
									<tr>
										<td>Ghi chú giao hàng</td>
										<td style="position:relative;">
											<span class="tooltip">Bấm [Ctrl + Enter] để lưu</span>
											<textarea class="inputs deliving-note" placeholder="Ghi chú dành cho bên vận chuyển">' . $orderNote3 . '</textarea>
										</td>
									</tr>
									<tr>
										<td>Tình trạng</td>
										<td>
											<span data-id="' . $order['state_id'] . '" class="state-label state' . $order['state_id'] . '">' . $stateLabel . '</span>
											<select class="change-state selects" data-code="' . $order['order_code'] . '" data-id="' . $order['order_id'] . '">
												<option value="1">Đã tiếp nhận</option>
												<option value="2">Đang đóng gói</option>
												<option value="3">Đang giao hàng</option>
												<option value="4">Đã giao hàng</option>
												<option value="5">Đã hủy</option>
											</select>
										</td>
									</tr>
									<tr>
										<td colspan="2" style="text-align: right;">
											' . $nextOrder . '
										</td>
									</tr>
								</table>
							</div>
						</div>
						<input type="radio" name="tab" id="tab4" class="tab-radio" />
						<label for="tab4" class="tab-label">Dòng thời gian</label>
						<div class="tab-content">
							<table class="tables module timeline">
								<tr class="title-rows">
									<td>Thời gian</td>
									<td>Hành động</td>
								</tr>
								' . $logTable . '
							</table>
						</div>
					</div>
				</section>
			';
		}
		echo $html;
	}
	
?>