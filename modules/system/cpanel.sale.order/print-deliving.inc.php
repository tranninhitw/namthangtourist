<?php
	$exec = new Exec( HOST, USER, PASS, DBNAME );
	$sql = new Sql();
	
	//Get company information
	$comInfos = $exec -> get( $sql -> get( 180 ), array(
		':setting_name' => 'company_information'
	) );
	$comInfos = json_decode( $comInfos[0]['setting_value'], true );
	
	
	//Get order information
	$orderCode = strip_tags( $_GET['order_code'] );
	$order = $exec -> get( $sql -> get( 107 ), array( 
		':order_code' => $orderCode
	) );
	
	$order = $order[0];
	
	//Get recipient information
	$recipient = json_decode( $order['order_recipient_infos'], true );
	if( $recipient['different_address'] == '0' ) {
		$recipient = json_decode( $order['order_who_make_infos'], true );
	}
	
	//Get city and district
	$cities = $exec -> get( $sql -> get( 180 ), array(
		':setting_name' => 'city'
	) );
	$cities = json_decode( $cities[0]['setting_value'], true );
	foreach( $cities as $key => $arr ) {
		if( $arr['id'] == $recipient['city'] ) {
			$city = $arr['name'];
			foreach( $arr['district'] as $key => $arr2 ) {
				if( $arr2['id'] == $recipient['district'] ) {
					$district = $arr2['name'];
				}
			}
		}
	}
	
	//Get VAT information
	$vat = json_decode( $order['order_bill_infos'], true );
	$hasBill = (int)$vat['need_bill'] == 1 ? 'Có xuất hóa đơn' : 'Không xuất hóa đơn';
	
	//Get payment method
	switch( $order['payment_id'] ) {
		case 1:
			$paymentMethod = 'COD';
			break;
		case 2:
			$paymentMethod = 'Thẻ tín dụng';
			break;
		case 3:
			$paymentMethod = 'Internet Banking';
			break;
		case 4:
			$paymentMethod = 'Chuyển khoản';
			break;
	}
	
	//Get username
	$userInfos = $exec -> get( $sql -> get( 105 ), array(
		':user_id' => $order['user_id']
	) );
	$username = $userInfos[0]['user_username'];
	
	//Get buyer Type
	$buyerType = $userInfos[0]['group_id'] == 0 ? 'Đại lý' : 'Nhân viên';
	
	//Get product list
	$productList = '';
	$products = json_decode( $order['order_product'], true );
	$i = 0;
	$totalMoney = 0;
	foreach( $products as $key => $product ) {
		$i++;
		$productList .= '
			<tr>
				<td style="text-align: center;">' . $i . '</td>
				<td>' . $product['product_name'] . '</td>
				<td style="text-align: center;">' . $product['product_code'] . '</td>
				<td style="text-align: center;">' . $product['buy_amount'] . '</td>
				<td>' . number_format( $product['product_price'], 0, ',', '.' ) . '</td>
				<td>' . number_format( $product['product_price']*$product['buy_amount'], 0, ',', '.' ) . '</td>
			</tr>
		';
		
		//Increment totalMoney
		$totalMoney += $product['product_price'] * $product['buy_amount'];
	}
	
	//Get promote HTML
	$balance = '';
	$voucher = '';
	$commission = '';
	$discount = json_decode( $order['order_discount'], true );
	
	//Balance
	if( isset( $discount['balance'] ) && $discount['balance'] > 0 ) {
		$balance = '-' . number_format( $discount['balance'], 0, ',', '.' );
	}
	
	//Voucher
	$value = 0;
	if( isset( $discount['voucher'] ) && $discount['voucher'] > 0 ) {
		$value = $discount['voucher'] < 1 ? $discount['voucher']*$totalMoney/100 : $discount['voucher'];
		$voucher = '-' . number_format( $value, 0, ',', '.' );
	}
	
	//Commission
	if( isset( $discount['commission'] ) && $discount['commission'] > 0 ) {
		$commission = '-' . number_format( $discount['commission'], 0, ',', '.' );
	}
	
	//Get note
	$note = json_decode( $order['order_note'], true );
	
	$html = '
		<style>
			* {
				margin: 0;
				padding: 0;
			}
			
			hr {
				border: none;
				border-top: 1px solid #000;
				height: 1px;
			}
			
			.printable-box td {
				font-size: 12px;
			}
			
			.printable-box {
				height: 138mm;
				width: 210mm;
				padding: 5px;
				margin: 0;
				font-family: arial;
			}

			.printable-header img {
				width: 100px;
				height: auto;
			}

			.printable-box table {
				width: 100%;
			}

			.printable-header {
				margin: 2px 0;
			}

			.printable-imf-customer {
				margin: 5px 0;
				line-height: 18px;
			}

			.printable-imf-customer td:first-child {
				width: 14%;
				color: #1f497d;
				font-weight: 700;
			}

			.printable-imf-customer td:nth-child(4) {
				width: 15%;
				color: #1f497d;
				font-weight: 700;
			}

			.printable-imf-customer td:nth-child(6) {
				padding-left: 5px;
				width: 25%;
			}

			.printable-imf-product {
				table-layout: fixed;
				border-collapse: collapse;
			}

			.printable-imf-product tr:first-child td {
				height: 25px;
				border: 1px solid #000;
				padding: 5px;
				text-align: center;
				font-weight: 700;
			}

			.printable-imf-product td {
				padding: 2px;
				border: 1px solid #000;
			}

			.printable-imf-product tr:not(:first-child) td:nth-child(5),.printable-imf-product tr:not(:first-child) td:nth-child(6) {
				text-align: right;
			}

			.printable-imf-product td:first-child {
				width: 10%;
				width: 10%;
			}

			.printable-imf-product-note td {
				border: 1px solid #000;
				text-align: left !important;
				vertical-align: top;
			}

			.printable-imf-footer-box {
				margin: 10px 0;
				font-style: italic;
			}

			.printable-imf-footer-box td {
				width: 100%;
				font-style: italic;
				color: #069;
				font-weight: 700;
			}

			.printable-chuky tr td {
				font-weight: 700;
				text-align: center;
			}
		</style>
		<div class="printable-box">
			<table class="printable-header">
				<colgroup>
					<col width="30%">
					<col width="40%">
					<col width="30%">
				</colgroup>
				<tr>
					<td>
						<img src="/uploads/backgrounds/logofooter.png" />
					</td>
					<td style="border: 1px solid #000; text-align: center;">
						<h2>PHIẾU GIAO HÀNG</h2>
					</td>
					<td style="text-align: right; font-style: italic; text-decoration: underline; vertical-align: top;">
						quatangtraotay.com.vn<br />
						Hotline: 1900 7290
					</td>
				</tr>
			</table>
			<hr />
			<table class="printable-imf-customer">
				<colgroup>
					<col width="14%"></col>
					<col width="45%"></col>
					<col width="2%"></col>
					<col width="14%"></col>
					<col width="25%"></col>
				</colgroup>
				<tr>
					<td>Mã KH:</td>
					<td>' . $username . '</td>
					<td></td>
					<td>Đơn hàng:</td>
					<td>' . $buyerType . '</td>
				</tr>
				<tr>
					<td>Họ tên KH:</td>
					<td>' . $recipient['fullname'] . '</td>
					<td></td>
					<td>Ngày:</td>
					<td>' . date( 'd-m-Y', $order['order_time'] ) . '</td>
				</tr>
				<tr>
					<td>SĐT:</td>
					<td>' . $recipient['mobile'] . '</td>
					<td></td>
					<td>Mã ĐH:</td>
					<td>' . $order['order_code'] . '</td>
				</tr>
				<tr>
					<td>ĐC giao hàng:</td>
					<td>' . $recipient['address'] . '</td>
					<td></td>
					<td>PT thanh toán:</td>
					<td>' . $paymentMethod . '</td>
				</tr>
				<tr>
					<td>Huyện/Tỉnh:</td>
					<td>' . $district . '/' . $city . '</td>
					<td></td>
					<td>Xuất HĐ:</td>
					<td>' . $hasBill . '</td>
				</tr>
				<tr>
					<td>PT giao hàng:</td>
					<td>Tiêu chuẩn</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
			<table class="printable-imf-product">
				<colgroup>
					<col style="width: 5%"></col>
					<col style="width: 35%"></col>
					<col style="width: 15%"></col>
					<col style="width: 10%"></col>
					<col style="width: 15%"></col>
					<col style="width: 15%"></col>
				</colgroup>
				<tr>
					<td>STT</td>
					<td>Tên nhãn hiệu hàng hóa</td>
					<td>Mã hàng</td>
					<td>Số lượng</td>
					<td>Đơn giá</td>
					<td>Thành tiền</td>
				</tr>
				' . $productList . '
				<tr>
					<td style="border: none;"></td>
					<td style="border: none;"></td>
					<td style="border: none;"></td>
					<td style="border: none;"></td>
					<td style="border: none; font-weight: 700; text-align: right;">Tổng tạm tính:</td>
					<td style="border: none;">' . number_format( $totalMoney, 0, ',', '.' ) . '</td>
				</tr>
				<tr class="printable-imf-product-note">
					<td colspan="4" rowspan="5"><b><u>Ghi chú dành cho NV giao hàng:</u></b><br />' . $note['deliving_note'] . '</td>				
				</tr>
				<tr>
					<td style="text-align: right; border: none; font-weight: 700;">Mã giảm giá:</td>
					<td style="text-align: right; border: none;">' . $voucher . '</td>
				</tr>
				<tr>
					<td style="text-align: right; border: none; font-weight: 700;">TK thưởng:</td>
					<td style="text-align: right; border: none;">' . $balance . '</td>
				</tr>
				<tr>
					<td style="text-align: right; border: none; font-weight: 700;">Chiết khấu:</td>
					<td style="text-align: right; border: none;">' . $commission . '</td>
				</tr>
				<tr>
					<td style="text-align: right; border: none; font-weight: 700;">Tổng thu KH:</td>
					<td style="text-align: right; border: none;"><b><big>' . number_format( $order['order_final_money'], 0, ',', '.' ) . '</big></b></td>
				</tr>
			</table>
			<div class="printable-imf-footer-box">
				<table class="printable-imf-footer">
					<tr>
						<td>Lưu ý: Giao đúng người đặt hàng hoặc NV cùng phòng, không giao cho bảo vệ, lễ tân.</td>
					</tr>
					<tr>
						<td>Vui lòng phản hồi về số lượng và chất lượng hàng hóa trong 05 ngày (nếu có), quá 05 ngày ASGroup không chịu trách nhiệm!</td>
					</tr>
				</table>
			</div>
			<table class="printable-chuky">
				<tr>
					<td>Người Lập phiếu</td>
					<td>Người Giao Nhận<br />(Ghi rõ họ tên)</td>
					<td>Người Nhận tiền<br />(Ghi rõ họ tên)</td>
					<td>CTY Vận Chuyển<br />(Ghi rõ họ tên)</td>
					<td>Kế toán<br />(Ghi rõ họ tên)</td>
					<td>Người nhận hàng<br />(Ghi rõ họ tên)</td>
				</tr>
			</table>
		</div>
	';
	echo $html;
?>