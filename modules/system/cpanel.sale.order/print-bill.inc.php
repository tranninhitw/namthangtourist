<?php
	$exec = new Exec( HOST, USER, PASS, DBNAME );
	$sql = new Sql();
	
	function convert_number_to_words($number) {
		$hyphen      = ' ';
		$conjunction = ' ';
		$separator   = ' ';
		$negative    = 'âm ';
		$decimal     = ' phẩy ';
		$dictionary  = array(
			0                   => 'Không',
			1                   => 'Một',
			2                   => 'Hai',
			3                   => 'Ba',
			4                   => 'Bốn',
			5                   => 'Năm',
			6                   => 'Sáu',
			7                   => 'Bảy',
			8                   => 'Tám',
			9                   => 'Chín',
			10                  => 'Mười',
			11                  => 'Mười một',
			12                  => 'Mười hai',
			13                  => 'Mười ba',
			14                  => 'Mười bốn',
			15                  => 'Mười năm',
			16                  => 'Mười sáu',
			17                  => 'Mười bảy',
			18                  => 'Mười tám',
			19                  => 'Mười chín',
			20                  => 'Hai mươi',
			30                  => 'Ba mươi',
			40                  => 'Bốn mươi',
			50                  => 'Năm mươi',
			60                  => 'Sáu mươi',
			70                  => 'Bảy mươi',
			80                  => 'Tám mươi',
			90                  => 'Chín mươi',
			100                 => 'trăm',
			1000                => 'ngàn',
			1000000             => 'triệu',
			1000000000          => 'tỷ',
			1000000000000       => 'nghìn tỷ',
			1000000000000000    => 'ngàn triệu triệu',
			1000000000000000000 => 'tỷ tỷ'
		);
		 
		if (!is_numeric($number)) {
			return false;
		}
		 
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			return 'Số không hợp lệ';
		}
		 
		if ($number < 0) {
			return $negative . convert_number_to_words(abs($number));
		}
		 
		$string = $fraction = null;
		 
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		 
		switch (true) {
			case $number < 21:
				$string = $dictionary[$number];
				break;
			case $number < 100:
				$tens   = ((int) ($number / 10)) * 10;
				$units  = $number % 10;
				$string = $dictionary[$tens];
				if ($units) {
					$string .= $hyphen . $dictionary[$units];
				}
				break;
			case $number < 1000:
				$hundreds  = $number / 100;
				$remainder = $number % 100;
				$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
				if ($remainder) {
					$string .= $conjunction . convert_number_to_words($remainder);
				}
				break;
			default:
				$baseUnit = pow(1000, floor(log($number, 1000)));
				$numBaseUnits = (int) ($number / $baseUnit);
				$remainder = $number % $baseUnit;
				$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
			$string .= $remainder < 100 ? $conjunction : $separator;
			$string .= convert_number_to_words($remainder);
			}
			break;
		}
		 
		if (null !== $fraction && is_numeric($fraction)) {
		$string .= $decimal;
		$words = array();
		foreach (str_split((string) $fraction) as $number) {
		$words[] = $dictionary[$number];
		}
		$string .= implode(' ', $words);
		}
		 
		return $string;
	}
	
	//Get company information
	$comInfos = $exec -> get( $sql -> get( 180 ), array(
		':setting_name' => 'company_information'
	) );
	$comInfos = json_decode( $comInfos[0]['setting_value'], true );
	
	//Get tax_rate
	$tax_rate = $exec -> get( $sql -> get( 180 ), array(
		':setting_name' => 'tax_rate'
	) );
	$tax_rate = $tax_rate[0]['setting_value'];
	
	//Get order information
	$orderCode = strip_tags( $_GET['order_code'] );
	$order = $exec -> get( $sql -> get( 107 ), array( 
		':order_code' => $orderCode
	) );
	
	$order = $order[0];
	
	//Get buyer information
	$buyer = json_decode( $order['order_who_make_infos'], true );
	
	//Get VAT information
	$vat = json_decode( $order['order_bill_infos'], true );
	
	//Get payment method
	switch( $order['payment_id'] ) {
		case 1:
			$paymentMethod = 'COD';
			break;
		case 2:
			$paymentMethod = 'Thẻ tín dụng';
			break;
		case 3:
			$paymentMethod = 'Internet Banking';
			break;
		case 4:
			$paymentMethod = 'Chuyển khoản';
			break;
	}
	
	//Get product list & number of bill (3 products per a bill )
	$productList = array();
	$products = json_decode( $order['order_product'], true );
	$billAmount = ceil( count( $products ) / 4 );
	$totalMoney = array();
	$productPerBill = array();
	
	//Get totalMoneyOriginal
	$totalMoneyOriginal = 0;
	foreach( $products as $key => $product ) {
		$totalMoneyOriginal += $product['product_price'] * $product['buy_amount'];
	}
	
	//Split products array
	for( $i = 0; $i < $billAmount; $i++ ) {
		$x = $i*4;
		$temp = array();
		isset( $products[$x] ) ? array_push( $temp, $products[$x] ) : false;
		isset( $products[$x+1] ) ? array_push( $temp, $products[$x+1] ) : false;
		isset( $products[$x+2] ) ? array_push( $temp, $products[$x+2] ) : false;
		isset( $products[$x+3] ) ? array_push( $temp, $products[$x+3] ) : false;
		array_push( $productPerBill, $temp );
	}
	
	//Get commission
	$discount = json_decode( $order['order_discount'], true );
	if( isset( $discount['commission'] ) && $discount['commission'] > 0 ) {
		$commission = $discount['commission'];
	}
	//Balance
	if( isset( $discount['balance'] ) && $discount['balance'] > 0 ) {
		$commission += $discount['balance'];
	}
	
	//Voucher
	$value = 0;
	if( isset( $discount['voucher'] ) && $discount['voucher'] > 0 ) {
		$value = $discount['voucher'] < 1 ? $discount['voucher']*$totalMoneyOriginal/100 : $discount['voucher'];
		$commission += $value;
	}
	
	$commission = round( 100 - $commission / $totalMoneyOriginal * 100 , 0 );
	
	for( $x = 0; $x < $billAmount; $x++ ) {
		$totalMoney[$x] = 0;
		$productList[$x] = '';
		$i = 0;
		foreach( $productPerBill[$x] as $key => $product ) {
			$i++;
			$oPrice = $product['product_price'] / 1.1 / 100;
			$oPrice = $oPrice * $commission;
			$oMoney = $oPrice * $product['buy_amount'];
			$productList[$x] .= '
				<div>
					<span>' . $i . '</span>
					<span>' . $product['product_name'] . ' <small>(' . $product['product_code'] . ')</small></span>
					<span>' . $product['product_unit'] . '</span>
					<span>' . $product['buy_amount'] . '</span>
					<span>' . number_format( $oPrice, 0, ',', '.' ) . '</span>
					<span>' . number_format( $oMoney, 0, ',', '.' ) . '</span>
				</div>
			';
			
			//Increment totalMoney
			$totalMoney[$x] += $oMoney;
		}
	}
	
	//Get HTML
	$html = '
		<style>
			* {
				margin: 0;
				padding: 0;
			}

			.printable-box {
				height: 138mm;
				width: 210mm;
				margin: 0;
				font-size: 1em;
				font-family: arial;
			}

			.line-1 {
				padding-left: 25mm;
				text-align: center;
				padding-top: 8mm;
			}

			.line-1 span {
				margin-left: 10mm;
			}

			.customer {
				padding-top: 15mm;
				line-height: 5mm;
			}

			.line-2 {
				padding-left: 45mm;
			}

			.line-3,.line-4 {
				padding-left: 25mm;
			}

			.line-5 span:first-child {
				padding-left: 27mm;
				line-height: 6mm;
			}

			.line-5 span:last-child {
				margin-left: 56mm;
				line-height: 6mm;
			}

			.product-line {
				padding-top: 15mm;
				padding-left: 15mm;
				height: 27mm;
			}
			
			.product-line span {
				display: inline-block;
			}

			.product-line span:nth-child(2) {
				padding-left: 5mm;
				width: 90mm;
			}

			.product-line span:nth-child(3),
			.product-line span:nth-child(4)			{
				width: 15mm;
				text-align: center;
			}
			
			.product-line span:nth-child(5),.product-line span:nth-child(6) {
				text-align: right;
				padding-right: 10mm;
				width: 20mm;
			}

			.vat {
				padding-top: 12mm;
				padding-left: 45mm;
				float: left;
			}

			.final-money {
				float: right;
				padding-top: 6mm;
				text-align: right;
				padding-right: 10mm;
			}

			.final-money span {
				display: block;
				line-height: 5.5mm;
			}

			.money-word {
				clear: both;
				padding-top: 8mm;
				padding-left: 50mm;
			}
		</style>
	';
	for( $i = 0; $i < $billAmount; $i++ ) {
		//Get bill no
		$billNo = (int)$comInfos['company_bill_no'] + 1;
		$billNo = str_pad( $billNo, 7, '0', STR_PAD_LEFT );
		
		//Update bill no
		$comInfos['company_bill_no'] = $billNo;
		$exec -> exec( $sql -> get( 188 ), array(
			':setting_value' => json_encode( $comInfos ),
			':setting_name' => 'company_information'
		) );
		
		//Build HTML
		$html .= '
			<div class="printable-box">
				<div class="line-1">
					<span>' . date( 'd', time() ) . '</span>
					<span>' . date( 'm', time() ) . '</span>
					<span>' . date( 'Y', time() ) . '</span>
				</div>
				<div class="customer">
					<div class="line-2">
						<span>' . $buyer['fullname'] . '</span>
					</div>
					<div class="line-3">
						<span>' . $vat['company_name'] . '</span>
					</div>
					<div class="line-4">
						<span>' . $vat['company_address'] . '</span>
					</div>
					<div class="line-5">
						<span>' . $vat['company_tax_code'] . '</span>
						<span>' . $paymentMethod . '</span>
					</div>
				</div>
				<div class="product-line">
					' . $productList[$i] . '
					<div>
						<span></span>
						<span>ĐH: ' . $orderCode . '</span>
					</div>
				</div>
				<div class="vat">
					<span>' . $tax_rate . '</span>
				</div>
				<div class="final-money">
					<span>' . number_format( $totalMoney[$i], 0, ',', '.' ) . '</span>
					<span>' . number_format( $totalMoney[$i]*$tax_rate/100, 0, ',', '.' ) . '</span>
					<span>' . number_format( ($totalMoney[$i]*$tax_rate/100) + $totalMoney[$i], 0, ',', '.' ) . '</span>
				</div>
				<div class="money-word">
					<span>' . ucfirst( strtolower( convert_number_to_words( ( $totalMoney[$i]*$tax_rate/100 ) + $totalMoney[$i] ) ) ).' đồng</span>
				</div>
			</div>
		';
	}
	
	$html .= '
		<script>
			window.onload = function() {
				window.print();
			};
		</script>
	';
	echo $html;
?>