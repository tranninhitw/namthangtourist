<?php 
	if( isset( $_POST['order_id'] ) ) {
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		$sql = new Sql();
		
		$orderNote = array(
			'customer_note' => $_POST['customer_note'],
			'cs_note' => $_POST['cs_note'],
			'deliving_note' => $_POST['deliving_note']
		);
		
		$data = array(
			':order_note' => json_encode( $orderNote ),
			':order_id' => (int)$_POST['order_id']
		);
		
		$r = $exec -> exec( $sql -> get( 176 ), $data );
		$r ? print( '1|Lưu ghi chú thành công' ) : print( '0|Lỗi khi lưu ghi chú' );
	}
?>