<?php
	//Turn off autoload
	$autoloadFuncs = spl_autoload_functions();
	foreach($autoloadFuncs as $unregisterFunc)
	{
		spl_autoload_unregister($unregisterFunc);
	}
	require_once _CORE . '/phpexcel.cls.php';
	
	//Get order from db
	if( isset( $_GET['export'] ) && $_GET['export'] == 'xls' ) {
		
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		$sql = new Sql();
		
		//Build xls data
		$objPHPExcel = PHPExcel_IOFactory::load( _MODULES . '/cpanel.sale.order/template.xls' );
		if( isset( $_GET['order_code'] ) ) {
			//Get company information
			$comInfos = $exec -> get( $sql -> get( 180 ), array(
				':setting_name' => 'company_information'
			) );
			$comInfos = json_decode( $comInfos[0]['setting_value'], true );
			
			
			//Get order information
			$orderCode = strip_tags( $_GET['order_code'] );
			$order = $exec -> get( $sql -> get( 107 ), array( 
				':order_code' => $orderCode
			) );
			
			$order = $order[0];
			
			//Get recipient information
			$recipient = json_decode( $order['order_recipient_infos'], true );
			if( $recipient['different_address'] == '0' ) {
				$recipient = json_decode( $order['order_who_make_infos'], true );
			}
			
			//Get city and district
			$cities = $exec -> get( $sql -> get( 180 ), array(
				':setting_name' => 'city'
			) );
			$cities = json_decode( $cities[0]['setting_value'], true );
			foreach( $cities as $key => $arr ) {
				if( $arr['id'] == $recipient['city'] ) {
					$city = $arr['name'];
					foreach( $arr['district'] as $key => $arr2 ) {
						if( $arr2['id'] == $recipient['district'] ) {
							$district = $arr2['name'];
						}
					}
				}
			}
			
			//Get VAT information
			$vat = json_decode( $order['order_bill_infos'], true );
			$hasBill = (int)$vat['need_bill'] == 1 ? 'Có xuất hóa đơn' : 'Không xuất hóa đơn';
			
			//Get payment method
			switch( $order['payment_id'] ) {
				case 1:
					$paymentMethod = 'COD';
					break;
				case 2:
					$paymentMethod = 'Thẻ tín dụng';
					break;
				case 3:
					$paymentMethod = 'Internet Banking';
					break;
				case 4:
					$paymentMethod = 'Chuyển khoản';
					break;
			}
			
			//Get username
			$userInfos = $exec -> get( $sql -> get( 105 ), array(
				':user_id' => $order['user_id']
			) );
			$username = $userInfos[0]['user_username'];
			
			//Get buyer Type
			$buyerType = $userInfos[0]['group_id'] == 0 ? 'Đại lý' : 'Nhân viên';
			
			//Get promote HTML
			$balance = '';
			$voucher = '';
			$commission = '';
			$discount = json_decode( $order['order_discount'], true );
			
			//Balance
			if( isset( $discount['balance'] ) && $discount['balance'] > 0 ) {
				$balance = -1 * $discount['balance'];
			}
			
			//Voucher
			$value = 0;
			if( isset( $discount['voucher'] ) && $discount['voucher'] > 0 ) {
				$value = $discount['voucher'] < 1 ? $discount['voucher']*$totalMoney/100 : $discount['voucher'];
				$voucher = -1 * $value;
			}
			
			//Commission
			if( isset( $discount['commission'] ) && $discount['commission'] > 0 ) {
				$commission = -1 * $discount['commission'];
			}
			
			//Get note
			$note = json_decode( $order['order_note'], true );
	
			//Get product list
			$productList = '';
			$products = json_decode( $order['order_product'], true );
			$totalMoney = 0;
			
			foreach( $products as $key => $product ) {
				$offset = 11 + $key;
				$objPHPExcel -> getActiveSheet()
					-> setCellValue( 'B' . $offset, $key + 1 )
					-> setCellValue( 'C' . $offset, $product['product_name'] )
					-> setCellValue( 'D' . $offset, $product['product_code'] )
					-> setCellValue( 'E' . $offset, $product['buy_amount'] )
					-> setCellValue( 'F' . $offset, $product['product_price'] )
					-> setCellValue( 'G' . $offset, $product['product_price']*$product['buy_amount'] );
				
				//Increment totalMoney
				$totalMoney += $product['product_price'] * $product['buy_amount'];
			}
			
			//Fill data for customer infos
			$objPHPExcel -> getActiveSheet()
				-> setCellValue( 'C3', $username )
				-> setCellValue( 'C4', $recipient['fullname'] )
				-> setCellValue( 'C5', $recipient['mobile'] )
				-> setCellValue( 'C6', $recipient['address'] )
				-> setCellValue( 'C7', $district . '/' . $city )
				-> setCellValue( 'C8', 'Tiêu chuẩn' )
				-> setCellValue( 'G3', $buyerType )
				-> setCellValue( 'G4', date( 'd-m-Y', $order['order_time'] ) )
				-> setCellValue( 'G5', $order['order_code'] )
				-> setCellValue( 'G6', $paymentMethod )
				-> setCellValue( 'G7', $hasBill );
			
			//Fill data for money infos
			$objPHPExcel -> getActiveSheet()
				-> setCellValue( 'B44', $note['deliving_note'] )
				-> setCellValue( 'G42', $totalMoney )
				-> setCellValue( 'G43', $voucher )
				-> setCellValue( 'G44', $balance )
				-> setCellValue( 'G45', $commission )
				-> setCellValue( 'G46', $order['order_final_money'] );
			
			//Remove exceed rowS
			$end = count( $products ) + 11;
			$i = 41;
			while( $i >= $end ) {
				$objPHPExcel -> getActiveSheet() -> removeRow( $i );
				$i--;
			}
		}
		else {
			exit;
		}
	}
		
	// Rename worksheet
	$objPHPExcel->getActiveSheet()->setTitle('ĐH ' . strip_tags( $_GET['order_code'] ) );

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Quatangtraotay-report.xls"');
	header('Cache-Control: max-age=0');
	
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;
?>