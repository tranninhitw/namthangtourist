<?php 
	$exec = new Exec( HOST, USER, PASS, DBNAME );
	$sql = new Sql();
	
	$number = $exec -> get( $sql -> get( 115 ) );
	$number = $number[0]['number'];
	$number = isset( $_SESSION['total_rows'] ) && ( isset( $_GET['filter'] ) || isset( $_GET['s'] ) ) ? $_SESSION['total_rows'] : $number;
	$viewing = $number < 5 ? $number : 5;
	$html = '
		<div class="total-row">
			<span class="loadmore-btn" data-offset="0">Tải thêm</span>
			<p class="showing-info">Đang hiển thị <b class="viewing">' . $viewing . '</b> trong số <b>' . $number . '</b> đơn đặt hàng</p>
		</div>
	';
	echo $html;
?>