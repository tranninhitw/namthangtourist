<?php 
	
	function uni_convert( $str ) {
		return str_replace( array('["', '"]'), array('',''), json_encode( array( $str ) ) );
	}
	
	function filter( $exec, $sql ) {
		//Update shipping time for temporary table
		$tempOrders = $exec -> get( $sql -> get( 249 ) );
		foreach( $tempOrders as $key => $tempOrder ) {
			$time = json_decode( $tempOrder['order_shipping_infos'], true );
			$time = $time['time'];
			$exec -> exec( $sql -> get( 250 ), array(
				':order_id' => $tempOrder['order_id'],
				':order_shipping_time' => $time
			) );
		}
		
		//Get $_GET
		$sortByMoney = strip_tags( $_GET['sort_by_money'] );
		$filterByState = strip_tags( $_GET['filter_by_state'] );
		$filterByTimeFrom = strip_tags( $_GET['filter_by_time_from'] );
		$filterByTimeTo = strip_tags( $_GET['filter_by_time_to'] );
		$filterByShippingTimeFrom = strip_tags( $_GET['filter_by_shipping_time_from'] );
		$filterByShippingTimeTo = strip_tags( $_GET['filter_by_shipping_time_to'] );
		$filterByMoneyFrom = str_replace( '.', '', strip_tags( $_GET['filter_by_money_from'] ) );
		$filterByMoneyTo = str_replace( '.', '', strip_tags( $_GET['filter_by_money_to'] ) );
		$keyword = isset( $_GET['s'] ) ? $_GET['s'] : '';
		
		//Convert to time
		$timeF = strtotime( str_replace( '/', '-', $filterByTimeFrom ) );
		$timeT = strtotime( str_replace( '/', '-', $filterByTimeTo ) );
		$shipT = strtotime( str_replace( '/', '-', $filterByShippingTimeTo ) );
		$shipF = strtotime( str_replace( '/', '-', $filterByShippingTimeFrom ) );
		
		//Get SQL query
		$inClause = '';
		$sqlTextS = '';
		if( !empty( $keyword ) ) {
			$sqlTextS = ' AND MATCH( t1.order_code, t1.order_recipient_infos, t1.order_who_make_infos) AGAINST( \'' . str_replace( '\\', '\\\\', strip_tags( uni_convert( $keyword ) ) ) . '\' IN BOOLEAN MODE )';
		}
		
		$sqlText1 = $filterByState != '0' ? ' t2.state_id = ' . $filterByState : ' 1=1';
		$sqlText2 = $timeF > 0 ? ' AND t2.order_time >= ' . $timeF : '';
		$sqlText3 = $timeT > 0 ? ' AND ' . $timeT . ' >= t2.order_time' : '';
		$sqlText4 = $shipF > 0 ? ' AND t2.order_shipping_time >= ' . $shipF : '';
		$sqlText5 = $shipT > 0 ? ' AND ' . $shipT . ' >= t2.order_shipping_time' : '';
		$sqlText6 = (float)$filterByMoneyFrom > 0 ? ' AND t2.order_total_money > ' . (float)$filterByMoneyFrom : '';
		$sqlText7 = (float)$filterByMoneyTo > 0 ? ' AND t2.order_total_money < ' . (float)$filterByMoneyTo : '';
		$sqlText8 = $sortByMoney == 'desc' ? ' ORDER BY order_total_money DESC' : ' ORDER BY order_total_money ASC';
		$viewName = 'filter_view_' . str_replace( array( '.', ':' ), array( '', '' ), $_SERVER['REMOTE_ADDR'] );
		$sqlText = "CREATE VIEW " . $viewName . " AS SELECT t1.* FROM dp_orders AS t1 JOIN dp_temp_orders AS t2 ON t1.order_id = t2.order_id WHERE" . $sqlText1 . $sqlText2 . $sqlText3 . $sqlText4 . $sqlText5 . $sqlText6 . $sqlText7 . $sqlTextS . ';';
		
		//Create view
		$exec -> exec( $sqlText );
		
		$offset = isset( $_GET['offset'] ) ? $_GET['offset'] : 0;
		$totalRows = $exec -> get( "SELECT COUNT(*) AS rows FROM " . $viewName );
		$totalRows = $totalRows[0]['rows'];
		$_SESSION['total_rows'] = $totalRows;
		$orders = $exec -> get( "SELECT * FROM " . $viewName . $sqlText8 . " LIMIT 5 OFFSET :offset", array(
			':offset' => (int)$offset
		), true );
		
		$exec -> exec( "DROP VIEW IF EXISTS " . $viewName . ";" );
		
		return $orders;
	}
	
	$exec = new Exec( HOST, USER, PASS, DBNAME );
	$sql = new Sql();
	$html = '';
	if( isset( $_GET['offset'] ) ) {
		$offset = (int)$_GET['offset'];
	}
	else $offset = 0;
	
	//Search
	if( isset( $_GET['s'] ) ) {
		//Apply filter
		if( isset( $_GET['filter'] ) && $_GET['filter'] == 'true' ) {
			$orders = filter( $exec, $sql );
		}
		//Only search
		else {
			$offset = isset( $_GET['offset'] ) ? $_GET['offset'] : 0;
			$totalRows = $exec -> get( $sql -> get( 208 ), array(
				':keyword' => strip_tags( uni_convert( $_GET['s'] ) )
			), true );
			$totalRows = $totalRows[0]['rows'];
			$_SESSION['total_rows'] = $totalRows;
			$orders = $exec -> get( $sql -> get( 114 ), array(
				':keyword' => strip_tags( uni_convert( $_GET['s'] ) ),
				':offset' => (int)$offset
			), true );
		}
		if( count( $orders ) == 0 ) {
			$html = '
				<tr>
					<td colspan="6">Không tìm thấy đơn hàng với từ khóa <i>' . htmlspecialchars( $_GET['s'], ENT_QUOTES ) . '</i></td>
				</tr>
			';
		}
	}
	
	//No search
	else {
		//Apply filter
		if( isset( $_GET['filter'] ) && $_GET['filter'] == 'true' ) {
			$orders = filter( $exec, $sql );
		}
		
		//No apply filter
		else {
			$orders = $exec -> get( $sql -> get( 104 ), array( 
				':offset' => (int)$offset
			), true );
		}
		
		if( count( $orders ) == 0 ) {
			$html = '
				<tr>
					<td colspan="6">Không có đơn hàng nào</td>
				</tr>
			';
		}
	}
	foreach( $orders as $key => $order ) {
		
		//Get customer name
		$whomake = json_decode( $order['order_who_make_infos'], true );
		
		//Get state label
		$state = $exec -> get( $sql -> get( 106 ), array(
			':state_id' => $order['state_id']
		) );
		$stateLabel = $state[0]['state_name'];
		
		//Get shipping time
		$shippingInfos = json_decode( $order['order_shipping_infos'], true );
		$time = $shippingInfos['time'];
		
		if( $time < ( time() + 86400 ) && $order['state_id'] != 4 ) {
			$shippingTime = '<span title="Sắp đến hạn giao hàng" class="need-now">' . date( 'H:i:s d/m/Y', $time );
		}
		else {
			$shippingTime = '<span>' . date( 'H:i:s d/m/Y', $time );
		}
		
		$html .= '
			<tr>
				<td><a class="order-detail-link" href="/admin/banhang/dondathang/chitiet?order_code=' . $order['order_code'] . '">' . $order['order_code'] . '</a></td>
				<td>' . $whomake['fullname'] . '</td>
				<td><span data-id="' . $order['state_id'] . '" class="state-label state' . $order['state_id'] . '">' . $stateLabel . '</span></td>
				<td>' . number_format( $order['order_total_money'], 0, ',', '.' ) . 'đ</td>
				<td>' . date( 'H:i:s d/m/Y', $order['order_time'] ) . '</td>
				<td>' . $shippingTime . '</td>
			</tr>
		';
	}
	echo $html;
?>