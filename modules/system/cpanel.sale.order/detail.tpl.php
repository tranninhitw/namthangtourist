{@style}
<section class="module-wrapper">
	<section class="module-toolbar">
		<h3 class="module-heading">Bán hàng / Chi tiết đơn hàng <small>Cpanel</small></h3>
		<div class="module-button-area">
			<span class="come-back buttons normal-buttons detail-order-cancel-btn"><i class="fa fa-undo" aria-hidden="true"></i> Trở lại</span>
		</div>
	</section>
	{@detail}
</section>
{@script}