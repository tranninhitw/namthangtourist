{@style}
<section class="module-wrapper">
	<section class="module-toolbar">
		<h3 class="module-heading">Bán hàng / Đơn đặt hàng <small>Cpanel</small></h3>
		<div class="module-button-area">
			<input class="inputs order-search-textbox" placeholder="Mã đơn hàng / Họ tên / Điện thoại (enter)" />
			<span class="buttons normal-buttons filter-btn"><i class="fa fa-filter" aria-hidden="true"></i> Lọc</span>
			<span class="buttons disabled-buttons cancel-btn"><i class="fa fa-undo" aria-hidden="true"></i> Hủy</span>
		</div>
	</section>
	<section class="module-content">
		<div class="tabs module-addnew">
			<span class="tab-title">Lọc và sắp xếp</span>
			<div class="module-addnew-btn">
				<span class="buttons disabled-buttons filter-apply-btn save-btn"><i class="fa fa-floppy-o" aria-hidden="true"></i> Áp dụng</span>
				<a href="include/49?export=xls" target="_blank" class="buttons normal-buttons export-xls"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Xuất excel</a>
			</div>
			{@filter-form}
		</div>
		<div class="order-list-wrapper">
			<table class="tables order-table">
				<tr class="title-rows">
					<td>Mã đơn hàng</td>
					<td>Tên khách hàng</td>
					<td>Trạng thái</td>
					<td>Tổng cộng</td>
					<td>Ngày đặt hàng</td>
					<td>Ngày giao hàng</td>
				</tr>
				{@list}
			</table>
		</div>
		{@pager}
	</section>
</section>
{@script}