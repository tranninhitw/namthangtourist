$( document ).ready( function() {
	var PARAMS = ULTI.queryString();
	
	$( document ).on( 'click', '.come-back', function() {
		DOM.loadTplThisEl( '/admin/banhang/dondathang' );
	} );
	
	/*Show change state selectbox*/
	$( document ).on( 'click', '.state-label', function() {
		$( this ).hide();
		var selectBox = $( this ).next();
		var value = $( this ).data( 'id' );
		selectBox.find( 'option[value="' + value + '"]' ).prop( 'selected', true );
		selectBox.show().focus();
	} );
	
	/*Change state*/
	$( document ).off( 'change', '.change-state' ).on( 'change', '.change-state', function() {
		var changeFn = function( target ) {
			$( '.loading-wrapper, .loading' ).show();
			var state = target.val();
			var orderCode = target.data( 'code' );
			var id = target.data( 'id' );
			var text = target.find( 'option[value="' + state + '"]' ).text();
			var currentState = target.prev().data( 'id' );
			var url = 'include/9';
			var data = {
				'order_code': orderCode,
				'state_id': state,
				'id': id,
				'current_state': currentState
			};
			var callback = function( html ) {
				console.log(html);
				$( '.loading-wrapper, .loading' ).hide();
				var html = html.split( '|' );
				DOM.showStatus( html[1], html[0] );
				DOM.loadTplThisEl( location.href );
			};
			AJAX.post( url, data, callback );
		};
		var text = 'Bạn có chắc chắn muốn đổi tình trạng đơn hàng này?';
		DOM.showPopup( text, changeFn, $( this ) );
	} );
	
	/*Hide selectbox when blur*/
	$( document ).on( 'blur', '.change-state', function() {
		$( this ).hide();
		$( this ).prev().show();
	} );
	
	/*Show tooltip when change note*/
	$( document ).off( 'change input', '.deliving-note, .cs-note' ).on( 'change input', '.deliving-note, .cs-note', function() {
		$( this ).prev().fadeIn( 'medium' );
		$( this ).on( 'blur', function() {
			$( this ).prev().fadeOut( 'medium' );
		} );
	} );
	
	/*Save note when press Ctrl+Enter*/
	$( document ).off( 'keyup', '.deliving-note, .cs-note' ).on( 'keyup', '.deliving-note, .cs-note', function( e ) {
		var keyCode = e.which || e.keyCode;
		if( keyCode == 13 && e.ctrlKey ) {
			var csNote = $( '.cs-note' ).val();
			var delivingNote = $( '.deliving-note' ).val();
			var customerNote = $( '.customer-note' ).val();
			var orderId = $( '.order-id-storer' ).val() ;
			var url = 'include/33';
			var _this = $( this );
			var data = {
				'cs_note' : csNote,
				'deliving_note': delivingNote,
				'customer_note': customerNote,
				'order_id': orderId
			};
			var callback = function( html ) {
				htmlArr = html.split( '|' );
				DOM.showStatus( htmlArr[1], htmlArr[0] );
				_this.blur();
			};
			AJAX.post( url, data, callback );
		}
	} );
} );