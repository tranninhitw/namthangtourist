<?php
	if( isset( $_GET['action'], $_GET['id'] ) && $_GET['action'] == 'edit' ) {
		//Get product
		$shop = new Shop( HOST, USER, PASS, DBNAME );
		$product = $shop -> get_product_by_id( (int)$_GET['id'] );
		$product = $product[0];
		
		//Decode all json column
		$extraTab = json_decode( $product['product_extra_tab'], true );
		$abb = json_decode( $product['product_amount_by_branch'], true );
		$amount = $abb[1];
		$customColumn = json_decode( $product['product_custom_column'], true );
		$publish = $product['product_publish'] == 1 ? 'checked' : '';
		$isNew = $customColumn['new_product'] == 1 ? 'checked' : '';
		$url = substr( $product['product_seo_url'], 0, strlen( $product['product_seo_url'] ) - 9 );
		$images = json_decode( $product['product_images'], true );
		
		//Get image preview
		$imagePreviewHTML = '';
		foreach( $images as $value ) {
			$imagePreviewHTML .= '
				<div class="preview-el">
					<img src="' . _WWW . '/uploads/media/' . $value . '" />
				</div>
			';
		}
		
		//Get cate
		$cateHTML = '';
		$cateIDs = array_filter( explode( ',', $product['cate_id'] ), 'strlen' );
		foreach( $cateIDs as $id ) {
			$cate = $shop -> get_cate_by_id( $id );
			$cate = $cate[0];
			$cateHTML .= '
				<span data-id="' . $id . '">' . $cate['cate_name'] . ' <i class="fa fa-times" aria-hidden="true"></i></span>
			';
		}
		
		//Get avatar
		$avatar = json_decode( $product['product_avatar'], true );
		$avatar = $avatar[0];
		
		$html = '
			<div class="tabs module-addnew">
				<span class="tab-title">Sửa sản phẩm</span>
				<div class="module-addnew-btn">
					<span class="buttons disabled-buttons product-save-new save-btn"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</span>
				</div>
				<input type="radio" name="tab" id="tab1" class="tab-radio" checked />
				<label for="tab1" class="tab-label">Tổng quát</label>
				<div class="tab-content">
					<table class="form-tables tab1-tbl">
						<tr>
							<td>Tên sản phẩm (*)</td>
							<td>
								<input value="' . $product['product_name'] . '" name="product_name" type="text" class="inputs required-inputs product-textbox product-name" placeholder="Tên sản phẩm" />
							</td>
						</tr>
						<tr>
							<td>Thông tin chung (*)</td>
							<td>
								<textarea name="product_description" class="inputs wysiwyg required-inputs product-textbox" placeholder="Thông tin chung">' . $product['product_description'] . '</textarea>
							</td>
						</tr>
						<tr>
							<td>Quy cách sản phẩm (*)</td>
							<td>
								<textarea name="product_technical_description" class="inputs wysiwyg required-inputs product-textbox" placeholder="Quy cách sản phẩm">' . $product['product_technical_description'] . '</textarea>
							</td>
						</tr>
						<tr>
							<td>Thông tin thêm</td>
							<td>
								<textarea name="product_promote_text" class="inputs wysiwyg required-inputs product-textbox" placeholder="Thông tin thêm">' . $extraTab['promote_text'] . '</textarea>
							</td>
						</tr>
					</table>
				</div>
				<input type="radio" name="tab" id="tab2" class="tab-radio" />
				<label for="tab2" class="tab-label">Dữ liệu</label>
				<div class="tab-content">
					<table class="form-tables tab2-tbl">
						<tr>
							<td>Danh mục (*)</td>
							<td>
								<div class="suggest-lists">
									<input type="text" class="inputs suggest-list-textbox" placeholder="Danh mục" />
									<div class="suggest-list-panel">
										{@cate}
									</div>
								</div>
								<div class="cate-selected suggest-list-result">
									<input name="cate_id" type="text" class="hidden inputs product-textbox required-inputs" value="' . $product['cate_id'] . '" readonly />
									' . $cateHTML . '
								</div>
							</td>
						</tr>
						<tr>
							<td>Mã sản phẩm (*)</td>
							<td>
								<input name="product_code" type="text" class="inputs product-textbox required-inputs" value="' . $product['product_code'] . '" />
							</td>
						</tr>
						<tr>
							<td>Số lượng</td>
							<td>
								<input name="product_amount" type="text" class="inputs product-textbox number-inputs" data-max="100000" data-min="1" placeholder="0" value="' . $amount . '" />
							</td>
						</tr>
						<tr>
							<td>Giá bán (*)</td>
							<td>
								<span class="currency-input-wrapper"><input name="product_price" type="text" class="inputs currency-inputs product-textbox required-inputs" value="' . number_format( $product['product_price'], 0, ',', '.' ) . '" /></span>
							</td>
						</tr>
						<tr>
							<td>Đơn vị tính (*)</td>
							<td>
								<input name="product_unit" type="text" class="inputs product-textbox required-inputs" value="' . $product['product_unit'] . '" />
							</td>
						</tr>
						<tr>
							<td>Text tùy chỉnh 1</td>
							<td>
								<input value="' . $customColumn['home_text_1'] . '" name="product_custom_text1" type="text" class="inputs product-textbox" placeholder="Nhập text tùy chỉnh" />
							</td>
						</tr>
						<tr>
							<td>Text tùy chỉnh 2</td>
							<td>
								<input value="' . $customColumn['home_text_2'] . '" name="product_custom_text2" type="text" class="inputs product-textbox" placeholder="Nhập text tùy chỉnh" />
							</td>
						</tr>
						<tr>
							<td>Text bảo hành</td>
							<td>
								<input value="' . $extraTab['guarantee_text'] . '" name="product_guarantee_text" type="text" class="inputs product-textbox" placeholder="Nhập text bảo hành" />
							</td>
						</tr>
						<tr>
							<td>Thứ tự (lớn nằm trước)</td>
							<td>
								<input value="' . $product['product_order'] . '" name="product_order" type="text" class="inputs product-textbox number-inputs" data-max="100000" data-min="1" placeholder="1" value="1" />
							</td>
						</tr>
						<tr>
							<td>
								Cho phép mua sản phẩm này?
							</td>
							<td>
								<input name="product_publish" id="product-publish" class="checkboxes product-textbox" type="checkbox" class="inputs" value="1" ' . $publish . ' />
								<label for="product-publish"></label>
							</td>
						</tr>
						<tr>
							<td>
								Đánh dấu là sản phẩm mới?
							</td>
							<td>
								<input name="product_is_new" id="product-is-new" class="checkboxes product-textbox" type="checkbox" class="inputs" value="1" ' . $isNew . ' />
								<label for="product-is-new"></label>
							</td>
						</tr>
					</table>
				</div>
				<input type="radio" name="tab" id="tab3" class="tab-radio" />
				<label for="tab3" class="tab-label">SEO</label>
				<div class="tab-content">
					<table class="form-tables tab3-tbl">
						<tr>
							<td>Page title (*)</td>
							<td>
								<input value="' . $product['product_page_title'] . '" type="text" name="product_page_title" class="inputs product-textbox required-inputs" placeholder="Page title" />
							</td>
						</tr>
						<tr>
							<td>Url (*)</td>
							<td>
								<input value="' . $url . '" type="text" name="product_seo_url" class="inputs product-textbox required-inputs product-url" placeholder="Url" />
							</td>
						</tr>
						<tr>
							<td>Meta description (*)</td>
							<td>
								<textarea name="product_meta_description" class="inputs product-textbox required-inputs" placeholder="Meta description">' . $product['product_meta_description'] . '</textarea>
							</td>
						</tr>
						<tr>
							<td>Meta keywords (*)</td>
							<td>
								<textarea name="product_meta_keywords" class="inputs product-textbox required-inputs" placeholder="Meta keywords">' . $product['product_meta_keywords'] . '</textarea>
							</td>
						</tr>
						<tr>
							<td>Tags (*)</td>
							<td>
								<input value="' . $product['product_tags'] . '" name="product_tags" type="text" class="product-textbox required-inputs product-tag-result hidden" readonly />
								<div class="product-tag-wrapper">
									<div></div>
									<input type="text" class="inputs product-tags" placeholder="Tags (nhập và enter)" />
								</div>	
							</td>
						</tr>
					</table>
				</div>
				<input type="radio" name="tab" id="tab4" class="tab-radio" />
				<label for="tab4" class="tab-label">Hình ảnh</label>
				<div class="tab-content">
					<table class="form-tables tab4-tbl full-width">
						<tr>
							<td>Chọn hình đại diện (*)</td>
							<td>
								<button class="upload-image2 hidden">Upload</button>
								<input value="' . htmlspecialchars( $product['product_avatar'], ENT_QUOTES ) . '" name="product_avatar" type="text" class="product-avatar-input inputs hidden product-textbox" readonly />
								<input type="file" class="file-inputs product-avatar product-textbox" id="product-avatar" accept="image/*" />
								<label for="product-avatar">
									<span>Đổi hình ảnh</span>
									<span>Chưa chọn file...</span>
								</label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div class="product-avatar-preview">
									<div class="preview-el">
										<img src="/uploads/media/' . $avatar . '" />
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>Chọn hình ảnh để tải lên (*)</td>
							<td>
								<button class="upload-image hidden">Upload</button>
								<input value="' . htmlspecialchars( $product['product_images'], ENT_QUOTES ) . '" name="product_images" type="text" class="product-images hidden inputs product-textbox" readonly />
								<input type="file" class="file-inputs product-image product-textbox" id="product-image" accept="image/*" multiple />
								<label for="product-image">
									<span>Đổi hình ảnh</span>
									<span>Chưa chọn file...</span>
								</label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div class="product-image-preview">
									<!-- Image preview here -->
									' . $imagePreviewHTML . '
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		';
	}
	else {		
		$html = '
			<div class="tabs module-addnew">
				<span class="tab-title">Thêm sản phẩm</span>
				<div class="module-addnew-btn">
					<span class="buttons disabled-buttons product-save-new save-btn"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</span>
				</div>
				<input type="radio" name="tab" id="tab1" class="tab-radio" checked />
				<label for="tab1" class="tab-label">Tổng quát</label>
				<div class="tab-content">
					<table class="form-tables tab1-tbl">
						<tr>
							<td>Tên sản phẩm (*)</td>
							<td>
								<input name="product_name" type="text" class="inputs required-inputs product-textbox product-name" placeholder="Tên sản phẩm" />
							</td>
						</tr>
						<tr>
							<td>Thông tin chung (*)</td>
							<td>
								<textarea name="product_description" class="inputs wysiwyg required-inputs product-textbox" placeholder="Thông tin chung"></textarea>
							</td>
						</tr>
						<tr>
							<td>Quy cách sản phẩm (*)</td>
							<td>
								<textarea name="product_technical_description" class="inputs wysiwyg required-inputs product-textbox" placeholder="Quy cách sản phẩm"></textarea>
							</td>
						</tr>
						<tr>
							<td>Thông tin thêm</td>
							<td>
								<textarea name="product_promote_text" class="inputs wysiwyg required-inputs product-textbox" placeholder="Thông tin thêm"></textarea>
							</td>
						</tr>
					</table>
				</div>
				<input type="radio" name="tab" id="tab2" class="tab-radio" />
				<label for="tab2" class="tab-label">Dữ liệu</label>
				<div class="tab-content">
					<table class="form-tables tab2-tbl">
						<tr>
							<td>Danh mục (*)</td>
							<td>
								<div class="suggest-lists">
									<input type="text" class="inputs suggest-list-textbox" placeholder="Danh mục" />
									<div class="suggest-list-panel">
										{@cate}
									</div>
								</div>
								<div class="cate-selected suggest-list-result">
									<input name="cate_id" type="text" class="hidden inputs product-textbox required-inputs" readonly />
								</div>
							</td>
						</tr>
						<tr>
							<td>Mã sản phẩm (*)</td>
							<td>
								<input name="product_code" type="text" class="inputs product-textbox required-inputs" />
							</td>
						</tr>
						<tr>
							<td>Số lượng</td>
							<td>
								<input name="product_amount" type="text" class="inputs product-textbox number-inputs" data-max="100000" data-min="1" placeholder="0" value="1" />
							</td>
						</tr>
						<tr>
							<td>Giá bán (*)</td>
							<td>
								<span class="currency-input-wrapper"><input name="product_price" type="text" class="inputs currency-inputs product-textbox required-inputs" /></span>
							</td>
						</tr>
						<tr>
							<td>Đơn vị tính (*)</td>
							<td>
								<input name="product_unit" type="text" class="inputs product-textbox required-inputs" />
							</td>
						</tr>
						<tr>
							<td>Text tùy chỉnh 1</td>
							<td>
								<input name="product_custom_text1" type="text" class="inputs product-textbox" placeholder="Nhập text tùy chỉnh" />
							</td>
						</tr>
						<tr>
							<td>Text tùy chỉnh 2</td>
							<td>
								<input name="product_custom_text2" type="text" class="inputs product-textbox" placeholder="Nhập text tùy chỉnh" />
							</td>
						</tr>
						<tr>
							<td>Text bảo hành</td>
							<td>
								<input name="product_guarantee_text" type="text" class="inputs product-textbox" placeholder="Nhập text bảo hành" />
							</td>
						</tr>
						<tr>
							<td>Thứ tự (lớn nằm trước)</td>
							<td>
								<input name="product_order" type="text" class="inputs product-textbox number-inputs" data-max="100000" data-min="1" placeholder="1" value="1" />
							</td>
						</tr>
						<tr>
							<td>
								Cho phép mua sản phẩm này?
							</td>
							<td>
								<input name="product_publish" id="product-publish" class="checkboxes product-textbox" type="checkbox" class="inputs" value="1" checked />
								<label for="product-publish"></label>
							</td>
						</tr>
						<tr>
							<td>
								Đánh dấu là sản phẩm mới?
							</td>
							<td>
								<input name="product_is_new" id="product-is-new" class="checkboxes product-textbox" type="checkbox" class="inputs" value="1" />
								<label for="product-is-new"></label>
							</td>
						</tr>
					</table>
				</div>
				<input type="radio" name="tab" id="tab3" class="tab-radio" />
				<label for="tab3" class="tab-label">SEO</label>
				<div class="tab-content">
					<table class="form-tables tab3-tbl">
						<tr>
							<td>Page title (*)</td>
							<td>
								<input type="text" name="product_page_title" class="inputs product-textbox required-inputs" placeholder="Page title" />
							</td>
						</tr>
						<tr>
							<td>Url (*)</td>
							<td>
								<input type="text" name="product_seo_url" class="inputs product-textbox required-inputs product-url" placeholder="Url" />
							</td>
						</tr>
						<tr>
							<td>Meta description (*)</td>
							<td>
								<textarea name="product_meta_description" class="inputs product-textbox required-inputs" placeholder="Meta description"></textarea>
							</td>
						</tr>
						<tr>
							<td>Meta keywords (*)</td>
							<td>
								<textarea name="product_meta_keywords" class="inputs product-textbox required-inputs" placeholder="Meta keywords"></textarea>
							</td>
						</tr>
						<tr>
							<td>Tags (*)</td>
							<td>
								<input name="product_tags" type="text" class="product-textbox required-inputs product-tag-result hidden" readonly />
								<div class="product-tag-wrapper">
									<div></div>
									<input type="text" class="inputs product-tags" placeholder="Tags (nhập và enter)" />
								</div>	
							</td>
						</tr>
					</table>
				</div>
				<input type="radio" name="tab" id="tab4" class="tab-radio" />
				<label for="tab4" class="tab-label">Hình ảnh</label>
				<div class="tab-content">
					<table class="form-tables tab4-tbl full-width">
						<tr>
							<td>Chọn hình đại diện (*)</td>
							<td>
								<button class="upload-image2 hidden">Upload</button>
								<input name="product_avatar" type="text" class="product-avatar-input inputs hidden product-textbox" readonly />
								<input type="file" class="file-inputs product-avatar required-inputs product-textbox" id="product-avatar" accept="image/*" />
								<label for="product-avatar">
									<span>Chọn hình ảnh</span>
									<span>Chưa chọn file...</span>
								</label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div class="product-avatar-preview"></div>
							</td>
						</tr>
						<tr>
							<td>Chọn hình ảnh để tải lên (*)</td>
							<td>
								<button class="upload-image hidden">Upload</button>
								<input name="product_images" type="text" class="product-images inputs hidden product-textbox" readonly />
								<input type="file" class="file-inputs product-image required-inputs product-textbox" id="product-image" accept="image/*" multiple />
								<label for="product-image">
									<span>Chọn hình ảnh</span>
									<span>Chưa chọn file...</span>
								</label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div class="product-image-preview">
									<!-- Image preview here -->
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		';
	}
	echo $html;
?>