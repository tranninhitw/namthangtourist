<?php 
	$shop = new Shop( HOST, USER, PASS, DBNAME );
	
	$cates = $shop -> get_all_cates();
	
	$html = '';
	foreach( $cates as $key => $cate ) {
		$html .= '<span data-id="' . $cate['cate_id'] . '">' . $cate['cate_name'] . '</span>';
	}
	echo $html;
?>