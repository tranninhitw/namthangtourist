<?php
	//Turn off autoload
	$autoloadFuncs = spl_autoload_functions();
	foreach($autoloadFuncs as $unregisterFunc)
	{
		spl_autoload_unregister($unregisterFunc);
	}
	require_once _CORE . '/phpexcel.cls.php';
	
	//Get order from db
	if( isset( $_GET['export'] ) && $_GET['export'] == 'xls' ) {
		
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		$sql = new Sql();
		
		//If not apply filter
		$products = $exec -> get( $sql -> get( 216 ) );
	}
	
	//Build xls data
	$objPHPExcel = new PHPExcel();
	
	//Set title of document
	$objPHPExcel -> getActiveSheet() 
		-> setCellValue( 'A1', 'BÁO CÁO SẢN PHẨM' )
		-> setCellValue( 'A2', 'Ngày ' . date( 'd/m/Y', time() ) );
		
	$objPHPExcel -> getActiveSheet() -> mergeCells( 'A1:F1' );
	$objPHPExcel -> getActiveSheet() -> mergeCells( 'A2:F2' );
	$objPHPExcel -> getActiveSheet() 
		-> setCellValue( 'A3', 'STT' )
		-> setCellValue( 'B3', 'Mã sản phẩm' )
		-> setCellValue( 'C3', 'Tên sản phẩm' )
		-> setCellValue( 'D3', 'Giá' )
		-> setCellValue( 'E3', 'Tồn kho' )
		-> setCellValue( 'F3', 'Đơn vị tính' );
	$columns = range( 'A', 'F' );
	$titleRows = 4;
	
	foreach( $products as $key => $product ) {
		//Amount
		$amount = json_decode( $product['product_amount_by_branch'], true );
		$amount = $amount[1];
		
		//Rows for sheet
		$rows = array(
			$key + 1,
			$product['product_code'],
			$product['product_name'],
			$product['product_price'],
			$amount,
			$product['product_unit']
		);
		foreach( $columns as $key1 => $column ) {
			$objPHPExcel -> getActiveSheet() -> setCellValue( $column . ( $key + $titleRows ), $rows[$key1] );
		}
	}
	
	//Styling sheet
	$styleArray = array(
		'font'  => array(
			'bold'  => false,
			'color' => array('rgb' => '515151'),
			'size'  => 12,
			'name'  => 'Arial',
		),
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);
	
	$objPHPExcel -> getActiveSheet() -> getStyle( 'A3:' . $objPHPExcel -> getActiveSheet() -> getHighestColumn() . $objPHPExcel -> getActiveSheet() -> getHighestRow() ) -> applyFromArray( $styleArray );
	
	//Styling heading
	$styleArray = array(
		'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => 'FF0000'),
			'size'  => 14,
			'name'  => 'Arial',
		),
		'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
	);
	$objPHPExcel -> getActiveSheet() -> getStyle( 'A1:F2' ) -> applyFromArray( $styleArray );
	
	//Styling heading of table
	$styleArray = array(
		'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => '515151'),
			'size'  => 12,
			'name'  => 'Arial',
		),
		'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        ),
		'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'BDF2FC')
        )
	);
	$objPHPExcel -> getActiveSheet() -> getStyle( 'A3:F3' ) -> applyFromArray( $styleArray );
	
	//Autosize column width
	PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
	foreach( range( 'A','I' ) as $columnID ) {
		$objPHPExcel -> getActiveSheet() -> getColumnDimension( $columnID ) -> setAutoSize( true );
	}
	
	// Rename worksheet
	$objPHPExcel->getActiveSheet()->setTitle('Sản phẩm');

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Quatangtraotay-report.xls"');
	header('Cache-Control: max-age=0');
	
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;
?>