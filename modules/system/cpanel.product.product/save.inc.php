<?php 
	if( isset( $_POST['product_name'] ) ) {
		$exec = new Exec( HOST, USER, PASS, DBNAME );
		$sql = new Sql();
		
		//Get data
		foreach( $_POST as $key => $value ) {
			$$key = $value;
		}
		
		/**
		 * Build data for SQL
		*/
		
		//Extra tab
		$extraTab = array(
			'promote_text' => $product_promote_text,
			'guarantee_text' => $product_guarantee_text
		);
		
		//Amount by branch
		$abb = array(
			'1' => $product_amount
		);
		
		//Amount by attribute
		$aba = array(
			'1' => $product_amount
		);
	
		//Custom column
		$customColumn = array(
			'new_product' => isset( $product_is_new ) ? $product_is_new : 0,
			'home_text_1' => $product_custom_text1,
			'home_text_2' => $product_custom_text2
		);
		
		//Product image
		$productImages = str_replace( '&quot;', '"', $product_images );
		$productAvatar = str_replace( '&quot;', '"', $product_avatar );
		
		//Publish
		$product_publish = isset( $product_publish ) ? $product_publish : '';
		$data = array(
			 ":producer_id" => 0, 
			 ":affiliate_id" => 0, 
			 ":cate_id" => trim( $cate_id, ',' ), 
			 ":product_extra_tab" => json_encode( $extraTab ), 
			 ":product_badge" => null, 
			 ":product_name" => $product_name, 
			 ":product_amount_by_branch" => json_encode( $abb ), 
			 ":product_amount_by_attribute" => json_encode( $aba ), 
			 ":product_code" => $product_code, 
			 ":product_description" => $product_description, 
			 ":product_technical_description" => $product_technical_description, 
			 ":product_page_title" => $product_page_title, 
			 ":product_seo_url" => preg_replace( '/[^0-9a-zA-Z- ]/', '', $product_seo_url ) . '-' . strtolower( $product_code ), 
			 ":product_meta_description" => $product_meta_description, 
			 ":product_meta_keywords" => $product_meta_keywords, 
			 ":product_price" => str_replace( '.', '', $product_price ), 
			 ":product_images" => $productImages, 
			 ":product_tags" => trim( $product_tags, ',' ), 
			 ":product_publish" => $product_publish, 
			 ":product_custom_column" => json_encode( $customColumn ),
			 ":product_avatar" => $productAvatar,
			 ":product_order" => isset( $product_order ) ? $product_order : 1,
			 ":product_unit" => $product_unit
		);
		
		if( isset( $action ) && $action == 'edit' ) {
			$data[':product_id'] = (int)$id;
			$r = $exec -> exec( $sql -> get( 102 ), $data );
		}
		else {
			$r = $exec -> add( $sql -> get( 100 ), $data );
		}
		
		if( $r ) {
			echo '1|Lưu thành công';
		} else {
			echo '0|Lưu thất bại';
		}
	}
?>