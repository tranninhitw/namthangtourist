<?php 
	$exec = new Exec( HOST, USER, PASS, DBNAME );
	$shop = new Shop( HOST, USER, PASS, DBNAME );
	$sql = new Sql();
	
	$products = $exec -> get( $sql -> get( 109 ) );
	
	$html = '';
	
	if( count( $products ) === 0 ) {
		echo '
			<tr>
				<td colspan="7">Chưa có sản phẩm nào</td>
			</tr>
		';
	}
	
	foreach( $products as $key => $product ) {
		//Get image of product
		$images = json_decode( $product['product_avatar'], true );
		$productImage = _WWW . '/uploads/media/' . $images[0];
		
		//Get catename
		$cate = $shop -> get_cate_by_id( $product['cate_id'] );
		$cateName = $cate[0]['cate_name'];
		
		//Amount
		$amount = json_decode( $product['product_amount_by_branch'], true );
		$amount = $amount[1];
		
		//Class 'high' or 'low'
		$min = $shop -> get_minimum_in_stock();
		$class = $amount > $min ? 'high' : 'low';
		
		$html .= '
			<tr>
				<td>' . $product['product_code'] . '</td>
				<td>
					<img src="' . $productImage . '" class="product-image" />
				</td>
				<td><span data-href="' . _WWW . '/admin/sanpham/sanpham?action=edit&id=' . $product['product_id'] . '" class="product-name2">' . $product['product_name'] . '</span></td>
				<td>' . $cateName . '</td>
				<td>' . number_format( $product['product_price'], 0, ',', '.' ) . '</td>			
				<td>
					<span class="amount ' . $class . '">' . $amount . '</span>
				</td>
				<td>
					<button data-href="' . _WWW . '/admin/sanpham/sanpham?action=edit&id=' . $product['product_id'] . '" class="mini-buttons normal-buttons edit-product-btn" title="Sửa" data-id="' . $product['product_id'] . '"><i class="fa fa-pencil" arial-hidden="true"></i></button>
					<button data-id="' . $product['product_id'] . '" class="mini-buttons cancel-buttons delete-product-btn" title="Xóa"><i class="fa fa-trash" aria-hidden="true"></i></button>
				</td>
			</tr>
		';
	}
	echo $html;
?>