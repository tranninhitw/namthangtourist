$( document ).ready( function() {
	/*Init wysiwyg*/
	DOM.initWysiwyg();
	
	/*Global fileList*/
	var fileList = [];
	
	/*PARAMS*/
	PARAMS = ULTI.queryString();
	
	/*Auto-fill url*/
	$( document ).on( 'input', '.product-name', function() {
		var value = $( this ).val();
		value = value.latinise();
		value = value.replace( / /g, '-' );
		value = value.toLowerCase();
		$( '.product-url' ).val( value );
	} );
	
	/*Preview when upload image*/
	$( document ).off( 'change', '.product-image' ).on( 'change', '.product-image', function() {
		fileList = [];
		Array.prototype.push.apply( fileList, this.files );
		var len = fileList.length;
		if( len > 0 ) {
			$( '.product-image-preview' ).empty();
			for( var i = 0; i < len; i++ ) {
				var reader = new FileReader();
				reader.onload = function( e ) {
					$( '.product-image-preview' ).append( '<div class="preview-el"><img src="' + e.target.result + '" /><span><i class="fa fa-times" aria-hidden="true"></i></span></div>' );
					$( '.tab-radio' ).change();
				};
				reader.readAsDataURL( fileList[i] );
			}
		}
		else {
			$( '.product-image-preview' ).empty();
			$( '.tab-radio' ).change();
		}
	} );
	
	/*Preview when upload avatar*/
	$( document ).off( 'change', '.product-avatar' ).on( 'change', '.product-avatar', function() {
		var len = this.files.length;
		if( len > 0 ) {
			$( '.product-avatar-preview' ).empty();
			var reader = new FileReader();
			reader.onload = function( e ) {
				$( '.product-avatar-preview' ).append( '<div class="preview-el"><img src="' + e.target.result + '" /></div>' );
				$( '.tab-radio' ).change();
			};
			reader.readAsDataURL( this.files[0] );
		}
		else {
			$( '.product-avatar-preview' ).empty();
			$( '.tab-radio' ).change();
		}
	} );
	
	/*Delete preview image*/
	$( document ).on( 'click', '.preview-el span', function() {
		var preview = $( this ).parent();
		var index = preview.index();
		preview.remove();
		delete fileList[index];
		fileList = fileList.filter(function(val){return val});
	} );
	
	/*When click addnew*/
	$( document ).on( 'click', '.product-addnew-btn', function() {
		MODULE.cmd( 'addnew', $( '.product-cancel-btn' ) );
	} );
	
	/*When click cancel*/
	$( document ).on( 'click', '.product-cancel-btn', function() {
		MODULE.cmd( 'cancel', $( '.product-cancel-btn' ) );
		//Disable save buttons
		DOM.disableSaveBtn();
	} );
	
	/*On input product-textbox*/
	$( document ).on( 'input change', '.product-textbox', function() {
		var allDone = ULTI.validate( $( '.module-addnew .required-inputs' ), 'empty' );
		if( allDone ) {
			DOM.enableSaveBtn();
		}
		else {
			DOM.disableSaveBtn();
		}
	} );
	
	/*Product tags seperator*/
	$( document ).off( 'keyup', '.product-tags' ).on( 'keyup', '.product-tags', function( e ) {
		//When type comma
		if( e.keyCode == 13 || e.which == 13 ) {
			//Assign value
			var value = $( '.product-tag-result' ).val();
			var newValue = value + ',' + $( this ).val();
			$( '.product-tag-result' ).val( newValue );
			var valueArr = newValue.split( ',' );
			var childDiv = $( '.product-tag-wrapper div' );
			childDiv.empty();
			$( this ).val( '' );
			for( var i = 0, len = valueArr.length; i < len; i++ ) {
				if( valueArr[i]  ) {
					var text = valueArr[i].toLowerCase();
					var span = '<span>' + text + ' <i class="fa fa-times" aria-hidden="true"></i></span>';
					childDiv.append( span );
					$( '.product-tags' ).css( 'padding-left', childDiv.width() + 10 + 'px' );
				}
			}
		}
	} );
	
	/*Remove tags*/
	$( document ).on( 'click', '.product-tag-wrapper span', function() {
		var text = $( this ).text();
		var value = $( '.product-tag-result' ).val();
		var regex = new RegExp( text, 'g' );
		value = value.replace( regex, '' );
		$( '.product-tag-result' ).val( value );
		$( this ).remove();
		$( '.product-tags' ).css( 'padding-left', $( '.product-tag-wrapper div' ).width() + 10 + 'px' );
	} );
	
	
	/*Upload image when click upload*/
	$( document ).off( 'click', '.upload-image' ).on( 'click', '.upload-image', function() {
		var file = fileList;
		if( file.length > 0 ) {
			var input = $( '.product-images' );
			var destination = 'media';
			var imageName = $( '.product-url' ).val();
			var callback = function( html ) {
				input.val( html );
			};
			ULTI.upload( file, destination, imageName, callback );
		}
	} );
	
	/*Upload avatar when click upload*/
	$( document ).off( 'click', '.upload-image2' ).on( 'click', '.upload-image2', function() {
		var file = $( '.product-avatar' )[0].files;
		if( file.length > 0 ) {
			var input = $( '.product-avatar-input' );
			var destination = 'media';
			var imageName = $( '.product-url' ).val() + '-avatar';
			var callback = function( html ) {
				input.val( html );
			};
			ULTI.upload( file, destination, imageName, callback );
		}
	} );
	
	/*Save new product*/
	$( document ).off( 'click', '.product-save-new' ).on( 'click', '.product-save-new', function() {
		$( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i> Đang lưu' );
		//Upload image
		$( '.upload-image, .upload-image2' ).click();
		
		//Save data
		var data = $( '.module-addnew .product-textbox' ).serialize();
		
		if( PARAMS.action == 'edit' ) {
			data += '&action=edit&id=' + PARAMS.id;
		}
		
		var url = 'include/7';
		var callback = function( html ) {
			var htmlArr = html.split( '|' );
			if( htmlArr[0] == '1' ) {
				ULTI.done( htmlArr[1] );
			}
			else {
				DOM.showStatus( htmlArr[1], parseInt( htmlArr[0] ) );
				$( '.product-save-new' ).removeClass( 'saving' ).html( '<i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu' );
			}
		};
		AJAX.post( url, data, callback );
	} );
	
	/*Edit product*/
	$( document ).off( 'click', '.product-name2, .edit-product-btn' ).on( 'click', '.product-name2, .edit-product-btn', function() {
		//Load tpl
		DOM.loadTplThisEl( $( this ) );
	} );
	
	/*When edit*/
	$( document ).ready( function() {
		if( PARAMS.action == 'edit' ) {
			//Trigger tags when edit product
			var enter = jQuery.Event( 'keyup' );
			enter.which = 13;
			$( '.product-tags' ).trigger( enter );
			
			//Fade In addnew form
			$( '.product-addnew-btn' ).click();
		}
	} );
	
	/*Delete product*/
	$( document ).on( 'click', '.delete-product-btn', function() {
		var deleteFn = function( target ) {
			var id = target.data( 'id' );
			
			var url = 'include/8';
			var data = {
				'product_id': id
			};
			var callback = function( html ) {
				ULTI.done( 'Xóa thành công' );
			};
			AJAX.post( url, data, callback );
		};
		var text = 'Bạn có chắc chắn muốn xóa sản phẩm?';
		DOM.showPopup( text, deleteFn, $( this ) );
	} );
} ); 