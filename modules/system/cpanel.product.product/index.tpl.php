{@style}
<div class="module-wrapper">
	<section class="module-toolbar">
		<h3 class="module-heading">Sản phẩm / Danh sách <small>Cpanel</small></h3>
		<div class="module-button-area">
			<span class="buttons normal-buttons product-addnew-btn"><i class="fa fa-plus" aria-hidden="true"></i> Thêm</span>
			<a href="include/51?export=xls" target="_blank" class="buttons normal-buttons export-xls"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Xuất excel</a>
			<span class="buttons disabled-buttons product-cancel-btn"><i class="fa fa-undo" aria-hidden="true"></i> Hủy</span>
		</div>
	</section>
	<section class="module-content">
		{@form-addnew}
		<div class="product-list-wrapper">
			<table class="tables">
				<tr class="title-rows">
					<td>Mã</td>
					<td>Hình ảnh</td>
					<td>Tên sản phẩm</td>
					<td>Danh mục</td>
					<td>Giá</td>			
					<td>Số lượng</td>
					<td>Thao tác</td>
				</tr>
				{@list}
			</table>
		</div>
	</section>
</div>
{@script}