<?php
$exec = new Exec( HOST, USER, PASS, DBNAME );
$sql = new Sql();

$html = '
		<table class="form-tables">
                <tr>
                    <td>Tên danh mục tour (*)</td>
                    <td>
                        <input name="cate_tour_name" class="inputs normal-inputs required-inputs cate-tour-textbox cate-tour-name-textbox" type="text" placeholder="Nhập tên danh mục tour" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Title (*)
                    </td>
                    <td>
                        <input name="cate_tour_title" type="text" class="inputs required-inputs cate-tour-textbox" placeholder="Page title" />
                    </td>
                </tr>
                <tr>
                    <td>
                        URL (*)
                    </td>
                    <td>
                        <input name="cate_tour_seo_url" type="text" class="inputs required-inputs cate-tour-textbox" placeholder="Url" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Meta description (*)
                    </td>
                    <td>
                        <textarea name="cate_tour_meta_description" class="inputs required-inputs cate-tour-textbox" placeholder="Meta description"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        Meta keywords (*)
                    </td>
                    <td>
                        <textarea name="cate_tour_meta_keywords" class="inputs required-inputs cate-tour-textbox" placeholder="Meta keywords"></textarea>
                    </td>
                </tr>

            </table>
	';

if( isset( $_GET['action'], $_GET['id'] ) && $_GET['action'] == 'edit' ) {
    //Get hotel by id
    $data = array(
        ':cate_tour_id' => (int)$_GET['id'],
    );
    $cateTour = $exec -> get( $sql -> get( 15 ), $data );
    $cateTour = $cateTour[0];

    $cateTourName = $cateTour['cate_tour_name'];
    $cateTourSeoUrl = $cateTour['cate_tour_seo_url'];
    $cateTourTitle = $cateTour['cate_tour_title'];
    $cateTourMetaDescription = $cateTour['cate_tour_meta_description'];
    $cateTourMetaKeywords = $cateTour['cate_tour_meta_keywords'];

    $html = '
		<table class="form-tables">
                <tr>
                    <td>Tên danh mục tour (*)</td>
                    <td>
                        <input name="cate_tour_name" value="' . $cateTourName . '" class="inputs normal-inputs required-inputs cate-tour-textbox cate-tour-name-textbox" type="text" placeholder="Nhập tên danh mục" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Title (*)
                    </td>
                    <td>
                        <input name="cate_tour_title" value="' . $cateTourTitle . '" type="text" class="inputs required-inputs cate-tour-textbox" placeholder="Page title" />
                    </td>
                </tr>
                <tr>
                    <td>
                        URL (*)
                    </td>
                    <td>
                        <input name="cate_tour_seo_url" value="' . $cateTourSeoUrl . '" type="text" class="inputs required-inputs cate-tour-textbox" placeholder="Url" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Meta description (*)
                    </td>
                    <td>
                        <textarea name="cate_tour_meta_description" class="inputs required-inputs cate-tour-textbox" placeholder="Meta description">
                            ' . $cateTourMetaDescription . '
                        </textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        Meta keywords (*)
                    </td>
                    <td>
                        <textarea name="cate_tour_meta_keywords" class="inputs required-inputs cate-tour-textbox" placeholder="Meta keywords">
                            ' . $cateTourMetaKeywords . '
                        </textarea>
                    </td>
                </tr>
            </table>
	';
}
echo $html;
?>
