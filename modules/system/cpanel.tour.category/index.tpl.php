{@style}
<section class="module-wrapper">
    <section class="module-toolbar">
        <h3 class="module-heading">Quản lý / Danh mục tour<small>Cpanel</small></h3>
        <div class="module-button-area">
            <span class="buttons normal-buttons cate-tour-addnew-btn"><i class="fa fa-plus" aria-hidden="true"></i> Thêm</span>
            <span class="buttons disabled-buttons cate-tour-cancel-btn"><i class="fa fa-undo" aria-hidden="true"></i> Hủy</span>
        </div>
    </section>
    <section class="module-content">
        <div class="module-addnew cate-tour-addnew">
            <span class="buttons disabled-buttons cate-tour-name-save-btn"><i class="fa fa-floppy-o"></i> Lưu</span>
            {@form-addnew}
        </div>
        <div class="cate-tour-list-wrapper">
            <div class="module-button-area">
                <span class="buttons disabled-buttons cate-tour-save-order-btn save-btn"><i class="fa fa-floppy-o"></i> Lưu</span>
                <span class="buttons disabled-buttons cate-tour-undo-order-btn undo-btn"><i class="fa fa-undo"></i> Hủy</span>
            </div>
            <div class="area-list-wrapper">
                <table class="tables">
                    <tr class="title-rows">
                        <td>Tên danh mục tour</td>
                        <td>Tên seo url</td>
                        <td>Số điện thoại</td>
                        <td>Website</td>
                    </tr>
                    {@list}
                </table>
            </div>
            {@list}
        </div>
    </section>
</section>
{@script}