$( document ).ready( function() {

    var PARAMS = ULTI.queryString();

    /*Show addnew form*/
    $( document ).off( 'click', '.cate-tour-addnew-btn' ).on( 'click', '.cate-tour-addnew-btn', function() {
        MODULE.cmd( 'addnew', $( '.cate-tour-cancel-btn' ) );
    } );

    /*Hide addnew form*/
    $( document ).off( 'click', '.cate-tour-cancel-btn' ).on( 'click', '.cate-tour-cancel-btn', function() {
        MODULE.cmd( 'cancel', $( '.cate-tour-cancel-btn' ) );
    } );

    /*Validate form*/
    $( document ).on( 'input change', '.cate-tour-textbox', function() {
        var allDone = ULTI.validate( $( '.module-addnew .required-inputs' ), 'empty' );
        if( allDone ) {
            DOM.enableSaveBtn();
        }
        else {
            DOM.disableSaveBtn();
        }
    } );


    /*Save area*/
    $( document ).off( 'click', '.save-new-cate-tour' ).on( 'click', '.save-new-cate-tour', function() {
        $( this ).addClass( 'saving' ).html( '<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i> Đang lưu' );

        var url = 'include/23';
        var data = $( '.cate-tour-form .area-textbox' ).serialize();
        if( PARAMS.action == 'edit' ) {
            data += '&action=edit&id=' + PARAMS.id;
        }
        var callback = function( html ) {
            htmlArr = html.split( '|' );
            if( htmlArr[0] == '1' ) {
                ULTI.done( htmlArr[1] );
            }
            else {
                DOM.showStatus( htmlArr[1], parseInt( htmlArr[0] ) );
                $( '.save-new-cate-tour' ).removeClass( 'saving' ).html( '<i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu' );
            }
        };
        AJAX.post( url, data, callback );
    } );

    /*Edit cate-tour*/
    $( document ).off( 'click', '.edit-cate-tour' ).on( 'click', '.edit-cate-tour', function() {
        DOM.loadTplThisEl( '/admin/caidat/diadanh?action=edit&id=' + $( this ).data( 'id' ) );
    } );

    /*When edit*/
    $( document ).ready( function() {
        if( PARAMS.action == 'edit' ) {
            $( '.cate-tour-addnew-btn' ).click();
            DOM.enableSaveBtn();
        }
    } );

    /*Delete cate-tour*/
    $( document ).off( 'click', '.delete-cate-tour' ).on( 'click', '.delete-cate-tour', function() {
        var deleteFn = function( target ) {
            var url = 'include/24';
            var data = {
                'id': target.data( 'id' )
            };
            var callback = function( html ) {
                var htmlArr = html.split( '|' );
                DOM.showStatus( htmlArr[1], parseInt(htmlArr[0]) );
                DOM.loadTplThisEl( '/admin/caidat/diadanh' );
            };
            AJAX.post( url, data, callback );
        };
        var text = "Bạn có chắc chắn xóa danh mục tour này?";
        DOM.showPopup( text, deleteFn, $( this ) );
    } );
} );