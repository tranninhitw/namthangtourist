{@style}
<section class="route-contents faq">
    <div class="route-title">
        <h1>Hỏi - đáp</h1>
        <p>Các câu hỏi thường gặp</p>
    </div>
    <section class="faq-menu">
        <ul>
            <li class="selected" data-index="0"> Danh muc 1 </li>
            <li data-index="1"> Danh muc 2 </li>
            <li data-index="2"> Danh muc 3 </li>
            <li data-index="3"> Danh muc 4 </li>
            <li data-index="4"> Danh muc 5</li>
        </ul>
    </section>

    <section class="faq-content">
        <section class="visible">
            <div class="faq-item-header"></div>
            <div class="faq-item-content"></div>
        </section>
    </section>

    <div class="clearfix"></div>
</section>
{@script}
