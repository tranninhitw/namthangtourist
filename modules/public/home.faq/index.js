$(function() {
  var faqContent = {
    data: [
      [
        {
          header: "What is CloudSocial?",
          content:
            "CloudSocial is a unified social media management & engagement platform for businesses and enables them to easily and effectively manage publishing, listening and responding on social media"
        },
        {
          header: "Who uses CloudSocial?",
          content: "Content for who uses CloudSocial"
        },
        {
          header: "What is CloudSocial?",
          content:
            "CloudSocial is a unified social media management & engagement platform for businesses and enables them to easily and effectively manage publishing, listening and responding on social media"
        },
        {
          header: "Who uses CloudSocial?",
          content: "Content for who uses CloudSocial"
        }
      ],
      [
        {
          header: "Function #1",
          content: "Function #1 is a unified social media management & engagement platform for businesses and enables them to easily and effectively manage publishing, listening and responding on social media"
        },
        {
          header: "Function #2",
          content: "Function #2 Content is a unified social media management & engagement platform for businesses and enables them to easily and effectively manage publishing, listening and responding on social media"
        }
      ],
      [
        {
          header: "Cloud Social #1",
          content: "Cloud Social #1 is a unified social media management & engagement platform for businesses and enables them to easily and effectively manage publishing, listening and responding on social media"
        },
        {
          header: "Cloud Social #2",
          content: "Cloud Social #2 Content is a unified social media management & engagement platform for businesses and enables them to easily and effectively manage publishing, listening and responding on social media"
        }
      ],
      [
        {
          header: "Misc #1",
          content: "Misc Social #1 is a unified social media management & engagement platform for businesses and enables them to easily and effectively manage publishing, listening and responding on social media"
        },
        {
          header: "Misc Social #2",
          content: "Misc Social #2 Content is a unified social media management & engagement platform for businesses and enables them to easily and effectively manage publishing, listening and responding on social media"
        }
      ],[
        {
          header: "Agencies Social #1",
          content: "Agencies Social #1 is a unified social media management & engagement platform for businesses and enables them to easily and effectively manage publishing, listening and responding on social media"
        },
        {
          header: "Agencies Social #2",
          content: "Agencies Social #2 Content is a unified social media management & engagement platform for businesses and enables them to easily and effectively manage publishing, listening and responding on social media"
        }
      ]
    ],
    init: function() {
      this.cacheDOM();
      this.bindEvent();
      this.renderFaqContent(0);
    },
    cacheDOM: function() {
      this.$faqContent = $(".faq-content");
      this.$faqMenuItem = $(".faq-menu ul li");
    },
    toggleAccordian: function() {
      var $this = $(this);
      var $arrow = $(".arrow");
      $arrow.removeClass("arrow-up").addClass("arrow-down");

      if($this.next().css('display') === 'none'){
        $(this).find('.arrow').removeClass("arrow-down").addClass("arrow-up");
        $(this)
          .next()
          .show(300)
          .parent()
          .siblings()
          .find(".faq-item-content")
          .hide(300);
      } else {
        $this
          .next()
          .hide(300)
      }
    },
    bindEvent: function() {
      var self = this;
      this.$faqMenuItem.on("click", function() {
        var $li = $(this);
        $li
          .addClass("selected")
          .siblings()
          .removeClass("selected");
        var index = $li.data("index");
        self.renderFaqContent(index);
      });
    },
    renderFaqContent: function(index) {
      var values = this.data[index];
      var self = this;
      if (!values) {
        return;
      }
      var newContent = values.map(function(value, index) {
        var visible = index === 0 ? "visible" : "";
        var $section = $("<section />", {
          class: visible
        });

        var $arrow = $("<span />", {
          class: visible ? "arrow arrow-up" : "arrow arrow-down"
        })


        var $header = $("<div />", {
          class: "faq-item-header",
          html: value.header,
          on: {
            click: self.toggleAccordian
          }
        });
        $header.append($arrow)

        var $content = $("<div />", {
          class: "faq-item-content",
          html: value.content
        });

        $section.append($header);
        $section.append($content);

        return $section;
      });

      this.$faqContent.html(newContent);
    }
  };

  faqContent.init();
});
