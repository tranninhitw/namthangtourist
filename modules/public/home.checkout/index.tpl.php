<div class="checkout-wrapper">
    <div class="section">
        <h3 class="title">THÔNG TIN DỊCH VỤ</h3>
        <div class="content">
            <h1><a href="{@www}/tour">Liên Tuyến Bắc Trung: Hà Nội - Sapa - Hạ Long - Ninh Bình - Huế - Động Thiên Đường & Phong Nha - Đà Nẵng - Bà Nà - Hội An</a></h1>
            <div class="tour-rating">
                <span class="star js_rate-yo" data-readonly="yes" data-rate="4.6"></span>
                <span class="number">4.6/5 (1642 đánh giá)</span>
                <div class="clearfix"></div>
            </div>
            <div class="tour-picture">
                <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                    <img src="assets/images/tours/nga.jpg" alt="" />
                </a>
            </div>
            <div class="tour-meta">
                <div class="meta-left">
                    <p> <i class="far fa-calendar-alt"></i> Ngày đi: 22/08/2019</p>
                    <p> <i class="far fa-clock"></i> Giờ đi: 17:30</p>
                    <p> <i class="fas fa-dollar-sign"></i> Giá: 13.320.000 đ</p>
                </div>
                <div class="meta-right">
                    <p> <i class="fas fa-barcode"></i> Mã tour: HAH9327-SL-O</p>
                    <p> <i class="fas fa-ticket-alt"></i> Còn lại: 2 vé</p>
                    <p> <i class="fas fa-hourglass-half"></i> Lộ trình: 6 ngày</p>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <h3 class="title">GIÁ ĐẦY ĐỦ</h3>
        <div class="content">
            <table>
                <tr>
                    <th>Người lớn</th>
                    <td>6.900.000 đ</td>
                </tr>
                <tr>
                    <th>Trẻ em (Từ 5 -9 tuổi)</th>
                    <td>3.900.000 đ</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="section">
        <h3 class="title">Thông tin liên lạc</h3>
        <div class="content">
            <div class="group">
                <label>Họ và tên:</label>
                <input type="text" name="" placeholder="Nhập quý danh" />
            </div>
            <div class="group">
                <label>Địa chỉ:</label>
                <input type="text" name="" placeholder="Nhập địa chỉ" />
            </div>
            <div class="group">
                <label>Số người lớn:</label>
                <input type="text" name="" placeholder="Số người lớn" />
            </div>
            <div class="group">
                <label>Số trẻ em:</label>
                <input type="text" name="" placeholder="Số trẻ em (Từ 5 - 9 tuổi)" />
            </div>
            <div class="group">
                <label>Ghi chú:</label>
                <textarea placeholder="Quý khách có dặn dò khác không?"></textarea>
            </div>
        </div>
    </div>
    <div class="section">
        <h3 class="title">Danh sách khách hàng</h3>
        <div class="content">
            <table>
                <tr>
                    <td>
                        <label>Họ tên đầy đủ:</label>
                        <input type="text" name="" placeholder="Nhập năm sinh" />
                    </td>
                    <td>
                        <label>Giới tính:</label>
                        <select class="" name="">
                            <option value="1">Nam</option>
                            <option value="2">Nữ</option>
                            <option value="3">Khác</option>
                        </select>
                    </td>
                    <td>
                        <label>Ngày, tháng, năm sinh:</label>
                        <input type="text" name="" placeholder="ngày/tháng/năm" />
                    </td>
                    <td>
                        <label>Thành tiền:</label>
                        <label>2.300.000 đ</label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="section">
        <h3 class="title">Tổng tiền & Thanh Toán</h3>
        <div class="content">
            <div class="total">
                Tổng số tiền phải thanh toán: <b>10.260.000</b> đ (Chưa gồm VAT)
            </div>
            <div class="payment-option">
                <div class="payment-item">
                    <input type="radio" name="" value="1" />
                    <span>Tiền mặt</span>
                </div>
                <div class="payment-item">
                    <input type="radio" name="" value="1" />
                    <span>Chuyển khoản</span>
                </div>
                <div class="payment-item">
                    <input type="radio" name="" value="1" />
                    <span>ATM / Internet Banking</span>
                </div>
            </div>
            <div class="payment-tip">
                <div class="tip-item">
                    Quý khách vui lòng thanh toán tại bất kỳ văn phòng Nam Thắng Tourist trên toàn quốc.
                    Xin lưu ý, Quý khách nên liên lạc trước khi đến để biết rõ hơn về giờ làm việc và các hồ sơ cần chuẩn bị khi thanh toán.
                </div>
                <div class="tip-item">
                    Thông tin hướng dẫn chuyển khoản
                </div>
                <div class="tip-item">
                    Thanh toán qua cổng VNPay
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <h3 class="title">Điều khoản</h3>
        <div class="content">
            <p>Vui lòng đọc kỹ thỏa thuận dịch vụ của chúng tôi <a href="{@www}/thoa-thuan-su-dung">tại đây</a></p>
            <input type="checkbox" name="" value="1" />
            <span>Tôi đồng ý với điều khoản sử dụng dịch vụ</span>
            <button>Đặt dịch vụ</button>
        </div>
    </div>
</div>
