{@style}
<section class="route-contents">
    <div class="route-title">
        <h1>Sơ đồ trang</h1>
        <p>Tổng hợp tất cả đường dẫn trên trang</p>
    </div>
    <ul class="sitemap">
        <li>
            <a href="{@www}">{@www} <b>Trang chủ</b> </a>
            <ul>
                <li>
                    <a href="{@www}/gioi-thieu">{@www}/gioi-thieu <b>Giới thiệu</b></a>
                </li>
                <li>
                    <a href="{@www}/chinh-sach-bao-mat">{@www}/chinh-sach-bao-mat <b>Chính sách bảo mật</b></a>
                </li>
                <li>
                    <a href="{@www}/thoa-thuan-su-dung">{@www}/thoa-thuan-su-dung <b>Thỏa thuận sử dụng</b></a>
                </li>
                <li>
                    <a href="{@www}/cau-hoi-thuong-gap">{@www}/cau-hoi-thuong-gap <b>Hỏi đáp</b></a>
                </li>
                <li>
                    <a href="#"><b>Du lịch</b></a>
                    <ul>
                        <li>
                            <a href="{@www}/so-do-trang-web">{@www}/so-do-trang-web <b>Sơ đồ trang</b></a>
                        </li>
                        <li>
                            <a href="{@www}/so-do-trang-web">{@www}/so-do-trang-web <b>Sơ đồ trang</b></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><b>Dịch vụ</b></a>
                    <ul>
                        <li>
                            <a href="{@www}/so-do-trang-web">{@www}/so-do-trang-web <b>Sơ đồ trang</b></a>
                        </li>
                        <li>
                            <a href="{@www}/so-do-trang-web">{@www}/so-do-trang-web <b>Sơ đồ trang</b></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><b>Tin tức</b></a>
                    <ul>
                        <li>
                            <a href="{@www}/so-do-trang-web">{@www}/so-do-trang-web <b>Sơ đồ trang</b></a>
                        </li>
                        <li>
                            <a href="{@www}/so-do-trang-web">{@www}/so-do-trang-web <b>Sơ đồ trang</b></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
    </ul>
</section>
