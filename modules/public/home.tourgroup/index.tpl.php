{@style}
<section class="route-contents">
    <div class="tourgroup">
        <div class="tourgroup-sidebar">
            <div class="filter">
                <h3>Lọc tour</h3>
                <form>
                    <label>Chọn nơi đến:</label>
                    <ul class="selectbox">
                        <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Chọn nơi đến" />
                        <div class="selectbox-options js_selectbox-options">
                            <li class="selectbox-option" data-value="1">Sapa</li>
                            <li class="selectbox-option" data-value="2">Hà Khẩu</li>
                            <li class="selectbox-option" data-value="3">Hà Giang</li>
                            <li class="selectbox-option" data-value="4">Ninh Bình</li>
                            <li class="selectbox-option" data-value="5">Hà Nội</li>
                            <li class="selectbox-option" data-value="6">Phú Quốc</li>
                            <li class="selectbox-option" data-value="7">Côn Đảo</li>
                        </div>
                    </ul>
                    <label>Chọn nơi đi:</label>
                    <ul class="selectbox">
                        <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Chọn nơi đi" />
                        <div class="selectbox-options js_selectbox-options">
                            <li class="selectbox-option" data-value="1">Sapa</li>
                            <li class="selectbox-option" data-value="2">Hà Khẩu</li>
                            <li class="selectbox-option" data-value="3">Hà Giang</li>
                            <li class="selectbox-option" data-value="4">Ninh Bình</li>
                            <li class="selectbox-option" data-value="5">Hà Nội</li>
                            <li class="selectbox-option" data-value="6">Phú Quốc</li>
                            <li class="selectbox-option" data-value="7">Côn Đảo</li>
                        </div>
                    </ul>
                    <label>Loại tour:</label>
                    <ul class="selectbox">
                        <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Loại tour" />
                        <div class="selectbox-options js_selectbox-options">
                            <li class="selectbox-option" data-value="1">Tour riêng</li>
                            <li class="selectbox-option" data-value="2">Tour ghép</li>
                            <li class="selectbox-option" data-value="3">Tour sốc</li>
                            <li class="selectbox-option" data-value="4">Combo</li>
                        </div>
                    </ul>
                    <label>Giá tour:</label>
                    <ul class="selectbox">
                        <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Giá tour" />
                        <div class="selectbox-options js_selectbox-options">
                            <li class="selectbox-option" data-value="1">Tour thường</li>
                            <li class="selectbox-option" data-value="2">Tour cao cấp</li>
                            <li class="selectbox-option" data-value="3">Tour hạng sang</li>
                        </div>
                    </ul>
                    <label>Ngày khởi hành:</label>
                    <input type="text" class="filter-textbox js_datetimepicker" value="Ngày khởi hành" />
                    <input type="submit" class="filter-button" value="Tìm ngay" />
                </form>
            </div>
            <div class="recent-viewed">
                <h3>XEM GẦN ĐÂY</h3>
                <p>Bạn đã xem 3 tour gần đây</p>
                <div class="cycle-slideshow" data-cycle-slides="> div" data-cycle-log="false" data-cycle-fx="tileBlind" data-cycle-pager="#pager" data-cycle-pause-on-hover="true" data-cycle-speed="200">
                    <div class="tour">
                        <div class="tour-picture">
                            <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                                <img src="assets/images/tours/nga.jpg" alt="" />
                                <div class="tour-date">
                                    <p> <i class="far fa-calendar-alt"></i> Lộ trình 6 ngày</p>
                                    <p> <i class="fas fa-ticket-alt"></i> Còn 2 vé</p>
                                </div>
                            </a>
                        </div>
                        <div class="tour-meta">
                            <p>Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
                            <span>9,290,000 đ</span>
                            <span>8,290,000 đ</span>
                        </div>
                    </div>
                    <div class="tour">
                        <div class="tour-picture">
                            <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                                <img src="assets/images/tours/fansiphan.jpg" alt="" />
                                <div class="tour-date">
                                    <p> <i class="far fa-calendar-alt"></i> Lộ trình 6 ngày</p>
                                    <p> <i class="fas fa-ticket-alt"></i> Còn 2 vé</p>
                                </div>
                            </a>
                        </div>
                        <div class="tour-meta">
                            <p>Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
                            <span>10,290,000 đ</span>
                            <span>7,290,000 đ</span>
                        </div>
                    </div>
                </div>
                <div id="pager"></div>
            </div>
        </div>
        <div class="tourgroup-list">
            <div class="tourgroup-description">
                <h1>Tour ghép</h1>
                <p>Nam sagittis sit amet sapien ut congue. Pellentesque vel vulputate sem. Praesent id diam tristique, dictum odio id, efficitur nulla. </p>
                <div class="sharethis-inline-share-buttons"></div>
            </div>
            <div class="the-tour">
                <h1><a href="{@www}/tour">Liên Tuyến Bắc Trung: Hà Nội - Sapa - Hạ Long - Ninh Bình - Huế - Động Thiên Đường & Phong Nha - Đà Nẵng - Bà Nà - Hội An</a></h1>
                <div class="tour-rating">
                    <span class="star js_rate-yo" data-readonly="yes" data-rate="4.6"></span>
                    <span class="number">4.6/5 (1642 đánh giá)</span>
                    <div class="clearfix"></div>
                </div>
                <div class="tour-picture">
                    <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                        <img src="assets/images/tours/nga.jpg" alt="" />
                    </a>
                </div>
                <div class="tour-meta">
                    <div class="meta-left">
                        <p> <i class="far fa-calendar-alt"></i> Ngày đi: 22/08/2019</p>
                        <p> <i class="far fa-clock"></i> Giờ đi: 17:30</p>
                        <p> <i class="fas fa-dollar-sign"></i> Giá: 13.320.000 đ</p>
                    </div>
                    <div class="meta-right">
                        <p> <i class="fas fa-barcode"></i> Mã tour: HAH9327-SL-O</p>
                        <p> <i class="fas fa-ticket-alt"></i> Còn lại: 2 vé</p>
                        <p> <i class="fas fa-hourglass-half"></i> Lộ trình: 6 ngày</p>
                    </div>
                </div>
                <div class="tour-button">
                    <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa</h3>
                    <h3><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</h3>
                    <a href="{@www}/tour">ĐẶT NGAY</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="the-tour">
                <h1><a href="{@www}/tour">Liên Tuyến Bắc Trung: Hà Nội - Sapa - Hạ Long - Ninh Bình - Huế - Động Thiên Đường & Phong Nha - Đà Nẵng - Bà Nà - Hội An</a></h1>
                <div class="tour-rating">
                    <span class="star js_rate-yo" data-readonly="yes" data-rate="4.6"></span>
                    <span class="number">4.6/5 (1642 đánh giá)</span>
                    <div class="clearfix"></div>
                </div>
                <div class="tour-picture">
                    <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                        <img src="assets/images/tours/nga.jpg" alt="" />
                    </a>
                </div>
                <div class="tour-meta">
                    <div class="meta-left">
                        <p> <i class="far fa-calendar-alt"></i> Ngày đi: 22/08/2019</p>
                        <p> <i class="far fa-clock"></i> Giờ đi: 17:30</p>
                        <p> <i class="fas fa-dollar-sign"></i> Giá: 13.320.000 đ</p>
                    </div>
                    <div class="meta-right">
                        <p> <i class="fas fa-barcode"></i> Mã tour: HAH9327-SL-O</p>
                        <p> <i class="fas fa-ticket-alt"></i> Còn lại: 2 vé</p>
                        <p> <i class="fas fa-hourglass-half"></i> Lộ trình: 6 ngày</p>
                    </div>
                </div>
                <div class="tour-button">
                    <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa</h3>
                    <h3><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</h3>
                    <a href="{@www}/tour">ĐẶT NGAY</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="the-tour">
                <h1><a href="{@www}/tour">Liên Tuyến Bắc Trung: Hà Nội - Sapa - Hạ Long - Ninh Bình - Huế - Động Thiên Đường & Phong Nha - Đà Nẵng - Bà Nà - Hội An</a></h1>
                <div class="tour-rating">
                    <span class="star js_rate-yo" data-readonly="yes" data-rate="4.6"></span>
                    <span class="number">4.6/5 (1642 đánh giá)</span>
                    <div class="clearfix"></div>
                </div>
                <div class="tour-picture">
                    <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                        <img src="assets/images/tours/nga.jpg" alt="" />
                    </a>
                </div>
                <div class="tour-meta">
                    <div class="meta-left">
                        <p> <i class="far fa-calendar-alt"></i> Ngày đi: 22/08/2019</p>
                        <p> <i class="far fa-clock"></i> Giờ đi: 17:30</p>
                        <p> <i class="fas fa-dollar-sign"></i> Giá: 13.320.000 đ</p>
                    </div>
                    <div class="meta-right">
                        <p> <i class="fas fa-barcode"></i> Mã tour: HAH9327-SL-O</p>
                        <p> <i class="fas fa-ticket-alt"></i> Còn lại: 2 vé</p>
                        <p> <i class="fas fa-hourglass-half"></i> Lộ trình: 6 ngày</p>
                    </div>
                </div>
                <div class="tour-button">
                    <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa</h3>
                    <h3><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</h3>
                    <a href="{@www}/tour">ĐẶT NGAY</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="the-tour">
                <h1><a href="{@www}/tour">Liên Tuyến Bắc Trung: Hà Nội - Sapa - Hạ Long - Ninh Bình - Huế - Động Thiên Đường & Phong Nha - Đà Nẵng - Bà Nà - Hội An</a></h1>
                <div class="tour-rating">
                    <span class="star js_rate-yo" data-readonly="yes" data-rate="4.6"></span>
                    <span class="number">4.6/5 (1642 đánh giá)</span>
                    <div class="clearfix"></div>
                </div>
                <div class="tour-picture">
                    <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                        <img src="assets/images/tours/nga.jpg" alt="" />
                    </a>
                </div>
                <div class="tour-meta">
                    <div class="meta-left">
                        <p> <i class="far fa-calendar-alt"></i> Ngày đi: 22/08/2019</p>
                        <p> <i class="far fa-clock"></i> Giờ đi: 17:30</p>
                        <p> <i class="fas fa-dollar-sign"></i> Giá: 13.320.000 đ</p>
                    </div>
                    <div class="meta-right">
                        <p> <i class="fas fa-barcode"></i> Mã tour: HAH9327-SL-O</p>
                        <p> <i class="fas fa-ticket-alt"></i> Còn lại: 2 vé</p>
                        <p> <i class="fas fa-hourglass-half"></i> Lộ trình: 6 ngày</p>
                    </div>
                </div>
                <div class="tour-button">
                    <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa</h3>
                    <h3><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</h3>
                    <a href="{@www}/tour">ĐẶT NGAY</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="the-tour">
                <h1><a href="{@www}/tour">Liên Tuyến Bắc Trung: Hà Nội - Sapa - Hạ Long - Ninh Bình - Huế - Động Thiên Đường & Phong Nha - Đà Nẵng - Bà Nà - Hội An</a></h1>
                <div class="tour-rating">
                    <span class="star js_rate-yo" data-readonly="yes" data-rate="4.6"></span>
                    <span class="number">4.6/5 (1642 đánh giá)</span>
                    <div class="clearfix"></div>
                </div>
                <div class="tour-picture">
                    <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                        <img src="assets/images/tours/nga.jpg" alt="" />
                    </a>
                </div>
                <div class="tour-meta">
                    <div class="meta-left">
                        <p> <i class="far fa-calendar-alt"></i> Ngày đi: 22/08/2019</p>
                        <p> <i class="far fa-clock"></i> Giờ đi: 17:30</p>
                        <p> <i class="fas fa-dollar-sign"></i> Giá: 13.320.000 đ</p>
                    </div>
                    <div class="meta-right">
                        <p> <i class="fas fa-barcode"></i> Mã tour: HAH9327-SL-O</p>
                        <p> <i class="fas fa-ticket-alt"></i> Còn lại: 2 vé</p>
                        <p> <i class="fas fa-hourglass-half"></i> Lộ trình: 6 ngày</p>
                    </div>
                </div>
                <div class="tour-button">
                    <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa</h3>
                    <h3><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</h3>
                    <a href="{@www}/tour">ĐẶT NGAY</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="the-tour">
                <h1><a href="{@www}/tour">Liên Tuyến Bắc Trung: Hà Nội - Sapa - Hạ Long - Ninh Bình - Huế - Động Thiên Đường & Phong Nha - Đà Nẵng - Bà Nà - Hội An</a></h1>
                <div class="tour-rating">
                    <span class="star js_rate-yo" data-readonly="yes" data-rate="4.6"></span>
                    <span class="number">4.6/5 (1642 đánh giá)</span>
                    <div class="clearfix"></div>
                </div>
                <div class="tour-picture">
                    <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                        <img src="assets/images/tours/nga.jpg" alt="" />
                    </a>
                </div>
                <div class="tour-meta">
                    <div class="meta-left">
                        <p> <i class="far fa-calendar-alt"></i> Ngày đi: 22/08/2019</p>
                        <p> <i class="far fa-clock"></i> Giờ đi: 17:30</p>
                        <p> <i class="fas fa-dollar-sign"></i> Giá: 13.320.000 đ</p>
                    </div>
                    <div class="meta-right">
                        <p> <i class="fas fa-barcode"></i> Mã tour: HAH9327-SL-O</p>
                        <p> <i class="fas fa-ticket-alt"></i> Còn lại: 2 vé</p>
                        <p> <i class="fas fa-hourglass-half"></i> Lộ trình: 6 ngày</p>
                    </div>
                </div>
                <div class="tour-button">
                    <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa</h3>
                    <h3><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</h3>
                    <a href="{@www}/tour">ĐẶT NGAY</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="the-tour">
                <h1><a href="{@www}/tour">Liên Tuyến Bắc Trung: Hà Nội - Sapa - Hạ Long - Ninh Bình - Huế - Động Thiên Đường & Phong Nha - Đà Nẵng - Bà Nà - Hội An</a></h1>
                <div class="tour-rating">
                    <span class="star js_rate-yo" data-readonly="yes" data-rate="4.6"></span>
                    <span class="number">4.6/5 (1642 đánh giá)</span>
                    <div class="clearfix"></div>
                </div>
                <div class="tour-picture">
                    <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                        <img src="assets/images/tours/nga.jpg" alt="" />
                    </a>
                </div>
                <div class="tour-meta">
                    <div class="meta-left">
                        <p> <i class="far fa-calendar-alt"></i> Ngày đi: 22/08/2019</p>
                        <p> <i class="far fa-clock"></i> Giờ đi: 17:30</p>
                        <p> <i class="fas fa-dollar-sign"></i> Giá: 13.320.000 đ</p>
                    </div>
                    <div class="meta-right">
                        <p> <i class="fas fa-barcode"></i> Mã tour: HAH9327-SL-O</p>
                        <p> <i class="fas fa-ticket-alt"></i> Còn lại: 2 vé</p>
                        <p> <i class="fas fa-hourglass-half"></i> Lộ trình: 6 ngày</p>
                    </div>
                </div>
                <div class="tour-button">
                    <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa</h3>
                    <h3><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</h3>
                    <a href="{@www}/tour">ĐẶT NGAY</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="the-tour">
                <h1><a href="{@www}/tour">Liên Tuyến Bắc Trung: Hà Nội - Sapa - Hạ Long - Ninh Bình - Huế - Động Thiên Đường & Phong Nha - Đà Nẵng - Bà Nà - Hội An</a></h1>
                <div class="tour-rating">
                    <span class="star js_rate-yo" data-readonly="yes" data-rate="4.6"></span>
                    <span class="number">4.6/5 (1642 đánh giá)</span>
                    <div class="clearfix"></div>
                </div>
                <div class="tour-picture">
                    <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                        <img src="assets/images/tours/nga.jpg" alt="" />
                    </a>
                </div>
                <div class="tour-meta">
                    <div class="meta-left">
                        <p> <i class="far fa-calendar-alt"></i> Ngày đi: 22/08/2019</p>
                        <p> <i class="far fa-clock"></i> Giờ đi: 17:30</p>
                        <p> <i class="fas fa-dollar-sign"></i> Giá: 13.320.000 đ</p>
                    </div>
                    <div class="meta-right">
                        <p> <i class="fas fa-barcode"></i> Mã tour: HAH9327-SL-O</p>
                        <p> <i class="fas fa-ticket-alt"></i> Còn lại: 2 vé</p>
                        <p> <i class="fas fa-hourglass-half"></i> Lộ trình: 6 ngày</p>
                    </div>
                </div>
                <div class="tour-button">
                    <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa</h3>
                    <h3><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</h3>
                    <a href="{@www}/tour">ĐẶT NGAY</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="tourgroup-pager">
        <ul>
            <li><a href="#"><i class="fas fa-chevron-left"></i></a></li>
            <li><a href="#">1</a></li>
            <li class="active"><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#"><i class="fas fa-chevron-right"></i></a></li>
        </ul>
    </div>
</section>
{@script}
<script src="{@www}/vendors/js/jquery.cycle2.min.js"></script>
<script src="{@www}/vendors/js/jquery.cycle2.carousel.min.js"></script>
