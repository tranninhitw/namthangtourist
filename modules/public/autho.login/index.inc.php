<?php
$db = unserialize( TP_DB_PARAMS );
$exec = new Exec( $db['DB_HOST'], $db['DB_USERNAME'], $db['DB_PASSWORD'], $db['DB_SCHEMA_NAME'] );
$sql = new Sql();
$hasher = new Password( 8, false );

if( isset( $_POST['login'] ) )
{
    $email    = strip_tags($_POST['email']);
    $password = strip_tags($_POST['password']);

    //Remove special characters
    $email = preg_replace('/[^a-zA-Z0-9@\.]+/', '', $email);

    $data = array(
        ':user_email' => $email
    );

    $r = $exec->get($sql->get(3), $data);

    //    print_r($data);
    if (count($r) == 1)
    {
        $_SESSION['tk_user_data'] = serialize( $r[0] );
        // mã hash của pảssword
        $hash = $r[0]['user_password'];
           $isRight = $hasher -> CheckPassword( $password, $hash );
                if( $isRight ) {
                    $_SESSION['tk_client_logged'] = true;
                    $_SESSION['tk_user_client_data'] = serialize( $r[0] );
                    header("location: ". TP_REL_ROOT );
                }
                else
                    echo 'Sai thông tin đăng nhập!';
            }
            else
                echo 'Sai thông tin đăng nhập!';
    }
?>
