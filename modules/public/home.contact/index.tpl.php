{@style}
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<section class="route-contents">
    <div class="route-title">
        <h1>Liên hệ</h1>
        <p>Gửi yêu cầu và góp ý về Ban giám đốc của Nam Thắng Tourist</p>
    </div>

    <div class="contact-info">
        <p><b>CÔNG TY CP TM DV NAM THẮNG TRAVEL PRO</b></p>
        <p>
            <i class="fas fa-building"></i>
            <b>Trụ sở:</b> 12C/220 Phố Bắc Cầu, Phường Ngọc Thụy, Quận Long Biên, Hà Nội
        </p>
        <p>
            <i class="fas fa-map-marker-alt"></i>
            <b>VPGD:</b> Số 98 Phố Hồng Tiến, phường Bồ Đề, Quận Long Biên, Hà Nội
        </p>
        <p>
            <i class="fas fa-envelope"></i>
            <b>Email:</b> support@namthangtourist.com
        </p>
        <p>
            <i class="fas fa-phone"></i>
            <b>Các số điện thoại liên hệ:</b>
        </p>
        <p>
            <a href="tel:0944 062 525"><b>Tàu xe:</b> 0944 062 525</a>
            <a href="tel:0944 862 525"><b>Tour mạo hiểm:</b> 0944 862 525</a>
            <a href="tel:0944 972 525"><b>Tư vấn mua hàng:</b> 0944 972 525</a>
        </p>
    </div>
    <div class="contact-form">
        <form class="">
            <label>Liên hệ để?</label>
            <ul class="selectbox">
                <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Chọn nơi đến" />
                <div class="selectbox-options js_selectbox-options">
                    <li class="selectbox-option" data-value="1">Góp ý</li>
                    <li class="selectbox-option" data-value="2">Khiếu nại</li>
                    <li class="selectbox-option" data-value="3">Yêu cầu tư vấn</li>
                    <li class="selectbox-option" data-value="4">Mong muốn hợp tác</li>
                </div>
            </ul>
            <label>Họ và tên:</label>
            <input type="text" name="message_autho" />
            <label>Email:</label>
            <input type="text" name="message_email" />
            <label>Điện thoại:</label>
            <input type="text" name="message_mobile" />
            <label>Tiêu đề:</label>
            <input type="text" name="message_title" />
            <label>Nội dung:</label>
            <textarea name="message_content"></textarea>
            <label>Mã xác nhận:</label>
            <div class="g-recaptcha" data-secret-key="6LcV5rMUAAAAAMj_otUORqKPdM4w_u1x5pkAoQLr" data-sitekey="6LcV5rMUAAAAADbiUuISb_lp43LnCUkjF0bx7UYU"></div>
            <input type="submit" name="" value="Gửi đi" />
        </form>
    </div>
    <div class="clearfix"></div>
</section>
{@script}
