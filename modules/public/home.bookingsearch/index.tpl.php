{@style}
<section class="route-contents">
    <div class="tourgroup">
        <div class="tourgroup-sidebar">
            <div class="filter">
                <h3>Lọc kết quả</h3>
                <form>
                    <label>Chọn địa phương:</label>
                    <ul class="selectbox">
                        <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Chọn địa phương" />
                        <div class="selectbox-options js_selectbox-options">
                            <li class="selectbox-option" data-value="1">Sapa</li>
                            <li class="selectbox-option" data-value="2">Hà Khẩu</li>
                            <li class="selectbox-option" data-value="3">Hà Giang</li>
                            <li class="selectbox-option" data-value="4">Ninh Bình</li>
                            <li class="selectbox-option" data-value="5">Hà Nội</li>
                            <li class="selectbox-option" data-value="6">Phú Quốc</li>
                            <li class="selectbox-option" data-value="7">Côn Đảo</li>
                        </div>
                    </ul>
                    <label>Giá cả:</label>
                    <ul class="selectbox">
                        <input type="text" class="selectbox-selected js_selectbox-selected" readonly value="Giá cả" />
                        <div class="selectbox-options js_selectbox-options">
                            <li class="selectbox-option" data-value="1">Thường (dưới 1 triệu)</li>
                            <li class="selectbox-option" data-value="2">Cao cấp (dưới 5 triệu)</li>
                            <li class="selectbox-option" data-value="3">Hạng sang (trên 5 triệu)</li>
                        </div>
                    </ul>
                    <label>Ngày:</label>
                    <input type="text" class="filter-textbox js_datetimepicker" value="Ngày" />
                    <input type="submit" class="filter-button" value="Tìm ngay" />
                </form>
            </div>
        </div>
        <div class="tourgroup-list">
            <div class="tourgroup-description">
                <h1>Tìm khách sạn</h1>
                <p>Nam sagittis sit amet sapien ut congue. Pellentesque vel vulputate sem. Praesent id diam tristique, dictum odio id, efficitur nulla. </p>
                <div class="sharethis-inline-share-buttons"></div>
            </div>
            <div class="the-tour">
                <h1><a href="{@www}/ve">Golden Hill Ets</a></h1>
                <div class="tour-rating">
                    <span class="star js_rate-yo" data-readonly="yes" data-rate="4.6"></span>
                    <span class="number">4.6/5 (1642 đánh giá)</span>
                    <div class="clearfix"></div>
                </div>
                <div class="available">Còn 2 phòng</div>
                <div class="tour-picture">
                    <a href="{@www}/ve" title="Golden Hill Ets">
                        <img src="assets/images/hotel1.jpg" alt="" />
                    </a>
                </div>
                <div class="tour-button">
                        <a href="{@www}/ve">ĐẶT NGAY</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="the-tour">
                <h1><a href="{@www}/ve">Golden Hill Ets</a></h1>
                <div class="tour-rating">
                    <span class="star js_rate-yo" data-readonly="yes" data-rate="4.6"></span>
                    <span class="number">4.6/5 (1642 đánh giá)</span>
                    <div class="clearfix"></div>
                </div>
                <div class="available">Còn 2 phòng</div>
                <div class="tour-picture">
                    <a href="{@www}/ve" title="Golden Hill Ets">
                        <img src="assets/images/hotel1.jpg" alt="" />
                    </a>
                </div>
                <div class="tour-button">
                        <a href="{@www}/ve">ĐẶT NGAY</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="the-tour">
                <h1><a href="{@www}/ve">Golden Hill Ets</a></h1>
                <div class="tour-rating">
                    <span class="star js_rate-yo" data-readonly="yes" data-rate="4.6"></span>
                    <span class="number">4.6/5 (1642 đánh giá)</span>
                    <div class="clearfix"></div>
                </div>
                <div class="available">Còn 2 phòng</div>
                <div class="tour-picture">
                    <a href="{@www}/ve" title="Golden Hill Ets">
                        <img src="assets/images/hotel1.jpg" alt="" />
                    </a>
                </div>
                <div class="tour-button">
                        <a href="{@www}/ve">ĐẶT NGAY</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="the-tour">
                <h1><a href="{@www}/ve">Golden Hill Ets</a></h1>
                <div class="tour-rating">
                    <span class="star js_rate-yo" data-readonly="yes" data-rate="4.6"></span>
                    <span class="number">4.6/5 (1642 đánh giá)</span>
                    <div class="clearfix"></div>
                </div>
                <div class="available">Còn 2 phòng</div>
                <div class="tour-picture">
                    <a href="{@www}/ve" title="Golden Hill Ets">
                        <img src="assets/images/hotel1.jpg" alt="" />
                    </a>
                </div>
                <div class="tour-button">
                        <a href="{@www}/ve">ĐẶT NGAY</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="the-tour">
                <h1><a href="{@www}/ve">Golden Hill Ets</a></h1>
                <div class="tour-rating">
                    <span class="star js_rate-yo" data-readonly="yes" data-rate="4.6"></span>
                    <span class="number">4.6/5 (1642 đánh giá)</span>
                    <div class="clearfix"></div>
                </div>
                <div class="available">Còn 2 phòng</div>
                <div class="tour-picture">
                    <a href="{@www}/ve" title="Golden Hill Ets">
                        <img src="assets/images/hotel1.jpg" alt="" />
                    </a>
                </div>
                <div class="tour-button">
                        <a href="{@www}/ve">ĐẶT NGAY</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="the-tour">
                <h1><a href="{@www}/ve">Golden Hill Ets</a></h1>
                <div class="tour-rating">
                    <span class="star js_rate-yo" data-readonly="yes" data-rate="4.6"></span>
                    <span class="number">4.6/5 (1642 đánh giá)</span>
                    <div class="clearfix"></div>
                </div>
                <div class="available">Còn 2 phòng</div>
                <div class="tour-picture">
                    <a href="{@www}/ve" title="Golden Hill Ets">
                        <img src="assets/images/hotel1.jpg" alt="" />
                    </a>
                </div>
                <div class="tour-button">
                        <a href="{@www}/ve">ĐẶT NGAY</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="tourgroup-pager">
        <ul>
            <li><a href="#"><i class="fas fa-chevron-left"></i></a></li>
            <li><a href="#">1</a></li>
            <li class="active"><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#"><i class="fas fa-chevron-right"></i></a></li>
        </ul>
    </div>
</section>
{@script}
<script src="{@www}/vendors/js/jquery.cycle2.min.js"></script>
<script src="{@www}/vendors/js/jquery.cycle2.carousel.min.js"></script>
