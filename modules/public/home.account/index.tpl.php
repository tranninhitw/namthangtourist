{@style}
<section class="route-contents account">
    <div class="route-title">
        <h1>Tài khoản của bạn</h1>
        <p>Tra cứu và thiết lập thông tin</p>
    </div>
    <section class="account-menu">
        <ul>
            <li class="js_tab active" data-tab="0"> Thông tin cá nhân </li>
            <li class="js_tab" data-tab="1"> Lịch sử thanh toán </li>
            <li class="js_tab" data-tab="2"> Lịch sử dịch vụ </li>
            <li class="js_tab" data-tab="3"> Danh sách yêu thích </li>
            <li class="js_tab" data-tab="4"> Điểm thưởng</li>
            <li class="js_tab" data-tab="5"> Cảm nhận</li>
        </ul>
    </section>

    <section class="account-content">
        <div class="js_tab-content active" data-tab="0">
            <div class="profile">
                <h3>Thông tin cá nhân</h3>
                <form class="profile-form">
                    <label>Họ và tên:</label>
                    <input type="text" name="" placeholder="Họ và tên quý khách" />
                    <label>Ngày, tháng, năm sinh:</label>
                    <input type="text" name="" placeholder="ngày/tháng/năm" />
                    <label>Địa chỉ:</label>
                    <input type="text" name="" placeholder="Địa chỉ nơi ở" />
                    <label>Số điện thoại:</label>
                    <input type="text" name="" placeholder="Điện thoại di động" />
                    <label>Giới tính:</label>
                    <select class="" name="">
                        <option value="1">Nam</option>
                        <option value="2">Nữ</option>
                        <option value="3">Khác</option>
                    </select>
                    <input type="submit" name="" value="Lưu thông tin" />
                </form>
            </div>
            <div class="security">
                <h3>Bảo mật</h3>
                <form class="security-form">
                    <label>Địa chỉ email:</label>
                    <input type="text" name="" placeholder="Email của quý khách" />
                    <label>Đổi mật khẩu:</label>
                    <input type="password" name="" placeholder="Mật khẩu hiện tại" />
                    <input type="text" name="" placeholder="Mật khẩu mới" />
                    <input type="submit" name="" value="Lưu thông tin" />
                </form>
            </div>
        </div>
        <div class="js_tab-content" data-tab="1">
            <div class="payment">
                <h3>Lịch sử thanh toán</h3>
                <div class="table-wrapper">
                    <table class="payment-table">
                        <tr>
                            <th>STT</th>
                            <th>Mã giao dịch</th>
                            <th>Thời gian giao dịch</th>
                            <th>Nội dung giao dịch</th>
                            <th>Số tiền</th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>#1028AU28</td>
                            <td>19:20:30 10/02/2019</td>
                            <td>Thanh toán tour AHSUE8281-AH-281</td>
                            <td>12.800.000</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>#1028AU28</td>
                            <td>19:20:30 10/02/2019</td>
                            <td>Thanh toán tour AHSUE8281-AH-281</td>
                            <td>12.800.000</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>#1028AU28</td>
                            <td>19:20:30 10/02/2019</td>
                            <td>Thanh toán tour AHSUE8281-AH-281</td>
                            <td>12.800.000</td>
                        </tr>
                    </table>
                </div>
                <button class="loadmore">Xem thêm</button>
            </div>
        </div>
        <div class="js_tab-content" data-tab="2">
            <div class="service">
                <h3>Lịch sử dịch vụ</h3>
                <div class="table-wrapper">
                    <table class="service-table">
                        <tr>
                            <th>STT</th>
                            <th>Tên dịch vụ</th>
                            <th>Thời gian</th>
                            <th>Giá dịch vụ</th>
                            <th>Tích điểm</th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Tour Sài Gòn - Đà Lạt #H8293-FE-283</td>
                            <td>19:20:30 10/02/2019</td>
                            <td>12.800.000</td>
                            <td>+ 1.200</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Tour Sài Gòn - Đà Lạt #H8293-FE-283</td>
                            <td>19:20:30 10/02/2019</td>
                            <td>12.800.000</td>
                            <td>+ 1.200</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Tour Sài Gòn - Đà Lạt #H8293-FE-283</td>
                            <td>19:20:30 10/02/2019</td>
                            <td>12.800.000</td>
                            <td>+ 1.200.000 điểm</td>
                        </tr>
                    </table>
                </div>
                <button class="loadmore">Xem thêm</button>
            </div>
        </div>
        <div class="js_tab-content" data-tab="3">
            <div class="wishlist">
                <h3>Danh sách dịch vụ đã thích</h3>
                <div class="wish-item">
                    <div class="picture">
                        <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                            <img src="assets/images/tours/nga.jpg" alt="" />
                            <div class="title">
                                <p>Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
                            </div>
                        </a>
                    </div>
                    <div class="picture">
                        <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                            <img src="assets/images/tours/nga.jpg" alt="" />
                            <div class="title">
                                <p>Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
                            </div>
                        </a>
                    </div>
                    <div class="picture">
                        <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                            <img src="assets/images/tours/nga.jpg" alt="" />
                            <div class="title">
                                <p>Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
                            </div>
                        </a>
                    </div>
                </div>
                <button class="loadmore">Xem thêm</button>
            </div>
        </div>
        <div class="js_tab-content" data-tab="4">
            <div class="reward">
                <h3>Điểm thưởng</h3>
                <div class="point">
                    <h1>Điểm hiện có: <b>12.000.000</b></h1>
                    <p>Tương đương với 1.200.000 đ</p>
                </div>
                <form class="use-point">
                    <label>Tìm ngay dịch vụ để dùng số tiền này:</label>
                    <input type="text" name="" placeholder="Bạn muốn đi đâu?" />
                    <input type="submit" name="" value="Xem ngay" />
                </form>
                <div class="clearfix"></div>
                <div class="tip">
                    <p><i class="fas fa-question-circle"></i> Bạn có biết?</p>
                    <p>Điểm thưởng có thể sử dụng để khấu trừ vào những lần sử dụng dịch vụ kế tiếp</p>
                    <p>10 điểm thưởng tương đương với 1.000 đ</p>
                    <p>Quý khách được cộng điểm thưởng mỗi khi:</p>
                    <ul>
                        <li>Đặt thành công 1 tour bất kỳ: +10% giá trị tour (điểm)</li>
                        <li>Để lại bình luận ở trang xem chi tiết tour sau khi đi tour: +3% giá trị tour (điểm)</li>
                        <li>Chấm điểm tour ở trang xem chi tiết tour sau khi đi tour: +3% giá trị tour (điểm)</li>
                        <li>Viết bài cảm nhận chia sẻ chuyến đi: +6% giá trị tour (điểm)</li>
                        <li>Chia sẻ tour bằng đường dẫn của bạn và có người đặt tour qua đường dẫn đó: +3% giá trị mỗi tour (điểm)</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="js_tab-content" data-tab="5">
            <div class="write">
                <h3>Chia sẻ chuyến đi của bạn</h3>
                <form class="write-form">
                    <label>Tiêu đề:</label>
                    <input type="text" name="" placeholder="Viết khoảng 50 từ đắt giá nhất mô tả chuyến đi của bạn" />
                    <label>Nội dung:</label>
                    <textarea class="wysiwyg"></textarea>
                    <input type="submit" name="" value="Đăng ngay" />
                </form>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
</section>
<script src="{@www}/vendors/js/tinymce/tinymce.min.js"></script>
{@script}
