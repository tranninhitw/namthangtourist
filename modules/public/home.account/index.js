$(document).ready(function() {
    tinymce.init( {
        selector: '.wysiwyg',
        width: '100%',
        plugins: [
            'advlist autolink lists link image imagetools textcolor charmap preview anchor autoresize codesample',
            'insertdatetime media table contextmenu paste responsivefilemanager emoticons charmap hr'
        ],
        toolbar1: 'undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | forecolor backcolor | fontselect fontsizeselect | bullist numlist | image media emoticons',
        image_advtab: true,
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,
        menubar: false,
        external_filemanager_path: _WWW + "modules/system/filemanager/",
        filemanager_title:"File Manager" ,
        external_plugins: { "filemanager" : _WWW + "modules/system/filemanager/plugin.min.js"},
        setup: function ( ed ) {
            ed.off( 'change keyup' ).on( 'change keyup', function() {
                tinymce.triggerSave();
                $( '.wysiwyg' ).change();
            });
        }
    });


    // Tabable
    $('.js_tab').click(function() {
        var tabName = $(this).data('tab');

        // Reset
        $('.js_tab').removeClass('active');
        $('.js_tab-content').removeClass('active');

        // Set active
        $(this).addClass('active');
        $('.js_tab-content[data-tab="' + tabName + '"]').addClass('active');
    });
});
