{@style}
<div class="tour-detail-wrapper">
    <div class="breadcrumb">
        <span><a href="{@www}">Trang chủ</a><i class="fas fa-angle-double-right"></i></span>
        <span><a href="{@www}/nhom-tour">Tour sốc</a><i class="fas fa-angle-double-right"></i></span>
        <span><a href="{@www}/tour">Hà Nội - Lào Cai - Sapa - Fansipan - Tặng vé xe lửa Mường Hoa - 1 đêm k/s 4 sao</a></span>
    </div>
    <div class="tour-title">
        <h1>Hà Nội - Lào Cai - Sapa - Fansipan - Tặng vé xe lửa Mường Hoa - 1 đêm k/s 4 sao</h1>
        <p><i class="fas fa-barcode"></i>NDSGN185-085-250819VJ-V</p>
    </div>
    <div class="tour-picture">
        <div class="left">
            <div class="cycle-slideshow" data-cycle-log="false" data-cycle-fx="tileBlind" data-cycle-pager="#pager" data-cycle-pause-on-hover="true" data-cycle-speed="200">
                <img src="assets/images/tours/fansiphan.jpg" alt="" />
                <img src="assets/images/tours/nga.jpg" alt="" />
            </div>
            <div id="pager"></div>
            <div class="toolbox">
                <button class="js_love-tour" title="Thêm vào danh sách yêu thích"><i class="fas fa-heart"></i></button>
                <button class="js_download-tour" title="Tải về chương trình tour"><i class="fas fa-print"></i></button>
                <button class="js_send-tour" title="Gửi thông tin tour qua email"><i class="fas fa-envelope"></i></button>
            </div>
        </div>
        <div class="right">
            <div class="rating">
                <span class="star js_rate-yo" data-readonly="yes" data-rate="4.6"></span>
                <span class="number">4.6/5 (1642 đánh giá)</span>
                <div class="clearfix"></div>
            </div>
            <div class="counting">
                <span title="Lượt xem"><i class="far fa-eye"></i> 420</span>
                <span title="Lượt thêm vào danh sách yêu thích"><i class="far fa-heart"></i> 20</span>
                <span title="Lượt bình luận"><i class="far fa-comment"></i> 0</span>
            </div>
            <div class="social sharethis-inline-share-buttons"></div>
            <div class="info">
                <ul>
                    <li><i class="far fa-calendar-alt"></i> Ngày đi: 25/08/2019</li>
                    <li><i class="far fa-clock"></i> Giờ đi: 08:00 AM</li>
                    <li><i class="fas fa-hourglass-half"></i> Lộ trình: 6 ngày</li>
                    <li><i class="fas fa-ticket-alt"></i> Còn lại: 0 vé</li>
                </ul>
            </div>
            <div class="price">
                <span class="now-price">7.800.000 đ</span>
                <span class="old-price">8.800.000 đ</span>
            </div>
            <div class="book">
                <a href="{@www}/nhom-tour" class="lack-ticket">Hết vé</a>
                <a href="{@www}/thanh-toan" class="have-ticket">Đặt ngay</a>
            </div>
        </div>
    </div>
    <div class="tour-main-wrapper">
        <div class="tour-content">
        <div class="tabs">
            <ul>
                <li data-tab="program" class="js_tab active"><i class="fas fa-hourglass-half"></i> Chương trình tour</li>
                <li data-tab="detail" class="js_tab"><i class="fas fa-list-ul"></i> Chi tiết tour</li>
                <li data-tab="service" class="js_tab"><i class="fas fa-hot-tub"></i> Dịch vụ đi kèm</li>
                <li data-tab="warning" class="js_tab"><i class="fas fa-exclamation-triangle"></i> Lưu ý</li>
                <li data-tab="feedback" class="js_tab"><i class="fas fa-comments"></i> Ý kiến khách hàng</li>
            </ul>
        </div>
        <div class="content">
            <div class="program js_tab-content active" data-tab="program">
                <div class="compact">
                    <h3>Điểm nhấn:</h3>
                    <p>Chinh phục đỉnh Fanxipan nóc nhà Đông Dương hùng vỹ</p>
                    <p>Hà Nội nên thơ với Hồ Gươm yên bình và phố cổ cổ kính</p>
                    <p>Vịnh Hạ Long ngoạn mục với hệ thống hang động tuyệt đẹp </p>
                    <p>Danh thắng Tràng An kỳ vỹ với hệ thống hang động xuyên thủy</p>
                </div>
                <div class="timetable">
                    <div class="item">
                        <div class="date">
                            <span class="number">01.</span>
                            <span class="datetime">28/09/2019</span>
                        </div>
                        <div class="activity">
                            <h3 class="place"><i class="fas fa-map-marker-alt"></i> TP. HỒ CHÍ MINH - NỘI BÀI (HÀ NỘI) - LÀO CAI – SAPA (Ăn trưa, chiều)</h3>
                            <p class="content">
                                Quý khách tập trung tại sân bay Tân Sơn Nhất - ga đi trong nước. Hướng dẫn viên làm thủ tục cho đoàn đáp chuyến bay đi Hà Nội.
                                Xe Vietravel đón đoàn tại sân bay Nội Bài, khởi hành đi Sa Pa theo cung đường cao tốc hiện đại và dài nhất Việt Nam.
                                Đến Sapa, nhận phòng nghỉ ngơi.
                                Buổi chiều quý khách thăm:
                                - Bản Cát Cát đẹp như một bức tranh giữa vùng phố cổ Sapa, nơi đây thu hút du khách bởi cầu treo, thác nước, guồng nước và những mảng màu hoa mê hoặc du khách khi lạc bước đến đây.
                                - Thăm những nếp nhà của người Mông trong bản, du khách sẽ không khỏi ngỡ.
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="date">
                            <span class="number">01.</span>
                            <span class="datetime">28/09/2019</span>
                        </div>
                        <div class="activity">
                            <h3 class="place"><i class="fas fa-map-marker-alt"></i> TP. HỒ CHÍ MINH - NỘI BÀI (HÀ NỘI) - LÀO CAI – SAPA (Ăn trưa, chiều)</h3>
                            <p class="content">
                                Quý khách tập trung tại sân bay Tân Sơn Nhất - ga đi trong nước. Hướng dẫn viên làm thủ tục cho đoàn đáp chuyến bay đi Hà Nội.
                                Xe Vietravel đón đoàn tại sân bay Nội Bài, khởi hành đi Sa Pa theo cung đường cao tốc hiện đại và dài nhất Việt Nam.
                                Đến Sapa, nhận phòng nghỉ ngơi.
                                Buổi chiều quý khách thăm:
                                - Bản Cát Cát đẹp như một bức tranh giữa vùng phố cổ Sapa, nơi đây thu hút du khách bởi cầu treo, thác nước, guồng nước và những mảng màu hoa mê hoặc du khách khi lạc bước đến đây.
                                - Thăm những nếp nhà của người Mông trong bản, du khách sẽ không khỏi ngỡ.
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="date">
                            <span class="number">01.</span>
                            <span class="datetime">28/09/2019</span>
                        </div>
                        <div class="activity">
                            <h3 class="place"><i class="fas fa-map-marker-alt"></i> TP. HỒ CHÍ MINH - NỘI BÀI (HÀ NỘI) - LÀO CAI – SAPA (Ăn trưa, chiều)</h3>
                            <p class="content">
                                Quý khách tập trung tại sân bay Tân Sơn Nhất - ga đi trong nước. Hướng dẫn viên làm thủ tục cho đoàn đáp chuyến bay đi Hà Nội.
                                Xe Vietravel đón đoàn tại sân bay Nội Bài, khởi hành đi Sa Pa theo cung đường cao tốc hiện đại và dài nhất Việt Nam.
                                Đến Sapa, nhận phòng nghỉ ngơi.
                                Buổi chiều quý khách thăm:
                                - Bản Cát Cát đẹp như một bức tranh giữa vùng phố cổ Sapa, nơi đây thu hút du khách bởi cầu treo, thác nước, guồng nước và những mảng màu hoa mê hoặc du khách khi lạc bước đến đây.
                                - Thăm những nếp nhà của người Mông trong bản, du khách sẽ không khỏi ngỡ.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="places">
                    <h3 class="title"><i class="fas fa-map-marked-alt"></i> NƠI ĐẾN</h3>
                    <div class="place-wrapper">
                        <div class="place">
                            <a href="{@www}/nhom-tour">
                                <img class="picture" src="assets/images/tours/fansiphan.jpg" alt="" />
                                <span class="name">Sapa</span>
                            </a>
                        </div>
                        <div class="place">
                            <a href="{@www}/nhom-tour">
                                <img class="picture" src="assets/images/tours/fansiphan.jpg" alt="" />
                                <span class="name">Sapa</span>
                            </a>
                        </div>
                        <div class="place">
                            <a href="{@www}/nhom-tour">
                                <img class="picture" src="assets/images/tours/fansiphan.jpg" alt="" />
                                <span class="name">Sapa</span>
                            </a>
                        </div>
                        <div class="place">
                            <a href="{@www}/nhom-tour">
                                <img class="picture" src="assets/images/tours/fansiphan.jpg" alt="" />
                                <span class="name">Sapa</span>
                            </a>
                        </div>
                        <div class="place">
                            <a href="{@www}/nhom-tour">
                                <img class="picture" src="assets/images/tours/fansiphan.jpg" alt="" />
                                <span class="name">Sapa</span>
                            </a>
                        </div>
                        <div class="place">
                            <a href="{@www}/nhom-tour">
                                <img class="picture" src="assets/images/tours/fansiphan.jpg" alt="" />
                                <span class="name">Sapa</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="detail js_tab-content" data-tab="detail">
                <div class="item">
                    <h3 class="title"><i class="fas fa-plane"></i> PHƯƠNG TIỆN</h3>
                    <div class="wrapper">
                        <p><b>Phương tiện:</b> Máy bay VietJet</p>
                        <p>
                            <b>Ngày đi:</b>
                            <span>Xuất phát: 09:20 28/09/2019</span>
                            <span>Đến nơi: 22:20 28/09/2019</span>
                        </p>
                        <p>
                            <b>Ngày về:</b>
                            <span>Xuất phát: 09:20 28/09/2019</span>
                            <span>Đến nơi: 22:20 28/09/2019</span>
                        </p>
                    </div>
                </div>
                <div class="item">
                    <h3 class="title"><i class="fas fa-hotel"></i> KHÁCH SẠN</h3>
                    <div class="wrapper">
                        <div>
                            <h3>Ngày 1 đến ngày 3:</h3>
                            <p>KHÁCH SẠN GOLDEN HILL</p>
                            <p>Địa chỉ: 129 Cộng Hòa - Hồ Chí Minh</p>
                            <p>Điện thoại: 0896. 665. 258</p>
                        </div>
                        <div>
                            <h3>Ngày 3 đến ngày 5:</h3>
                            <p>KHÁCH SẠN GOLDEN HILL</p>
                            <p>Địa chỉ: 129 Cộng Hòa - Hồ Chí Minh</p>
                            <p>Điện thoại: 0896. 665. 258</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <h3 class="title"><i class="fas fa-hiking"></i> HƯỚNG DẪN VIÊN</h3>
                    <div class="wrapper">
                        <div>
                            <h3>Ngày 1 đến ngày 3:</h3>
                            <p>NGUYỄN VĂN A</p>
                            <p>Địa chỉ: 129 Cộng Hòa - Hồ Chí Minh</p>
                            <p>Điện thoại: 0896. 665. 258</p>
                        </div>
                        <div>
                            <h3>Ngày 3 đến ngày 5:</h3>
                            <p>NGUYỄN VĂN B</p>
                            <p>Địa chỉ: 129 Cộng Hòa - Hồ Chí Minh</p>
                            <p>Điện thoại: 0896. 665. 258</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <h3 class="title"><i class="fas fa-street-view"></i> TẬP TRUNG</h3>
                    <div class="wrapper">
                        <p>Tập trung lúc 05:00 sáng tại sân bay</p>
                    </div>
                </div>
                <div class="item">
                    <h3 class="title"><i class="fas fa-hand-holding-usd"></i> GIÁ TOUR</h3>
                    <div class="wrapper">
                        <div>
                            <h3>Giá tour:</h3>
                            <p>Người lớn (Từ 12 tuổi trở lên): 9.800.000 đ</p>
                            <p>Trẻ em (Từ 5 tuổi đến dưới 12 tuổi): 6.800.000 đ</p>
                            <p>Trẻ nhỏ (Từ 2 tuổi đến dưới 5 tuổi): 3.800.000 đ</p>
                            <p>Em bé (Dưới 2 tuổi): 300.000 đ</p>
                            <p>Phụ thu phòng đơn: 1.000.000 đ</p>
                        </div>
                        <div>
                            <h3>Land tour:</h3>
                            <p>Người lớn (Từ 12 tuổi trở lên): 9.800.000 đ</p>
                            <p>Trẻ em (Từ 5 tuổi đến dưới 12 tuổi): 6.800.000 đ</p>
                            <p>Trẻ nhỏ (Từ 2 tuổi đến dưới 5 tuổi): 3.800.000 đ</p>
                            <p>Em bé (Dưới 2 tuổi): 300.000 đ</p>
                            <p>Phụ thu phòng đơn: 1.000.000 đ</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="service js_tab-content" data-tab="service">
                <ul>
                    <li><i class="fas fa-utensils"></i> Bữa ăn theo chương trình</li>
                    <li><i class="fas fa-shield-alt"></i> Bảo hiểm</li>
                    <li><i class="fas fa-hiking"></i> Hướng dẫn viên</li>
                    <li><i class="fas fa-ticket-alt"></i> Vé tham quan</li>
                    <li><i class="fas fa-bus-alt"></i> Vận chuyển</li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="warning js_tab-content" data-tab="warning">
                Noi dung o day
            </div>
            <div class="feedback js_tab-content" data-tab="feedback">
                <h3>Bình luận bên dưới. Hoặc viết bài chia sẻ chuyến đi của bạn với mọi người.</h3>
                <div class="feedback-button">
                    <a href="{@www}/tai-khoan" class="write-feedback-btn"><i class="fas fa-edit"></i> Viết bài chia sẻ</a>
                </div>
                <div class="comment-now">
                    <div class="alert">
                        <p class="logged-required-alert">Vui lòng <a href="{@www}/dang-nhap">đăng nhập</a> để viết bình luận và chấm điểm</p>
                        <p class="booked-required-alert">Quý khách chưa tham gia tour này nên không thể chấm điểm chuyến đi</p>
                    </div>
                    <div class="rating-box">
                        <span>Chấm điểm chuyến đi:</span>
                        <span class="star js_rate-yo" data-readonly="no" data-rate="4.6"></span>
                    </div>
                    <form class="cmt-form">
                        <textarea placeholder="Bạn đang nghĩ gì?"></textarea>
                        <input type="submit" name="" value="Bình luận" />
                        <div class="clearfix"></div>
                    </form>
                    <div class="cmt-list">
                        <div class="the-cmt">
                            <div class="avatar">
                                <img src="assets/images/avatar.jpeg" alt="" />
                            </div>
                            <div class="content">
                                <p>
                                    <a href="{@www}/tai-khoan">Đỗ Long</a>
                                    <span>20:38 20/09/2019</span>
                                </p>
                                <p>Maecenas pretium sollicitudin ligula et consectetur. Nullam tincidunt volutpat orci, eget rutrum libero dignissim sit amet. Pellentesque consequat mattis sem</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="the-cmt">
                            <div class="avatar">
                                <img src="assets/images/avatar.jpeg" alt="" />
                            </div>
                            <div class="content">
                                <p>
                                    <a href="{@www}/tai-khoan">Đỗ Long</a>
                                    <span>20:38 20/09/2019</span>
                                </p>
                                <p>Maecenas pretium sollicitudin ligula et consectetur. Nullam tincidunt volutpat orci, eget rutrum libero dignissim sit amet. Pellentesque consequat mattis sem</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="the-cmt">
                            <div class="avatar">
                                <img src="assets/images/avatar.jpeg" alt="" />
                            </div>
                            <div class="content">
                                <p>
                                    <a href="{@www}/tai-khoan">Đỗ Long</a>
                                    <span>20:38 20/09/2019</span>
                                </p>
                                <p>Maecenas pretium sollicitudin ligula et consectetur. Nullam tincidunt volutpat orci, eget rutrum libero dignissim sit amet. Pellentesque consequat mattis sem</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="the-cmt">
                            <div class="avatar">
                                <img src="assets/images/avatar.jpeg" alt="" />
                            </div>
                            <div class="content">
                                <p>
                                    <a href="{@www}/tai-khoan">Đỗ Long</a>
                                    <span>20:38 20/09/2019</span>
                                </p>
                                <p>Maecenas pretium sollicitudin ligula et consectetur. Nullam tincidunt volutpat orci, eget rutrum libero dignissim sit amet. Pellentesque consequat mattis sem</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="related-tours">
            <h3>TOUR TƯƠNG TỰ</h3>
            <p>Có 3 tour tương tự</p>
            <div class="cycle-slideshow" data-cycle-slides="> div" data-cycle-log="false" data-cycle-fx="tileBlind" data-cycle-pager="#pager2" data-cycle-pause-on-hover="true" data-cycle-speed="200">
                <div class="tour">
                    <div class="tour-picture2">
                        <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                            <img src="assets/images/tours/nga.jpg" alt="" />
                            <div class="tour-date">
                                <p> <i class="far fa-calendar-alt"></i> Lộ trình 6 ngày</p>
                                <p> <i class="fas fa-ticket-alt"></i> Còn 2 vé</p>
                            </div>
                        </a>
                    </div>
                    <div class="tour-meta">
                        <p>Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
                        <span>9,290,000 đ</span>
                        <span>8,290,000 đ</span>
                    </div>
                </div>
                <div class="tour">
                    <div class="tour-picture2">
                        <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                            <img src="assets/images/tours/fansiphan.jpg" alt="" />
                            <div class="tour-date">
                                <p> <i class="far fa-calendar-alt"></i> Lộ trình 6 ngày</p>
                                <p> <i class="fas fa-ticket-alt"></i> Còn 2 vé</p>
                            </div>
                        </a>
                    </div>
                    <div class="tour-meta">
                        <p>Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
                        <span>10,290,000 đ</span>
                        <span>7,290,000 đ</span>
                    </div>
                </div>
            </div>
            <div id="pager2"></div>
        </div>
    </div>
</div>
{@script}
<script src="{@www}/vendors/js/jquery.cycle2.min.js"></script>
<script src="{@www}/vendors/js/jquery.cycle2.carousel.min.js"></script>
