{@style}
<div class="tour-detail-wrapper">
    <div class="breadcrumb">
        <span><a href="{@www}">Trang chủ</a><i class="fas fa-angle-double-right"></i></span>
        <span><a href="{@www}/tim-ve">Khách sạn</a><i class="fas fa-angle-double-right"></i></span>
        <span><a href="{@www}/tour">Super Golden Elst</a></span>
    </div>
    <div class="tour-title">
        <h1>Super Golden Elst (5 sao)</h1>
    </div>
    <div class="tour-picture">
        <div class="left">
            <div class="cycle-slideshow" data-cycle-log="false" data-cycle-fx="tileBlind" data-cycle-pager="#pager" data-cycle-pause-on-hover="true" data-cycle-speed="200">
                <img src="assets/images/hotel1.jpg" alt="" />
                <img src="assets/images/hotel2.jpg" alt="" />
            </div>
            <div id="pager"></div>
        </div>
        <div class="right">
            <div class="rating">
                <span class="star js_rate-yo" data-readonly="yes" data-rate="4.6"></span>
                <span class="number">4.6/5 (1642 đánh giá)</span>
                <div class="clearfix"></div>
            </div>
            <div class="counting">
                <span title="Lượt xem"><i class="far fa-eye"></i> 420</span>
                <span title="Lượt thêm vào danh sách yêu thích"><i class="far fa-heart"></i> 20</span>
                <span title="Lượt bình luận"><i class="far fa-comment"></i> 0</span>
            </div>
            <div class="social sharethis-inline-share-buttons"></div>
            <div class="info">
                Còn lại 2 phòng
            </div>
            <div class="price">
                <span class="now-price">2.800.000 đ</span>
                <span class="old-price">3.800.000 đ</span>
            </div>
            <div class="book">
                <a href="{@www}/nhom-tour" class="lack-ticket">Hết phòng</a>
                <a href="{@www}/thanh-toan" class="have-ticket">Đặt ngay</a>
            </div>
        </div>
    </div>
    <div class="tour-main-wrapper">
        <div class="tour-content">
            <div class="tabs">
                <ul>
                    <li data-tab="detail" class="js_tab active"><i class="fas fa-list-ul"></i> Thông tin</li>
                    <li data-tab="service" class="js_tab"><i class="fas fa-hot-tub"></i> Dịch vụ đi kèm</li>
                    <li data-tab="warning" class="js_tab"><i class="fas fa-exclamation-triangle"></i> Lưu ý</li>
                    <li data-tab="feedback" class="js_tab"><i class="fas fa-comments"></i> Ý kiến khách hàng</li>
                </ul>
            </div>
            <div class="content">
                <div class="detail js_tab-content active" data-tab="detail">
                    <div class="item">
                        <div class="wrapper">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum at sodales quam. Aliquam consectetur velit posuere lectus finibus, vel hendrerit diam finibus. Aenean imperdiet sapien non commodo semper. Donec rutrum neque eu dui congue pulvinar. Aenean ultricies lacinia mauris, a aliquam eros. Suspendisse vel leo sed sapien aliquet imperdiet ut sed purus. Donec quis nisi odio. Curabitur facilisis, nisi et ultrices elementum, metus massa tempus odio, sit amet tempus odio magna ut nunc. Mauris a auctor lorem, sed cursus leo. Quisque sit amet quam ultricies, bibendum ligula quis, lacinia nunc.</p>
                            <p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Phasellus lacus felis, sagittis non volutpat in, congue at neque. Pellentesque risus diam, lobortis et metus et, interdum vulputate neque. Donec tincidunt scelerisque diam vel gravida. Sed neque elit, convallis eget libero et, aliquet tempor tortor. Integer vulputate leo nec congue condimentum. Fusce purus purus, egestas at sapien sit amet, fringilla porttitor velit. Etiam non nulla mollis, aliquam felis eget, aliquet urna.</p>
                        </div>
                    </div>
                </div>
                <div class="service js_tab-content" data-tab="service">
                    <ul>
                        <li><i class="fas fa-swimmer"></i> Hồ bơi</li>
                        <li><i class="fas fa-star"></i> 5 sao</li>
                        <li><i class="fas fa-utensils"></i> Nhà hàng</li>
                        <li><i class="fas fa-wifi"></i> Wifi miễn phí</li>
                        <li><i class="fas fa-dumbbell"></i> Phòng tập gym</li>
                        <li><i class="fas fa-exchange-alt"></i> Đổi ngoại tệ</li>
                        <li><i class="fas fa-spa"></i> Chăm sóc sức khỏe</li>
                        <li><i class="fas fa-bed"></i> Giường nằm</li>
                        <li><i class="fas fa-wheelchair"></i> Ghế ngã</li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="warning js_tab-content" data-tab="warning">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum at sodales quam. Aliquam consectetur velit posuere lectus finibus, vel hendrerit diam finibus. Aenean imperdiet sapien non commodo semper. Donec rutrum neque eu dui congue pulvinar. Aenean ultricies lacinia mauris, a aliquam eros. Suspendisse vel leo sed sapien aliquet imperdiet ut sed purus. Donec quis nisi odio. Curabitur facilisis, nisi et ultrices elementum, metus massa tempus odio, sit amet tempus odio magna ut nunc. Mauris a auctor lorem, sed cursus leo. Quisque sit amet quam ultricies, bibendum ligula quis, lacinia nunc.

                    Interdum et malesuada fames ac ante ipsum primis in faucibus. Phasellus lacus felis, sagittis non volutpat in, congue at neque. Pellentesque risus diam, lobortis et metus et, interdum vulputate neque. Donec tincidunt scelerisque diam vel gravida. Sed neque elit, convallis eget libero et, aliquet tempor tortor. Integer vulputate leo nec congue condimentum. Fusce purus purus, egestas at sapien sit amet, fringilla porttitor velit. Etiam non nulla mollis, aliquam felis eget, aliquet urna.
                </div>
                <div class="feedback js_tab-content" data-tab="feedback">
                    <h3>Chia sẻ ý kiến với mọi người:</h3>
                    <div class="comment-now">
                        <div class="alert">
                            <p class="logged-required-alert">Vui lòng <a href="{@www}/dang-nhap">đăng nhập</a> để viết bình luận và chấm điểm</p>
                            <p class="booked-required-alert">Quý khách chưa từng sử dụng dịch vụ này nên không thể chấm điểm</p>
                        </div>
                        <div class="rating-box">
                            <span>Chấm điểm dịch vụ:</span>
                            <span class="star js_rate-yo" data-readonly="no" data-rate="4.6"></span>
                        </div>
                        <form class="cmt-form">
                            <textarea placeholder="Bạn đang nghĩ gì?"></textarea>
                            <input type="submit" name="" value="Bình luận" />
                            <div class="clearfix"></div>
                        </form>
                        <div class="cmt-list">
                            <div class="the-cmt">
                                <div class="avatar">
                                    <img src="assets/images/avatar.jpeg" alt="" />
                                </div>
                                <div class="content">
                                    <p>
                                        <a href="{@www}/tai-khoan">Đỗ Long</a>
                                        <span>20:38 20/09/2019</span>
                                    </p>
                                    <p>Maecenas pretium sollicitudin ligula et consectetur. Nullam tincidunt volutpat orci, eget rutrum libero dignissim sit amet. Pellentesque consequat mattis sem</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="the-cmt">
                                <div class="avatar">
                                    <img src="assets/images/avatar.jpeg" alt="" />
                                </div>
                                <div class="content">
                                    <p>
                                        <a href="{@www}/tai-khoan">Đỗ Long</a>
                                        <span>20:38 20/09/2019</span>
                                    </p>
                                    <p>Maecenas pretium sollicitudin ligula et consectetur. Nullam tincidunt volutpat orci, eget rutrum libero dignissim sit amet. Pellentesque consequat mattis sem</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="the-cmt">
                                <div class="avatar">
                                    <img src="assets/images/avatar.jpeg" alt="" />
                                </div>
                                <div class="content">
                                    <p>
                                        <a href="{@www}/tai-khoan">Đỗ Long</a>
                                        <span>20:38 20/09/2019</span>
                                    </p>
                                    <p>Maecenas pretium sollicitudin ligula et consectetur. Nullam tincidunt volutpat orci, eget rutrum libero dignissim sit amet. Pellentesque consequat mattis sem</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="the-cmt">
                                <div class="avatar">
                                    <img src="assets/images/avatar.jpeg" alt="" />
                                </div>
                                <div class="content">
                                    <p>
                                        <a href="{@www}/tai-khoan">Đỗ Long</a>
                                        <span>20:38 20/09/2019</span>
                                    </p>
                                    <p>Maecenas pretium sollicitudin ligula et consectetur. Nullam tincidunt volutpat orci, eget rutrum libero dignissim sit amet. Pellentesque consequat mattis sem</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{@script}
<script src="{@www}/vendors/js/jquery.cycle2.min.js"></script>
<script src="{@www}/vendors/js/jquery.cycle2.carousel.min.js"></script>
