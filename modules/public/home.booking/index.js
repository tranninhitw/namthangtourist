$(document).ready(function() {

    // Tabable
    $('.js_tab').click(function() {
        var tabName = $(this).data('tab');

        // Reset
        $('.js_tab').removeClass('active');
        $('.js_tab-content').removeClass('active');

        // Set active
        $(this).addClass('active');
        $('.js_tab-content[data-tab="' + tabName + '"]').addClass('active');

        // Scroll animate
        var beforeElWidth = 0,
            index = $(this).index();
        if(index > 0) {
            for(var i = 1; i < index + 1; i++) {
                beforeElWidth += $('.js_tab:nth-child(' + i + ')').outerWidth();
            }
        }
        $(this).parent().animate({ scrollLeft: beforeElWidth }, 'fast');
    });
});
