<?php
if (!isset($_SESSION['tk_client_logged']) || $_SESSION['tk_client_logged'] == true) {
    //$userData = unserialize( $_SESSION['tk_user_client_data']);
    $html = '
        <ul class="login-widget">
            <li>
                <i class="fas fa-sign-in-alt"></i>
                <a href="' . TP_REL_ROOT . 'dang-nhap">
                    Đăng nhập
                </a>
            </li>
        </ul>
    ';
    echo $html;
} else {
    $html = '
        <ul class="login-widget">
            <li>
                <a href="' . TP_REL_ROOT . 'tai-khoan">
                    Xin chào Tung
                </a>
                &nbsp;<i class="fas fa-user"></i>
                <span class="badge">6</span>
            </li>
        </ul>
    ';
    echo $html;
}
