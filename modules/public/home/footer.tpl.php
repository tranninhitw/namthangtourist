        <!-- FOOTER-->
        <footer>
            <div class="footer-main">
                <div class="footer-hotline">
                    <h3 class="footer-title">ĐIỆN THOẠI HỖ TRỢ</h3>
                    <div class="footer-hotline-content">
                        <div class="hotline-left">
                            <img src="assets/images/icons/hotline.svg" alt="" />
                        </div>
                        <div class="hotline-right">
                            <a href="tel:0925 525 252">0925 525 252</a>
                            <p>Phục vụ 24/7</p>
                        </div>
                    </div>
                </div>
                <div class="footer-social">
                    <h3 class="footer-title">Kết nối với chúng tôi</h3>
                    <div class="icon-social">
                        <a href="" title="Xem tin tức về Nam Thắng trên Facebook">
                            <i class="fab fa-facebook"></i>
                        </a>
                        <a href="" title="Xem tin tức về Nam Thắng trên Instagram">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="" title="Xem tin tức về Nam Thắng trên Youtube">
                            <i class="fab fa-youtube"></i>
                        </a>
                        <a href="" title="Xem tin tức về Nam Thắng trên Pinterest">
                            <i class="fab fa-pinterest"></i>
                        </a>
                    </div>
                </div>
                <div class="footer-payment">
                    <h3 class="footer-title">Chấp nhận thanh toán</h3>
                    <div class="icon-payment">
                        <img src="assets/images/logo/acb.png" alt="" />
                        <img src="assets/images/logo/bidv.png" alt="" />
                        <img src="assets/images/logo/dongabank.png" alt="" />
                        <img src="assets/images/logo/eximbank.png" alt="" />
                        <img src="assets/images/logo/sacombank.png" alt="" />
                        <img src="assets/images/logo/techcombank.png" alt="" />
                        <img src="assets/images/logo/vietcombank.png" alt="" />
                        <img src="assets/images/logo/vietinbank.png" alt="" />
                        <img src="assets/images/logo/jcb.png" alt="" />
                        <img src="assets/images/logo/mastercard.png" alt="" />
                        <img src="assets/images/logo/visa.png" alt="" />
                    </div>
                </div>
                <div class="footer-contact">
                    <h3 class="footer-title">Liên hệ</h3>
                    <p><b>CÔNG TY CP TM DV NAM THẮNG TRAVEL PRO</b></p>
                    <p>
                        <i class="fas fa-building"></i>
                        <b>Trụ sở:</b> 12C/220 Phố Bắc Cầu, Phường Ngọc Thụy, Quận Long Biên, Hà Nội
                    </p>
                    <p>
                        <i class="fas fa-map-marker-alt"></i>
                        <b>VPGD:</b> Số 98 Phố Hồng Tiến, phường Bồ Đề, Quận Long Biên, Hà Nội
                    </p>
                    <p>
                        <i class="fas fa-envelope"></i>
                        <b>Email:</b> support@namthangtourist.com
                    </p>
                    <p>
                        <i class="fas fa-phone"></i>
                        <b>Các số điện thoại liên hệ:</b>
                    </p>
                    <p>
                        <a href="tel:0944 062 525"><b>Tàu xe:</b> 0944 062 525</a>
                        <a href="tel:0944 862 525"><b>Tour mạo hiểm:</b> 0944 862 525</a>
                        <a href="tel:0944 972 525"><b>Tư vấn mua hàng:</b> 0944 972 525</a>
                    </p>
                </div>
                <div class="footer-copyright">
                    <p>Bản quyền của Nam Thắng &copy; 2019</p>
                    <p>Ghi rõ nguồn "www.namthangtourist.com.vn" &reg; khi sử dụng lại thông tin từ website này.</p>
                </div>
            </div>
        </footer>
    </div>
</div> <!-- END .body-wrapper -->
