<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mua tour du lịch ngay tại nhà bạn với Nam Thắng Tourist</title>
        <meta name="keywords" content="nam thang, nam thang tourist, tour du lich, du lich, du lich trong nuoc, du lich viet nam, du lich thai lan, du lich campuchia">
        <meta name="description" content="Nam Thắng Tourist - Nhà tổ chức du lịch chuyên nghiệp. Website namthangtourist.com tự hào là mạng bán tour du lịch trực tuyến hàng đầu tại Việt Nam.">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, width=device-width" />
        <link rel="canonical" href="http://namthangtourist.com/">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        {@base}
        <!-- STYLE CSS-->
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Montserrat:900&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="{@www}/vendors/css/fontawesome-all.min.css" />
        <link rel="stylesheet" href="assets/css/style.css" />
        <link rel="stylesheet" href="assets/css/jquery.rateyo.min.css" />
        <link rel="stylesheet" href="{@www}/vendors/css/jquery.datetimepicker.min.css" />
        <script src="{@www}/vendors/js/jquery-3.0.0.min.js"></script>
    </head>
