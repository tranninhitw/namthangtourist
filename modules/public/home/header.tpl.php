{@header}
<body>
<div class="body-wrapper">
    <!-- MENU MOBILE-->
    <div class="wrapper-mobile-nav js_wrapper-mobile-nav js_hidden" style="background-image: url('assets/images/c2a-1.jpg');">
        <button type="button" class="close-button js_close-button"><i class="fas fa-times"></i></button>
        <form class="c2a c2a-1">
            <h2>Hôm nay khuyến mãi 30% cho tour Thái Lan</h2>
            <input type="text" name="" placeholder="Nhập email..." />
            <input type="submit" name="" value="Nhận ngay mã khuyến mãi" />
        </form>
        <ul class="menu-mobile nav-links nav navbar-nav">
            <li class="slidedown js_slide-toggle">
                <a href="{@www}" class="main-menu">
                    <span class="text">Trang Chủ</span>
                </a>
            </li>
            <li class="slidedown js_slide-toggle">
                <a href="" class="main-menu">
                    <span class="text">giới thiệu <i class="fa fa-angle-down"></i></span>
                </a>

                <ul class="slidedown-menu js_slidedown-menu">
                    <li>
                        <a href="{@www}/gioi-thieu" class="link-page">Giới thiệu</a>
                    </li>
                    <li>
                        <a href="{@www}/chinh-sach-bao-mat" class="link-page">Chính sách</a>
                    </li>
                    <li>
                        <a href="{@www}/thoa-thuan-su-dung" class="link-page">Thỏa thuận</a>
                    </li>
                    <li>
                        <a href="{@www}/cau-hoi-thuong-gap" class="link-page">Hỏi đáp</a>
                    </li>
                    <li>
                        <a href="{@www}/so-do-trang-web" class="link-page">Sơ đồ trang</a>
                    </li>
                </ul>
            </li>
            <li class="slidedown js_slide-toggle">
                <a href="" class="main-menu ">
                    <span class="text">Du lịch <i class="fa fa-angle-down"></i></span>
                </a>
                <ul class="slidedown-menu js_slidedown-menu">
                    <li>
                        <a href="{@www}/nhom-tour" class="link-page">Tour riêng</a>
                    </li>
                    <li>
                        <a href="{@www}/nhom-tour" class="link-page">Tour ghép</a>
                    </li>
                    <li>
                        <a href="{@www}/nhom-tour" class="link-page">Tour sốc</a>
                    </li>
                    <li>
                        <a href="{@www}/nhom-tour" class="link-page">Combo</a>
                    </li>
                </ul>
            </li>
            <li class="slidedown js_slide-toggle">
                <a href="" class="main-menu">
                    <span class="text">Dịch vụ <i class="fa fa-angle-down"></i></span>
                </a>
                <ul class="slidedown-menu js_slidedown-menu">
                    <li>
                        <a href="{@www}/nhom-tour" class="link-page">Tàu lửa</a>
                    </li>
                    <li>
                        <a href="{@www}/nhom-tour" class="link-page">Xe buýt</a>
                    </li>
                    <li>
                        <a href="{@www}/nhom-tour" class="link-page">Xe riêng</a>
                    </li>
                    <li>
                        <a href="{@www}/nhom-tour" class="link-page">Máy bay</a>
                    </li>
                    <li>
                        <a href="{@www}/nhom-tour" class="link-page">Khách sạn</a>
                    </li>
                </ul>
            </li>
            <li class="slidedown js_slide-toggle">
                <a href="" class="main-menu">
                    <span class="text">Tin tức <i class="fa fa-angle-down"></i></span>
                </a>
                <ul class="slidedown-menu js_slidedown-menu">
                    <li>
                        <a href="{@www}/danh-muc" class="link-page">Khuyến mãi</a>
                    </li>
                    <li>
                        <a href="{@www}/danh-muc" class="link-page">Kinh nghiệm du lịch</a>
                    </li>
                    <li>
                        <a href="{@www}/danh-muc" class="link-page">Cảm nhận khách hàng</a>
                    </li>
                </ul>
            </li>
            <li class="slidedown js_slide-toggle">
                <a href="{@www}/lien-he" class="main-menu">
                    <span class="text">Liên Hệ</span>
                </a>
            </li>
        </ul>
    </div>
    <!-- WRAPPER CONTENT-->
    <div class="wrapper-content">
        <!-- HEADER-->
        <header class="js_slider-background">
            <div class="header-topbar">
                <button class="language js_language">
                    <img src="assets/images/icons/vn.png" alt="" /> Ngôn ngữ
                </button>
                <button class="currency js_currency">
                    VND - Việt Nam Đồng
                </button>
                <div class="popup-overlay js_popup-overlay">
                    <ul class="popup popup-change-language js_popup-change-language">
                    <li>
                        <img src="assets/images/icons/vn.png" alt="" />
                        Tiếng Việt
                    </li>
                    <li>
                        <img src="assets/images/icons/en.png" alt="" />
                        English
                    </li>
                </ul>
                    <ul class="popup popup-change-currency js_popup-change-currency">
                    <li>
                        <span>USD</span>
                        -
                        US DOLLAR
                    </li>
                    <li>
                        <span>BAHT</span>
                        -
                        THAI BAHT
                    </li>
                    <li>
                        <span>AUD</span>
                        -
                        AUSTRALIAN DOLLAR
                    </li>
                    <li>
                        <span>CAD</span>
                        -
                        CANADIAN DOLLAR
                    </li>
                    <li>
                        <span>EUR</span>
                        -
                        EURO
                    </li>
                    <li>
                        <span>SGD</span>
                        -
                        SINGAPORE DOLLAR
                    </li>
                    <li>
                        <span>HKD</span>
                        -
                        HONGKONG DOLLARS
                    </li>
                    <li>
                        <span>JPY</span>
                        -
                        JAPANESE YEN
                    </li>
                    <li>
                        <span>GBP</span>
                        -
                        BRITISH POUND
                    </li>
                    <li>
                        <span>NZD</span>
                        -
                        NEW ZEALEND DOLLAR
                    </li>
                    <li>
                        <span>RUB</span>
                        -
                        RUSSIAN RUBLE
                    </li>
                    <li>
                        <span>SAR</span>
                        -
                        SAUDI RIAL
                    </li>
                    <li>
                        <span>KRW</span>
                        -
                        SOUTH KOREAN WON
                    </li>
                </ul>
                </div>
            </div>
            <div class="header-main">
                <div class="header-main-wrapper">
                    <div class="mobile-menu-toggle js_mobile-menu-toggle">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div class="navbar-header">
                        {@headers}
                    </div>
                    <div class="clearfix"></div>
                    <ul class="navigation">
                        <li class="dropdown">
                            <a href="{@www}" class="main-menu">
                                <span class="text">Trang chủ</span>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="javascript:void(0)" class="main-menu">
                                <span class="text">Giới thiệu <i class="fa fa-angle-down"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{@www}/gioi-thieu" class="link-page">Giới thiệu</a>
                                </li>
                                <li>
                                    <a href="{@www}/chinh-sach-bao-mat" class="link-page">Chính sách</a>
                                </li>
                                <li>
                                    <a href="{@www}/thoa-thuan-su-dung" class="link-page">Thỏa thuận</a>
                                </li>
                                <li>
                                    <a href="{@www}/cau-hoi-thuong-gap" class="link-page">Hỏi đáp</a>
                                </li>
                                <li>
                                    <a href="{@www}/so-do-trang-web" class="link-page">Sơ đồ trang</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="javascript:void(0)" class="main-menu">
                                <span class="text">Du Lịch <i class="fa fa-angle-down"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{@www}/nhom-tour" class="link-page">Tour riêng</a>
                                </li>
                                <li>
                                    <a href="{@www}/nhom-tour" class="link-page">Tour ghép</a>
                                </li>
                                <li>
                                    <a href="{@www}/nhom-tour" class="link-page">Tour sốc</a>
                                </li>
                                <li>
                                    <a href="{@www}/nhom-tour" class="link-page">Combo</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="javascript:void(0)" class="main-menu">
                                <span class="text">Dịch vụ <i class="fa fa-angle-down"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{@www}/nhom-tour" class="link-page">Tàu lửa</a>
                                </li>
                                <li>
                                    <a href="{@www}/nhom-tour" class="link-page">Xe buýt</a>
                                </li>
                                <li>
                                    <a href="{@www}/nhom-tour" class="link-page">Xe riêng</a>
                                </li>
                                <li>
                                    <a href="{@www}/nhom-tour" class="link-page">Máy bay</a>
                                </li>
                                <li>
                                    <a href="{@www}/nhom-tour" class="link-page">Khách sạn</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="javascript:void(0)" class="main-menu">
                                <span class="text">Tin tức <i class="fa fa-angle-down"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{@www}/danh-muc" class="link-page">Khuyến mãi</a>
                                </li>
                                <li>
                                    <a href="{@www}/danh-muc" class="link-page">Kinh nghiệm du lịch</a>
                                </li>
                                <li>
                                    <a href="{@www}/danh-muc" class="link-page">Cảm nhận khách hàng</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="{@www}/lien-he" class="main-menu">
                                <span class="text">Liên hệ</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <section class="main-banner">
                <img src="assets/images/logo/logo.png" alt="Logo Nam Thắng Tourist" />
                <h1>Nam Thắng Tourist</h1>
                <p>Chuyên tour 5 sao</p>
            </section>
        </header>
        <!-- WRAPPER-->
        <div id="wrapper-content">
            <!-- MAIN CONTENT-->
            <div class="main-content">
                {#plu_homesearch}
            </div>

            <!-- BUTTON BACK TO TOP-->
            <div id="back-top" class="js_back-top">
                <i class="fa fa-angle-double-up"></i>
            </div>
        </div>
