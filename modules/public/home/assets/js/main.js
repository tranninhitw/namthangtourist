$(document).ready(function() {
    // Khi ấn vào nút close menu di động
    $('.js_close-button').click(function() {
        $('.js_wrapper-mobile-nav').removeClass('js_showed').addClass('js_hidden');
    });

    // Đóng mở menu con của link trên di động
    $('.js_slide-toggle').click(function(e) {
        e.preventDefault();
        var clickedEl = $(this),
            slideDownMenu = clickedEl.find('.js_slidedown-menu');

        $('.js_slidedown-menu').slideUp('fast');
        $('.js_slide-toggle').find('.fa').removeClass('fa-angle-up').addClass('fa-angle-down');

        if(slideDownMenu.is(':visible')) {
            slideDownMenu.slideUp('fast');
            clickedEl.find('.fa').removeClass('fa-angle-up').addClass('fa-angle-down');
        } else {
            slideDownMenu.slideDown('fast');
            clickedEl.find('.fa').removeClass('fa-angle-down').addClass('fa-angle-up');
        }
    });

    // Mở popup khi click vào nút đổi ngôn ngữ
    $('.js_language').click(function() {
        $('.js_popup-overlay, .js_popup-change-language').addClass('js_showed');
    });

    // Mở popup khi click vào nút đổi tiền tệ
    $('.js_currency').click(function() {
        $('.js_popup-overlay, .js_popup-change-currency').addClass('js_showed');
    });

    // Mở menu mobile khi click vô cái humberger
    $('.js_mobile-menu-toggle').click(function() {
        $('.js_wrapper-mobile-nav').removeClass('js_hidden').addClass('js_showed');
    });

    // Sliders trang chủ
    var currentBg = 0;
    var bg = [
        'main-banner-bg.jpg',
        'main-banner-bg-2.jpg',
        'main-banner-bg-3.jpg'
    ];
    var urls = '';
    var sizes = [];
    for(var i = 0; i < bg.length; i++) {
        urls += 'url("assets/images/' + bg[i] + '"),';
        sizes[i] = '0';
    }
    urls = urls.substring(0, urls.length - 1);
    $('.js_slider-background').css('background-image', urls);
    window.setInterval(function() {
        var newSizes = sizes.slice(0); // Copy from old array
        newSizes[currentBg] = 'cover';
        newSizes = newSizes.join(',');
        $('.js_slider-background').css('background-size', newSizes);
        if(currentBg < bg.length - 1) {
            currentBg++;
        } else {
            currentBg = 0;
        }
    }, 5000);

    // Show nút Back Top khi cuộn trang xuống hơn 1/3 màn hình
    $('body').scroll(function() {
        var fromTop = $('body').scrollTop();
        var viewHeight = $('body').height();
        if(fromTop > viewHeight / 3) {
            $('.js_back-top').addClass('showed');
        } else {
            $('.js_back-top').removeClass('showed');
        }
    });

    // Cuộn lên đầu trang khi ấn vào nút Backtop
    $('.js_back-top').click(function() {
        $('html,body').animate({ scrollTop: 0 }, 'fast');
    });

    /**
     * Selectbox
    */
    $('.js_selectbox-selected').click(function() {
        var selectOpts = $(this).next();
        if(selectOpts.hasClass('opened')) {
            $(this).removeClass('opened');
            selectOpts.removeClass('opened');
        } else {
            $('.js_selectbox-options').removeClass('opened');
            $('.js_selectbox-selected').removeClass('opened');
            $(this).addClass('opened');
            selectOpts.addClass('opened');
        }
    });

    $('.js_selectbox-options li').click(function() {
        var selectedTxt = $(this).parent().prev();
        selectedTxt.val($(this).html());
        selectedTxt.attr('data-value', $(this).data('value'));
        $('.js_selectbox-selected').removeClass('opened');
        $(this).parent().removeClass('opened');
    });

    /**
     * datetimepicker
    */
    $('.js_datetimepicker').focus(function(e) {
        e.preventDefault();
        $(this).blur();
    });
    jQuery.datetimepicker.setLocale('vi');
    $('.js_datetimepicker').datetimepicker({
        i18n:{
            vi:{
                months:[
                    'Tháng 1','Tháng 2','Tháng 3','Tháng 4',
                    'Tháng 5','Tháng 6','Tháng 7','Tháng 8',
                    'Tháng 9','Tháng 10','Tháng 11','Tháng 12',
                ],
                dayOfWeek:[
                    "CN", "T2", "T3", "T4",
                    "T5", "T6", "T7",
                ]
            }
        },
        timepicker:false,
        format:'d.m.Y'
    });

    /**
     * Rating
    */
    $(".js_rate-yo").each(function() {
        var rate = parseFloat($(this).data('rate')),
            width = '',
            readonly = $(this).data('readonly') == 'yes' ? true : false;
        if (screen.width>=1024) {
            width = '16px';
        } else {
            width = '20px';
        }
        $(this).rateYo({
            rating: rate,
            halfStar: true,
            starWidth: width,
            readOnly: readonly,
            spacing: "2px"
        });
    });
});
