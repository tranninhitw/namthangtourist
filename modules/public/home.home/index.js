$(document).ready(function(){

    // Đồng hồ điếm ngược tour giờ chót
    $.each($('.js_countdown-timer'), function() {
        var endTime = $(this).data('end');
        $(this).countdown(endTime, function(event) {
            $(this).text(
                event.strftime('%D ngày %H:%M:%S')
            );
        });
    });

    // Carousel
    var i = 1;
    window.setInterval(function() {
        var px = i * $('.js_carousel-el').width();
        $('.js_carousel-view').css('transform', 'translateX(-' + px + 'px)');
        if(i < 2) {
            i++;
        } else {
            i = 0;
        }
    }, 5000);

    // Hiện Main popup khi vào trang
    $('.js_main-popup').addClass('showed');

    // Ẩn Main Popup khi click nút close
    $('.js_main-popup-close').click(function() {
        $('.js_main-popup').removeClass('showed');
    });
});
