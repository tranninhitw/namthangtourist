{@style}
<!-- Tour giờ chót -->
<section class="tour-group">
    <div class="group-title">
        <h1>Giá tốt giờ "G"</h1>
        <p>Tour sắp khởi hành giảm giá mạnh cho các vé cuối</p>
    </div>

    <div class="tour">
        <div class="tour-picture">
            <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                <img src="assets/images/tours/nga.jpg" alt="" />
                <span> <i class="far fa-clock"></i> Xuất phát: <span class="js_countdown-timer" data-end="2019/09/05 10:00:00"></span> </span>
                <div class="tour-date">
                    <p> <i class="far fa-calendar-alt"></i> Lộ trình 6 ngày</p>
                    <p> <i class="fas fa-ticket-alt"></i> Còn 2 vé</p>
                </div>
            </a>
        </div>
        <div class="tour-meta">
            <p>Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
            <span>9,290,000 đ</span>
            <span>8,290,000 đ</span>
        </div>
        <div class="tour-button">
            <a href="{@www}/tour">ĐẶT NGAY</a>
            <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)</h3>
            <p><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</p>
        </div>
    </div>
    <div class="tour">
        <div class="tour-picture">
            <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                <img src="assets/images/tours/nga.jpg" alt="" />
                <span> <i class="far fa-clock"></i> Xuất phát: <span class="js_countdown-timer" data-end="2019/09/05 10:00:00"></span> </span>
                <div class="tour-date">
                    <p> <i class="far fa-calendar-alt"></i> Lộ trình 6 ngày</p>
                    <p> <i class="fas fa-ticket-alt"></i> Còn 2 vé</p>
                </div>
            </a>
        </div>
        <div class="tour-meta">
            <p>Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
            <span>9,290,000 đ</span>
            <span>8,290,000 đ</span>
        </div>
        <div class="tour-button">
            <a href="{@www}/tour">ĐẶT NGAY</a>
            <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)</h3>
            <p><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</p>
        </div>
    </div>
    <div class="tour">
        <div class="tour-picture">
            <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                <img src="assets/images/tours/nga.jpg" alt="" />
                <span> <i class="far fa-clock"></i> Xuất phát: <span class="js_countdown-timer" data-end="2019/09/05 10:00:00"></span> </span>
                <div class="tour-date">
                    <p> <i class="far fa-calendar-alt"></i> Lộ trình 6 ngày</p>
                    <p> <i class="fas fa-ticket-alt"></i> Còn 2 vé</p>
                </div>
            </a>
        </div>
        <div class="tour-meta">
            <p>Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
            <span>9,290,000 đ</span>
            <span>8,290,000 đ</span>
        </div>
        <div class="tour-button">
            <a href="{@www}/tour">ĐẶT NGAY</a>
            <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)</h3>
            <p><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</p>
        </div>
    </div>
    <div class="tour">
        <div class="tour-picture">
            <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                <img src="assets/images/tours/nga.jpg" alt="" />
                <span> <i class="far fa-clock"></i> Xuất phát: <span class="js_countdown-timer" data-end="2019/09/05 10:00:00"></span> </span>
                <div class="tour-date">
                    <p> <i class="far fa-calendar-alt"></i> Lộ trình 6 ngày</p>
                    <p> <i class="fas fa-ticket-alt"></i> Còn 2 vé</p>
                </div>
            </a>
        </div>
        <div class="tour-meta">
            <p>Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
            <span>9,290,000 đ</span>
            <span>8,290,000 đ</span>
        </div>
        <div class="tour-button">
            <a href="{@www}/tour">ĐẶT NGAY</a>
            <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)</h3>
            <p><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</p>
        </div>
    </div>
    <div class="tour">
        <div class="tour-picture">
            <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                <img src="assets/images/tours/nga.jpg" alt="" />
                <span> <i class="far fa-clock"></i> Xuất phát: <span class="js_countdown-timer" data-end="2019/09/05 10:00:00"></span> </span>
                <div class="tour-date">
                    <p> <i class="far fa-calendar-alt"></i> Lộ trình 6 ngày</p>
                    <p> <i class="fas fa-ticket-alt"></i> Còn 2 vé</p>
                </div>
            </a>
        </div>
        <div class="tour-meta">
            <p>Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
            <span>9,290,000 đ</span>
            <span>8,290,000 đ</span>
        </div>
        <div class="tour-button">
            <a href="{@www}/tour">ĐẶT NGAY</a>
            <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)</h3>
            <p><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</p>
        </div>
    </div>
    <div class="tour">
        <div class="tour-picture">
            <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                <img src="assets/images/tours/nga.jpg" alt="" />
                <span> <i class="far fa-clock"></i> Xuất phát: <span class="js_countdown-timer" data-end="2019/09/05 10:00:00"></span> </span>
                <div class="tour-date">
                    <p> <i class="far fa-calendar-alt"></i> Lộ trình 6 ngày</p>
                    <p> <i class="fas fa-ticket-alt"></i> Còn 2 vé</p>
                </div>
            </a>
        </div>
        <div class="tour-meta">
            <p>Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
            <span>9,290,000 đ</span>
            <span>8,290,000 đ</span>
        </div>
        <div class="tour-button">
            <a href="{@www}/tour">ĐẶT NGAY</a>
            <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)</h3>
            <p><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<!-- Giảm giá khi thanh toán online -->
<section class="tour-group">
    <div class="group-title">
        <h1>Giá rẻ nhờ thẻ</h1>
        <p>Những tour giảm giá khi thanh toán trực tuyến trên Nam Thắng Tourist</p>
    </div>

    <div class="tour">
        <div class="tour-picture">
            <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                <img src="assets/images/tours/nga.jpg" alt="" />
                <span> <i class="fas fa-credit-card"></i> Vietcombank -15%</span>
                <div class="tour-date">
                    <p> <i class="far fa-calendar-alt"></i> Lộ trình 6 ngày</p>
                    <p> <i class="fas fa-ticket-alt"></i> Còn 2 vé</p>
                </div>
            </a>
        </div>
        <div class="tour-meta">
            <p>Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
            <span>9,290,000 đ</span>
            <span>8,290,000 đ</span>
        </div>
        <div class="tour-button">
            <a href="{@www}/tour">ĐẶT NGAY</a>
            <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)</h3>
            <p><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</p>
        </div>
    </div>
    <div class="tour">
        <div class="tour-picture">
            <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                <img src="assets/images/tours/nga.jpg" alt="" />
                <span> <i class="fas fa-credit-card"></i> Vietcombank -15%</span>
                <div class="tour-date">
                    <p> <i class="far fa-calendar-alt"></i> Lộ trình 6 ngày</p>
                    <p> <i class="fas fa-ticket-alt"></i> Còn 2 vé</p>
                </div>
            </a>
        </div>
        <div class="tour-meta">
            <p>Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
            <span>9,290,000 đ</span>
            <span>8,290,000 đ</span>
        </div>
        <div class="tour-button">
            <a href="{@www}/tour">ĐẶT NGAY</a>
            <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)</h3>
            <p><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</p>
        </div>
    </div>
    <div class="tour">
        <div class="tour-picture">
            <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                <img src="assets/images/tours/nga.jpg" alt="" />
                <span> <i class="fas fa-credit-card"></i> Vietcombank -15%</span>
                <div class="tour-date">
                    <p> <i class="far fa-calendar-alt"></i> Lộ trình 6 ngày</p>
                    <p> <i class="fas fa-ticket-alt"></i> Còn 2 vé</p>
                </div>
            </a>
        </div>
        <div class="tour-meta">
            <p>Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
            <span>9,290,000 đ</span>
            <span>8,290,000 đ</span>
        </div>
        <div class="tour-button">
            <a href="{@www}/tour">ĐẶT NGAY</a>
            <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)</h3>
            <p><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</p>
        </div>
    </div>
    <div class="tour">
        <div class="tour-picture">
            <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                <img src="assets/images/tours/nga.jpg" alt="" />
                <span> <i class="fas fa-credit-card"></i> Vietcombank -15%</span>
                <div class="tour-date">
                    <p> <i class="far fa-calendar-alt"></i> Lộ trình 6 ngày</p>
                    <p> <i class="fas fa-ticket-alt"></i> Còn 2 vé</p>
                </div>
            </a>
        </div>
        <div class="tour-meta">
            <p>Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
            <span>9,290,000 đ</span>
            <span>8,290,000 đ</span>
        </div>
        <div class="tour-button">
            <a href="{@www}/tour">ĐẶT NGAY</a>
            <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)</h3>
            <p><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</p>
        </div>
    </div>
    <div class="tour">
        <div class="tour-picture">
            <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                <img src="assets/images/tours/nga.jpg" alt="" />
                <span> <i class="fas fa-credit-card"></i> Vietcombank -15%</span>
                <div class="tour-date">
                    <p> <i class="far fa-calendar-alt"></i> Lộ trình 6 ngày</p>
                    <p> <i class="fas fa-ticket-alt"></i> Còn 2 vé</p>
                </div>
            </a>
        </div>
        <div class="tour-meta">
            <p>Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
            <span>9,290,000 đ</span>
            <span>8,290,000 đ</span>
        </div>
        <div class="tour-button">
            <a href="{@www}/tour">ĐẶT NGAY</a>
            <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)</h3>
            <p><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</p>
        </div>
    </div>
    <div class="tour">
        <div class="tour-picture">
            <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                <img src="assets/images/tours/nga.jpg" alt="" />
                <span> <i class="fas fa-credit-card"></i> Vietcombank -15%</span>
                <div class="tour-date">
                    <p> <i class="far fa-calendar-alt"></i> Lộ trình 6 ngày</p>
                    <p> <i class="fas fa-ticket-alt"></i> Còn 2 vé</p>
                </div>
            </a>
        </div>
        <div class="tour-meta">
            <p>Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
            <span>9,290,000 đ</span>
            <span>8,290,000 đ</span>
        </div>
        <div class="tour-button">
            <a href="{@www}/tour">ĐẶT NGAY</a>
            <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)</h3>
            <p><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</p>
        </div>
    </div>
    <div class="tour">
        <div class="tour-picture">
            <a href="{@www}/tour" title="Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm - Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)">
                <img src="assets/images/tours/nga.jpg" alt="" />
                <span> <i class="fas fa-credit-card"></i> Vietcombank -15%</span>
                <div class="tour-date">
                    <p> <i class="far fa-calendar-alt"></i> Lộ trình 6 ngày</p>
                    <p> <i class="fas fa-ticket-alt"></i> Còn 2 vé</p>
                </div>
            </a>
        </div>
        <div class="tour-meta">
            <p>Hà Nội - Tây Bắc : Đền Hùng - Nghĩa Lộ - Tú Lệ - Mù Cang Chải - Yên Bái - Sapa - Điện Biên - Sơn La - Mộc Châu - Thác Dải Yếm</p>
            <span>9,290,000 đ</span>
            <span>8,290,000 đ</span>
        </div>
        <div class="tour-button">
            <a href="{@www}/tour">ĐẶT NGAY</a>
            <h3><img src="assets/images/icons/gift.svg" alt="Quà tặng" /> Tặng Show Múa Xòe Dân Tộc Thái và Vé Xe Lửa Mường Hoa (Tour Tiết Kiệm)</h3>
            <p><img src="assets/images/icons/bought.svg" alt="Người đã đặt tour" /> Đã có <b>75</b> người đặt tour này</p>
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<!-- Điểm đến yêu thích -->
<section class="tour-group">
    <div class="group-title">
        <h1>Địa danh hot</h1>
        <p>Top 6 địa danh thu hút nhiều lượt khách nhất trên Nam Thắng Tourist</p>
    </div>

    <div class="place">
        <img src="assets/images/tours/nga.jpg" alt="" />
        <h3>Sapa</h3>
        <p>Đã có 1278 lượt khách</p>
        <a href="{@www}/nhom-tour">Xem tour đến đây</a>
    </div>
    <div class="place">
        <img src="assets/images/tours/nga.jpg" alt="" />
        <h3>Sapa</h3>
        <p>Đã có 1278 lượt khách</p>
        <a href="{@www}/nhom-tour">Xem tour đến đây</a>
    </div>
    <div class="place">
        <img src="assets/images/tours/nga.jpg" alt="" />
        <h3>Sapa</h3>
        <p>Đã có 1278 lượt khách</p>
        <a href="{@www}/nhom-tour">Xem tour đến đây</a>
    </div>
    <div class="place">
        <img src="assets/images/tours/nga.jpg" alt="" />
        <h3>Sapa</h3>
        <p>Đã có 1278 lượt khách</p>
        <a href="{@www}/nhom-tour">Xem tour đến đây</a>
    </div>
    <div class="place">
        <img src="assets/images/tours/nga.jpg" alt="" />
        <h3>Sapa</h3>
        <p>Đã có 1278 lượt khách</p>
        <a href="{@www}/nhom-tour">Xem tour đến đây</a>
    </div>
    <div class="place">
        <img src="assets/images/tours/nga.jpg" alt="" />
        <h3>Sapa</h3>
        <p>Đã có 1278 lượt khách</p>
        <a href="{@www}/nhom-tour">Xem tour đến đây</a>
    </div>
    <div class="clearfix"></div>
</section>

<!-- Vì sao chọn Nam Thắng -->
<section class="why-choose-us">
    <div class="title">
        <h1>Đã có 3500+</h1>
        <p>Du khách chọn Nam Thắng Tourist</p>
    </div>
    <div class="reason-wrapper">
        <div class="reason">
            <span><i class="fas fa-credit-card"></i></span>
            <h3>THANH TOÁN</h3>
            <p>An toàn và linh hoạt</p>
            <p>Liên kết với các tổ chức tài chính</p>
        </div>
        <div class="reason">
            <span><i class="fas fa-hand-holding-usd"></i></span>
            <h3>GIÁ CẢ</h3>
            <p>Luôn có mức giá tốt nhất</p>
            <p>Bảo đảm giá tốt</p>
        </div>
        <div class="reason">
            <span><i class="fas fa-plane-departure"></i></span>
            <h3>SẢN PHẨM</h3>
            <p>Đa dạng, chất lượng</p>
            <p>Đạt chất lượng tốt nhất</p>
        </div>
        <div class="reason">
            <span><i class="fas fa-umbrella-beach"></i></span>
            <h3>ĐẶT TOUR</h3>
            <p>Dễ dàng và nhanh chóng</p>
            <p>Đặt tour chỉ với 3 bước</p>
        </div>
        <div class="reason">
            <span><i class="fas fa-headset"></i></span>
            <h3>HỖ TRỢ</h3>
            <p>24/7/365</p>
            <p>Hotline và hỗ trợ trực tuyến</p>
        </div>
    </div>
</section>

<!-- Ý kiến khách hàng -->
<section class="customer-words">
    <div class="title">
        <h1>Đã có 700+</h1>
        <p>Du khách để lại cảm nhận về chuyến đi</p>
    </div>
    <div class="carousel words">
        <div class="carousel-view js_carousel-view">
            <div class="carousel-el js_carousel-el word">
                <div class="avatar">
                    <img src="assets/images/avatar.jpeg" alt="" />
                    <h3>Uyên Đỗ</h3>
                    <p><a href="{@www}/tour">Đã hoàn tất chuyến đi ngày 20/12/2018</a></p>
                </div>
                <div class="feedback">
                    <h3>Chuyến đi thật đáng nhớ</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam est odio, interdum vitae dolor vitae, tempus posuere turpis. In at elit quis odio posuere molestie. Nulla tempus mi eget felis vehicula, ut placerat lectus molestie. Proin bibendum vel ipsum at varius. Vivamus vitae pharetra ex. Sed dui tellus, gravida id varius nec, sagittis sed est. Curabitur nisi ligula, ullamcorper vel quam ac, tincidunt posuere leo. Sed eget sollicitudin risus.</p>
                    <a href="{@www}/bai-viet">Xem bài viết</a>
                </div>
            </div>
            <div class="carousel-el js_carousel-el word">
                <div class="avatar">
                    <img src="assets/images/avatar.jpeg" alt="" />
                    <h3>Uyên Đỗ</h3>
                    <p><a href="{@www}/tour">Đã hoàn tất chuyến đi ngày 20/12/2018</a></p>
                </div>
                <div class="feedback">
                    <h3>Chuyến đi thật đáng nhớ</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam est odio, interdum vitae dolor vitae, tempus posuere turpis. In at elit quis odio posuere molestie. Nulla tempus mi eget felis vehicula, ut placerat lectus molestie. Proin bibendum vel ipsum at varius. Vivamus vitae pharetra ex. Sed dui tellus, gravida id varius nec, sagittis sed est. Curabitur nisi ligula, ullamcorper vel quam ac, tincidunt posuere leo. Sed eget sollicitudin risus.</p>
                    <a href="{@www}/bai-viet">Xem bài viết</a>
                </div>
            </div>
            <div class="carousel-el js_carousel-el word">
                <div class="avatar">
                    <img src="assets/images/avatar.jpeg" alt="" />
                    <h3>Uyên Đỗ</h3>
                    <p><a href="{@www}/tour">Đã hoàn tất chuyến đi ngày 20/12/2018</a></p>
                </div>
                <div class="feedback">
                    <h3>Chuyến đi thật đáng nhớ</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam est odio, interdum vitae dolor vitae, tempus posuere turpis. In at elit quis odio posuere molestie. Nulla tempus mi eget felis vehicula, ut placerat lectus molestie. Proin bibendum vel ipsum at varius. Vivamus vitae pharetra ex. Sed dui tellus, gravida id varius nec, sagittis sed est. Curabitur nisi ligula, ullamcorper vel quam ac, tincidunt posuere leo. Sed eget sollicitudin risus.</p>
                    <a href="{@www}/bai-viet">Xem bài viết</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Nhận thư khuyến mại -->
<section class="c2a c2a-2">
    <div class="video">
        <video src="assets/images/c2a-2.webm" playsinline autoplay muted loop>
            <source src="assets/images/c2a-2.mp4" type="video/mp4" />
            <source src="assets/images/c2a-2.webm" type="video/webm" />
        </video>
    </div>
    <form>
        <h2>Hôm nay khuyến mãi 30% cho tour Thái Lan</h2>
        <div class="">
            <input type="text" name="" placeholder="Nhập email..." />
            <input type="submit" name="" value="Nhận ngay mã khuyến mãi" />
        </div>
    </form>
</section>

<!-- Homepage main popup -->
<section class="homepage-main-popup overlay js_main-popup">
    <div class="content c2a c2a-3">
        <button class="close-btn js_main-popup-close"><i class="fas fa-times"></i></button>
        <a href="{@www}/tour">
            <img src="assets/images/main-popup.jpg" alt="" />
            <div class="caption">
                <h3>Tour Hạ Long 2 ngày 3 đêm chỉ còn 4.560.000</h3>
                <button>Xem thêm</button>
            </div>
        </a>
    </div>
</section>

{@script}
