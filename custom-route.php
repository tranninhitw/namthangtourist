<?php
	/**
	 * TAKA+ v1.0
	 * @package takaplus
	 * @date 12.1.2016
	 * @version 0.0.1
	 * @website namthangtourist.com
	 *
	 * THÊM CÁC ROUTE CỦA BẠN BÊN DƯỚI
	 *
	*/

    // Homepage
    $route -> add( '', 'public/home.home/index', 'public/home' );
    $route -> add( 'cau-hoi-thuong-gap', 'public/home.faq/index', 'public/home' );
    $route -> add( 'so-do-trang-web', 'public/home.sitemap/index', 'public/home' );
    $route -> add( 'nhom-tour', 'public/home.tourgroup/index', 'public/home' );
    $route -> add( 'tour', 'public/home.tour/index', 'public/home' );
    $route -> add( 've', 'public/home.booking/index', 'public/home' );
    $route -> add( 'tim-ve', 'public/home.bookingsearch/index', 'public/home' );
    $route -> add( 'thanh-toan', 'public/home.checkout/index', 'public/home' );
    $route -> add( 'danh-muc', 'public/home.postgroup/index', 'public/home' );
    $route -> add( 'bai-viet', 'public/home.post/index', 'public/home' );
    $route -> add( 'lien-he', 'public/home.contact/index', 'public/home' );
    $route -> add( 'tai-khoan', 'public/home.account/index', 'public/home' );
    $route -> add( 'dang-nhap', 'public/autho.login/index', 'public/autho' );
    $route -> add( 'dang-ky', 'public/autho.register/index', 'public/autho' );
    $route -> add( 'phuc-hoi-mat-khau', 'public/autho.forget/index', 'public/autho' );
    $route -> add_dynamic('SELECT page_seo_url FROM tp_pages', 'public/home.page/index', 'public/home');




	/**
	 * Kết thúc phần custom route
	 *
	 * CODING OR DOING AN EXCEPTION ...
	*/
?>
