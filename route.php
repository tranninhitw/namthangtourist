<?php 
	/**
	 * TAKA+ v1.0
	 * @package takaplus
	 * @date 12.1.2016
	 * @version 0.0.1
	 * @website taka.com.vn
	 *
	 * FILE CỦA HỆ THỐNG
	 * TUYỆT ĐỐI KHÔNG CHỈNH SỬA
	 * 
	*/
	
	$route = new Route();
	
	/**
	 * Load các custom route
	*/
	require_once( 'custom-route.php' );
	
	/**
	 * Route of autho
	*/
	$route -> add( 'nguoidung/dangnhap', 'system/autho.login/index', 'system/autho' ); //Login page
	$route -> add( 'nguoidung/quenmatkhau', 'system/autho.resetpass/index', 'system/autho' );
	$route -> add( 'nguoidung/doimatkhau', 'system/autho.changepass/index', 'system/autho' );
	$route -> add( 'admin/dangxuat', 'system/autho.login/logout', 'system/autho' );
	
	/**
	 * Route of cpanel
	*/
	//Overview
	$route -> add( 'admin', 'system/cpanel.overview/index', 'system/cpanel' );
	
	//Product
	$route -> add( 'admin/sanpham/danhmuc', 'system/cpanel.product.category/index', 'system/cpanel' );
	$route -> add( 'admin/sanpham/sanpham', 'system/cpanel.product.product/index', 'system/cpanel' );
	
	//Sale
	$route -> add( 'admin/banhang/dondathang', 'system/cpanel.sale.order/index', 'system/cpanel' );
	$route -> add( 'admin/banhang/dontrahang', 'system/cpanel.sale.reorder/index', 'system/cpanel' );
	$route -> add( 'admin/banhang/voucher', 'system/cpanel.sale.voucher/index', 'system/cpanel' );
	$route -> add( 'admin/banhang/chietkhau', 'system/cpanel.sale.commission/index', 'system/cpanel' );
	$route -> add( 'admin/banhang/dondathang/chitiet', 'system/cpanel.sale.order/detail', 'system/cpanel' );
	$route -> add( 'admin/banhang/dontrahang/chitiet', 'system/cpanel.sale.reorder/detail', 'system/cpanel' );
	
	//Customer
	$route -> add( 'admin/khachhang/khachhang', 'system/cpanel.customer.customer/index', 'system/cpanel' );
	$route -> add( 'admin/khachhang/nhomkhachhang', 'system/cpanel.customer.group/index', 'system/cpanel' );
	$route -> add( 'admin/khachhang/chitiet', 'system/cpanel.customer.customer/detail', 'system/cpanel' );
	
	//Post
	$route -> add( 'admin/noidung/baiviet', 'system/cpanel.content.post/index', 'system/cpanel' );
	$route -> add( 'admin/noidung/binhluan', 'system/cpanel.content.comment/index', 'system/cpanel' );
	$route -> add( 'admin/noidung/trang', 'system/cpanel.content.page/index', 'system/cpanel' );
	$route -> add( 'admin/noidung/cauhoi', 'system/cpanel.content.question/index', 'system/cpanel' );
	
	//Statist
	$route -> add( 'admin/thongke/muahang', 'system/cpanel.statist.sale/index', 'system/cpanel' );
	$route -> add( 'admin/thongke/tonkho', 'system/cpanel.statist.inventory/index', 'system/cpanel' );
	$route -> add( 'admin/thongke/thanhtoan', 'system/cpanel.statist.payment/index', 'system/cpanel' );
	$route -> add( 'admin/thongke/doanhso', 'system/cpanel.statist.revenue/index', 'system/cpanel' );
	$route -> add( 'admin/thongke/donhang', 'system/cpanel.statist.order/index', 'system/cpanel' );
	$route -> add( 'admin/thongke/truycap', 'system/cpanel.statist.site/index', 'system/cpanel' );
	
	//Appearance
	$route -> add( 'admin/giaodien/slider', 'system/cpanel.appearance.slider/index', 'system/cpanel' );
	$route -> add( 'admin/giaodien/slider/detail', 'system/cpanel.appearance.slider/detail', 'system/cpanel' );
	
	//System
    //	$route -> add( 'admin/caidat/caidatchung', 'system/cpanel.setting.general/index', 'system/cpanel' );
	$route -> add( 'admin/caidat/taikhoan', 'system/cpanel.setting.account/index', 'system/cpanel' );
    //	$route -> add( 'admin/caidat/diadanh', 'system/cpanel.setting.area/index', 'system/cpanel' );

    // Area
    $route -> add( 'admin/hethong/diadanh', 'system/cpanel.data.area/index', 'system/cpanel' );

    // hotel
    $route -> add( 'admin/quanly/khachsan', 'system/cpanel.data.hotel/index', 'system/cpanel' );

    // guide
    $route -> add( 'admin/quanly/huongdanvien', 'system/cpanel.data.guide/index', 'system/cpanel' );

    // service
    $route -> add( 'admin/quanly/dichvudikem', 'system/cpanel.data.service/index', 'system/cpanel' );

    // category tour
    $route -> add( 'admin/quanly/danhmuctour', 'system/cpanel.tour.category/index', 'system/cpanel' );

    // place
    $route -> add( 'admin/quanly/diadiem', 'system/cpanel.data.place/index', 'system/cpanel' );

    // ticket
    $route -> add( 'admin/quanly/ve', 'system/cpanel.data.ticket/index', 'system/cpanel' );

    // phương tiện vận chuyển
    $route -> add( 'admin/quanly/phuongtienvanchuyen', 'system/cpanel.data.transportation/index', 'system/cpanel' );

    // room
    $route -> add( 'admin/quanly/phong', 'system/cpanel.data.room/index', 'system/cpanel' );

    //Personal
	$route -> add( 'admin/canhan', 'system/cpanel.personal/index', 'system/cpanel' );

	// setting information company
    $route -> add( 'admin/cauhinh/thongtincongty', 'system/cpanel.setting.general/index', 'system/cpanel' );
    $route -> add( 'admin/cauhinh/lienhe', 'system/cpanel.setting.contact/index', 'system/cpanel' );

    //Post
    $route -> add( 'admin/noidung/tour', 'system/cpanel.content.tour/index', 'system/cpanel' );

/**
	 * Route of *.inc.php
	 */
	$route -> add( 'include/1', 'system/cpanel.product.category/save.inc.php' );
	$route -> add( 'include/2', 'system/cpanel.product.category/delete.inc.php' );
	$route -> add( 'include/3', 'system/autho.login/index.inc.php' );
	$route -> add( 'include/4', 'system/autho.resetpass/index.inc.php' );
	$route -> add( 'include/5', 'system/autho.changepass/index.inc.php' );
	$route -> add( 'include/6', 'system/uploader/uploader.inc.php' );
	$route -> add( 'include/7', 'system/cpanel.product.product/save.inc.php' );
	$route -> add( 'include/8', 'system/cpanel.product.product/delete.inc.php' );
	$route -> add( 'include/9', 'system/cpanel.sale.order/changestate.inc.php' );
	$route -> add( 'include/10', 'system/cpanel.sale.order/list.inc.php' );
	$route -> add( 'include/11', 'system/cpanel.sale.reorder/changestate.inc.php' );
	$route -> add( 'include/12', 'system/cpanel.sale.commission/save.inc.php' );
	$route -> add( 'include/13', 'system/cpanel.sale.commission/delete.inc.php' );
	$route -> add( 'include/14', 'system/cpanel.content.page/save.inc.php' );
	$route -> add( 'include/15', 'system/cpanel.sale.voucher/add.inc.php' );
	$route -> add( 'include/16', 'system/cpanel.sale.voucher/delete2.inc.php' );
	$route -> add( 'include/17', 'system/cpanel.content.page/delete.inc.php' );
	$route -> add( 'include/18', 'system/cpanel.customer.customer/save.inc.php' );
	$route -> add( 'include/20', 'system/cpanel.overview/read.inc.php' );
	$route -> add( 'include/21', 'system/cpanel.overview/important.inc.php' );
	$route -> add( 'include/22', 'system/cpanel.overview/del.inc.php' );
	$route -> add( 'include/23', 'system/cpanel.setting.area/save.inc.php' );
	$route -> add( 'include/24', 'system/cpanel.setting.area/delete.inc.php' );
	$route -> add( 'include/25', 'system/cpanel.overview/list.inc.php' );
	$route -> add( 'include/26', 'system/cpanel.content.post/save.inc.php' );
	$route -> add( 'include/27', 'system/cpanel.customer.customer/block.inc.php' );
	$route -> add( 'include/28', 'system/cpanel.customer.customer/list.inc.php' );
	$route -> add( 'include/29', 'system/cpanel.content.post/delete.inc.php' );
	$route -> add( 'include/30', 'system/cpanel.content.post/list.inc.php' );
	$route -> add( 'include/31', 'system/cpanel.content.comment/list.inc.php' );
	$route -> add( 'include/32', 'system/cpanel.content.comment/block.inc.php' );
	$route -> add( 'include/33', 'system/cpanel.sale.order/save-note.inc.php' );
	$route -> add( 'include/34', 'system/cpanel.sale.reorder/list.inc.php' );
	$route -> add( 'include/35', 'system/cpanel.sale.order/print-bill.inc.php' );
	$route -> add( 'include/36', 'system/cpanel.sale.order/print-deliving.inc.php' );
	$route -> add( 'include/37', 'system/cpanel.setting.account/save.inc.php' );
	$route -> add( 'include/38', 'system/cpanel.setting.account/block.inc.php' );
	$route -> add( 'include/39', 'system/cpanel.appearance.slider/delete.inc.php' );
	$route -> add( 'include/40', 'system/cpanel.appearance.slider/save.inc.php' );
	$route -> add( 'include/41', 'system/cpanel.appearance.slider/update.inc.php' );
	$route -> add( 'include/42', 'system/cpanel.content.question/save.inc.php' );
	$route -> add( 'include/43', 'system/cpanel.content.question/delete.inc.php' );
	$route -> add( 'include/44', 'system/cpanel.setting.general/save.inc.php' );
	$route -> add( 'include/45', 'system/cpanel.setting.account/delete.inc.php' );
	$route -> add( 'include/46', 'system/cpanel.personal/save.inc.php' );
	$route -> add( 'include/47', 'system/cpanel/read-notification.inc.php' );
	$route -> add( 'include/48', 'system/cpanel.content.question/list.inc.php' );
	$route -> add( 'include/49', 'system/cpanel.sale.order/exportxls.inc.php' );
	$route -> add( 'include/50', 'system/cpanel.customer.customer/exportxls.inc.php' );
	$route -> add( 'include/51', 'system/cpanel.product.product/exportxls.inc.php' );
	$route -> add( 'include/52', 'system/cpanel.statist.sale/exportxls.inc.php' );
	$route -> add( 'include/53', 'system/cpanel.statist.inventory/exportxls.inc.php' );
	$route -> add( 'include/54', 'system/cpanel.statist.payment/exportxls.inc.php' );
	$route -> add( 'include/55', 'system/cpanel.statist.site/exportxls.inc.php' );
	$route -> add( 'include/56', 'system/cpanel.statist.revenue/exportxls.inc.php' );
	$route -> add( 'include/57', 'system/cpanel.statist.order/exportxls.inc.php' );
?>